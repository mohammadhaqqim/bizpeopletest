<?php

namespace App\Console\Commands\Tenant;

use App\Business;
use App\Models\Company;
use Illuminate\Console\Command;
use App\Tenant\Database\DatabaseManager;
use App\Tenant\Traits\FetchesTenant;
use App\Tenant\Traits\ForTenants;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\Migrations\Migrator;
use Illuminate\Database\Console\Migrations\MigrateCommand;
use PhpOffice\PhpSpreadsheet\Calculation\Database;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutput;

class Migrate extends MigrateCommand
{
    use FetchesTenant;

    protected $db;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run migrations for all tenants';
    /**
      * The migrator instance.
      *
      * @var \Illuminate\Database\Migrations\Migrator
      */
    protected $migrator;

    /**
     * The event dispatcher instance.
     *
     * @var \Illuminate\Contracts\Events\Dispatcher
     */
    protected $dispatcher;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Migrator $migrator, DatabaseManager $db)
    {
        parent::__construct($migrator);
        $this->setName('tenants:migrate');

        $this->specifyParameters();

        $this->db = $db;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if (!$this->confirmToProceed()) {
            return;
        };

        $this->input->setOption('database', 'tenant');

        $tenants = Business::query();

        if ($this->option('tenants')) {
            $tenants = $tenants->whereIn('id', $this->option('tenants'));
        }

//        dd($tenants->get());

        $this->tenants($this->option('tenants'))->each(function ($tenant) {
            $this->db->createConnection($tenant);
            $this->db->connectToTenant();
            echo('Migrating database '.$tenant->domain.PHP_EOL);

            parent::handle();
            
            $this->db->purge();
        });


//        return 0;
    }

    protected function getOptions()
    {
        return array_merge(
            parent::getOptions(),
            [
                ['tenants', null, InputOption::VALUE_IS_ARRAY | InputOption::VALUE_OPTIONAL, '', null]
            ]
        );
    }


    protected function getMigrationPaths()
    {
        return [database_path('migrations/tenant')];
    }
}
