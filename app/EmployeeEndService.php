<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmployeeEndService extends Model
{
    //
    protected $guarded = [];

    public function endEmployment()
    {
        return $this->belongsTo('App\EndEmploymentReason', 'end_reason_id');
    }

    public function Employee()
    {
        return $this->belongsTo('App\Employee', 'id', 'end_reason_id');
    }
}
