<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class PayrollEmployee extends Model
{
    use ForTenants;

    //
    protected $guarded = [];

    public function Master()
    {
        return $this->belongsTo('App\PayrollMaster', 'payroll_master_id');
    }

    public function Employee()
    {
//        return $this->belongsTo('App\Employee', 'employee_id', 'id');
        return $this->belongsTo(Employee::class);
    }
    
    public function Company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }
    
    public function PaymentMode()
    {
        return $this->belongsTo('App\PaymentMode', 'payment_mode_id');
    }

    public function PaymentType()
    {
        return $this->belongsTo('App\PaymentType', 'payment_type_id');
    }

    public function PreparedBy()
    {
        return $this->belongsTo('App\User', 'prepared_by');
    }

    public function Transactions()
    {
        return $this->hasMany('App\PayrollTransaction', 'payroll_employees_id');
    }

    public function Name()
    {
        $this->Employee->name;
    }

    public function EmployeeJob()
    {
        return $this->belongsTo('App\EmployeeJob', 'employee_job_id');
    }

    public function performCalculations()
    {
        $this->calculateTransactions();

        $this->calculateTaxes();

        $this->save();

        return;
    }

    // calculate all taxes
    // should be calculated after calculateTransactions()
    public function calculateTaxes()
    {
        // update unpaid leave
        //dd($this->Employee);
        $this->getTaxCalculation('tap');
        $this->getTaxCalculation('scp');
        $this->getTaxCalculation('epf');

        return;
    }

    // calculate the following
    // addition_amount
    // deduction_amount
    // unpaid_amount - unpaid leave
    private function calculateTransactions()
    {
        $this->calculateUnpaidLeave();
        $this->calculateAddDed();

        return;
    }

    private function calculateUnpaidLeave()
    {
        // date range
        $unpaid = PayrollTransaction::where('payroll_employees_id', $this->id)
            ->whereNull('deleted_at')
            ->where('title', 'like', '%unpaid%leave%')
            ->get();
//        dd($unpaid);

        $this->unpaid_amount = $unpaid->sum('amount');
        $this['unpaid_amount'] = $unpaid->sum('amount');

        $unpaid = EmployeeLeave::where('employee_id', $this->employee_id)
            ->whereNull('deleted_at')
            ->where('start_date', '>=', $this->Master->start_date)
            ->where('end_date', '<=', $this->Master->end_date)
            ->whereNotNull('approved_at')
            ->with(['LeaveType' => function ($query) {
                return $query->where('paid_time_off', '=', 0);
            }])
            ->get();

        $amount = $unpaid->sum('days_off');
        $day_rate = $this['base_salary'] / $this['work_days'];
        $amount = $amount * $day_rate;

        $this['unpaid_amount'] += round($amount, 2);

        ///dd($unpaid);

        return $unpaid->sum('amount');
    }

    public function getUnpaid()
    {
//        $payroll = $this->Master();
        //dd($this->Master);
        $unpaid = EmployeeLeave::where('employee_id', $this->employee_id)
            ->whereNull('deleted_at')
            ->where('start_date', '>=', $this->Master->start_date)
            ->where('end_date', '<=', $this->Master->end_date)
            ->whereNotNull('approved_at')
            ->with(['LeaveType' => function ($query) {
                return $query->where('paid_time_off', '=', 0);
            }])
            ->get();

        $amount = $unpaid->sum('days_off');
        $day_rate = $this['base_salary'] / $this['work_days'];
        $amount = $amount * $day_rate;

        dd(round($amount, 2));
        //if ($unpaid->LeaveType()->track_time == 'h') {
        //dd($unpaid->sum('hours_off'));
        //} else {
        dd($unpaid->sum('days_off'));
        //}
        dd($unpaid);
    }

    private function calculateAddDed()
    {
        //$this['overtime_amount'] = 99;

        $additions = PayrollTransaction::where('payroll_employees_id', $this->id)
            ->whereNull('deleted_at')
            ->where('addition', 1)
            ->get();

            
        $deductions = PayrollTransaction::where('payroll_employees_id', $this->id)
            ->whereNull('deleted_at')
            ->where('addition', 0)
            ->where('title', 'not like', '%unpaid%leave%')
            ->get();

        //dd($deductions);
            
        $this['addition_amount'] = $additions->sum('amount');
        $this['deduction_amount'] = $deductions->sum('amount');
        
        return;
    }

    public function getTaxCalculation($taxString)
    {
        $this[$taxString] = 0;
        $this[$taxString.'_ee'] = 0;

        $company = Company::find(1);
        if ($this->Employee[$taxString.'_member'] == 0 || $company['use_'.$taxString] == 0) {
            //dd('exit not member');
            return;
        }

        $min = $company[$taxString.'_min'];
        $max = $company[$taxString.'_max'];
        $percent = $company[$taxString.'_percent'];

        $tax = round(($this->base_salary - $this->unpaid_amount)*$percent)/100;

        if ($taxString == "tap" || $company[$taxString.'_decimals'] == 0) {
            //round to nearest dollar
            $tax = ceil($tax);
        }
    
        //dd($company[$taxString.'_decimals']);

        /*         if ($taxString == "scp") {
                    dd('tax '.$tax.' min '.$min);
                } */

        $this[$taxString.'_ee'] = $tax;
        $this[$taxString] = $tax;

        if ($tax < $min) {
            $this[$taxString] = $min;
        }

        if ($tax > $max && $max > 0) {
            $this[$taxString.'_ee'] = $max;
            $this[$taxString] = $max;
        }

        /*         if ($tax < $min) {
                    dd('first');
                    $this[$taxString] = ($taxString == "tap" ? $tax : $min);
                    $this[$taxString.'_ee'] = $tax;
                } elseif ($tax < $max || $max == 0) {
                    dd('second');
                    $this[$taxString.'_ee'] = $tax;
                    $this[$taxString] = $tax;
                } else {
                    dd('third');
                    $this[$taxString.'_ee'] = $max;
                    $this[$taxString] = $max;
                }
         */
        return;
    }

    public function loadEmployee($employee_id)
    {
        $employee = Employee::find($employee_id);

        $payment = $employee->getCurrentPayment();

        //$this->Employee()->save($employee);
        //$this->employee_id()->save($employee);

        $this->employee_id = $employee->id;

        $this->base_salary = is_null($payment) ? 0 : $payment->basic_salary;
        //dd($employee);
        $this->payment_mode_id = $employee->getCurrentPayment()->payment_mode_id;
        $this->payment_type_id = $employee->getCurrentPayment()->payment_type_id;
        //$payment_type_id = $employee->getCurrentPayment()->payment_type_id;

        $this->bank_id = $employee->bank_id;
        $this->bank_account = $employee->bank_account;
        // *** NEEDS CHECKING *** //
        
        if ($this->PaymentType->periods == 0) {
            $this->old_salary = $this->base_salary;
        } else {
            $old = $this->getOldSalary($employee_id);
            $this->old_salary = ($old == 0 ? $this->base_salary : $old);
        }
        
        //OR
        $jobID = DB::connection('tenant')->table('vw_current_jobs')
            ->select('jobs_id')
            ->where('employee_id', '=', $employee_id)
            ->first();

        $statusID = DB::connection('tenant')->table('vw_current_statuses')
            ->select('status_id')
            ->where('employee_id', '=', $employee_id)
            ->first();

        //save the current job id and status id - links [employee_jobs, employee_statuses]
        $this->employee_job_id = $jobID->jobs_id;//$employee->getCurrentJob()->id;
        $this->employee_status_id = $statusID->status_id;//$employee->getCurrentJob()->id;

        $this->unpaid_amount = 0;
    }

    private function getOldSalary($employee_id)
    {
        $old = DB::connection('tenant')->table('payroll_employees')
            ->select('base_salary')
            ->where('employee_id', '=', $employee_id)
            ->where('payroll_master_id', '<', $this->payroll_master_id)
            ->whereNull('deleted_at')
            ->orderBy('payroll_master_id', 'DESC')
            ->orderBy('id', 'DESC')
            ->first();

        return is_null($old) ? 0 : $old->base_salary;
    }

    public function getPayOut()
    {
        $taxes = $this->scp_ee+$this->tap_ee+$this->epf_ee;

        return $this->base_salary-$taxes-$this->decuation_amount+$this->addition_amount;
    }

    public function getGross()
    {
        return $this->base_salary-$this->deduction_amount+$this->addition_amount;
    }

    public function getNet()
    {
        $taxes = $this->scp_ee+$this->tap_ee+$this->epf_ee;

        return $this->getGross()-$taxes;
    }
}
