<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class EmployeeAllowance extends Model
{
    use ForTenants;

    //
    protected $guarded = [];

    public function Employee()
    {
        return $this->belongsTo('App\Employee');
    }

    public function Allowance()
    {
        return $this->belongsTo('App\Transaction', 'allowance_id', 'id');
    }

    public function getAddition()
    {
        $addition = $this->Allowance->addition;
        return $addition;
    }
}
