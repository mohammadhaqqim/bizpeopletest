<?php

namespace App;

use App\Tenant\Tenant;
use App\Tenant\Traits\isTenant;
use Illuminate\Database\Eloquent\Model;

class Business extends Model implements Tenant
{
    use isTenant;
    //

    protected $fillable = ['name', 'domain', 'uuid', 'owner_id'];
}
