<?php

namespace App\Events\Tenant;

use App\Tenant\Tenant;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class TenantDatabaseCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $tenant;
    

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Tenant $tenant)
    {
        $this->tenant = $tenant;
    }
}
