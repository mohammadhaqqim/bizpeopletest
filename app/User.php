<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    protected $table = 'users';

    use Notifiable;

    protected $connection = 'mysql';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'block', 'uuid', 'db_admin'
        //'employee_id',
        //'payroll', 'hr', 'hod', 'manager', 'admin', 'block'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /*     public function Company()
        {
            return $this->belongsTo('App\Company', 'company_id', 'id');
        }
     */

    /*     public function Employee()
        {
            return $this->belongsTo('App\Employee', 'employee_id');
        } */

    /*     public function getEmployeeID()
        {
            return $this->Employee->id;
        }
     */
    /*     public function getPayroll()
        {
            return $this->payroll;
        }
     */
    /*     public function getHR()
        {
            return $this->hr;
        }
     */
    /*     public function getAdministrator()
        {
            return $this->admin;
        }
     */

    public function getInitals()
    {
        $splitName = explode(' ', $this->name);

        $first_name = $splitName[0];
        $last_name = '';

        if (count($splitName) > 1) {
            $last_name = $splitName[count($splitName)-1];
        }

        if ($last_name != '') {
            return(strtoupper($first_name[0].$last_name[0]));
        } else {
            return(strtoupper($first_name[0]));
        }
    }

    public function companies()
    {
        return $this->belongsToMany(Business::class);
    }

    public function access()
    {
        return $this->hasMany(BusinessUser::class, 'user_id', 'id');
    }

    public function checkBusiness($business_id)
    {
        return $this->companies->contains('id', $business_id);
    }
}
