<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class documentation extends Model
{
        //Table Name
        protected $table = 'documentations';
        // Primary Key
        public $primaryKey = 'id';
        // Timestamps
        public $timestamps = true;
}
