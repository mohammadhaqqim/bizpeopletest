<?php

namespace App\Imports;

use App\Company;
use Carbon\Carbon;
use App\Designation;
use App\EmployeeStatus;
use App\EmploymentStatus;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Http\Controllers\Tenant\EmployeeController;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Http\Controllers\DesignationController;
use DateTime;

class EmployeeStatusImport implements ToCollection, WithHeadingRow
{
    private $service;

    public function collection(Collection $rows)
    {
        //dd($rows);
        $this->service = Company::find(1);
        
        foreach ($rows as $row) {
            if ($row['name'] == null) {
                break;
            }

            $employee_id = (new EmployeeController)->getEmployeeID($row);
            //$this->CheckEmployee($row);
            
            $effective = \Carbon\Carbon::now();

            $datejoined = null;
            if (isset($row['date_joined'])) {
                $datejoined = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row["date_joined"]);
            } elseif (isset($row['start_date'])) {
                $datejoined = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row["start_date"]);
            } else {
                $datejoined = date('Y-m-d', strtotime(Carbon::now()));
            }

            if ($effective > $datejoined) {
                $effective = $datejoined;
            }
    
            $status_id = $this->CheckEmploymentStatus($row);
            //dd($status_id);
            $status = $this->CheckEmployeeStatus($employee_id, $status_id);
            //dd($status);

            if ($status == null) {
                EmployeeStatus::create([
                    'effective_date' => $effective,
                    'employee_id' => $employee_id,
                    'employment_status_id' => $status_id,
                    'entered_by' => auth()->user()->id
                ]);
            }
        }
    }

    private function CheckEmployee($row)
    {
        // ic
        $id = -1;
        if (isset($row['ic'])) {
            $id = (new EmployeeController)->CheckIC($row['ic']);

            if (!is_null($id)) {
                return $id;
            }
        }

        if (isset($row['ic_number'])) {
            $id = (new EmployeeController)->CheckIC($row['ic_number']);

            if (!is_null($id)) {
                return $id;
            }
        }

        // name
        if (isset($row['name']) && $id == -1) {
            $id = (new EmployeeController)->CheckName($row['name']);
        }

        return $id;
    }

    private function CheckEmployeeStatus($employee_id, $status)
    {
        $emp_status = EmployeeStatus::where('employee_id', $employee_id)
            ->where('employment_status_id', $status)
            ->first();

        return $emp_status;
    }

    private function CheckEmploymentStatus($row)
    {
        if (isset($row['employment_status'])) {
            $emp_status = EmploymentStatus::where('title', '=', $row['employment_status'])
               ->first();
            
            // should be in database
            if (is_null($emp_status)) {
                // does not exist - generate
                $emp_status = EmploymentStatus::create([
                    'title' => $row['employment_status'],
                    'company_id' => 1
                ]);
            }
        } else {
            return $this->service->employment_status_id;
        }

        return $emp_status->id;
    }
}
