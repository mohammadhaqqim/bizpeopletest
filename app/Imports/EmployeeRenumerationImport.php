<?php

namespace App\Imports;

use App\Company;
use Carbon\Carbon;
use App\Designation;
use App\PaymentMode;
use App\PaymentType;
use App\EmployeePayment;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Http\Controllers\Tenant\EmployeeController;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Http\Controllers\DesignationController;

class EmployeeRenumerationImport implements ToCollection, WithHeadingRow
{
    private $service;

    public function collection(Collection $rows)
    {
        $this->service = Company::find(1);

        foreach ($rows as $row) {
            if ($row['name'] == null) {
                break;
            }

            $employee_id = (new EmployeeController)->getEmployeeID($row);

            if (isset($row['basic_salary'])) {
                $basic = str_replace('=', '', $row['basic_salary']);
                $basic = str_replace(',', '', $basic);
            } else {
                $basic = 0;
            }

            $effective = new Carbon($this->service->first_period_date);
            
            $datejoined = null;
            if (isset($row['date_joined'])) {
                $datejoined = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row["date_joined"]);
            } elseif (isset($row['start_date'])) {
                $datejoined = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row["start_date"]);
            } else {
                $datejoined = date('Y-m-d', strtotime(Carbon::now()));
            }
            
            
            if ($effective > $datejoined) {
                $effective = $datejoined;
            }
            
            $paymode = $this->checkPaymentMode($row);
            $paytype = $this->checkPaymentType($row);

            $pay = EmployeePayment::where('employee_id', $employee_id)
                ->where('basic_salary', (float)$basic)
                ->where('payment_mode_id', $paymode)
                ->where('payment_type_id', $paytype)
                ->first();
            //dd($pay);

            if ($pay == null) {
                EmployeePayment::create([
                    'effective_date' => $effective,
                    'employee_id' => $employee_id,
                    'basic_salary' => (float)$basic,
                    'currency_id' => $this->service->address_country_id,
                    'payment_mode_id' => $paymode,
                    'payment_type_id' => $paytype,
                    'entered_by' => auth()->user()->id
                ]);
            }
        }
    }

    private function checkPaymentMode($row)
    {
        if (isset($row['payment_mode'])) {
            $mode = PaymentMode::where('title', '=', $row['payment_mode'])
                ->first();

            if (is_null($mode)) {
                // does not exist - generate
                $mode = PaymentMode::create([
                    'title' => $row['payment_mode'],
                    //'company_id' => 1
                ]);
            }
        } else {
            // default value
            return $this->service->payment_mode_id;
        }

        return $mode->id;
    }

    private function checkPaymentType($row)
    {
        if (isset($row['payment_type'])) {
            $mode = PaymentType::where('title', '=', $row['payment_type'])
                ->first();

            if (is_null($mode)) {
                // does not exist - generate
                $mode = PaymentMode::create([
                    'title' => $row['payment_type'],
                    //'company_id' => 1
                ]);
            }
        } else {
            // default value
            return $this->service->payment_type_id;
        }

        return $mode->id;
    }
    
    private function CheckEmployee($row)
    {
        // ic
        $id = -1;
        if (isset($row['ic'])) {
            $id = (new EmployeeController)->CheckIC($row['ic']);

            if (!is_null($id)) {
                return $id;
            }
        }

        if (isset($row['ic_number'])) {
            $id = (new EmployeeController)->CheckIC($row['ic_number']);

            if (!is_null($id)) {
                return $id;
            }
        }
        // name
        if (isset($row['name']) && $id == -1) {
            $id = (new EmployeeController)->CheckName($row['name']);
        }

        return $id;
    }
}
