<?php

namespace App\Imports;

use App\Company;
use Carbon\Carbon;
use App\EmploymentStatus;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Http\Controllers\Tenant\EmployeeController;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Http\Controllers\DesignationController;

class EmploymentStatusImport implements ToCollection, WithHeadingRow
{
    private $service;
    private $employee_id;

    public function collection(Collection $rows)
    {
        $company_id = auth()->user()->company_id;

        foreach ($rows as $row) {
            if (is_null($row['name'])) {
                break;
            }

            if (isset($row['employment_status'])) {
                $emp_status = EmploymentStatus::where('title', '=', $row['employment_status'])
                   ->first();
                
                if (is_null($emp_status)) {
                    EmploymentStatus::create([
                        'title' => $row['employment_status'],
                        //'company_id' => $company_id
                    ]);
                }
            }
        }
    }
}
