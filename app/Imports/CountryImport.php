<?php

namespace App\Imports;

use App\Country;
use Maatwebsite\Excel\Concerns\ToModel;

class CountryImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Country([
            //
            'country'       => $row['country'],
            'nationality'   => $row['nationality'],
            'use_currency'  => $row['use_currency'],
            'country_code'  => $row['country_code']
        ]);
    }
}
