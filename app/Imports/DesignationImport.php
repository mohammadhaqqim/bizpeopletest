<?php

namespace App\Imports;

use App\Company;
use App\Designation;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Http\Controllers\DesignationController;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;

class DesignationImport implements ToCollection, WithHeadingRow
{
    private $service;
    private $employee_id;

    public function collection(Collection $rows)
    {
        $this->service = Company::find(auth()->user()->company_id);

        foreach ($rows as $row) {
            if ($row['name'] == null) {
                break;
            }

            $designation_id = null;

            if (isset($row['designation'])) {
                $designation_id = Designation::where('title', '=', $row['designation'])
                    ->first();
            };

            if ($designation_id == null && $row['designation'] != null) {
                Designation::create([
                    //
                    'title' => $row['designation'],
                ]);
            };
        }
    }
}
