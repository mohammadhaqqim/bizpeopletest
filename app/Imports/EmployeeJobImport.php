<?php

namespace App\Imports;

use App\Company;
use Carbon\Carbon;
use App\Department;
use App\Designation;
use App\EmployeeJob;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Http\Controllers\Tenant\EmployeeController;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Http\Controllers\Tenant\DepartmentController;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Http\Controllers\Tenant\DesignationController;

class EmployeeJobImport implements ToCollection, WithHeadingRow
{
    private $service;

    public function collection(Collection $rows)
    {
        //dd($rows);
        $this->service = Company::find(1);

        foreach ($rows as $row) {
            if ($row['name'] == null) {
                break;
            }

            $designation_id = null;
            $department_id = null;
            $employee_id = (new EmployeeController)->getEmployeeID($row);
//            $employee_id = $this->CheckEmployee($row);
            //dd($row);
            $effective = \Carbon\Carbon::now();
            
            $datejoined = null;
            if (isset($row['date_joined'])) {
                $datejoined = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row["date_joined"]);
            } elseif (isset($row['start_date'])) {
                $datejoined = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row["start_date"]);
            } else {
                $datejoined = date('Y-m-d', strtotime(Carbon::now()));
            }

            if ($effective > $datejoined) {
                $effective = $datejoined;
            }
            
            // Check designation
            if (isset($row['designation'])) {
                $designation_id = (new DesignationController)->checkDesignation($row['designation']);
            };

            
            //if null get default
            if ($designation_id == null) {
                $designation_id = $this->service->designation_id;
            }
            //dd($designation_id);
            
            if (isset($row['department'])) {
                $department_id = (new DepartmentController)->checkDepartment($row['department']);
            }
            
            //if null get default
            if ($department_id == null) {
                $department_id = $this->service->department_id;
            }
            //dd('Dept '.$department_id);

            //check if in database
            $job = EmployeeJob::where('employee_id', $employee_id)
                ->where('department_id', $department_id)
                ->where('designation_id', $designation_id)
                ->first();
            //dd($job);

            if ($job == null) {
                EmployeeJob::create([
                    'effective_date' => $effective,
                    'employee_id' => $employee_id,
                    'department_id' => $department_id,
                    'designation_id' => $designation_id,
                ]);
            }
        }
    }
}
