<?php

namespace App\Imports;

use App\Bank;
use App\Race;
use App\Gender;
use App\Company;
use App\Country;
use App\Employee;
use Carbon\Carbon;
use App\Designation;
use App\EmployeeJob;
use App\EmploymentStatus;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\ReligionController;
use App\Http\Controllers\DepartmentController;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Http\Controllers\DesignationController;

class TestImport implements ToModel, WithHeadingRow
{
    private $service;
    private $employee_id;

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        //dd('test import');
        $this->service = Company::find(1);

        $joined = isset($row['date_joined']) ? new Carbon($row['date_joined']) : date('Y-m-d', strtotime(Carbon::now()));
        
        $tap_acc = $this->CheckTax($row, 'tap_account');
        $scp_acc = $this->CheckTax($row, 'scp_account');
        $epf_acc = $this->CheckTax($row, 'epf_account');
        $bank_id = $this->CheckBank($row);

        if (is_null($joined)) {
            $joined = date('Y-m-d', strtotime(Carbon::now()));
        }


        $attributes = [
            'religion_id' => $this->CheckReligion($row),
            'company_id' => $this->service->id,
            'nationality_id' => $this->CheckNationality($row),
            'race_id' => isset($row['race']) ? null : $this->CheckRace($row),
            'name' => $row['name'],
            'date_of_birth'=> $this->CheckDateOfBirth($row),
            'gender_id' => isset($row['gender']) ? $this->checkGender($row['gender']) : null,
            'ic_number' => $row['ic_number'],
            'ic_expiry' => null,
            'marital_status_id' => 1,
            'address_country_id' => $this->service->address_country_id,
            'working_week_id' => $this->service->working_week_id,
            'date_joined' => $joined->format('Y-m-d'),
            'work_email' => isset($row['work_email']) ? $row['work_email'] : null,
            'self_service' => false,
            'driving_license' => 0,
            'email_payslip' => 0,
            'tap_account' => $tap_acc,
            'tap_member' => (is_null($tap_acc) ? 0 : 1),
            'scp_account' => $scp_acc,
            'scp_member' => (is_null($scp_acc) ? 0 : 1),
            'epf_account' => $epf_acc,
            'epf_member' => (is_null($epf_acc) ? 0 : 1),
            'identification_card_id' => null,
            'employment_type_id' => null,
            'employment_status_id' => $this->CheckEmploymentStatus($row),
            'location_id' => null,
            'bank_id' => $bank_id,
            'reports_to_id' => null,
            'payment_currency_id' => $this->service->address_country_id,
            'created_by' => auth()->user()->id,
            'updated_at' => Carbon::now(),
            'created_at' => Carbon::now(),
        ];

        return(new Employee($attributes));
        /*         dd
                $this->employee_id = $employee->id;

                $this->CreateEmployeeJob($row);

                return $employee;
         */

        return new Designation([
            'title' => $row['title'],
            'company_id' => 1
        ]);
    }

    private function CheckEmploymentStatus($row)
    {
        // no controller
        if (isset($row['employment_status'])) {
            $emp_status = EmploymentStatus::where('title', '=', $row['employment_status'])
               ->first();
            
            if (is_null($emp_status)) {
                // does not exist - generate
                $emp_status = EmploymentStatus::create([
                    'title' => $row['employment_status'],
                    'company_id' => 1
                ]);
            }
        } else {
            return $this->service->employment_status_id;
        }

        return $emp_status->id;
    }

    private function checkGender($gender)
    {
        if (is_null($gender)) {
            return null;
        }
        
        $g = strtolower($gender);
        $gend = Gender::where('title', '=', $g)
                ->orWhere('title', 'like', $gender[0].'%')
               ->first();

        return $gend->id;
    }


    private function CheckRace($row)
    {
        if (isset($row['race'])) {
            //find the race
            $race = (new CountryController)->checkNationality($row['race']);
        } else {
            //default
            $race = Race::find($this->service->race_id);
            if (is_null($race)) {
                return null;
            }
        }

        return $race->id;
    }

    private function CheckNationality($row)
    {
        if (isset($row['nationality'])) {
            $country = (new CountryController)->checkNationality($row['nationality']);
        } else {
            //default
            $country = Country::find(1);
        }

        return $country->id;
    }
    
    private function CheckReligion($row)
    {
        if (isset($row['religion'])) {
            $religion_id = (new ReligionController)->checkReligion($row['religion']);
        } else {
            //default value
            return $this->service->religion_id;
        }

        return $religion_id;
    }

    private function CheckDateOfBirth($row)
    {
        if (isset($row['date_of_birth'])) {
            if (is_numeric($row['date_of_birth'])) {
                //excel date field
                $date = new Carbon('1899-12-31');
                $date = $date->addDays($row['date_of_birth']-1);
            } else {
                $date = new Carbon($row['date_of_birth']);
            }
        } else {
            // default date of birth
            $date = new Carbon('2000-01-01');
        }

        return $date->format('Y-m-d');
    }

    private function CheckBank($row)
    {
        $bank = $row['bank'];

        $bank = Bank::where('bank_code', '=', $bank)
            ->first();

        return !is_null($bank) ? $bank->id : 0;
    }

    private function CheckTax($row, $tax_type)
    {
        //check if tap account is set and not "N/A"
        if (isset($row[$tax_type])) {
            $acc_no = Str::lower($row[$tax_type]);
            $acc = ($acc_no == 'n/a' ? null : $acc_no);
        } else {
            //default
            $acc = null;
        }
        
        return $acc;
    }

    private function CreateEmployeeJob($row)
    {
        $department_id = null;
        $designation_id = null;

        $effective = \Carbon\Carbon::now();
        
        if ($effective > $row['date_joined']) {
            $effective = new Carbon($row['date_joined']);
        }

        // Check department
        if (isset($row['department'])) {
            $department_id = (new DepartmentController)->checkDepartment($row['department']);
        };

        // Check designation
        if (isset($row['designation'])) {
            $designation_id = (new DesignationController)->checkDesignation($row['designation']);
        };

        $emp_job = new EmployeeJob([
            'effective_date' => $effective,
            'employee_id' => $this->employee_id,

            'department_id' => $department_id,
            'designation_id' => $designation_id,
        ]);

        $emp_job->save();
    }
}
