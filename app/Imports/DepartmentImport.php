<?php

namespace App\Imports;

use App\Company;
use App\Department;
use App\Designation;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Http\Controllers\DepartmentController;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Http\Controllers\DesignationController;

class DepartmentImport implements ToCollection, WithHeadingRow
{
    private $service;
    private $employee_id;

    public function collection(Collection $rows)
    {
        //dd($rows);
        $this->service = Company::find(1);

        foreach ($rows as $row) {
            if (is_null($row['name'])) {
                break;
            }

            if (isset($row['department'])) {
                $department = Department::where('title', $row['department'])
                    ->first();
                
                if ($department == null  && $row['department'] != null) {
                    Department::create([
                        'title' => $row['department'],
                        //'company_id' => 1
                    ]);
                }
            }
        }
    }
}
