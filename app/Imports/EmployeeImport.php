<?php

namespace App\Imports;

use App\Bank;
use App\Race;
use DateTime;
use App\Gender;
use App\Company;
use App\Country;
use App\Employee;
use App\Religion;
use Carbon\Carbon;
use App\Department;
use App\Designation;
use App\EmployeeJob;
use App\PaymentMode;
use App\PaymentType;
use App\EmployeeType;
use App\EmployeeStatus;
use App\EmployeePayment;
use App\EmploymentStatus;
use Illuminate\Support\Str;
use Hamcrest\Type\IsNumeric;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Http\Controllers\Tenant\CountryController;
use phpDocumentor\Reflection\Types\Boolean;
use App\Http\Controllers\Tenant\EmployeeController;
use App\Http\Controllers\Tenant\ReligionController;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Http\Controllers\DepartmentController;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Http\Controllers\DesignationController;

class EmployeeImport implements ToCollection, WithHeadingRow
{
    private $service;
    private $employee_id;

    public function collection(Collection $rows)
    {
        //abort_unless(auth()->user()->admin, 403);

        // get all company details
        $this->service = Company::find(1);

        foreach ($rows as $row) {
            //dd($row);
            if ($row['name'] == null) {
                break;
            }

            $this->employee_id = $this->CheckEmployee($row);

            if ($this->employee_id == -1) {
                $this->employee_id = $this->CreateNewEmployee($row);
            }
        }
    }

    private function CheckEmployee($row)
    {
        // ic
        $id = -1;
        if (isset($row['ic'])) {
            $id = (new EmployeeController)->CheckIC($row['ic']);

            if (!is_null($id)) {
                return $id;
            }
        }

        if (isset($row['ic_number'])) {
            $id = (new EmployeeController)->CheckIC($row['ic_number']);

            if (!is_null($id)) {
                return $id;
            }
        }

        // name
        if (isset($row['name']) && $id == -1) {
            $id = (new EmployeeController)->CheckName($row['name']);
        }

        return $id;
    }

    private function CreateNewEmployee($row)
    {
        $dob = $this->CheckDateOfBirth($row);

        $joined = null;
        if (isset($row['date_joined'])) {
            $joined = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row["date_joined"]);
        } elseif (isset($row['start_date'])) {
            $joined = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row["start_date"]);
        } else {
            $joined = date('Y-m-d', strtotime(Carbon::now()));
        }

        $tap_acc = $this->CheckTax($row, 'tap_account');
        $scp_acc = $this->CheckTax($row, 'scp_account');
        $epf_acc = $this->CheckTax($row, 'epf_account');
        $bank_id = $this->CheckBank($row);

        if (is_null($joined)) {
            $joined = date('Y-m-d', strtotime(Carbon::now()));
        };
        //dd($joined);

        Employee::create([
            'religion_id' => $this->CheckReligion($row),
            'employee_code' => $this->CheckEmployeeCode($row),
            //'company_id' => $this->service->id,
            'nationality_id' => $this->CheckNationality($row),
            'race_id' => isset($row['race']) ? null : $this->CheckRace($row),
            'name' => $row['name'],
            'date_of_birth'=> $dob,
            'gender_id' => isset($row['gender']) ? $this->checkGender($row['gender']) : null,
            'ic_number' => $row['ic_number'],
            'ic_expiry' => null,
            'marital_status_id' => 1,
            'address_country_id' => $this->service->address_country_id,
            'working_week_id' => $this->service->working_week_id,
            'date_joined' => $joined->format('Y-m-d'),
            'work_email' => isset($row['work_email']) ? $row['work_email'] : null,
            'self_service' => 0,
            'driving_license' => 0,
            'email_payslip' => 0,
            'tap_account' => $tap_acc,
            'tap_member' => (is_null($tap_acc) ? 0 : 1),
            'scp_account' => $scp_acc,
            'scp_member' => (is_null($scp_acc) ? 0 : 1),
            'epf_account' => $epf_acc,
            'epf_member' => (is_null($epf_acc) ? 0 : 1),
            'identification_card_id' => null,
            'employment_type_id' => null,
            'employment_status_id' => $this->CheckEmploymentStatus($row),
            'location_id' => null,
            'bank_id' => ($bank_id == 0) ? null : $bank_id,
            'reports_to_id' => null,
            'payment_currency_id' => $this->service->address_country_id,
            'passport_number' => isset($row['passport_number']) ? $row['passport_number'] : null,
            'created_by' => auth()->user()->id,
            'updated_at' => Carbon::now(),
            'created_at' => Carbon::now(),
        ]);
        //dd($attributes);

//        Employee::create($attributes);
    }

    private function CheckDateOfBirth($row)
    {
        if (isset($row['date_of_birth'])) {
            if (is_numeric($row['date_of_birth'])) {
                //excel date field
                $date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($row["date_of_birth"]);
            } else {
                $date = new Carbon($row['date_of_birth']);
            }
        } else {
            // default date of birth
            $date = new Carbon('2000-01-01');
        }

        return $date->format('Y-m-d');
    }


    // TEST THIS OUT
    // check if the gender is abreviated or not
    private function checkGender($gender)
    {
        if (is_null($gender)) {
            return null;
        }
        
        $g = strtolower($gender);
        $gend = Gender::where('title', '=', $g)
                ->orWhere('title', 'like', $gender[0].'%')
               ->first();
        return $gend->id;
    }

    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    /*     public function model(array $row)
        {
            return new Employee([
                //
            ]);
        }
     */
    private function CheckNationality($row)
    {
        if (isset($row['nationality'])) {
            $country = (new CountryController)->checkNationality($row['nationality']);
        } else {
            //default
            return $this->service->address_country_id;
//            $country = Country::find(1);
        }

        return $country->id;
    }

    private function CheckBank($row)
    {
        $bank = $row['bank'];

        $bank = Bank::where('title', '=', $bank)
            ->first();

        if (is_null($bank)) {
            return null;
        }

        return isset($bank->id) ?: null;
    }

    private function CheckTax($row, $tax_type)
    {
        //check if tap account is set and not "N/A"
        if (isset($row[$tax_type])) {
            $acc_no = Str::lower($row[$tax_type]);
            $acc = ($acc_no == 'n/a' ? null : $acc_no);
        } else {
            //default
            $acc = null;
        }
        
        return $acc;
    }

    private function CheckRace($row)
    {
        if (isset($row['race'])) {
            //find the race
            $race = (new CountryController)->checkNationality($row['race']);
        } else {
            //default
            $race = Race::find($this->service->race_id);
            if (is_null($race)) {
                return null;
            }
        }

        return $race->id;
    }

    private function CheckEmployeeCode($row)
    {
        $code = null;

        if (isset($row['employee_code'])) {
            $code = $row['employee_code'];
        }
        if (isset($row['staff_no'])) {
            $code = $row['staff_no'];
        }

        return $code;
    }

    private function CheckReligion($row)
    {
        if (isset($row['religion'])) {
            $religion_id = (new ReligionController)->checkReligion($row['religion']);
        } else {
            //default value
            return $this->service->address_country_id;
        }

        return $religion_id;
    }

    private function CheckEmploymentStatus($row)
    {
        // no controller
        if (isset($row['employment_status'])) {
            $emp_status = EmploymentStatus::where('title', '=', $row['employment_status'])
               ->first();
            
            if (is_null($emp_status)) {

                // does not exist - generate
                $emp_status = EmploymentStatus::create([
                    'title' => $row['employment_status'],
                    'company_id' => 1
                ]);
            }
        } else {
            return $this->service->employment_status_id;
        }

        return $emp_status->id;
    }
}
