<?php

namespace App\Tenant\Database;

use App\Tenant\Tenant;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\Calculation\Database;

class DatabaseCreator
{
    //protected $connection = 'moss';

    public function create(Tenant $tenant)
    {
        //dd(DB::connection());
        $connection = $tenant->tenantConnection;
//        $sql = "CREATE DATABASE ".env('db_prefix', 'bizp_').$tenant->domain;
        $sql = "CREATE DATABASE ".$tenant->domain."; ";
        $db = DB::connection('moss')->statement($sql);

        $sql = "GRANT ALL PRIVILEGES ON ".$tenant->domain.".* TO 'rlndbuser'@'%';";
        //$sql = "GRANT ALL PRIVILEGES ON ".$tenant->domain.".* TO 'central'@'%';";
        DB::connection('moss')->statement($sql);

        $sql = "FLUSH PRIVILEGES;";
        DB::connection('moss')->statement($sql);
        
        return $db;//DB::statement($sql);
    }
}
