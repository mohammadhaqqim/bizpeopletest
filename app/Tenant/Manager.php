<?php

namespace App\Tenant;

use App\BusinessUser;
use App\Tenant\Tenant;

class Manager
{
    protected $tenant;
    protected $userAccess;

    public function setTenant(Tenant $tenant)
    {
        $this->tenant = $tenant;

        $this->userAccess = BusinessUser::where('user_id', auth()->user()->id)
            ->where('business_id', $this->tenant->id)
            ->first();
    }

    public function getTenant()
    {
        return $this->tenant;
    }

    public function hasTenant()
    {
        return isset($this->tenant);
    }

    public function getUserAccess()
    {
        return $this->userAccess;
    }

    public function hasAcess()
    {
        return isset($this->userAccess);
    }
}
