<?php

namespace App\Tenant\Traits;

use App\Business;

trait FetchesTenant
{
    public function tenants($ids = null)
    {
        $tenants = Business::query();

        if ($ids) {
            $tenants = $tenants->whereIn('id', $ids);
        }

        return $tenants;
    }
}
