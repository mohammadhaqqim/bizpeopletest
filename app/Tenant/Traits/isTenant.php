<?php

namespace App\Tenant\Traits;

use App\BusinessUser;
use Illuminate\Support\Str;
use App\Tenant\Tenant;
use App\TenantConnection;
 
trait isTenant
{
    public static function boot()
    {
        parent::boot();

        static::creating(function ($tenant) {
            $tenant->uuid = Str::uuid();
        });

        static::created(function ($tenant) {
            $tenant->tenantConnection()->save(static::newDatabaseConnection($tenant));
        });
    }

    protected static function newDatabaseConnection(Tenant $tenant)
    {
        return new TenantConnection([
            'database' => $tenant->domain,
            'db_user' => env('NEW_DB_USERNAME', 'root'),
            'db_password' => env('NEW_DB_PASSWORD', 'p@55w0rd1')
        ]);
    }

    public function tenantConnection()
    {
        return $this->hasOne(TenantConnection::class, 'business_id', 'id');
    }
}
