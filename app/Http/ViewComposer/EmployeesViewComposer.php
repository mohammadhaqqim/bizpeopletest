<?php

namespace App\Http\ViewComposer;

use App\Employee;
use Carbon\Carbon;
use Illuminate\View\View;

class EmployeesViewComposer
{
    public function compose(View $view)
    {
        $view->with(
            'employeess',
            Employee::where('end_date', '>=', Carbon::now())
                ->orWhereNull('end_date')->get()
        );
        /*         Employee::where('end_date', '>=', '2021-01 -01')
                    ->orWhereNull('end_date')->get() */
    }
}
