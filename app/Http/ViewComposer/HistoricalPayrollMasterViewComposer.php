<?php

namespace App\Http\ViewComposer;

use App\PayrollMaster;
use Illuminate\View\View;

class PayrollMastersViewComposer
{
    public function compose(View $view)
    {
        $view->with(
            'payrollMasters',
            PayrollMaster::where('company_id', auth()->user()->company_id)
                ->whereNotNull('approved_by')
                ->get()
        );
    }
}
