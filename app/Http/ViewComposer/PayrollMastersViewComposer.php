<?php

namespace App\Http\ViewComposer;

use App\PayrollMaster;
use Illuminate\View\View;

class PayrollMastersViewComposer
{
    public function compose(View $view)
    {
        $view->with(
            'payrollMasters',
            PayrollMaster::whereNull('approved_by')
                ->where('company_id', auth()->user()->company_id)
                ->get()
        );
    }
}
