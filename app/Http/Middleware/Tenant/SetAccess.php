<?php

namespace App\Http\Middleware\Tenant;

use Closure;
use App\Business;
use App\BusinessUser;
use App\Events\Tenant\TenantIdentified;

class SetAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $business_access = $this->resolveAccess(session('business_id'));
        //$business_id = $this->resolveTenant(session('user_id'));
        
        if (!$business_access) {
            return $next($request);
        }
        dd($business_access->business_id);
        if (!auth()->user()->access->contains('business_id', $business_access->business_id)) {
//            if (!auth()->user()->companies->contains('id', $tenant->id)) {
            return redirect('/home');
        }

//        event(new TenantIdentified($tenant));

        //dd(session('tenant'));
        return $next($request);
    }

    protected function resolveAccess($business_id)
    {
        return BusinessUser::where('business_id', $business_id)
            ->where('user_id', auth()->user()->id)
            ->first();
    }
}
