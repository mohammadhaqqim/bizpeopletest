<?php

namespace App\Http\Middleware;

use App\Business;
use App\Company;
use Closure;
use Illuminate\Support\Facades\Auth;
use App\Providers\RouteServiceProvider;
use Illuminate\Contracts\Session\Session;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect(RouteServiceProvider::HOME);
        }

        
        return $next($request);
    }

    private function LoadCompanyDetails()
    {
//        Session::put(['company_id' => 1]);
        session()->put('company_id', 1);
        session()->put('user_id', 2);
    }
}
