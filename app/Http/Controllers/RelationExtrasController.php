<?php

namespace App\Http\Controllers;

use App\RelationExtras;
use Illuminate\Http\Request;

class RelationExtrasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RelationExtras  $relationExtras
     * @return \Illuminate\Http\Response
     */
    public function show(RelationExtras $relationExtras)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RelationExtras  $relationExtras
     * @return \Illuminate\Http\Response
     */
    public function edit(RelationExtras $relationExtras)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RelationExtras  $relationExtras
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RelationExtras $relationExtras)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RelationExtras  $relationExtras
     * @return \Illuminate\Http\Response
     */
    public function destroy(RelationExtras $relationExtras)
    {
        //
    }
}
