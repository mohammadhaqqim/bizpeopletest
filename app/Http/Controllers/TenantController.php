<?php

namespace App\Http\Controllers;

use App\Business;
use App\BusinessUser;
use Illuminate\Http\Request;
use App\Tenant\Manager;

class TenantController extends Controller
{
    public function switch(Request $request, Business $business)
    {
        session()->put('tenant', $business->uuid);
        session()->put('business_id', $business->id);
        session()->put('user_id', auth()->user()->id);
        //session()->put('bus_user_id',)

//        $access = BusinessUser::where('business_id', $business->id)->where('user_id', auth()->user()->id)->first();
//        session()->put('access', $access);
        
        return redirect('/home');
    }
}
