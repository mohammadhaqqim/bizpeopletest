<?php

namespace App\Http\Controllers;

use App\User;
use App\BusinessUser;
use App\Employee;
use App\Tenant\Manager;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class BusinessUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        abort_unless(app(Manager::class)->getUserAccess()->update_admin, 403);

        $business_id = app(Manager::class)->getTenant()->id;
        $users = BusinessUser::where('business_id', $business_id)->with('user')->get();

        return view('settings.useraccess', compact(['users']));
    }


    public function store(Request $request)
    {
        //$this->authorize('update-admin');
        
        //$user = DB::table('users')->where('email', $request['email'])->first();

        // check if email exists
        $user = User::where('email', $request['email'])->first();
        //dd($user);

        if (!isset($user)) {
            //add user
            $psw = Str::random();
            $user = User::create([
                'email' => $request['email'],
                'name' => $request['name'],
                'uuid' => Str::uuid(),
                'password' => Hash::make($psw),
            ]);
        }

        // exits already then exit back to page
        if ($user->checkBusiness($request['business_id'])) {
            return back()->with('warning', 'The user is already connected');
        }

        BusinessUser::create([
            'user_id' => $user->id,
            'business_id' => $request['business_id'],
            'payroll' => $request->exists('payroll_add') ? 1 : 0,
            'hr' => $request->exists('hr_add') ? 1 : 0,
            'hod' => $request->exists('hod_add') ? 1 : 0,
            'manager' => $request->exists('manager_add') ? 1 : 0,
            'admin' => $request->exists('admin_add') ? 1 : 0,
            'block' => $request->exists('block_add') ? 1 : 0,
            'employee_id' => $request['employee_id']
        ]);

        return back();
    }

    public function update($id, Request $request)
    {
        $this->authorize('update-admin');
        
        $businessUser = BusinessUser::findOrFail($id);

        if ($request['employee_id'] != "0" && $request['employee_id'] != null) {
            $employee = DB::connection('tenant')->table('employees')->where('id', $request['employee_id'])->first();
            $request['employee_name'] = $employee->name;
        }

        $businessUser->payroll = $request->exists('payroll') ? 1 : 0;
        $businessUser->hr = $request->exists('hr') ? 1 : 0;
        $businessUser->hod = $request->exists('hod') ? 1 : 0;
        $businessUser->manager = $request->exists('manager') ? 1 : 0;
        $businessUser->admin = $request->exists('admin') ? 1 : 0;
        $businessUser->block = $request->exists('block') ? 1 : 0;

        $attributes = request()->validate([
            'employee_id' => '',
            'employee_name' => ''
        ]);

        $businessUser->update($attributes);

        return back();
    }
}
