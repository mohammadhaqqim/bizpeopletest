<?php

namespace App\Http\Controllers;

use App\Business;
use Carbon\Carbon;
use App\BusinessUser;
use Illuminate\Http\Request;
use App\Events\Tenant\TenantWasCreated;
use Illuminate\Support\Facades\Session;

class BusinessController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        abort_unless(auth()->user()->db_admin, 403);

        $year = Carbon::now()->format('Y');
        $startDate = Carbon::createFromDate($year, 1, 1);
        $endDate = Carbon::createFromDate($year, 12, 31);

        return view('businesses.create')
            ->withStartDate($startDate)
            ->withEndDate($endDate);
    }

    public function store(Request $request)
    {
        //dd(auth()->user()->db_admin);
        abort_unless(auth()->user()->db_admin, 403);

        $company = Business::where('domain', env('db_prefix', 'bizp_').$request->domain)->first();

        if (isset($company)) {
            return back()->with('error', 'Domain has already been used');
        }

        $company = Business::make([
            'owner_id' => auth()->user()->id,
            'name' => $request->name,
            'domain' => env('db_prefix', 'bizp_').$request->domain
        ]);

        $request->user()->companies()->save($company);

        $this->updateUser($company->id);
    
        //add to session (fin year, start date, end date)
        session()->put('company_name', $request->name);
        session()->put('fin_year', $request->fin_year);
        session()->put('start_date', $request->start_date);
        session()->put('end_date', $request->end_date);

        // event
        event(new TenantWasCreated($company));

        return redirect()->route('tenant.switch', $company);
    }

    private function updateUser($company_id)
    {
        //abort_unless(auth()->user()->db_admin, 403);

        $bususer = BusinessUser::where('business_id', $company_id)->where('user_id', auth()->user()->id)->first();

        $bususer->update([
            'payroll' => 1,
            'hr' => 1,
            'admin' => 1,
            'update_admin' => 1,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
