<?php

namespace App\Http\Controllers;

use App\User;
use App\Company;
use App\Country;
use App\Employee;
use App\LeaveType;
use App\LeavePolicy;
use App\WorkingWeek;
use App\PaymentSchedule;
use App\ReportingPeriod;
use App\Transaction;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function company()
    {
        abort_unless(auth()->user()->admin, 403);
                
        $company = Company::find(1);
        
//        dd('company settings');
        
        $countries = Country::select('id', 'country')
            ->whereNull('deleted_at')
            ->get();

        //dd($countries);

        $payschedules = PaymentSchedule::select('id', 'title', 'periods_per_year', 'first_period_date')
            ->whereNull('deleted_at')
            ->get();

        $periods = ReportingPeriod::where('company_id', '=', $company->id)
            ->whereNull('deleted_at')
            ->get();

        return view('settings.company')
            ->withCountries($countries)
            ->withPeriods($periods)
            ->withPaySchedules($payschedules)
            ->withCompany($company);
    }

    public function access()
    {
        abort_unless(auth()->user()->admin, 403);

        $users = User::all();

        //dd($users);
        $employees = Employee::select('id', 'name')
            ->whereNull('deleted_at')
            ->get();

        return view('settings.access')
            ->withEmployees($employees)
            ->withUsers($users);
    }

    public function week()
    {
        abort_unless(auth()->user()->admin, 403);

        $company = Company::find(1);
        $workingweeks = WorkingWeek::all();
        
        return view('settings.week')
            ->withCompany($company)
            ->withWorkingweeks($workingweeks);
    }

    public function timeoff()
    {
        abort_unless(auth()->user()->admin, 403);

        /*         $leavetypes = LeaveType::whereNull('deleted_at')
                    ->with(['LeavePolicies'])
                    ->get(); */

        //dd($leavetypes);
        return view('settings.timeoff');
//            ->withleavetypes($leavetypes);
    }

    public function allowance()
    {
        abort_unless(auth()->user()->admin, 403);

        $allowances = Transaction::whereNull('deleted_at')
            ->where('active', true)
            ->get();

        return view('settings.allowances')
            ->withAllowances($allowances);
    }
}
