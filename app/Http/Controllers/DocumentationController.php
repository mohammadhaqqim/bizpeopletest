<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Documentation;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

use DB;

class DocumentationController extends Controller
{
       /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $documentations = documentation::all();

        $documentations = documentation::orderBy('created_at','asc')->Paginate(10);

      return view('documentations.index')->with('documentations', $documentations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        $documentations = documentation::all();

      return view('documentations.create')->with('documentations', $documentations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $this->validate($request, [
            'documentation_name' => 'required',
            'documentation_text' => 'required',
            'image_link' => 'image|nullable|max:1999',
        ]);

        //Handle File Upload
         if($request->hasFile('image_link')){
            //Get Filename with the Extension
            $filenameWithExt = $request->file('image_link')->getClientOriginalName();
            //Get Just Filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get just Ext
            $extension = $request->file('image_link')->getClientOriginalExtension();
            //Filename to Store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            //Upload Image
            $path = $request->file('image_link')->storeAs('public/image_link', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.jpg';
        }

        // Create documentation
          $documentation = new documentation;
          $documentation->documentation_name = $request->input('documentation_name');
          $documentation->documentation_text = $request->input('documentation_text');
          $documentation->image_link = $fileNameToStore;
          $documentation->save();

      return redirect('/documentations')->with('Success','Documentation Successfully Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $documentation = documentation::find($id);

        return view('documentations.show')->with('documentation', $documentation);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit(documentation $documentation)
    {

      return view('documentations.edit')->with('documentation', $documentation);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'documentation_name' => 'required',
            'documentation_text' => 'required',
            'image_link' => 'image|nullable|max:1999',
        ]);

        //Handle File Upload
         if($request->hasFile('image_link')){
            //Get Filename with the Extension
            $filenameWithExt = $request->file('image_link')->getClientOriginalName();
            //Get Just Filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //Get just Ext
            $extension = $request->file('image_link')->getClientOriginalExtension();
            //Filename to Store
            $fileNameToStore = $filename.'_'.time().'.'.$extension;
            //Upload Image
            $path = $request->file('image_link')->storeAs('public/image_link', $fileNameToStore);
        }

        // Update documentation
          $documentation = documentation::find($id);
          $documentation->documentation_name = $request->input('documentation_name');
          $documentation->documentation_text = $request->input('documentation_text');

          if($request->hasFile('image_link')){
            $documentation->image_link = $fileNameToStore;
          }
          $documentation->save();

        return redirect('/documentations')->with('Success','Documentation Successfully Changes');
      }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy(documentation $documentation)
    {
      //$documentation = documentation::find($id);
       $documentation->delete();

       //Delete Image
       if($documentation->image_link != 'noimage.jpg'){
       Storage::delete('public/image_link/'.$documentation->image_link);
    }

     return redirect('/documentations')->with('Success', 'Documentation Successfully Removed');
    }


    //Sidebar Documentation Only for View
    public function doctopic (){

        return view('documentations.doctopic');
    }

    public function docgettingstarted (){

        return view('documentations.docgettingstarted');
    }

    public function docpayroll (){

        return view('documentations.docpayroll');
    }

    public function docpeople (){

        return view('documentations.docpeople');
    }

    public function docreport (){

        return view('documentations.docreport');
    }


    public function docuser (){

        return view('documentations.docuser');
    }

    public function docsetting (){

        return view('documentations.docsetting');
    }

    public function docerror (){

        return view('documentations.docerror');
    }

    public function docterm (){

        return view('documentations.docterm');
    }

    public function docpolicy (){

        return view('documentations.docpolicy');
    }


    //Documentation Content Only for View

    public function doclogin (){

        return view('documentations.doclogin');
    }

    public function docforgetpassword (){

        return view('documentations.docforgetpassword');
    }

    public function docdepartment (){

        return view('documentations.docdepartment');
    }

    public function docemployee (){

        return view('documentations.docemployee');
    }


    //Documentation Learn Content Only for View

    // Learn Biz Get Start

    public function doclearnbizgetstart (){

        return view('documentations.doclearnbizgetstart');
    }

    public function doclearnbizgetdashboard (){

        return view('documentations.doclearnbizgetdashboard');
    }

    public function doclearnbizgetchangepassword (){

        return view('documentations.doclearnbizgetchangepassword');
    }


    // Learn Biz Payroll


    public function doclearnbizpayrollfeature (){

        return view('documentations.doclearnbizpayrollfeature');
    }

    public function doclearnbizpayrolltransaction (){

        return view('documentations.doclearnbizpayrolltransaction');
    }

    public function doclearnbizpayrollemployee (){

        return view('documentations.doclearnbizpayrollemployee');
    }

    public function doclearnbizpayrollhistory (){

        return view('documentations.doclearnbizpayrollhistory');
    }

    public function doclearnbizpayrollreport (){

        return view('documentations.doclearnbizpayrollreport');
    }


     // Learn Biz People

    public function doclearnbizpeople (){

        return view('documentations.doclearnbizpeople');
    }

    public function doclearnbizpeopleprofile (){

        return view('documentations.doclearnbizpeopleprofile');
    }

    public function doclearnbizpeopleprofilepersonal (){

        return view('documentations.doclearnbizpeopleprofilepersonal');
    }

    public function doclearnbizpeopleprofilejob (){

        return view('documentations.doclearnbizpeopleprofilejob');
    }

    public function doclearnbizpeopleprofileallowance (){

        return view('documentations.doclearnbizpeopleprofileallowance');
    }

    public function doclearnbizpeopleprofiletimeoff (){

        return view('documentations.doclearnbizpeopleprofiletimeoff');
    }

    public function doclearnbizpeopleprofilefamily (){

        return view('documentations.doclearnbizpeopleprofilefamily');
    }

    // Learn Biz Report

    public function doclearnbizreport (){

        return view('documentations.doclearnbizreport');
    }

}
