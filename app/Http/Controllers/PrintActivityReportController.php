<?php

namespace App\Http\Controllers;

use App\Company;
use Carbon\Carbon;
use App\PayrollMaster;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PrintActivityReportController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, $id)
    {
        $payperiod = PayrollMaster::findOrFail($id);
        abort_unless((auth()->user()->payroll || auth()->user()->admin) , 403);

        $path = $this->generateReport($id);
        $path = storage_path($path);

        return response()->file($path);
    }

    private function generateReport($id)
    {
        //get data
        $res = DB::connection('tenant')->table('payroll_employees')
            ->join('employees', 'employees.id', '=', 'payroll_employees.employee_id')
            ->join('payment_modes', 'payment_modes.id', '=', 'payroll_employees.payment_mode_id')
            ->select(
                'employees.id as id',
                'name',
                'base_salary as basic_salary',
                DB::RAW('unpaid_amount*-1 as unpaid'),
                'overtime_amount as overtime',
                'remarks as bonus',
                'addition_amount as additions',
                DB::RAW('(deduction_amount-unpaid_amount)*-1 as deduction'),
                DB::RAW('base_salary+(unpaid_amount*-1)+overtime_amount+addition_amount-deduction_amount as gross'),
                'tap',
                'scp',
                'scp_ee',
                'epf_ee',
                'scp_ee as scp2',
                'title as bank_mode'
            )
            ->where('payroll_master_id', '=', $id)
            ->whereNull('payroll_employees.deleted_at')
            ->get();

        $today = Carbon::now()->format('d-M-Y');
        $company = Company::find(1);
        $payperiod = PayrollMaster::find($id);
        
        $ps = $res;

        $pdf = PDF::loadView('payroll_totals', compact('ps', 'company', 'payperiod', 'today'))->setPaper('a4', 'Landscape');

        $folder = 'app/reports/'.$company->id.'/payroll-totals/';
        $this->CheckFolderExists($folder);

        $fname = $payperiod->title.'.pdf';
        $fname = $folder.$fname;

        $filename = storage_path($fname);
        $pdf->save($filename);

        return $fname;
    }

    private function CheckFolderExists($folder)
    {
        $exists = Storage::disk('local')->has($folder);
        if (!$exists) {
            Storage::disk('local')->makeDirectory($folder);
        }
    }
}
