<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegistrationController extends Controller
{
    //
    /*     public function __construct()
        {
            $this->middleware('auth');
        }
     */
    public function store(Request $request)
    {
        //dd($request);
        app('App\Http\Controllers\BusinessUserController')->store($request);
        
        //dd('hello');

        $request['payroll'] = $request->exists('payroll_0') ? 1 : 0;
        $request['hr'] = $request->exists('hr_0') ? 1 : 0;
        $request['hod'] = $request->exists('hod_0') ? 1 : 0;
        $request['manager'] = $request->exists('manager_0') ? 1 : 0;
        $request['admin'] = $request->exists('admin_0') ? 1 : 0;
        $request['block'] = $request->exists('block_0') ? 1 : 0;


        $attributes = $request->validate([
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191',
            'payroll' => 'boolean',
            'hr' => 'boolean',
            'hod' => 'boolean',
            'manager' => 'boolean',
            'admin' => 'boolean',
            'block' => 'boolean'
        ]);

        $user = User::create($attributes);

        return back();
    }



    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $user->payroll = $request->exists('payroll') ? 1 : 0;
        $user->hr = $request->exists('hr') ? 1 : 0;
        $user->hod = $request->exists('hod') ? 1 : 0;
        $user->manager = $request->exists('manager') ? 1 : 0;
        $user->admin = $request->exists('admin') ? 1 : 0;
        $user->block = $request->exists('block') ? 1 : 0;

        $attributes = request()->validate([
            'employee_id' => 'exists:employees,id'
        ]);

        $user->update($attributes);

        return back();
    }
}
