<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;

class EmployeeAvatarController extends Controller
{
    //

    public function edit($id)
    {
        //dd($id);

        $employee = Employee::find($id);

        return view('employee.avatar.create')
            ->withEmployee($employee);
    }
    
    public function create()
    {
        //
        //dd('create avatar');
        return view('employee.avatar.create');
    }

    public function store(Request $request)
    {
    }
}
