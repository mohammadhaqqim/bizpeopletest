<?php

namespace App\Http\Controllers;

use App\Employee;
use App\LeaveType;
use App\LeavePolicy;
use App\EmployeeLeave;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeLeaveController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee, LeaveType $leavetype)
    {
        //
        //dd($leavetype);


        return view('employee.leave_policy.create');
    }

    public function create(Request $request, Employee $employee, LeaveType $leavetype)
    {

        //
        //dd($leavetype);
        $policies = LeavePolicy::whereNull('deleted_at')
            ->where('active', '=', true)
            ->get();
        

        return view('employee.leave_policy.create');
    }

    public function getPublicHolidays($start, $end)
    {
        /*         $holidays = EmployeeLeave::whereNull('employee_id')
                    ->where(function ($query) use ($start) {
                        $query->where('start_date', '<=', $start)
                            ->where('end_date', '>=', $start);
                    })->orWhere(function ($query) use ($end) {
                        $query->where('start_date', '<=', $end)
                            ->where('end_date', '>=', $end);
                    });
         */
/*         $holidays = EmployeeLeave::whereNull('employee_id')
            ->where(function($q) use ($start, $end){
                $q->where('start_date','<=', $start);
                $q->where('end_date', '>=', $end);
            })

            ->get();
 */
/*         $holidays = DB::table('employee_leaves')
            ->whereNull('employee_id')
            ->where('', )->get();

        return $holidays; */
    }

    public function getHoliday($date)
    {
        $holiday = EmployeeLeave::whererNull('employee_id')
            ->where('start_date', '<=', $date)
            ->where('end_date', '>=', $date)
            ->get();
        
        return is_null($holiday) ? false : true;
    }
}
