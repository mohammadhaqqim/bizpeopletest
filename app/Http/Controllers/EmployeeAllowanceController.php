<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Allowance;
use App\Transaction;
use Auth;
use Carbon\Carbon;
use App\EmployeeAllowance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EmployeeAllowanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $allowances = Transaction::where('active', true)
            ->get();

        return view('settings.allowances')
            ->withAllowances($allowances);
    }
}
