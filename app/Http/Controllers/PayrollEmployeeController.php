<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\PayrollEmployee;
//use Barryvdh\DomPDF\PDF;
use PDF;
use App\PayrollTransaction;
use Illuminate\Http\Request;

class PayrollEmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        abort_unless(auth()->user()->payroll, 403);
        //
        $payrollEmployee = PayrollEmployee::find($id);

        if ($payrollEmployee->PaymentType->periods != 0) {
            $request['units'] = 0;
        } else {
//            $request['total_days'] = 1;
            $request['work_days'] = $request['units'];
//            $request['base_salary'] = $request['units'];
            $request['base_salary'] = $request['pay'];
        }

        $attributes = request()->validate([
            'payment_mode_id' => 'required|exists:payment_modes,id',
            'base_salary' => '',
            'bank_id' => 'nullable|exists:banks,id',
            'bank_account' => 'nullable|max:50',
            'work_days' => 'nullable|numeric',
            'units' => 'numeric'
        ]);
       
        $payrollEmployee->update($attributes);
        $payrollEmployee->performCalculations();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort_unless(auth()->user()->payroll, 403);

        //
        $payrollEmployee = PayrollEmployee::findOrFail($id);

        $payrollTransactions = PayrollTransaction::where('payroll_employees_id', $id)->get();

        foreach ($payrollTransactions as $trans) {
            $trans->deleted_at = Carbon::now();
            $trans->save();
        }

        $payrollEmployee->deleted_at = Carbon::now();
        $payrollEmployee->save();

        return back();
    }
}
