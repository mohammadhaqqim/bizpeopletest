<?php

namespace App\Http\Controllers;

use App\Company;
use App\EmploymentStatus;
use Illuminate\Http\Request;

class EmploymentStatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['company_id'] = 1;

        $attributes = $request->validate([
            'title' => 'required|max:100|unique:races,title',
            'company_id' => 'required|exists:countries,id'
        ]);

        EmploymentStatus::create($attributes);

        return back()->with('success', 'A new employment status was generated');
    }

        /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmploymentStatus  $status
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmploymentStatus $employmentstatus)
    {

        if ($request->has('default')) {
            $company = Company::find(1);
            $company->employment_status_id = $employmentstatus->id;
            $company->save();
        }

        $employmentstatus->title = $request->title;
        $employmentstatus->save();

        return back()->with('success', 'The employment status was edited successfully');
    }
    //
}
