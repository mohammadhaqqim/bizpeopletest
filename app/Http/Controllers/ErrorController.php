<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ErrorController extends Controller
{

    public function errorpage (){

        return view('errors.errorpage');
    }

    public function errorpage404 (){

        return view('errors.errorpage404');
    }

}
