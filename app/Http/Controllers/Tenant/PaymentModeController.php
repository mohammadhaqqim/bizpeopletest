<?php

namespace App\Http\Controllers\Tenant;

use App\Company;
use App\PaymentMode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentModeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('update-admin');

        $attributes = $request->validate([
            'title' => 'required|max:100|unique:tenant.payment_modes,title',
        ]);

        PaymentMode::create($attributes);

        return back()->with('success', 'A new payment mode was generated');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update-admin');

        $paymentmode = PaymentMode::find($id);
        //
        if ($request->has('default')) {
            $company = Company::find(1);
            $company->payment_mode_id = $paymentmode->id;
            $company->save();
        }

        $paymentmode->title = $request->title;
        $paymentmode->save();

        return back()->with('success', 'The payment mode was edited successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('update-admin');
        //
        $paymentmode = PaymentMode::find($id);
        $paymentmode->deleted_at = \Carbon\Carbon::now();
        $paymentmode->save();

        return redirect('settings/customise/payment_mode')
            ->with('success', 'The payment mode was deleted');
    }
}
