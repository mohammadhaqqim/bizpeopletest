<?php

namespace App\Http\Controllers\Tenant;

use App\Employee;
use Carbon\Carbon;
use App\EmployeeStatus;
use App\Tenant\Manager;
use App\EmploymentStatus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployeeStatusController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        dd('index-EmployeeStatus');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        dd('create-EmployeeStatus');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Employee $employee)
    {
        $this->authorize('update', $employee);

        $es = EmploymentStatus::select('id')
            ->where('title', request('employment_status_id'))
            ->first();

        if (request('effective_date') == Carbon::now()) {
            $employee->employment_type_id = $es->id;
        };

        $attributes = request()->validate([
            'effective_date' => 'required|date',
            'status_comment' => 'max:200'
        ]);

        $attributes['entered_by'] = app(Manager::class)->getUserAccess()->user_id;
        $attributes['employment_status_id'] = $es->id;
        $attributes['employee_id'] = $employee->id;
        
        EmployeeStatus::create($attributes);
            
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        //dd(request());
        $this->authorize('update', $employee);

        //dd('show-EmployeeStatus');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //dd('edit-EmployeeStatus');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeStatus $employeeStatus)
    {
        $this->authorize('update', $employeeStatus);
        
        $employeeStatus->update(request([
            'effective_date',
            'employment_status_id',
            'status_comment',
        ]));

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeStatus $employeeStatus)
    {
        // *
        $this->authorize('update', $employeeStatus);
    }
}
