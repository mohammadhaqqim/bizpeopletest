<?php

namespace App\Http\Controllers\Tenant;

use App\Company;
use App\Department;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('update-admin');

        $attributes = $request->validate([
            'title' => 'required|max:100|unique:tenant.departments,title',
          //  'company_id' => 'required|exists:countries,id'
        ]);
        // /dd($attributes);
        
        Department::create($attributes);

        return back()->with('success', 'A new department was generated');
    }

    public function create()
    {
        return view('department.create');
//        dd('create new designation');
    }

    public function update(Department $department, Request $request)
    {
        $this->authorize('update-admin');

        if ($request->has('default')) {
            $company = Company::find(1);
            $company->department_id = $department->id;
            $company->save();
        }

        $department->title = $request->title;
        $department->save();

        return back()->with('success', 'The department was edited successfully');
    }

    public function destroy(Department $department)
    {
        $this->authorize('update-admin');

//        $this->authorize('update', $department);
        $department->deleted_at = Carbon::now();
        $department->save();

        return redirect('settings/customise/departments')
            ->with('success', 'The department was deleted');
        //
    }

    public function checkDepartment($title)
    {
        $department = Department::where('title', '=', $title)
            ->first();

        if (is_null($department)) {
            // does not exist - generate
            $department = Department::create([
                'title' => $title
            ]);
        }

        return $department->id;
    }
}
