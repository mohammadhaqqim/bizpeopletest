<?php

namespace App\Http\Controllers\Tenant;

use App\Bank;
use App\Company;
use App\Employee;
use Carbon\Carbon;
use App\PaymentMode;
use App\Transaction;
use App\PayrollMaster;
use App\EmployeePayment;
use App\PayrollEmployee;
use App\PayrollTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class PayrollController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_unless($this->authorize('update-payroll'), 403);
        
        return view('payroll.index');
    }

    public function details()
    {

        //
        abort_unless($this->authorize('update-payroll'), 403);

        return view('payroll.details.edit');
    }

    public function transactions($id)
    {
        abort_unless($this->authorize('update-payroll'), 403);

        //get the payroll
        $payroll = PayrollMaster::find($id);

        //get payroll employees
        $employeePayrolls = app(PayrollEmployeeController::class)->getPayrollEmployees($payroll);

        $payrollTransactions = PayrollTransaction::whereHas('PayrollEmployee', function ($q) use ($id) {
            $q->where('payroll_master_id', '=', $id);
        })->whereNull('deleted_at')->get();

//        $employees = app(EmployeeController::class)->getEmployeesForPayperiod($payroll);
        $employees = Employee::select('id', 'name', 'employee_code')
            ->whereNull('deleted_at')
            ->where('date_joined', '<=', $payroll->end_date)
            ->where(function ($query) use ($payroll) {
                $query->whereNull('end_contract_date')
                    ->orWhere('end_contract_date', '>=', $payroll->start_date);
            })
            //->with(['currentjob','currentStatus', 'getPayrollEmployees'])
             ->with(['currentjob','currentStatus', 'getPayrollEmployees' => function ($query) use ($payroll) {
                 return $query->where('payroll_master_id', '=', $payroll->id);
             }])
             ->get();

        //dd($employees);


        return view('payroll.transactions.edit')
            ->withPayroll($payroll)
            ->withEmployees($employees)
            ->withEmployeePayrolls($employeePayrolls)
            ->withPayrollTransactions($payrollTransactions);

        //->withTransactions($transactions);
            //->withEmployees($employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //dd('PayrollMaster @ store');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function show(PayrollMaster $payrollMaster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function edit(PayrollMaster $payrollMaster)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PayrollMaster $payrollMaster)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function destroy(PayrollMaster $payrollMaster)
    {
        //
    }

    public function employees($id)
    {
        abort_unless($this->authorize('update-payroll'), 403);

        //get the payroll
        $payroll = PayrollMaster::find($id);

        //get payroll employees
        /*         $employeePayrolls = PayrollEmployee::where('payroll_master_id', $id)
                    ->whereNull('deleted_at')
                    ->with('Employee')
                    ->get();
         */

        $employeePayrolls = app(PayrollEmployeeController::class)->getPayrollEmployees($payroll);
        $employees = app(EmployeeController::class)->getEmployeesForPayperiod($payroll);
        
        /*        $employees = Employee::select('id', 'name', 'employee_code')
                   ->whereNull('deleted_at')
                   ->where('date_joined', '<=', $payroll->end_date)
                   ->where('end_contract_date', '>=', $payroll->start_date)
                   ->with(['currentjob','currentStatus'])
                   ->get(); */
            
        return view('payroll.details.edit')
            ->withPayroll($payroll)
            ->withEmployees($employees)
            ->withEmployeePayrolls($employeePayrolls);
    }

    public function employee()
    {
        //dd('hello employees');
    }

    public function historical()
    {
        //dd('historical employees');
    }
}
