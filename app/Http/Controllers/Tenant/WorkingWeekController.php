<?php

namespace App\Http\Controllers\Tenant;

use App\Company;
use App\WorkingWeek;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class WorkingWeekController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $company = Company::find(1);
        $workingweeks = WorkingWeek::whereNull('deleted_at')->get();
        
        return view('settings.week')
            ->withCompany($company)
            ->withWorkingweeks($workingweeks);
    }

    public function store(Request $request)
    {
        //dd('update wweek');
        $this->authorize('update-admin');

        $request['hours_day'] = $this->getAverageHoursDay($request);

        $attributes = $request->validate([
            'title' => 'required|max:100',
            'mon' => 'min:0|max:24',
            'tue' => 'min:0|max:24',
            'wed' => 'min:0|max:24',
            'thu' => 'min:0|max:24',
            'fri' => 'min:0|max:24',
            'sat' => 'min:0|max:24',
            'sun' => 'min:0|max:24',
        ]);
     
        // not sure required???
        $attributes['default_week'] = $request->has('defaultWeek') ? "1" : "0";

        $week = WorkingWeek::create($attributes);

        if ($request['defaultWeek'] == 'on') {
            $company = Company::find(1);
            $company->working_week_id = $week->id;
            $company->save();
        }

        return redirect('settings/week')
            ->with('success', 'Successful created a new working week');
    }


    private function resetDefault($company_id)
    {
        $update = "UPDATE working_weeks SET default_week = false ";
        $update = $update.$company_id;

        DB::connection('tenant')->statement($update);
    }

    public function update(Request $request, $id)
    {
        $this->authorize('update-admin');

        $request['hours_day'] = $this->getAverageHoursDay($request);
        $attributes = $request->validate([
            'title' => 'required|max:100',
            'mon' => 'min:0|max:24',
            'tue' => 'min:0|max:24',
            'wed' => 'min:0|max:24',
            'thu' => 'min:0|max:24',
            'fri' => 'min:0|max:24',
            'sat' => 'min:0|max:24',
            'sun' => 'min:0|max:24',
        ]);
            
        $workingWeek = WorkingWeek::find($id);
        //$attributes['default'] = $request['defaultWeek'] == 'on' ? "1" : "0";
        $attributes['default_week'] = $request->has('default_week') ? "1" : "0";

        $workingWeek->update($attributes);
        if ($request['defaultWeek'] == 'on') {
            $company = Company::find(1);
            $company->working_week_id = $workingWeek->id;
            $company->save();
        }

        return redirect('settings/week')
            ->with('success', 'Successful updated the working week - '.$workingWeek->title);
    }

    public function destroy($id)
    {
        $this->authorize('update-admin');
        
        $workingWeek = WorkingWeek::find($id);
        $workingWeek->deleted_at = \Carbon\Carbon::now();
        $workingWeek->update();

        return redirect('settings/week')
            ->with('success', 'Successful removal the working week - '.$workingWeek->title);
    }

    private function getAverageHoursDay($workingWeek)
    {
        $days = 0;
        $hours = $workingWeek['mon'] + $workingWeek['tue'] + $workingWeek['wed'] + $workingWeek['thu'];

        if ($workingWeek['mon'] > 0) {
            $days++;
        }
        
        if ($workingWeek['tue'] > 0) {
            $days++;
        }

        if ($workingWeek['wed'] > 0) {
            $days++;
        }
        
        if ($workingWeek['thu'] > 0) {
            $days++;
        }

        // /        dd($hours / $days);
//        dd('days '.$days.' hours '.$hours);

        //$avg = $avg / $days;
        //$this->WorkingWeek->hours_day = $avg;
        return $hours / $days;
    }
}
