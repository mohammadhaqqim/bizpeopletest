<?php

namespace App\Http\Controllers\Tenant;

use App\Employee;
use Carbon\Carbon;
use App\EmployeeLeave;
use App\Mail\LeaveActionResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class TimeOffApproval extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $leave = EmployeeLeave::find($id);

//        dd($leave);
        return view('employee.approval.show')
            ->withLeave($leave);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('employee.approval.show');
    }

    public function approve($id)
    {
        // or reports too
        abort_unless($this->authorize('update-hr') || $this->authorize('update-admin'), 403);

        $leave = EmployeeLeave::find($id);
        $leave->approved_by_id = auth()->user()->id;
        $leave->approved_at = \Carbon\Carbon::now();
        
        $leave->rejected_by_id = null;
        $leave->deny_at = null;
        $leave->rejected_at = null;
        $leave->save();
        
        // update totals on policy
        $leave->getCalculatePolicyTotals();
        //dd($leave);
        $this->SendEmail($leave->employee_id, $leave);

        return redirect('/home')->with('success', 'The time off has been successfully approved');
    }

    public function deny($id, Request $request)
    {
        abort_unless($this->authorize('update-hr') || $this->authorize('update-admin'), 403);

        $leave = EmployeeLeave::find($id);

        $leave->approved_by_id = null;
        $leave->approved_at = null;

        $leave->rejected_by_id = auth()->user()->id;
        $leave->deny_at = \Carbon\Carbon::now();
        $leave->deny_comment = request('deny_comment');
        $leave->rejected_at = \Carbon\Carbon::now();
        $leave->save();

        // update totals on policy
        $leave->getCalculatePolicyTotals();
        $this->SendEmail($leave->employee_id, $leave);

        return redirect('/home')->with('success', 'The time off has been denied approval');
    }

    // send email if there are email addresses to the staff member
    private function SendEmail($id, $leave)
    {
        $employee = Employee::find($id);

        if (!is_null($employee->work_email)) {
            Mail::to($employee->work_email)->send(
                new LeaveActionResponse($leave)
            );
        }
    }
}
