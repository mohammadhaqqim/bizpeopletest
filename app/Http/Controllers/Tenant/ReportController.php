<?php

namespace App\Http\Controllers\Tenant;

use PDF;
use Mail;
//use Excel;
use Response;
use ZipArchive;
use Carbon\Carbon;
use App\PayrollMaster;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Exports\PayrollTotalsExport;
use App\Http\Controllers\Controller;
use InfyOm\Generator\Utils\ResponseUtil;
use Psy\CodeCleaner\FunctionReturnInWriteContextPass;

use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function exportPayrollTotals($payroll_id)
    {
        return Excel::download(new PayrollTotalsExport('Payroll Totals Report', $payroll_id), 'PayrollTotals.xlsx');
    }

    public function index()
    {
//        abort_unless(auth()->user()->payroll, 403);
        $this->authorize('update-payroll');

        return redirect('payroll/reports/audit');
    }


    public function payroll_audit()
    {
//        abort_unless(auth()->user()->payroll, 403);
        $this->authorize('update-payroll');

        $payrolls = PayrollMaster::whereNull('deleted_at')
            ->select('id', 'created_by', 'title', 'end_date')
            ->with(['CreatedBy'])
            ->get();

        return view('payroll.reports.payroll_audit')
            ->withPayrolls($payrolls);
    }

    public function PayrollTotals($payroll_id)
    {
        Excel::create('payroll_totals', function ($excel) use ($payroll_id) {
        
            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Payroll');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($payroll_id) {
                $payrolltotal = $this->GetPayrollData($payroll_id);
                $period = $this->GetPeriod($payroll_id);

                //report information - overall header
                $this->SetReportTitle($sheet, $period, 'Salary Payments Register');

                //Load the data column headers
                $this->SetPayrollHeaders($sheet);

                //Load in the data
                $sheet->fromArray($payrolltotal, null, 'B8', false, false);

                //Add the formula
                $count = 1;
                foreach ($payrolltotal as $payroll) {
                    $row = $count + 8;
                    if ($row > 9) {
                        $therow = 'A'.$row.':O'.$row;
                        $this->SheetBorderRow($sheet, $therow);
                    }

                    $sheet->getstyle('D'.$row)->getNumberFormat()
                        ->setFormatCode('#,##0;[Red](#,##0)');
                    $sheet->getstyle('H'.$row)->getNumberFormat()
                        ->setFormatCode('#,##0;[Red](#,##0)');

                    $formula = '=ROUND(SUM(C'.$row.':H'.$row.'),2)';
                    $sheet->setCellValue('I'.$row, $formula);
                    $sheet->setCellValue('A'.$row, $count);

                    $formula = '=ROUND(SUM(I'.$row.'-J'.$row.'-L'.$row.'-M'.$row.'),2)';
                    $sheet->setCellValue('N'.$row, $formula);

                    $sheet->getStyle('I'.$row)->getFont()->setBold(true);
                    $sheet->getStyle('N'.$row)->getFont()->setBold(true);

                    $count++;
                }

                $count--;
                $row = $count + 8;
                $row--;
                $therow = 'A9:O'.$row;
                $this->SheetBorderThin($sheet, $therow, false);

                $count = $count+8;
                $row = $count;
                $count--;
                $sheet->getRowDimension(8)->setVisible(false);

                $this->IncludePayrollTotals($sheet, $count, $row);

                // borders
                $this->SetBorders($sheet, $row);

                $sheet->cell('L9:L'.$row, function ($cell) {
                    $cell->setAlignment('right');
                });

                $sheet->setHeight($row, 30);

                $sheet->cell('A'.$row.':B'.$row, function ($cell) {
                    $cell->setBorder('none', 'none', 'none', 'none');
                    $cell->setAlignment('right');
                });

                $insert = $row+3;
                $start = $insert;
                $formula = '=SUMIF(O9:O'.$row.', "Bank Transfer", N9:N'.$row.')';
                $sheet->setCellValue('N'.$insert, $formula);
                $sheet->setCellValue('O'.$insert, 'Bank Transfer');
                $insert++;

                $formula = '=ROUND(SUMIF(O9:O'.$row.', "Cash", N9:N'.$row.'),2)';
                $sheet->setCellValue('N'.$insert, $formula);
                $sheet->setCellValue('O'.$insert, 'Cash');
                $insert++;

                $formula = '=ROUND(SUMIF(O9:O'.$row.', "Cheque", N9:N'.$row.'),2)';
                $sheet->setCellValue('N'.$insert, $formula);
                $sheet->setCellValue('O'.$insert, 'Cheque');

                $formula = '=ROUND(SUM(N'.$start.':'.'N'.$insert.'),2)';
                $insert++;
                $sheet->setCellValue('N'.$insert, $formula);

                $sheet->cell('N'.$insert, function ($cell) {
                    $cell->setBorder('thin', 'none', 'double', 'none');
                });

                $sheet->setAutoSize(array(
                    'O'
                ));

                $this->SetColumnWidths($sheet);
                $this->SetPayrollFooter($sheet, $row);
            });
        })->download('xlsx');
    }


    public function SetPayrollFooter($sheet, $row)
    {
        $row=$row + 3;
        $sheet->setCellValue('B'.$row, 'Prepared by :');
        $sheet->setCellValue('E'.$row, 'Approved by :');

        $row += 3;

        $sheet->setCellValue('B'.$row, 'Name :');
        $sheet->setCellValue('E'.$row, 'Name :');

        $sheet->cell('B'.$row, function ($cell) {
            $cell->setBorder('thin', 'none', 'none', 'none');
        });

        $sheet->cell('E'.$row.':I'.$row, function ($cell) {
            $cell->setBorder('thin', 'none', 'none', 'none');
        });

        $row++;
        $sheet->setCellValue('B'.$row, 'Date :');
        $sheet->setCellValue('E'.$row, 'Date :');

        $row++;
        $row++;

        $sheet->setCellValue('B'.$row, 'Note :');
        $row++;
        $sheet->setCellValue('B'.$row, 'EE - Employee');
        $row++;
        $sheet->setCellValue('B'.$row, 'ER - Employer');
    }

    public function ActivityReport($payroll_id)
    {
        Excel::create('activity_report', function ($excel) use ($payroll_id) {
        
            // Set the spreadsheet title, creator, and description
            $excel->setTitle('Activity');

            // Build the spreadsheet, passing in the payments array
            $excel->sheet('sheet1', function ($sheet) use ($payroll_id) {
                $payrolltotal = $this->GetActivityData($payroll_id);
                $period = $this->GetPeriod($payroll_id);

                //report information - overall header
                $this->SetReportTitle($sheet, $period, 'Activity Report');

                //Load the data column headers
                $this->SetActivityHeaders($sheet);

                //Load in the data
                $sheet->fromArray($payrolltotal, null, 'B8', false, false);

                //Add the formula
                $count = 1;
                foreach ($payrolltotal as $payroll) {
                    $row = $count + 7;
                    if ($row > 8) {
                        $therow = 'A'.$row.':R'.$row;
                        $this->SheetBorderRow($sheet, $therow);
                    }

                    // $formula = '=SUM(F'.$row.':K'.$row.')';
                    // $sheet->setCellValue('L'.$row, $formula);
                    $sheet->setCellValue('A'.$row, $count);

                    // $formula = '=SUM(M'.$row.':P'.$row.')';
                    // $sheet->setCellValue('R'.$row, $formula);

                    $check = $sheet->getCell('E'.$row)->getValue();
                    $check = $check.$sheet->getCell('L'.$row)->getValue();
                    $check = $check.$sheet->getCell('R'.$row)->getValue();

                    if (trim($check) == "") {
                        $sheet->getRowDimension($row)->setVisible(false);
                    }

                    $count++;
                }

                // box data
                $count--;
                $row = $count + 8;
                $count = $count+8;
                $row = $count;
                $count--;

                $this->IncludeActivityTotals($sheet, $count, $row);

                // borders
                $this->SetBordersActivity($sheet, $row);

                // totals look
                $sheet->setHeight($row, 30);
                $sheet->cell('A'.$row.':B'.$row, function ($cell) {
                    $cell->setBorder('none', 'none', 'none', 'none');
                    $cell->setAlignment('right');
                });

                $this->SetColumnWidthsActivity($sheet);
            });
        })->download('xlsx');
    }

    private function SetReportTitle($sheet, $period, $title)
    {
        $sheet->setCellValue('B1', env('COMPANY_NAME'));
        $sheet->getStyle('B1')->getFont()->setBold(true);
        $sheet->setCellValue('B2', $title);
        $sheet->getStyle('B2')->getFont()->setBold(true);
        $sheet->setCellValue('B3', $period);
        $sheet->getStyle('B3')->getFont()->setBold(true);
    }

    private function SetBorders($sheet, $row)
    {
        $this->SheetBorderThin($sheet, 'C5:C'.$row, false);
        $this->SheetBorderThin($sheet, 'E5:E'.$row, false);
        $this->SheetBorderThin($sheet, 'G5:G'.$row, false);
        $this->SheetBorderThin($sheet, 'I5:I'.$row, false);
        $this->SheetBorderThin($sheet, 'K5:K'.$row, false);
        $this->SheetBorderThin($sheet, 'M5:M'.$row, false);
        $this->SheetBorderThin($sheet, 'N5:N'.$row, false);
        $this->SheetBorderThin($sheet, 'O5:O'.$row, false);
        $this->SheetBorderThin($sheet, 'C'.$row.':M'.$row, false);
    }

    private function SetBordersActivity($sheet, $row)
    {
        $this->SheetBorderThin($sheet, 'C7:R'.$row, false);
        $this->SheetBorderThin($sheet, 'C'.$row.':R'.$row, false);
        $this->SheetBorderThick($sheet, 'E5:E'.$row, false);
        $this->SheetBorderThick($sheet, 'L5:L'.$row, false);
        $this->SheetBorderThick($sheet, 'R5:R'.$row, false);

        $this->SheetBorderDotted($sheet, 'G5:G'.$row, false);
        $this->SheetBorderDotted($sheet, 'I5:I'.$row, false);
        $this->SheetBorderDotted($sheet, 'J5:J'.$row, false);
        $this->SheetBorderDotted($sheet, 'N5:N'.$row, false);
        $this->SheetBorderDotted($sheet, 'P5:P'.$row, false);
    }

    //Set up the totals of the report
    private function IncludePayrollTotals($sheet, $count, $row)
    {
        $sheet->setCellValue('A'.$row, '');

        $sheet->setCellValue('B'.$row, 'TOTAL');

        $formula = '=ROUND(SUM(C8:C'.$count.'),2)';
        $sheet->setCellValue('C'.$row, $formula);
        $formula = '=ROUND(SUM(D8:D'.$count.'),2)';
        $sheet->setCellValue('D'.$row, $formula);
        $formula = '=ROUND(SUM(E8:E'.$count.'),2)';
        $sheet->setCellValue('E'.$row, $formula);
        $formula = '=ROUND(SUM(F8:F'.$count.'),2)';
        $sheet->setCellValue('F'.$row, $formula);
        $formula = '=ROUND(SUM(G8:G'.$count.'),2)';
        $sheet->setCellValue('G'.$row, $formula);
        $formula = '=ROUND(SUM(H8:H'.$count.'),2)';
        $sheet->setCellValue('H'.$row, $formula);
        $formula = '=ROUND(SUM(I8:I'.$count.'),2)';
        $sheet->setCellValue('I'.$row, $formula);
        $formula = '=ROUND(SUM(J8:J'.$count.'),2)';
        $sheet->setCellValue('J'.$row, $formula);
        $formula = '=ROUND(SUM(K8:K'.$count.'),2)';
        $sheet->setCellValue('K'.$row, $formula);
        $formula = '=ROUND(SUM(L8:L'.$count.'),2)';
        $sheet->setCellValue('L'.$row, $formula);
        $formula = '=ROUND(SUM(M8:M'.$count.'),2)';
        $sheet->setCellValue('M'.$row, $formula);

        // allowances
        // if ($sheet->getCell('G'.$row)->getValue() == 0) {
        //     $sheet->getColumnDimension('G')->setVisible(false);
        // }

        // Deductions
        // if ($sheet->getCell('H'.$row)->getValue() == 0) {
        //     $sheet->getColumnDimension('H')->setVisible(false);
        // }

        // EPF
        // if ($sheet->getCell('M'.$row)->getValue() == 0) {
        //     $sheet->getColumnDimension('M')->setVisible(false);
        // }

        $sheet->getStyle('C'.$row.':N'.$row)->getFont()->setBold(true);
    }

    private function IncludeActivityTotals($sheet, $count, $row)
    {
        $sheet->setCellValue('B'.$row, 'TOTAL');

        $formula = '=ROUND(SUM(C8:C'.$count.'),2)';
        $sheet->setCellValue('C'.$row, $formula);
        $formula = '=ROUND(SUM(D8:D'.$count.'),2)';
        $sheet->setCellValue('D'.$row, $formula);
        $formula = '=ROUND(SUM(E8:E'.$count.'),2)';
        $sheet->setCellValue('E'.$row, $formula);
        $formula = '=ROUND(SUM(F8:F'.$count.'),2)';
        $sheet->setCellValue('F'.$row, $formula);
        $formula = '=ROUND(SUM(G8:G'.$count.'),2)';
        $sheet->setCellValue('G'.$row, $formula);
        $formula = '=ROUND(SUM(H8:H'.$count.'),2)';
        $sheet->setCellValue('H'.$row, $formula);
        $formula = '=ROUND(SUM(I8:I'.$count.'),2)';
        $sheet->setCellValue('I'.$row, $formula);
        $formula = '=ROUND(SUM(J8:J'.$count.'),2)';
        $sheet->setCellValue('J'.$row, $formula);
        $formula = '=ROUND(SUM(K8:K'.$count.'),2)';
        $sheet->setCellValue('K'.$row, $formula);
        $formula = '=ROUND(SUM(L8:L'.$count.'),2)';
        $sheet->setCellValue('L'.$row, $formula);

        $formula = '=ROUND(SUM(M9:M'.$count.'),2)';
        $sheet->setCellValue('M'.$row, $formula);
        $formula = '=ROUND(SUM(N9:N'.$count.'),2)';
        $sheet->setCellValue('N'.$row, $formula);
        $formula = '=ROUND(SUM(P9:P'.$count.'),2)';
        $sheet->setCellValue('P'.$row, $formula);
        $formula = '=ROUND(SUM(R9:R'.$count.'),2)';
        $sheet->setCellValue('R'.$row, $formula);

        $sheet->getStyle('C'.$row.':R'.$row)->getFont()->setBold(true);
    }

    private function SetColumnWidths($sheet)
    {
        $sheet->setWidth('A', 4);
        $sheet->setWidth('B', 44);
        $sheet->setWidth('C', 10);
        $sheet->setWidth('D', 10);
        $sheet->setWidth('E', 10);
        $sheet->setWidth('F', 10);
        $sheet->setWidth('G', 10);
        $sheet->setWidth('H', 10);
        $sheet->setWidth('I', 10);
        $sheet->setWidth('J', 10);
        $sheet->setWidth('K', 10);
        $sheet->setWidth('L', 10);
        $sheet->setWidth('M', 10);
    }

    private function SetColumnWidthsActivity($sheet)
    {
        $sheet->setWidth('A', 4);
        $sheet->setWidth('B', 44);
        $sheet->setWidth('C', 10);
        $sheet->setWidth('D', 10);
        $sheet->setWidth('E', 10);
        $sheet->setWidth('F', 10);
        $sheet->setWidth('G', 10);
        $sheet->setWidth('H', 10);
        $sheet->setWidth('I', 10);
        $sheet->setWidth('J', 10);
        $sheet->setWidth('K', 10);
        $sheet->setWidth('L', 10);
        $sheet->setWidth('M', 10);
        $sheet->setWidth('N', 10);
        $sheet->setWidth('O', 10);
        $sheet->setWidth('P', 10);
        $sheet->setWidth('Q', 10);
        $sheet->setWidth('R', 10);
    }

    private function GetActivityData($payroll_id)
    {
        $res = DB::connection('tenant')->table('tbl_payroll')
            ->select(
                'emp_name',
                'basic_salary',
                'old_salary',
                DB::RAW('(tbl_payroll.basic_salary - tbl_payroll.old_salary) as diff'),
                DB::RAW('(SELECT sum(amount) FROM tbl_payroll_add_ded 
                    WHERE payroll_id=tbl_payroll.id 
                    and addition=true
                    and deleted_at is null
                    and title like "%over%time%") as overtime'),
                DB::RAW('(SELECT sum(amount) FROM tbl_payroll_add_ded 
                    WHERE payroll_id=tbl_payroll.id 
                    and addition=true
                    and deleted_at is null
                    and title like "%double%time%") as doubletime'),
                DB::RAW('(SELECT sum(amount) FROM tbl_payroll_add_ded 
                    WHERE payroll_id=tbl_payroll.id 
                    and addition=true
                    and deleted_at is null
                    and title like "%transport%") as transport'),
                DB::RAW('(SELECT sum(amount) FROM tbl_payroll_add_ded 
                    WHERE payroll_id=tbl_payroll.id 
                    and addition=true
                    and deleted_at is null
                    and title like "%housing%") as housing'),
                DB::RAW('(SELECT sum(amount) FROM tbl_payroll_add_ded 
                    WHERE payroll_id=tbl_payroll.id 
                    and addition=true
                    and deleted_at is null
                    and title like "%meal%") as meal'),
                DB::RAW('(SELECT sum(amount) FROM tbl_payroll_add_ded 
                    WHERE payroll_id=tbl_payroll.id 
                    and addition=true
                    and deleted_at is null
                    and title not like "%meal%"
                    and title not like "%housing%"
                    and title not like "%transport%"
                    and title not like "%double%time%"
                    and title not like "%over%time%"
                    ) as other'),
                DB::RAW('(SELECT sum(amount) FROM tbl_payroll_add_ded 
                    WHERE payroll_id=tbl_payroll.id 
                    and addition=true
                    and deleted_at is null) as additions'),
                DB::RAW('(SELECT sum(amount) FROM tbl_payroll_add_ded 
                    WHERE payroll_id=tbl_payroll.id 
                    and addition=false
                    and deleted_at is null
                    and title like "%unpaid%leave%") as unpaid'),
                DB::RAW('(SELECT sum(amount) FROM tbl_payroll_add_ded 
                    WHERE payroll_id=tbl_payroll.id 
                    and addition=false
                    and deleted_at is null
                    and title like "%personal%") as personal'),
                'deleted_at as desc1',
                DB::RAW('(SELECT sum(amount) FROM tbl_payroll_add_ded 
                    WHERE payroll_id=tbl_payroll.id 
                    and addition=false
                    and deleted_at is null
                    and title like "%other%") as xyz'),
                'deleted_at as desc2',
                DB::RAW('(SELECT sum(amount) FROM tbl_payroll_add_ded 
                    WHERE payroll_id=tbl_payroll.id 
                    and addition=false
                    and deleted_at is null) as deductions')
            )
            ->where([
                ['payroll_id', '=', $payroll_id]
            ])
            ->get();

        $payrolltotal  = array();
//        $payrolltotal[] = ["Name","Basic Salary","Old Salary", "Change Salary", "Overtime","Double time", "Transport", "Housing", "Meal",
//            "+Other", "Additions", "Unpaid Leave", "Personal", "Per Desc", "-Other", "Other Desc", "Deductions"];
        foreach ($res as $pr) {
            //$row = array( )
            array_push($payrolltotal, (array)$pr);
        }

        return $payrolltotal;
    }

    private function GetPayrollData($payroll_id)
    {
        $res = DB::table('payroll_employees')
            ->join('employees', 'employees.id', '=', 'payroll_employees.employee_id')
            ->join('payment_modes', 'payment_modes.id', '=', 'payroll_employees.payment_mode_id')
            ->select(
                'name',
                'base_salary as basic_salary',
                'unpaid_amount as up_amt',
                'overtime_amount + doubletime_amount as overtime',
                '0 as bonus',
                'addition_amount as additions',
                'deduction_amount as deductions',
                'remarks',
                'tap',
                'scp',
                'scp_ee',
                'epf_ee',
                'scp_ee as scp2',
                'title as bank_mode'
            )
            ->where('payroll_master_id', '=', $payroll_id)
            ->get();

        /*         $res = DB::table('tbl_payroll')
                ->select(
                    'emp_name',
                    'basic_salary',
                    DB::RAW('(SELECT if(pay.ded_up_amt > 0, pay.ded_up_amt*-1, pay.ded_up_amt)
                        FROM tbl_payroll as pay
                        WHERE pay.id=tbl_payroll.id) as up_amt'),
                    DB::RAW('(SELECT sum(amount) FROM tbl_payroll_add_ded
                        WHERE payroll_id=tbl_payroll.id
                        and addition=true
                        and deleted_at is null
                        and (title like "%over%time%"
                        OR title like "%double%time%")) as overtime'),
                    DB::RAW('(SELECT sum(amount) FROM tbl_payroll_add_ded
                        WHERE payroll_id=tbl_payroll.id
                        and addition=true
                        and deleted_at is null
                        and title like "%bonus%"
                        ) as bonus'),
                    DB::RAW('(SELECT sum(amount) FROM tbl_payroll_add_ded
                        WHERE payroll_id=tbl_payroll.id
                        and addition=true
                        and deleted_at is null) as additions'),
                    DB::RAW('(SELECT sum(amount)*-1 FROM tbl_payroll_add_ded
                        WHERE payroll_id=tbl_payroll.id
                        and addition=false
                        and title not like "%unpaid%leave%"
                        and deleted_at is null) as deductions'),
                    'add_others_desc',
                    'tap',
                    'scp',
                    'scp_ee',
                    'epf_ee',
                    'scp_ee as scp2',
                    DB::RAW('(SELECT if(pay.payment_mode = "Bank", "Bank Transfer", pay.payment_mode)
                        FROM tbl_payroll as pay
                        WHERE pay.id=tbl_payroll.id) as bank_mode')
                )
                ->where('payroll_id', '=', $payroll_id)
                ->get();
         */
        
        $payrolltotal  = array();
        $payrolltotal[] = ["Name","Basic Salary","Unpaid Leave","Overtime", "Other", "Other Desc", "TAP", "SCP", "SCP", "EPF", "Allowance"];
        foreach ($res as $pr) {
            //$row = array( )
            array_push($payrolltotal, (array)$pr);
        }

        return $payrolltotal;
    }

    private function GetPeriod($payroll_id)
    {
        $period = DB::connection('tenant')->table('tbl_payroll_master')
            ->select(
                DB::RAW('DATE_FORMAT(created_at,"%M %Y") as period')
            )
            ->where('id', '=', $payroll_id)
            ->get();

        $paymonth = '';
        foreach ($period as $payperiod) {
            $paymonth = 'Month : '.$payperiod->period;
        }

        return $paymonth;
    }

    private function SetActivityHeaders($sheet)
    {
        $sheet->setColumnFormat(array(
            'C' => '#,##0.00',
            'D' => '#,##0.00',
            'E' => '#,##0.00',
            'F' => '#,##0.00',
            'G' => '#,##0.00',
            'H' => '#,##0.00',
            'I' => '#,##0.00',
            'J' => '#,##0.00',
            'K' => '#,##0.00',
            'L' => '#,##0.00',
            'N' => '#,##0.00',
            'M' => '#,##0.00',
            'P' => '#,##0.00',
            'R' => '#,##0.00'
        ));

        $sheet->setCellValue('A6', 'No');
        $sheet->getStyle('A6')->getFont()->setBold(true);

        $sheet->setCellValue('B6', 'Name');
        $sheet->getStyle('B6')->getFont()->setBold(true);

        $sheet->setCellValue('C5', 'Basic');
        $sheet->setCellValue('C6', 'Salary');
        $sheet->getStyle('C5:C6')->getFont()->setBold(true);
        $sheet->setCellValue('C7', 'B$');

        $sheet->setCellValue('D5', 'Old');
        $sheet->setCellValue('D6', 'Salary');
        $sheet->getStyle('D5:D6')->getFont()->setBold(true);
        $sheet->setCellValue('D7', 'B$');

        $sheet->setCellValue('E5', 'Salary');
        $sheet->setCellValue('E6', 'Diff.');
        $sheet->getStyle('E5:E6')->getFont()->setBold(true);
        $sheet->setCellValue('E7', 'B$');

        $sheet->setCellValue('F5', 'Overtime');
        $sheet->setCellValue('F6', 'Amt');
        $sheet->getStyle('F5:F6')->getFont()->setBold(true);
        $sheet->setCellValue('F7', 'B$');

        $sheet->setCellValue('G5', 'Doubletime');
        $sheet->setCellValue('G6', 'Amt');
        $sheet->getStyle('G5:G6')->getFont()->setBold(true);
        $sheet->setCellValue('G7', 'B$');

        $sheet->setCellValue('H5', 'Transport');
        $sheet->getStyle('H5')->getFont()->setBold(true);
        $sheet->setCellValue('H7', 'B$');

        $sheet->setCellValue('I5', 'Housing');
        $sheet->getStyle('I5')->getFont()->setBold(true);
        $sheet->setCellValue('I7', 'B$');

        $sheet->setCellValue('J5', 'Meal');
        $sheet->getStyle('J5')->getFont()->setBold(true);
        $sheet->setCellValue('J7', 'B$');

        $sheet->setCellValue('K5', 'Other');
        $sheet->getStyle('K5')->getFont()->setBold(true);
        $sheet->setCellValue('K7', 'B$');

        $sheet->setCellValue('L5', 'Additions');
        $sheet->getStyle('L5')->getFont()->setBold(true);
        $sheet->setCellValue('L7', 'B$');

        $sheet->setCellValue('M5', 'Unpaid');
        $sheet->setCellValue('M6', 'Leave');
        $sheet->getStyle('M5:M6')->getFont()->setBold(true);
        $sheet->setCellValue('M7', 'B$');

        $sheet->setCellValue('N5', 'Personal');
        $sheet->setCellValue('N6', 'Amt');
        $sheet->getStyle('N5:N6')->getFont()->setBold(true);
        $sheet->setCellValue('N7', 'B$');

        $sheet->setCellValue('O5', 'Personal');
        $sheet->setCellValue('O6', 'Desc.');
        $sheet->getStyle('O5:O6')->getFont()->setBold(true);
        $sheet->setCellValue('O6', '');

        $sheet->setCellValue('P5', 'Other');
        $sheet->setCellValue('P6', 'Amt');
        $sheet->getStyle('P5:P6')->getFont()->setBold(true);
        $sheet->setCellValue('P7', 'B$');

        $sheet->setCellValue('Q5', 'Other');
        $sheet->setCellValue('Q6', 'Desc.');
        $sheet->getStyle('Q5:Q6')->getFont()->setBold(true);

        $sheet->setCellValue('R5', 'Deductions');
        $sheet->getStyle('R5')->getFont()->setBold(true);
        $sheet->setCellValue('R7', 'B$');

        $this->SheetBorderThin($sheet, 'A5:R7', true);
        $this->SheetBorderThin($sheet, 'C5:R6', false);
    }

    private function SetPayrollHeaders($sheet)
    {
        $sheet->setColumnFormat(array(
            'C' => '#,##0.00',
            'D' => '#,##0.00',
            'E' => '#,##0.00',
            'F' => '#,##0.00',
            'G' => '#,##0.00',
            'H' => '#,##0.00',
            'I' => '#,##0.00',
            'J' => '#,##0.00',
            'K' => '#,##0.00',
            'L' => '#,##0.00',
            'M' => '#,##0.00',
            'N' => '#,##0.00'
        ));

        $sheet->setCellValue('A6', 'No');
        $sheet->getStyle('A6')->getFont()->setBold(true);

        $sheet->setCellValue('B6', 'Name');
        $sheet->getStyle('B6')->getFont()->setBold(true);

        $sheet->setCellValue('C5', 'Basic');
        $sheet->setCellValue('C6', 'Salary');
        $sheet->getStyle('C5:C6')->getFont()->setBold(true);
        $sheet->setCellValue('C7', 'B$');

        $sheet->setCellValue('D5', 'Unpaid');
        $sheet->setCellValue('D6', 'Leave');
        $sheet->getStyle('D5:D6')->getFont()->setBold(true);
        $sheet->setCellValue('D7', 'B$');

        $sheet->setCellValue('E5', 'Overtime');
        $sheet->getStyle('E5')->getFont()->setBold(true);
        $sheet->setCellValue('E7', 'B$');

        $sheet->setCellValue('F5', 'Bonus');
        $sheet->getStyle('F5')->getFont()->setBold(true);
        $sheet->setCellValue('F7', 'B$');

        $sheet->setCellValue('G5', 'Allowance');
        $sheet->getStyle('G5:G6')->getFont()->setBold(true);
        $sheet->setCellValue('G6', '');
        $sheet->setCellValue('G7', 'B$');

        $sheet->setCellValue('H5', 'Deduction');
        $sheet->getStyle('H5:H6')->getFont()->setBold(true);
        $sheet->setCellValue('H6', '');
        $sheet->setCellValue('H7', 'B$');

        $sheet->setCellValue('I5', 'Gross');
        $sheet->setCellValue('I6', 'Salary');
        $sheet->getStyle('I5:I6')->getFont()->setBold(true);
        $sheet->setCellValue('I7', 'B$');

        $sheet->setCellValue('J5', 'TAP');
        $sheet->setCellValue('J6', 'ER / EE');
        $sheet->getStyle('J5:J6')->getFont()->setBold(true);
        $sheet->setCellValue('J7', 'B$');

        $sheet->setCellValue('K5', 'SCP');
        $sheet->setCellValue('K6', 'Emp.r');
        $sheet->getStyle('K5:K6')->getFont()->setBold(true);
        $sheet->setCellValue('K7', 'B$');

        $sheet->setCellValue('L5', 'SCP');
        $sheet->setCellValue('L6', 'Emp.e');
        $sheet->getStyle('L5:L6')->getFont()->setBold(true);
        $sheet->setCellValue('L7', 'B$');

        $sheet->setCellValue('M5', 'EPF');
        $sheet->setCellValue('M6', 'ER / EE');
        $sheet->getStyle('M5:M6')->getFont()->setBold(true);
        $sheet->setCellValue('M7', 'B$');

        $sheet->setCellValue('N5', 'Net');
        $sheet->setCellValue('N6', 'Salary');
        $sheet->getStyle('N5:N6')->getFont()->setBold(true);
        $sheet->setCellValue('N7', 'B$');

        $sheet->setCellValue('O5', 'Payment');
        $sheet->setCellValue('O6', 'Mode');
        $sheet->getStyle('O5:O6')->getFont()->setBold(true);

        // $sheet->setCellValue('M5', 'Mode');
        // $sheet->getStyle('M5')->getFont()->setBold(true);

        $this->SheetBorderThin($sheet, 'A5:O7', true);
        $this->SheetBorderThin($sheet, 'C5:O6', false);
    }

    private function SheetBorderThin($sheet, $cells, $center)
    {
        $sheet->cell($cells, function ($cell) {
            $cell->setBorder('thin', 'thin', 'thin', 'thin');
        });

        if ($center) {
            $sheet->cell($cells, function ($cell) {
                $cell->setAlignment('center');
            });
        }
    }

    private function SheetBorderThick($sheet, $cells, $center)
    {
        $sheet->cell($cells, function ($cell) {
            $cell->setBorder('thick', 'thick', 'thick', 'thick');
        });

        if ($center) {
            $sheet->cell($cells, function ($cell) {
                $cell->setAlignment('center');
            });
        }
    }

    private function SheetBorderDotted($sheet, $cells, $center)
    {
        $sheet->cell($cells, function ($cell) {
            $cell->setBorder('thin', 'dotted', 'thin', 'dotted');
        });

        if ($center) {
            $sheet->cell($cells, function ($cell) {
                $cell->setAlignment('center');
            });
        }
    }

    private function SheetBorderRow($sheet, $cells)
    {
        $sheet->cell($cells, function ($cell) {
            $cell->setBorder('dotted', 'none', 'dotted', 'none');
            //$cell->setAlignment('right');
        });
    }
}
