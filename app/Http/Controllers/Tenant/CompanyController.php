<?php

namespace App\Http\Controllers\Tenant;

use App\Company;
use App\Country;
use App\WorkingWeek;
use App\Tenant\Manager;
use App\PaymentSchedule;
use App\ReportingPeriod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//use App\Tenant\Traits\ForTenants;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_unless(app(Manager::class)->getUserAccess()->update_admin, 403);

        $company = Company::find(1);
//        dd($company);
        return view('settings.company')
            ->withCompany($company);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $this->authorize('update-admin');
        //dd($request);
        $attributes = $request->validate([
            'company_name' => 'required|max:100',
            'first_period_date' => 'required|date',
            'periods_per_year' => 'required|numeric',
            'address1' => 'max:100',
            'address2' => 'max:100',
            'city' => 'max:50',
/*             'area' => 'max:50', */
            'payment_period_id' => 'exists:tenant.payment_schedules,id',
            'address_country_id' => 'exists:tenant.countries,id',
            'registration_no' => 'max:20',
            'license_no' => 'max:20',
            'postcode' => 'max:20'
        ]);
        //dd($attributes);
        $company->update($attributes);

        return back();
    }

    public function accounts(Request $request)
    {
        $this->authorize('update-admin');

        $attributes = $request->validate([
            'tap_percent' => 'min:0|max:100',
            'tap_min' => 'min:0',
            'tap_max' => 'min:0',
            'scp_percent' => 'min:0|max:100',
            'scp_min' => 'min:0',
            'scp_max' => 'min:0',
            'epf_percent' => 'min:0|max:100'
/*             'epf_min' => 'min:0|max:100',
            'epf_max' => 'min:0|max:100'
 */        ]);

        $attributes['use_tap'] = $request['use_tap'] == 'on' ? "1" : "0";
        $attributes['use_scp'] = $request['use_scp'] == 'on' ? "1" : "0";
        $attributes['use_epf'] = $request['use_epf'] == 'on' ? "1" : "0";

        $company = Company::find(1);
        $company->update($attributes);

        return back();
    }

    public function workweek(Request $request, Company $company)
    {
        $this->authorize('update-admin');

        $attributes = $request->validate([
            'title' => 'required|max:100',
            'mon' => 'min:0|max:100',
            'tue' => 'min:0|max:100',
            'wed' => 'min:0|max:100',
            'thu' => 'min:0|max:100',
            'fri' => 'min:0|max:100',
            'sat' => 'min:0|max:100',
            'sun' => 'min:0|max:100'
        ]);
     
        $attributes['company_id'] = $company->id;
        $attributes['default'] = $request['default'] == 'on' ? "1" : "0";

        WorkingWeek::create($attributes);

        return back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->authorize('update-admin');
    }

    public function logo(Request $request)
    {
        $this->authorize('update-admin');

        $company = Company::find(1);

        if ($request->hasfile('logo')) {
            //dd(app(Manager::class)->getTenant());
            $location = app(Manager::class)->getTenant()->domain.'/logo';
            $attributes['logo'] = request('logo')->store($location);
            $company->update($attributes);

            /*             dd($attributes['logo']);
                        dd($location); */
        }
        
        return back();
    }

    public function UpdateDefault($id_field, $id_value)
    {
        $this->authorize('update-admin');

        $company = Company::find(1);
        $company[$id_field] = $id_value;
        $company->save();

        return;
    }
}
