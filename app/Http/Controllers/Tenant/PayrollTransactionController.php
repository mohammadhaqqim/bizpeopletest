<?php

namespace App\Http\Controllers\Tenant;

use App\Employee;
use Carbon\Carbon;
use App\Transaction;
use App\PayrollMaster;
use App\PayrollEmployee;
use App\PayrollTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PayrollTransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        // abort unless authorised user is payroll
        abort_unless($this->authorize('update-payroll'), 403);

        // find employee via name
        $employee = Employee::select('id')
            ->where('name', $request->employee_id)
            ->get()
            ->first();
 
        //respond with error message
        if (is_null($employee)) {
            return back();
        }

        // find the PayrollEmployee (for id)
        $payrollEmployee = PayrollEmployee::select('id', 'approved')
            ->where('payroll_master_id', $id)
            ->where('employee_id', $employee->id)
            ->whereNull('deleted_at')
            ->get()->first();

        if ($payrollEmployee->approved) {
            return back()->with('warning', 'The employee has been approved. Unable to make any changes.');
        }

        $transaction_id = 0;
        $addition = 0;

        // if addition
        if ($request->add_ded_id == "1") {
            $transaction_id = $request->additions;
            $addition = 1;
        } else {
            $transaction_id = $request->deductions;
        }
        
        //get transaction (base)
        $transaction = Transaction::findOrFail($transaction_id);

        if ($transaction->calculated) {
            $request['hours'] = is_null($request['calHours']) ? 0 : $request['calHours'];
            $request['rate'] = is_null($request['calRate']) ? 0 : $request['calRate'];
        } else {
            $request['hours'] = 0;
            $request['rate'] = 0;
        }

        PayrollTransaction::create([
            'payroll_employees_id' => $payrollEmployee->id,
            'transaction_id' => $transaction->id,
            'title' => $transaction->title,
            'amount' => $request->amount,
            'addition' => $addition,
            'hours' => $request['hours'],
            'calculated' => $transaction->calculated,
            'fixed' => 0,
            'rate' => $request['rate']
        ]);

        // update summary informartion
        $payrollEmployee = PayrollEmployee::findOrFail($payrollEmployee->id);
        $payrollEmployee->performCalculations();

        return back();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PayrollTransaction  $payrollTransaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // abort unless authorised user is payroll
        abort_unless($this->authorize('update-payroll'), 403);

        //
        $payrollTransaction = PayrollTransaction::findOrFail($id);

        $attributes = request()->validate([
            'amount' => 'required'
        ]);

        $payrollTransaction->update(
            $attributes
        );

        $payrollEmployee = PayrollEmployee::findOrFail($payrollTransaction->payroll_employees_id);
        $payrollEmployee->performCalculations();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PayrollTransaction  $payrollTransaction
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // abort unless authorised user is payroll
        abort_unless($this->authorize('update-payroll'), 403);
        
        $payrollTransaction = PayrollTransaction::findOrFail($id);

        $payrollTransaction->deleted_at = Carbon::now();
        $payrollTransaction->save();

        $payrollEmployee = PayrollEmployee::findOrFail($payrollTransaction->payroll_employees_id);
        $payrollEmployee->performCalculations();

        return back();
    }
}
