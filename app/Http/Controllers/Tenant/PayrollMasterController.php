<?php

namespace App\Http\Controllers\Tenant;

use App\Company;
use App\Employee;
use App\Allowance;
use App\PaymentMode;
use App\PayrollMaster;
use App\PayrollEmployee;
use App\EmployeeAllowance;
use App\PayrollTransaction;
use App\WorkingWeek;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PayrollMasterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        abort_unless($this->authorize('update-payroll'), 403);

        $payrollMasters = PayrollMaster::select(
            'id',
            'title',
            'run_payroll',
            'remarks',
            'start_date',
            'end_date',
            'working_days',
            'due_date',
            'pay_date',
            'approved_by'
        )
            ->whereNull('approved_by')
            ->where('company_id', 1)
            ->get();

        return view('payroll.index')
            ->withPayrollMasters($payrollMasters);
    }

    public function details()
    {
        abort_unless($this->authorize('update-payroll'), 403);

        return view('payroll.details.edit');
    }

    public function transactions()
    {
        abort_unless($this->authorize('update-payroll'), 403);

        return view('payroll.transactions.edit');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_unless($this->authorize('update-payroll'), 403);
        $company = Company::find(1);

        //dd($company);

        $last_period = PayrollMaster::where('company_id', '=', $company->id)
            ->orderByDesc('start_date')
            ->first();
            
        $first_date = \Carbon\Carbon::createFromFormat('Y-m-d', $company->first_period_date);

        if ($last_period == null) {
            $startDate = \Carbon\Carbon::createFromFormat('Y-m-d', $company->first_period_date);
        } else {
            $startDate = \Carbon\Carbon::createFromFormat('Y-m-d', $last_period->end_date);
            $startDate = $startDate->addDays(1);
        }
        //dd('payroll create');
        
        // periods per year
        $paymentType = $company->PaymentType;
        //dd($paymentType);

        $endDate = \Carbon\Carbon::parse($startDate);
        
        $title = 'New Period';
        
        // end date
        switch ($paymentType->periods) {
            case 12:
                $endDate = $endDate->addMonth(1);
                $endDate = $endDate->addDays(-1);
                $title = $endDate->format('Y F');
                break;

            case 26:
                $endDate = $endDate->addDays(13);

                $title = $endDate->format('Y') + ' Period ';

                $endDate = $endDate->addMonth(1);
                $endDate = $endDate->addDays(-1);

                break;
    
            case 52:
                $endDate = $endDate->addDays(6);

                $title = $endDate->format('Y') + ' Period ';
                break;
        }
        //dd('swith');
        $workingDays = $this->getWorkingDays($startDate, $endDate);

        return view('payroll.create')
            ->withEndDate($endDate)
            ->withStartDate($startDate)
            ->withWorkingDays($workingDays)
            ->withTitle($title);
    }

    private function getFirstDate($company)
    {
        $last_period = PayrollMaster::all()
            ->whereNull('deleted_at')
            ->orderByDesc('startDate')
            ->first();

        if ($last_period == null) {
            $startDate = \Carbon\Carbon::createFromFormat('Y-m-d', $company->first_period_date);
        } else {
            $startDate = \Carbon\Carbon::createFromFormat('Y-m-d', $last_period->endDate);
            $startDate = $startDate->addDays(1);
        }
    }

    public function getWorkingDays($start, $end, $employee_id = null)
    {
        $emp = null;
        if (!$employee_id) {
            $company = Company::find(1);
            $workingweek = WorkingWeek::find($company->working_week_id);
        } else {
            $emp = Employee::find($employee_id);
            $workingweek = $emp->WorkingWeek;
        }

        //if start date
        if ($emp != null) {
            if ($start < $emp->getCurrentStatus()->effective_date) {
                $start = $emp->getCurrentStatus()->effective_date;
            }

            // needs fixing
            if ($emp->EndService->count() > 0) {
                if ($emp->EndService[0]->end_service_date < $end) {
                    $end = $emp->EndService[0]->end_service_date;
                }
            }
        }

        $daysoff = $workingweek->getDaysOff();
        $off = 0;
        for ($i = \Carbon\Carbon::parse($start); $i <= $end; $i->modify('+1 day')) {
            if ($daysoff->contains($i->format('w'))) {
                $off++;
            }
        }

        //dd(date(strtotime($start)));
        $st = \Carbon\Carbon::parse($start);
        $end = \Carbon\Carbon::parse($end);

        //$start = strtotime($start);

        //days in the interval
        // $interval = $start->diff($end)->days + 1; //old
        $interval = $end->diffInDays($st) + 1;
        //minus the days not worked
        $interval -= $off;

        return $interval;
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort_unless($this->authorize('update-payroll'), 403);
        
        $request['created_by'] = auth()->user()->id;//Auth::id();
        $request['company_id'] = auth()->user()->id;//Auth::id();

        $attributes = $request->validate([
            'title' => 'required|max:100',
            'created_by' => 'required|exists:users,id',
            'company_id' => 'required|exists:tenant.companies,id',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'due_date' => 'required|date',
            'pay_date' => 'required|date',
            'working_days' => 'required|numeric',
            'remarks' => 'nullable'
        ]);
        //dd($attributes);

        $payroll_master = PayrollMaster::create($attributes);

        return redirect('/payroll')->with('success', 'New payroll period generated: '.$payroll_master->title);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PayrollMaster  $payrollMaster
     * @return \Illuminate\Http\Response
     */
    public function show(PayrollMaster $payrollMaster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PayrollMaster  $payrollMaster
     * @return \Illuminate\Http\Response
     */
    public function edit(PayrollMaster $payrollMaster)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PayrollMaster  $payrollMaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        abort_unless($this->authorize('update-payroll'), 403);

        $payroll = PayrollMaster::find($id);

        $attributes = $request->validate([
            'title' => 'required|max:100',
            'start_date' => 'date|required',
            'end_date' => 'date|required',
            'due_date' => 'date|required',
            'pay_date' => 'date|required',
            'working_days' => 'numeric',
            'remarks' => ''
        ]);

        $payroll->update($attributes);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PayrollMaster  $payrollMaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(PayrollMaster $payrollMaster)
    {
        //
    }

    /**
     * Run the payroll for the payroll id
     */
    public function runPayroll($id)
    {
        abort_unless($this->authorize('update-payroll'), 403);
        
        $payroll = PayrollMaster::find($id);

        //load employees
        $employees = app('App\Http\Controllers\Tenant\EmployeeController')->getEmployeesForPayperiod($payroll);
        //create employees for payroll-details (PayrollEmployee)
        foreach ($employees as $employee) {
            //dd($employee);
            app(PayrollEmployeeController::class)->addEmployeePayroll($employee, $payroll);
        }

        $payroll->run_payroll = true;
        $payroll->save();

        return back();
    }

    public function addEmployeeToPayroll(Request $request, $id)
    {
        abort_unless($this->authorize('update-payroll'), 403);

        // find payroll master
        $payroll = PayrollMaster::findOrFail($id);

        // find employee
        $employee = Employee::select('id', 'name')
            ->where('id', $request->employee_id)
            ->first();

        // check if employee exists already in payroll - check
        // throw into controller
        $employeePayrolls = PayrollEmployee::where('payroll_master_id', $id)
            ->where('employee_id', $employee->id)
            ->whereNull('deleted_at')
            ->first();

        // delete employee to then reinstall employee
        if (!is_null($employeePayrolls)) {
            app('App\Http\Controllers\PayrollEmployeeController')->destroy($employeePayrolls->id);

            $employeePayrolls = PayrollEmployee::where('payroll_master_id', $id)
                ->where('employee_id', $employee->id)
                ->whereNull('deleted_at')
                ->first();
        }

        app(PayrollEmployeeController::class)->addEmployeePayroll($employee->id, $payroll);
//        $this->addEmployeePayroll($employee, $payroll);

        return back()->with('success', 'The employee has been added');
    }


    private function add_Employee_Payroll($employee, $payroll)
    {
        // generate employee payroll entry
        $emp_payroll = $this->CreateEmployeePayroll($employee->id, $payroll);

        // generate allowances
        $this->CreateEmployeeTransactions($employee->id, $emp_payroll->id, $payroll);

        $emp_payroll->performCalculations();
    }

    private function GetCurrentEmployees($end_date)
    {
        $employees = Employee::select('id', 'name', 'date_joined')
            ->where(function ($q) use ($end_date) {
                $q->whereNull('end_date')
                ->orWhere('end_date', '<', $end_date);
            })->where('date_joined', '<=', $end_date)->get();
        return $employees;
    }

    private function CreateEmployeeTransactions($employee_id, $payroll_master_id, $payroll)
    {
        // get all the current employee allowances
        $allowances = EmployeeAllowance::where('employee_id', $employee_id)
            ->whereNull('deleted_at')
            ->where('effective_date', '<=', $payroll->end_date)
            ->get();
        dd($allowances);

        if ($allowances->count() == 0) {
            return;
        }
        
        foreach ($allowances as $transaction) {
            $payroll_transaction = new PayrollTransaction();
            $payroll_transaction->title = $transaction->title;
            $payroll_transaction->amount = $transaction->amount;
            //??
            $payroll_transaction->payroll_employees_id = $payroll_master_id;
            $payroll_transaction->addition = $transaction->Allowance->addition;
            $payroll_transaction->transaction_id = $transaction->allowance_id;
            
            $payroll_transaction->fixed = $transaction->fixed == null ? 0 : $transaction->fixed;
            
            $payroll_transaction->save();
        }
    }

    private function CreateEmployeePayroll($employee_id, $payroll)
    {
        $exists = PayrollEmployee::where('payroll_master_id', $payroll->id)
            ->where('employee_id', $employee_id)
            ->whereNull('deleted_at')
            ->get();

        //if employee payroll exists then exit
        if ($exists->count() > 0) {
            return $exists[0];
        }

        $emp_payroll = new PayrollEmployee();
        $emp_payroll->payroll_master_id = $payroll->id;
        
        $emp_payroll->loadEmployee($employee_id);
        
        //?? not sure needed as in master payroll
        $emp_payroll->company_id = 1;
        // current user
        $emp_payroll->prepared_by = auth()->user()->id;
        //calculate taxes
        $emp_payroll->calculateTaxes();

        $emp_payroll->work_days = $this->getWorkingDays($payroll->start_date, $payroll->end_date, $employee_id);
        $emp_payroll->save();

        return $emp_payroll;
    }

    
    public function employees()
    {
        abort_unless($this->authorize('update-payroll'), 403);

        /*         //get all employees
                $employees = Employee::whereNull('deleted_at')
                    ->with(['currentjob','currentStatus'])
                    ->get();
                //dd($employees);
                $paymentModes = PaymentMode::whereNull('deleted_at')
                    ->get(); */

        //dd('employee index');

        return view('payroll.employee.index');
        //->withPaymentModes($paymentModes);
            //->withEmployees($employees);
    }

    public function historical()
    {
        abort_unless($this->authorize('update-payroll'), 403);
        
        $payrollMasters = PayrollMaster::select(
            'id',
            'title',
            'run_payroll',
            'remarks',
            'start_date',
            'end_date',
            'working_days',
            'due_date',
            'pay_date',
            'approved_by'
        )->whereNotNull('approved_by')
            ->get();

        return view('payroll.index')
            ->withPayrollMasters($payrollMasters);
    }
}
