<?php

namespace App\Http\Controllers\Tenant;

use App\Company;
use App\Religion;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReligionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('update-admin');

        //
        //$request['company_id'] = Auth()->user()->company_id;

        $attributes = $request->validate([
            'title' => 'required|max:100|unique:tenant.religions,title',
        //    'company_id' => 'required|exists:countries,id'
        ]);

        $religion = Religion::create($attributes);

        //dd($religion);
        $request->session()->put('religion_id', $religion->id);

        return back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Religion  $religion
     * @return \Illuminate\Http\Response
     */
    public function edit(Religion $religion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Religion  $religion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Religion $religion)
    {
        $this->authorize('update-admin');

        //
        if ($request->has('default')) {
            $company = Company::find(1);
            $company->religion_id = $religion->id;
            $company->save();
        }

        $religion->title = $request->title;
        $religion->save();

        return back()->with('success', 'The religion was edited successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Religion  $religion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Religion $religion)
    {
        $this->authorize('update-admin');

        $religion->deleted_at = \Carbon\Carbon::now();
        $religion->save();

        return redirect('settings/customise/religions')
            ->with('success', 'The religion was deleted');
    }

    public function checkReligion($title)
    {
        $religion = Religion::where('title', '=', $title)
            ->first();

        if (is_null($religion)) {
            // does not exist - generate
            $religion = Religion::create([
                'title' => $title
            ]);
        }

        return $religion->id;
    }
}
