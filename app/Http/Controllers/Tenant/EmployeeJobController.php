<?php

namespace App\Http\Controllers\Tenant;

use App\c;
use App\Bank;
use App\Company;
use App\Country;
use App\Employee;
use Carbon\Carbon;
use App\Department;
use App\Designation;
use App\EmployeeJob;
use App\PaymentMode;
use App\PaymentType;
use App\WorkingWeek;
use App\EmployeeStatus;
use App\EmployeePayment;
use App\PaymentSchedule;
use App\EmploymentStatus;
use App\EndEmploymentReason;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class EmployeeJobController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Employee $employee)
    {
        abort_unless($this->authorize('update-hr'), 403);

        EmployeeJob::create([
            'employee_id' => $employee->id,
            'effective_date' => request('effective_date'),
            'department_id' => request('department_id'),
            'designation_id' => request('designation_id'),
            'report_to_id' => request('report_to_id'),
        ]);

        return back()->with('success', 'New job information: '.$employee->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
//        //abort_unless(auth()->user()->hr || auth()->user()->employee_id, 403);
        abort_unless($this->authorize('update-hr'), 403);

        $employeeStatus = EmployeeStatus::select('id', 'effective_date', 'employment_status_id', 'status_comment')
            ->where('employee_id', '=', $id)
            ->whereNull('deleted_at')
            ->get()
            ->sortByDesc('effective_date');
        //dd($employeeStatus);

        $employeeJobs = EmployeeJob::select('id', 'effective_date', 'department_id', 'division_id', 'designation_id', 'report_to_id')
            ->where('employee_id', '=', $id)
            ->whereNull('deleted_at')
            ->get()
            ->sortByDesc('effective_date');
        
        $employeePayments = EmployeePayment::select('id', 'effective_date', 'basic_salary', 'overtime', 'currency_id', 'payment_mode_id', 'payment_type_id', 'comment')
            ->where('employee_id', '=', $id)
            ->whereNull('deleted_at')
            ->get()
            ->sortByDesc('effective_date');

        return view('job.edit')
            ->withEmployee($employee)
            ->withEmployeeStatus($employeeStatus)
            ->withEmployeeJobs($employeeJobs)
            ->withEmployeePayments($employeePayments);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\c  $c
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeJob $employeeJob)
    {
        abort_unless($this->authorize('update-hr') || $this->authorize('update-payroll'), 403);
        
        $employeeJob->update(request([
            'effective_date',
//            'employee_id',
            'department_id',
//            'division_id',
            'designation_id',
            'report_to_id'
        ]));

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\c  $c
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
    }

    // not sure this is used???
    public function job_information(Request $request, Employee $employee)
    {
        $attributes = $request->validate([
            'date_joined' => 'required|date',
            'work_permit_expiry' => 'date',
            'labour_license_expiry' => 'date',
            'email_payslip_to' => 'email',
            'payment_mode_id' => 'exists:payment_modes,id',
            'bank_id' => 'exists:banks,id',
            'working_week_id' => 'required|exists:working_week,id',
            'bank_account' => 'max:50',
            'tap_account' => 'max:50',
            'scp_account' => 'max:50',
            'epf_account' => 'max:50',
        ]);

        //dd($attributes);

        $attributes['email_payslip'] = $request['email_payslip'] == 'on' ? "1" : "0";
        $attributes['tap_member'] = $request['tap_member'] == 'on' ? "1" : "0";
        $attributes['scp_member'] = $request['scp_member'] == 'on' ? "1" : "0";
        $attributes['epf_member'] = $request['epf_member'] == 'on' ? "1" : "0";
        
        //dd($attributes);

        $employee->update($attributes);

        return back();
    }
}
