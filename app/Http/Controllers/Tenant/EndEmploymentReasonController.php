<?php

namespace App\Http\Controllers\Tenant;

use App\Employee;
use App\EndEmploymentReason;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EndEmploymentReasonController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {
        //$request['company_id'] = Auth()->user()->company_id;

        $attributes = $request->validate([
            'title' => 'required|max:100|unique:tenant.end_employment_reasons,title'
        ]);

        EndEmploymentReason::create($attributes);

        return back()->with('success', 'A new reason was generated');
    }

    public function update(Request $request, EndEmploymentReason $endEmploymentReason)
    {
        //
        $endEmploymentReason->title = $request->title;
        $endEmploymentReason->save();

        return back()->with('success', 'The reason was edited successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\race  $race
     * @return \Illuminate\Http\Response
     */
    public function destroy(EndEmploymentReason $endEmploymentReason)
    {
        //
        $emp = Employee::where('employee_end_service_id', $endEmploymentReason->id);
        $emp->employee_end_service_id = null;
        $emp->end_date = null;
    }
}
