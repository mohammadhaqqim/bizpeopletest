<?php

namespace App\Http\Controllers\Tenant;

use App\Country;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $attributes = $request->validate([
            'country' => 'required|max:100|unique:countries,country',
            'nationality' => 'required|max:100',

            'currency_code' => 'max:3',
            'currency_symbol' => 'max:3'
        ]);

        $country = Country::create($attributes);

        $request->session()->put('nationality_id', $country->id);
        $request->session()->put('country_id', $country->id);
        
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function cacluate_value()
    {
    }
    
    public function checkNationality($nationality)
    {
        $country = Country::where('nationality', '=', $nationality)
            ->orWhere('country', '=', $nationality)
            ->first();

        if (is_null($country)) {
            // does not exist - generate
            $country = Country::create([
                'country' => '',
                'nationality' => '',
                'use_currency' => '',
                'curreny_code' => '',
                'currency_symbol' => ''
            ]);
        }

        return $country;
    }
}
