<?php

namespace App\Http\Controllers\Tenant;

use PDF;
use App\Employee;
//use Barryvdh\DomPDF\PDF;
use Carbon\Carbon;
use App\PayrollMaster;
use App\PayrollEmployee;
use App\EmployeeAllowance;
use App\PayrollTransaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PayrollEmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $payroll_id)
    {
        //dd('update - payroll');

        abort_unless($this->authorize('update-payroll'), 403);

        // find payroll master
        $payroll = PayrollMaster::findOrFail($payroll_id);
        
        // find employee
        $employee = Employee::select('id', 'name')
            ->where('id', $request->employee_id)
            ->first();

        // check if employee exists already in payroll - check
        $employeePayrolls = PayrollEmployee::where('payroll_master_id', $payroll_id)
            ->where('employee_id', $employee->id)
            ->whereNull('deleted_at')
            ->first();
            
        if (!is_null($employeePayrolls)) {
            if ($employeePayrolls->approved) {
                return back()->with('warning', 'The employee has been approved. Unable to make change.');
            }
        }

        // delete employee to then reinstall employee
        if (!is_null($employeePayrolls)) {
            $this->destroy($employeePayrolls->id);
            //dd('test deleted');

            $employeePayrolls = PayrollEmployee::where('payroll_master_id', $payroll_id)
                ->where('employee_id', $employee->id)
                ->whereNull('deleted_at')
                ->first();
            //dd('test after delete');
        }
        
        $this->addEmployeePayroll($employee, $payroll);
        
        return back()->with('success', 'The employee has been amended');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        abort_unless($this->authorize('update-payroll'), 403);
        //
        $payrollEmployee = PayrollEmployee::find($id);

        $attributes = request()->validate([
            'payment_mode_id' => 'required|exists:tenant.payment_modes,id',
            'base_salary' => 'numeric',
            'bank_id' => 'nullable|exists:tenant.banks,id',
            'bank_account' => 'nullable|max:50',
            'work_days' => 'nullable|numeric'
        ]);
       
        $payrollEmployee->update($attributes);
        $payrollEmployee->performCalculations();

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort_unless($this->authorize('update-payroll'), 403);
        
        $payrollEmployee = PayrollEmployee::findOrFail($id);
        
        $payrollTransactions = PayrollTransaction::where('payroll_employees_id', $id)->get();

        foreach ($payrollTransactions as $trans) {
            $trans->deleted_at = Carbon::now();
            $trans->save();
        }

        $payrollEmployee->deleted_at = Carbon::now();
        $payrollEmployee->save();

        return back();
    }


    public function addEmployeePayroll(Employee $employee, PayrollMaster $payroll)
    {
        abort_unless($this->authorize('update-payroll'), 403);

        // generate employee payroll entry
        $emp_payroll = $this->CreateEmployeePayroll($employee->id, $payroll);
        
        // generate allowances
        $this->CreateEmployeeTransactions($employee, $emp_payroll->id, $payroll);
        
        $emp_payroll->performCalculations();
        //dd('test');
    }

    private function CreateEmployeePayroll($employee_id, $payroll)
    {
        //if employee payroll exists then exit
        $exists = $this->getPayrollEmployee($payroll, $employee_id);
        if ($exists->count() > 0) {
            return $exists[0];
        }
   
        // else generate new payroll employee
        $emp_payroll = new PayrollEmployee();
        $emp_payroll->payroll_master_id = $payroll->id;
        $emp_payroll->loadEmployee($employee_id);
        $emp_payroll->prepared_by = auth()->user()->id;
        //calculate taxes
        $emp_payroll->calculateTaxes();
//        dd('dates - '.$payroll->start_date.' to '.$payroll->end_date);
        //date('Y-m-d H:i:s', $date)
        $emp_payroll->work_days =
            app(PayrollMasterController::class)
            ->getWorkingDays(
                \Carbon\Carbon::createFromFormat('Y-m-d', $payroll->start_date),
                \Carbon\Carbon::createFromFormat('Y-m-d', $payroll->end_date),
                $employee_id
            );
        
        unset($emp_payroll->payment_type_id);
        $emp_payroll->save();
        //dd($emp_payroll);

        return $emp_payroll;
    }


    private function CreateEmployeeTransactions($employee, $employee_payroll_id, $payroll)
    {
        // get all the current employee allowances
        $allowances = EmployeeAllowance::where('employee_id', $employee->id)
            ->whereNull('deleted_at')
            ->where('effective_date', '<=', $payroll->end_date)
            ->get();

        if ($allowances->count() == 0) {
            return;
        }
        
        // no calculation for part if calculated ?

        // for each allowance create new entry
        foreach ($allowances as $transaction) {
            $payroll_transaction = new PayrollTransaction();
            $payroll_transaction->title = $transaction->title;
            $payroll_transaction->amount = $transaction->amount;
            $payroll_transaction->payroll_employees_id = $employee_payroll_id;
            $payroll_transaction->addition = $transaction->Allowance->addition;
            $payroll_transaction->transaction_id = $transaction->allowance_id;
            $payroll_transaction->fixed = $transaction->Allowance->fixed;
            
            if ($transaction->effective_date > $payroll->start_date && $payroll_transaction->fixed == 0) {
                $payrollEmployee = PayrollEmployee::where('employee_id', $employee->id)
                    ->where('payroll_master_id', $payroll->id)
                    ->whereNull('deleted_at')
                    ->first();

                $days_worked =
                    app(PayrollMasterController::class)->getWorkingDays($transaction->effective_date, $payroll->end_date, $employee->id);

                $payroll_transaction->amount = $payroll_transaction->amount * $days_worked / $payrollEmployee->work_days;
            }

            if (!is_null($employee->end_contract_date) && $payroll_transaction->fixed == 0) {
                if ($employee->end_contract_date < $payroll->end_date) {
                    $payrollEmployee = PayrollEmployee::where('employee_id', $employee->id)
                        ->where('payroll_master_id', $payroll->id)
                        ->whereNull('deleted_at')
                        ->first();

                    $days_worked =
                    app(PayrollMasterController::class)->getWorkingDays($transaction->effective_date, $payroll->end_date, $employee->id);
                    $payroll_transaction->amount = $payroll_transaction->amount * $days_worked / $payrollEmployee->work_days;
                }
            }


            $payroll_transaction->save();
        }
    }

    public function getPayrollEmployees(PayrollMaster $payroll)
    {
        $employeePayrolls = PayrollEmployee::where('payroll_master_id', $payroll->id)
            ->whereNull('deleted_at')
            ->with('Employee')
            ->get();

        return $employeePayrolls;
    }

    public function getPayrollEmployee(PayrollMaster $payroll, $employee_id)
    {
        $employeePayroll = PayrollEmployee::where('payroll_master_id', $payroll->id)
            ->where('employee_id', $employee_id)
            ->whereNull('deleted_at')
            ->get();

        return $employeePayroll;
    }
}
