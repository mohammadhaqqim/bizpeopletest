<?php

namespace App\Http\Controllers\Tenant;

use App\Bank;
use App\Company;
use App\Race;
use App\User;
use App\Gender;
use App\Country;
use App\IcColor;
use App\PayType;
use App\Employee;
use App\Religion;
use Carbon\Carbon;
use App\Department;
use App\Designation;
use App\EmployeeJob;
use App\PaymentMode;
use App\PaymentType;
use App\WorkingWeek;
use App\MaritalStatus;
use App\PayrollMaster;
use App\EmployeeStatus;
use App\Tenant\Manager;
use App\EmployeePayment;
use App\PaymentSchedule;
use App\EmploymentStatus;
use App\EmployeeEndService;
use App\Imports\TestImport;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use App\Imports\EmployeeImport;
use App\Imports\DepartmentImport;
use App\Imports\DesignationImport;
use App\Imports\EmployeeJobImport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\EmployeeStatusImport;
use App\Imports\EmploymentStatusImport;
use Illuminate\Support\Facades\Password;
use App\Imports\EmployeeRenumerationImport;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class EmployeeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::select('id', 'name', 'work_email', 'work_phone', 'work_phone_ext', 'hand_phone')
            ->whereNull('deleted_at')
            ->WhereNull('end_contract_date')
            ->get();
        //dd($employees);
        return view('employee.index')
            ->withEmployees($employees);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('update-personal');

        return view('employee.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('update-personal');
        //dd($request);
        DB::transaction(function () use ($request) {

            //$employee_id = DB::connection('tenant')->table('employees')
            //->insertGetId($this->saveEmployee($request));

            $request['effective_date'] = $request['date_joined'];
            $employee = $this->saveEmployee($request);

            $employee->statuses()->create(
                $this->validateStatus($request)
            );
            //dd('valid status');

            $employee->payments()->create(
                $this->validateEmployeePayment($request)
            );
            //dd('valid payment');


            $employee->jobs()->create(
                $this->validateJob($request)
            );
            //dd('valid job');

            if ($request['self_service']) {
                $this->sendSelfServiceEmail($employee);
            };

            return redirect('/employee/'.$employee->id.'/edit');
        });

        return redirect('/people');

        /*         DB::beginTransaction();

                try {
                    //create employee
                    $employee_id = DB::connection('tenant')->table('employees')
                        ->insertGetId($this->saveEmployee($request));

                    DB::rollback();

                    dd($employee_id);
                    //$employee = $this->saveEmployee($request);
                    $request['effective_date'] = $request['date_joined'];
                    $request['employee_id'] = $employee_id;

                    $this->storeEmployeeStatus($request);

                                 // create employee payment
                                $employee->employment_status_id = $this->storeEmployeePayment($request)->id;
                                // creaet employee job
                                $employee->designation_id = $this->storeEmployeeJob($request)->id;
                                $employee->save();

                                if ($request['self_service']) {
                                    $this->sendSelfServiceEmail($employee);
                                }

                    DB::commit();
                } catch (ValidationException $e) {
                    DB::rollback();
                    return redirect('/employee/create')
                        ->withErrors($e->errors())
                        ->withInput();
                } catch (\Exception $e) {
                    DB::rollback();
                    throw $e;
                }

                return redirect('/employee/'.$employee->id.'/edit'); */
    }


    private function saveEmployee(Request $request)
    {
        $request['created_by'] = Auth::id();
        $request['entered_by'] = Auth::id();
        $request['company_id'] = 1;

        // test needs to be completed
        $email = $request->has('home_email') ? $request['home_email'] : '';
        $email = $request->has('work_email') ? $request['work_email'] : '';
        $request['email_payslip_to'] = $email;

        // ???
        if (request()->filled('avatar')) {
            $attributes['avatar'] = request('avatar')->store('avatars');
        }

        $request['driving_license'] = $request->has('driving_license') ? true : false;
        $request['self_service'] = $request->has('self_service') ? 1 : 0;
        $request['working_week_id'] = Company::find(1)->working_week_id;

        if ($request['bank_id'] < 1) {
            $request['bank_id'] = Company::find(1)->bank_id;
        }

        $attributes = $request->validate([
            //            'department_id' => 'required',
            //            'reports_to_id' => 'required',
            //            'location_id' => 'required',
            //            'employee_payment_id' => 'required',
            'race_id' => 'required',
            'religion_id' => 'required',
            'nationality_id' => 'required|exists:tenant.countries,id',
            'employment_status_id' => 'required|exists:tenant.employment_statuses,id',
            'bank_id' => 'required|exists:tenant.banks,id',
            'employee_code' => 'nullable|max:20',
            'name' => 'required|max:255',
            'preferred_name' => 'max:50',
            'date_of_birth' => 'required|date',
            'gender_id' => 'required|exists:tenant.genders,id',
            'ic_number' => 'required|max:20',
            'ic_expiry' => 'date|nullable',
            'ic_color_id' => 'required|exists:tenant.ic_colors,id',
            'marital_status_id' => 'required|integer',
            'address1' => 'max:100',
            'address2' => 'max:100',
            'city' => 'max:50',
            'area' => 'nullable|max:50',
            'postcode' => 'nullable|max:20',
            'address_country_id' => 'required|integer',
            'work_phone' => 'max:50',
            'work_phone_ext' => 'nullable|max:5',
            'home_phone' => 'max:50',
            'hand_phone' => 'max:50',
            'date_joined' => 'required|date',
            'work_email' => 'max:100',
            'home_email' => 'max:100',
            'self_service' => '',
            'avatar' => 'file',
            'bank_id' => 'nullable',
            'bank_account' => 'max:50',
            'email_payslip_to' => '',
            'passport_number' => 'max:30',
            'passport_expiry' => 'nullable|date',
            'passport_place_issued' => 'max:100',
            'payment_mode_id' => 'exists:tenant.payment_modes,id',
            'work_permit_expiry' => 'nullable|date',
            'working_week_id' => 'exists:tenant.working_weeks,id',
            'driving_license' => 'boolean',
            'created_by' => ''
        ]);

        return Employee::create($attributes);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        $this->authorize('update-manager', $employee);
        
        $e = Employee::find(42);

        // if there is a race_id, religion_id, nationality_id then set as
        if (request()->session()->has('race_id')) {
            $employee->race_id = session()->get('race_id');
        }

        if (request()->session()->has('religion_id')) {
            $employee->religion_id = session()->get('religion_id');
        }

        if (request()->session()->has('nationality_id')) {
            $employee->nationality_id = session()->get('nationality_id');
        }

        return view('employee.edit', compact('employee'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Employee $employee, Request $request)
    {
        $this->authorize('update', $employee);

        $self_service = $employee->self_service;

        $request['self_service']= request()->has('self_service') ? 1 : 0;

        $request['driving_license'] = request()->has('driving_license');

        if ($request['race_id'] == 0) {
            $request['race_id'] = null;
        }

        $attributes = request()->validate([
            'race_id' => 'nullable',
            'religion_id' => 'exists:tenant.religions,id',
            'nationality_id' => 'required|exists:tenant.countries,id',
            'employee_code' => 'nullable|unique:tenant.employees,employee_code,'.$employee->id,
            'name' => 'required|max:255',
            'preferred_name' => 'max:50',
            'date_of_birth' => 'required|date',
            'gender_id' => 'required|integer',
            'ic_number' => 'required|max:50',
            'ic_expiry' => 'nullable|date',
            'ic_color_id' => 'nullable|exists:tenant.ic_colors,id',
            'marital_status_id' => 'required|integer',
            'address1' => 'max:100',
            'address2' => 'max:100',
            'city' => 'max:50',
            'area' => 'nullable|max:50',
            'postcode' => 'nullable|max:20',
            'address_country_id' => 'required|integer',
            'work_phone' => 'max:50',
            'work_phone_ext' => 'nullable|max:5',
            'home_phone' => 'max:50',
            'hand_phone' => 'max:50',
            'work_email' => 'max:100',
            'home_email' => 'max:100',
            'self_service' => '',

            'place_birth' => 'max:100',
            'passport_number' => 'nullable|max:50',
            'passport_expiry' => 'nullable|date',
            'passport_place_issued' => 'nullable|max:100',
            'driving_license' => 'boolean',
            'working_week_id' => 'exists:tenant.working_weeks,id',
            //'created_by' => ''
            ]);
            
        $employee->update(
            $attributes
        );

        // remove any new values added from session state
        //request()->session()->forget('race_id');
        request()->session()->forget('religion_id');
        request()->session()->forget('nationality_id');
        request()->session()->forget('country_id');

        // if there is a change in the self service then send email
        if ($employee->self_service && $self_service == 0) {
            $this->sendSelfServiceEmail($employee);
        }

        return back()->with('success', 'Employee '.$employee->name.' updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $this->authorize('update', $employee);

        //
    }

    private function sendSelfServiceEmail(Employee $employee)
    {
        // check if user exists
        $user = User::where('employee_id', '=', $employee->id)
            ->get();

        if ($user->count() < 1) {
            $user = $this->newUser($employee);
        }

        // send user a password reset email
        $credentials = ['email' => $user->email];
        $response = Password::sendResetLink($credentials, function (Message $message) {
            $message->subject('Self Service Access');
        });
    }

    // generate the new user
    private function newUser(Employee $employee)
    {
        $user = new User;
        $user->name =
            $employee->employee_code ? $employee->employee_code : strtok($employee->work_email, '@');
        $user->email = $employee->work_email ? $employee->work_email : $employee->home_email;
        //$user->company_id = Auth()->user()->company_id;
        $psw = Str::random(10);
        $user->password = Hash::make($psw);
        $user->employee_id = $employee->id;

        $user->save();

        return $user;
    }

    private function validateJob($request)
    {
        $job_attributes = $request->validate([
            'effective_date' => 'required|date',
            'department_id' => 'required|exists:tenant.departments,id',
            'designation_id' => 'required|exists:tenant.designations,id',
        ]);

        return $job_attributes;
    }

    private function validateStatus($request)
    {
        $employmentstatus = $request->validate([
            'effective_date' => 'required|date',
            'employment_status_id' => 'required|exists:tenant.employment_statuses,id',
            'entered_by' => 'required'
        ]);
        return $employmentstatus;
    }

    private function validateEmployeePayment($request)
    {
        $renumeration = $request->validate([
            'effective_date' => 'required|date',
            'basic_salary' => 'required',
            'currency_id' => 'required',
            'payment_mode_id' => 'required|exists:tenant.payment_modes,id',
            'payment_type_id' => 'required|exists:tenant.payment_types,id',
            'entered_by' => '',
        ]);

        return $renumeration;
    }

    private function storeEmployeeStatus($request)
    {
        //employment_status_id ??     not employment_type_id
        // table required
        $employmentstatus = $request->validate([
            'effective_date' => 'required|date',
            'employment_status_id' => 'required|exists:tenant.employment_statuses,id',
            'entered_by' => 'required'
        ]);
        
        $status = EmployeeStatus::create($employmentstatus);

        return $status;
    }

    public function endService(Request $request, Employee $employee)
    {
        abort_unless($this->authorize('update-hr') || $this->authorize('update-payroll'), 403);
        
        $attributes = $request->validate([
            'end_reason_id' => 'required|exists:tenant.end_employment_reasons,id',
            'end_contract_date' => 'required|date',
            'end_employment_comment' => 'max:191',
        ]);

        $endService = EmployeeEndService::create($attributes);

        $employee->employee_end_service_id = $endService->id;
        $employee->end_date = $endService->end_service_date;
        $employee->save();
         
        return redirect('/people')->with('success', 'End of service was updated: '.$employee->name);
    }

    public function clearEndService(Request $request, Employee $employee)
    {
        abort_unless($this->authorize('update-hr') || $this->authorize('update-payroll'), 403);

        $employee->end_reason_id = null;
        $employee->end_employment_comment = null;
//        $employee->end_contract_date = null;
        $employee->update();

        return Redirect('/people/jobs/{{ $employee->id }}/edit');
    }

    private function storeEmployeeJob($request)
    {
        //working week ??
        // table required
        $job_attributes = $request->validate([
            'effective_date' => 'required|date',
            'employee_id' => 'required|exists:tenant.employees,id',
            'department_id' => 'required|exists:tenant.departments,id',
            'designation_id' => 'required|exists:tenant.designations,id',
        ]);

        $job = EmployeeJob::create($job_attributes);

        return $job;
    }

    private function storeEmployeePayment($request)
    {
        // table required
        $renumeration = $request->validate([
            'effective_date' => 'required|date',
            'employee_id' => 'required|exists:tenant.departments,id',
            'basic_salary' => 'required',
            'currency_id' => 'required',
            'payment_mode_id' => 'required|exists:tenant.payment_modes,id',
            'payment_type_id' => 'required|exists:tenant.payment_types,id',
            'entered_by' => '',
        ]);

        $payment = EmployeePayment::create($renumeration);

        return $payment;
    }

    public function importExportView()
    {
        return view('employee.import');
    }

    public function import()
    {
        Log::info('import employees');

        if (request()->file('file') != null) {
            Excel::import(new DesignationImport, request()->file('file'));
            Excel::import(new EmploymentStatusImport, request()->file('file'));
            Excel::import(new DepartmentImport, request()->file('file'));
            Excel::import(new EmployeeImport, request()->file('file'));
            Excel::import(new EmployeeStatusImport, request()->file('file'));
            Excel::import(new EmployeeJobImport, request()->file('file'));
            Excel::import(new EmployeeRenumerationImport, request()->file('file'));
        };

        return redirect('employee-import')->with('success', 'Import completed succesffuly');
    }

    public function CheckName($name)
    {
        $employee = Employee::select('id')
            ->where('name', '=', $name)
            ->whereNull('deleted_at')
            ->first();

        if (!is_null($employee)) {
            return $employee->id;
        };

        return -1;
    }

    public function CheckIC($ic)
    {
        $employee = Employee::select('id')
            ->where('ic_number', '=', $ic)
            ->whereNull('deleted_at')
            ->first();

        if (!is_null($employee)) {
            return $employee->id;
        };

        return -1;
    }

    public function checkEmployeeIDByIC($ic)
    {
        $employee = Employee::select('id')
            ->where('ic_number', '=', $ic)
            ->whereNull('deleted_at')
            ->first();

        if (!is_null($employee)) {
            return $employee->id;
        };

        return -1;
    }

    public function checkEmployeeIDByName($name)
    {
        $employee = Employee::select('id')
            ->where('name', '=', $name)
            ->whereNull('deleted_at')
            ->first();

        if (!is_null($employee)) {
            return $employee->id;
        };

        return -1;
    }

    public function getEmployeeID($row)
    {
        $id = -1;

        // name
        if (isset($row['name'])) {
            $id = $this->checkEmployeeIDByName($row['name']);

            if ($id > 0) {
                return $id;
            }
        }

        // ic
        if (isset($row['ic'])) {
            $id = $this->checkEmployeeIDByIC($row['ic']);

            if ($id > 0) {
                return $id;
            }
        }

        if (isset($row['ic_number']) && $id == -1) {
            $id = $this->checkEmployeeIDByIC($row['ic_number']);
        }


        return $id;
    }

    public function getEmployeesForPayperiod(PayrollMaster $payroll)
    {
        /*         $employees = DB::connection('tenant')->table('employees')
                    ->select('employees.id', 'name', 'employee_code', 'approved', 'payroll_master_id', 'payroll_employees.id as pe_id')
                    ->whereNull('employees.deleted_at')
                    ->where('date_joined', '<=', $payroll->end_date)
                    ->where(function ($query) use ($payroll) {
                        $query->whereNull('end_contract_date')
                            ->orWhere('end_contract_date', '>=', $payroll->start_date);
                    })
                    ->join('payroll_employees', 'payroll_employees.employee_id', '=', 'employees.id')
                    ->where('payroll_employees.payroll_master_id', '=', $payroll->id)
                    ->whereNull('payroll_employees.deleted_at')
                    ->get();
         */        //dd($employees);
        
        $employees = Employee::select('id', 'name', 'employee_code')
            ->whereNull('deleted_at')
            ->where('date_joined', '<=', $payroll->end_date)
            ->where(function ($query) use ($payroll) {
                $query->whereNull('end_contract_date')
                    ->orWhere('end_contract_date', '>=', $payroll->start_date);
            })
            ->with(['currentjob','currentStatus', 'getPayrollEmployees' => function ($query) use ($payroll) {
                return $query->where('payroll_master_id', '=', $payroll->id);
            }])
            ->get();
         
        //dd($employees);

        /*  $employees = Employee::select('id', 'name', 'employee_code')
              ->whereNull('deleted_at')
              ->where('date_joined', '<=', $payroll->end_date)
              ->where(function ($query) use ($payroll) {
                  $query->whereNull('end_contract_date')
                      ->orWhere('end_contract_date', '>=', $payroll->start_date);
              })
              ->get(); */
            
        //dd($employees);
        return $employees;
    }
}
