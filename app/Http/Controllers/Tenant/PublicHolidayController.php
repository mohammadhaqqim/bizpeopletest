<?php

namespace App\Http\Controllers\Tenant;

use App\EmployeeLeave;
use App\Tenant\Manager;
use App\PublicHolidays;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PublicHolidayController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        abort_unless(app(Manager::class)->getUserAccess()->update_admin, 403);
        
        /*         $holidays = EmployeeLeave::where('start_date', '>=', $request->session()->get('first_period_date'))
                    ->whereNull('employee_id')
                    ->orderBy('start_date')
                    ->get(); */
        $holidays = EmployeeLeave::whereNull('employee_id')
            ->orderBy('start_date')
            ->get();

        return view('settings.holidays')
            ->withHolidays($holidays);
    }

    //
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('update-admin');

        $request['variable_date'] = request()->has('variable_date') ? true : false;
        //$request['company_id'] = auth()->user()->company_id;
        $request['approved_by_id'] = auth()->user()->id;
        $request['from_half'] = false;
        $request['to_half'] = false;

        $attributes = $request->validate([
            'title' => 'required|max:100',
            'start_date' => 'required|date',
            'end_date' => '',
            'variable_date' => 'required',
            'from_half' => 'boolean',
            'to_half' => 'boolean',
//            'approved_by_id' => 'exists:users,id',
            //'company_id' => ''
        ]);

        EmployeeLeave::create($attributes);
        
        return back()->with('success', 'A new holiday was created successfully');
    }

    public function update(Request $request, $id)
    {
        $this->authorize('update-admin');

        $holiday = EmployeeLeave::find($id);

        $request['variable_date'] = $request->has('variabledate') ? 1 : 0;
        $request['approved_by_id'] = auth()->user()->id;

        if ($request['end_date'] == null) {
            $request['end_date'] = $request['start_date'];
        }

        $attributes = request()->validate([
            'title' => 'required|max:100',
            'start_date' => 'required|date',
            'end_date' => 'date',
            'variable_date' => 'boolean'
        ]);

        $holiday->update($attributes);

        return back()->with('success', 'The holiday was successfully updated');
    }

    public function destroy(Request $request, $id)
    {
        $this->authorize('update-admin');

        $holiday = EmployeeLeave::find($id);
        $holiday->delete();

        return back()->with('success', 'A new holiday was removed successfully');
    }
}
