<?php

namespace App\Http\Controllers\Tenant;

use App\Company;
use App\PaymentType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentTypeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('update-admin');

        $attributes = $request->validate([
            'title' => 'required|max:100|unique:tenant.payment_types,title',
//            'company_id' => 'required|exists:countries,id'
        ]);

        PaymentType::create($attributes);

        return back()->with('success', 'A new payment mode was generated');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update-admin');

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update-admin');

        $paymenttype = PaymentType::find($id);
        //
        if ($request->has('default')) {
            $company = Company::find(1);
            $company->payment_type_id = $paymenttype->id;
            $company->save();
        }

        $paymenttype->title = $request->title;
        $paymenttype->save();

        return back()->with('success', 'The payment mode was edited successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('update-admin');
        
        $paymenttype = PaymentType::find($id);

        $paymenttype->deleted_at = \Carbon\Carbon::now();
        $paymenttype->save();

        return redirect('settings/customise/payment_types')
            ->with('success', 'The payment type was deleted');
    }
}
