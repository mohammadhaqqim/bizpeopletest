<?php

namespace App\Http\Controllers\Tenant;

use App\Employee;
use App\LeaveType;
use Carbon\Carbon;
use App\LeavePolicy;
use App\WorkingWeek;
use App\EmployeeLeave;
use App\PublicHolidays;
use App\EmployeeDaysOff;
use App\Mail\LeaveRequest;
use App\Mail\LeaveCreated;
use App\EmployeeLeavePolicy;
use App\Mail\LeaveRequestAction;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Dotenv\Exception\ValidationException;
use App\Http\Controllers\Controller;
use App\Policies\EmployeeStatusPolicy;

//use FontLib\TrueType\Collection;

class EmployeeTimeOffController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //        abort_unless(auth()->user()->hr || auth()->user()->payroll || auth()->user()->employee_id == $id, 403);
        abort_unless($this->authorize('update-hr') || $this->authorize('update-payroll') || auth()->user()->employee_id == $id, 403);
        
        $employee = Employee::find($id);
        
        $events = EmployeeLeave::where('start_date', '>=', Carbon::now())
             ->where(function ($q) use ($employee) {
                 $q->where('employee_id', $employee->id)
                    ->orWhereNull('employee_id');
             })
             ->whereNull('deleted_at')
            ->with('LeaveType')
            ->orderBy('start_date')
            ->get();
        //dd($events);
//        $history = EmployeeLeave::where('start_date', '>=', session('first_period_date'))

        $history = EmployeeLeave::where('start_date', '>=', new Carbon('2021-01-01 00:00:00'))
            ->where('start_date', '<', Carbon::now())
            ->where(function ($q) use ($employee) {
                $q->where('employee_id', $employee->id);
            })
            ->with('LeaveType')
            ->with('ApprovedBy')
//            ->with('DeniedBy')
//            ->with('RejectedBy')
            ->orderBy('start_date')
            ->get();

        //dd($history);

        if (is_null($employee->WorkingWeek)) {
            // sat, sun
            session(['daysoff1' => 6 ]);
            session(['daysoff2' => 0 ]);
        } else {
            session(['daysoff1' => $employee->WorkingWeek->getDaysOff()[0] ]);
            if ($employee->WorkingWeek->getDaysOff()->count() == 2) {
                session(['daysoff2' => $employee->WorkingWeek->getDaysOff()[1] ]);
            }
        }

        $leaveTypes = LeaveType::whereNull('deleted_at')
            ->where('active', '=', true)
            ->with(['LeavePolicies'])
            ->get();

//        dd($leaveTypes);
        
        return view('employee.timeoff.index')
            ->withEvents($events)
            ->withHistory($history)
            ->withEmployee($employee);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Employee $employee, Request $request)
    {
        abort_unless($this->authorize('update-hr') || $employee->id == auth()->user()->id, 403);
        
        $policy = $request->exists('policy') ? $request->policy : -2;
        $employee = Employee::find($employee->id);
        
        // abort unless authorised user is hr or the employee is connected to the user
//        abort_unless($this->authorize('update-hr') || auth()->user()->employee_id == $employee->id, 403);

        $events = EmployeeLeave::where('start_date', '>=', Carbon::now())
            ->where('start_date', '<', Carbon::now()->addMonth(1))
            ->orderBy('start_date')
            ->get();

        return view('employee.timeoff.create')
            ->withEvents($events)
            ->withPolicy($policy)
            ->withEmployee($employee);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        //dd('save timeoff');
        abort_unless($this->authorize('update-hr'), 403);
        
        $request['created_by_id'] = auth()->user()->id;
        $request['company_id'] = 1;
        $request['employee_id'] = $id;

        $avg = $this->getWorkingWeek($id, true);
        $avg = $avg / 5;

        $hours = $this->sumDaysOff($request);

        $request['hours_off'] = $hours;
        $request['days_off'] = $hours/$avg;

        // validate employee_leave
        $attributesLeave = $request->validate([
            //'company_id' => 'required|exists:tenant.companies,id',
            'employee_id' => 'required|exists:tenant.employees,id',
            'leave_type_id' => 'required|exists:tenant.leave_types,id',
            'created_by_id' => 'required|exists:users,id',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'hours_off' => '',
            'days_off' => '',
            'comment' => 'max:200'
        ]);

        //dd($attributesLeave);

        DB::beginTransaction();

        try {
            $newLeave = EmployeeLeave::create($attributesLeave);
            $newLeave->employee;
            $newLeave->leavetype;
            //dd($newLeave->leavetype);
        } catch (ValidationException $e) {
            DB::rollback();
            return back()->with('error', 'Issue with Employee Leave entry.');
        } catch (\Exception $e) {
            DB::rollBack();
            return back()->with('error', 'Issue with Employee Leave entry:');
        }

        //dd('stage 1');

        try {
            $startDate = new \Carbon\Carbon($request['start_date']);
            $working = $this->getWorkingWeek($id);

            for ($i = 0; $i < $this->getDaysOff($request) + 1; $i++) {

                //get hours worked for the day
                EmployeeDaysOff::create([
                    'employee_leave_id' => $newLeave->id,
                    'off_day' => $startDate,
                    'work_hours' => $working[$startDate->format('N')-1],
                    'off_hours' => $request['day'.$i],
                    'day_off' => ($working[$startDate->format('N')-1] == 0 ? 0 : $request['day'.$i]/$working[$startDate->format('N')-1])
                ]);

                $startDate->addDay(1);
            }
        } catch (ValidationException $e) {
            DB::rollback();
            return back()->with('error', 'Issue with one Day Off - Validation');
        } catch (\Exception $e) {
            DB::rollBack();
            return back()->with('error', 'Issue with one Day off - Exception '.$e);
        }

//        dd('stage 2');


        DB::commit();

        //email policy to line manager and employee
        $this->SendEmail($id, $newLeave);
        $newLeave->getCalculatePolicyTotals();

        return redirect('/people/timeoff/'.$id)->with('success', 'Leave request has been generated');
    }


    // send email if there are email addresses to
    // worker and reports to employees
    private function SendEmail($id, $newLeave)
    {
        $employee = Employee::find($id);

        //$emails = new Collection();

        if (!is_null($employee->work_email)) {
            Mail::to($employee->work_email)->send(
                new LeaveRequest($newLeave)
            );
        }

        if (!is_null($employee->ReportsTo)) {
            if (!is_null($employee->ReportsTo->work_email)) {
                Mail::to($employee->ReportsTo->work_email)->send(
                    new LeaveRequestAction($newLeave)
                );
            }
        }
    }

    private function getWorkingWeek($id, $hours=null)
    {
        $employee = Employee::find($id);

        $working = [8,8,8,8,8,0,0];

        if (!is_null($employee->WorkingWeek)) {
            $working = [
                $employee->WorkingWeek->mon,
                $employee->WorkingWeek->tue,
                $employee->WorkingWeek->wed,
                $employee->WorkingWeek->thu,
                $employee->WorkingWeek->fri,
                $employee->WorkingWeek->sat,
                $employee->WorkingWeek->sun];
        }

        if (is_null($hours)) {
            return $working;
        }

        return $working[0] + $working[1] + $working[2] + $working[3] + $working[4] + $working[5] + $working[6];
    }

    private function GetDaysHours(Request $request)
    {
        $days = $this->getDaysOff($request) + 1;

        $hour = 0;
        $day = 0.00;

        for ($i = 0; $i < $days; $i++) {
            $hour += $request['day'.$i];

            if ($request['day'.$i] > 0) {
                $day = $day + $request['day'.$i]/8;
            }
        }

        $request['hours_off'] = $hour;
        $request['days_off'] = $day;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // load employee leave -> find employee->id
        $empLeave = EmployeeLeave::find($id);

        abort_unless($this->authorize('update-hr') || $empLeave->employee_id == app(Manager::class)->getUserAccess()->employee_id, 403);

        // load employee
        $employee = Employee::find($empLeave->employee_id);

        $days = EmployeeDaysOff::where('employee_leave_id', '=', $id)
            ->orderBy('off_day')
            ->get();
            
        $leaves = LeavePolicy::whereNull('deleted_at')
            ->get();
        
        //sick days
        $empLeavePolicy = $empLeave->getCalculatePolicyTotals();
        $leaveType = LeaveType::find($empLeave->leave_type_id);

        if ($leaveType->track_time == 'h') {
            $empLeavePolicy->scheduled -= $empLeave->hours_off;
        } else {
            $empLeavePolicy->scheduled -= $empLeave->days_off;
        }

        $leavePolicy = LeavePolicy::find($empLeavePolicy->leave_policy_id);

        $daysOff = $days->sum('off_hours');
        
        return view('employee.timeoff.edit')
            ->withLeaves($leaves)
            ->withDays($days)
            ->withDaysOff($daysOff)
            ->withLeaveType($leaveType)
            ->withLeavePolicy($leavePolicy)
            ->withEmpLeave($empLeave)
            ->withEmpLeavePolicy($empLeavePolicy)
            ->withEmployee($employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $empLeave = EmployeeLeave::find($id);
        
        abort_unless($this->authorize('update-hr') || $empLeave->employee_id == app(Manager::class)->getUserAccess()->employee_id, 403);

        $current_type_id = $empLeave->leave_type_id;
        $leaveID = $empLeave->id;
        
        $avg = $this->getWorkingWeek($empLeave->employee_id, true);
        $avg = $avg / 5;
        $hours = $this->sumDaysOff($request);
        
        $request['hours_off'] = $hours;
        $request['days_off'] = $hours/$avg;

        $attributesLeave = $request->validate([
            'leave_type_id' => 'required|exists:tenant.leave_types,id',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'hours_off' => '',
            'days_off' => '',
            'comment' => 'max:200'
        ]);

        DB::beginTransaction();

        try {
            $empLeave->update($attributesLeave);
            $empLeave->employee;
            $empLeave->leavetype;
        } catch (ValidationException $e) {
            DB::rollback();
            return back()->with('error', 'Issue with Employee Leave entry.');
        } catch (\Exception $e) {
            DB::rollBack();
            return back()->with('error', 'Issue with Employee Leave entry:');
        }

        try {
            $days_off = [];
            
            // delete all current days off in table
            EmployeeDaysOff::where('employee_leave_id', '=', $leaveID)->delete();

            $startDate = new \Carbon\Carbon($request['start_date']);
            $working = $this->getWorkingWeek($empLeave->employee_id);

            ////////////
            for ($i = 0; $i < $this->getDaysOff($request) + 1; $i++) {
                //get hours worked for the day

                $day_off = EmployeeDaysOff::create([
                    'employee_leave_id' => $leaveID,
                    'off_day' => $startDate->format('Y-m-d'),
                    'work_hours' => $working[$startDate->format('N')-1],
                    'off_hours' => $request['day'.$i],
                    'day_off' => ($working[$startDate->format('N')-1] == 0 ? 0 : $request['day'.$i]/$working[$startDate->format('N')-1])
                ]);

                array_push($days_off, $day_off);

                $startDate->addDay(1);
            }
        } catch (ValidationException $e) {
            DB::rollback();
            return back()->with('error', 'Issue with one Day Off 1');
        } catch (\Exception $e) {
            DB::rollBack();
            return back()->with('error', 'Issue with one Day off 2');
        }

        DB::commit();

        //email policy to line manager and employee
        $this->SendEmail($empLeave->employee_id, $empLeave);
        $empLeave->getCalculatePolicyTotals();

        // recalculate old type if changed
        if ($empLeave->leave_type_id != $current_type_id) {
            $this->recalculateOldTotals($empLeave->employee_id, $current_type_id);
        }
        
        return redirect('/people/timeoff/'.$empLeave->employee_id)->with('success', 'Leave request has been updated');
    }

    protected function recalculateOldTotals($employee_id, $leave_type_id)
    {
        $policy_id = DB::connection('tenant')->table('employee_leave_policies')
            ->join('leave_policies', 'leave_policies.id', '=', 'employee_leave_policies.leave_policy_id')
            ->where('employee_leave_policies.employee_id', '=', $employee_id)
            ->where('leave_policies.leave_type_id', '=', $leave_type_id)
            ->where('employee_leave_policies.active', '=', 1)
            ->select('employee_leave_policies.id as id')
            ->first();

        $current_policy = EmployeeLeavePolicy::find($policy_id->id);
        $current_policy->getRecalculateTotals();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $leave = EmployeeLeave::find($id);
        
        abort_unless(auth()->user()->hr || auth()->user()->admin || auth()->user()->id == $leave->employee_id, 403);

        //Delete - Days off
        EmployeeDaysOff::where('employee_leave_id', '=', $leave->id)->delete();

        $leave->deleted_at = Carbon::now();
        $leave->update();

        return back()->with('success', 'The leave has been removed successfully.');
    }

    private function sumDaysOff($request)
    {
        $days = $this->getDaysOff($request) + 1;

        $hours = 0;
        for ($i = 0; $i < $days; $i++) {
            $hours += is_null($request['day'.$i]) ? 0 : $request['day'.$i];
        }

        return $hours;
    }

    private function getDaysOff($request)
    {
        $start = new \Carbon\Carbon($request['start_date']);
        $end = new \Carbon\Carbon($request['end_date']);

        return $start->diffInDays($end);
    }
}
