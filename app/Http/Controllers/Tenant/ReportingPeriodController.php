<?php

namespace App\Http\Controllers\Tenant;

use App\Company;
use App\ReportingPeriod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReportingPeriodController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $this->authorize('update-personal');

        $attributes = $request->validate([
            'title' => 'required|max:100',
        //'company_id' => '',
            'start_date' => 'date',
            'end_date' => 'date',
            'comment' => 'max:191'
        ]);

        $period = ReportingPeriod::create($attributes);

        $periods = ReportingPeriod::whereNull('deleted_at')
            ->get();

        $company = Company::find(1);

        // if only one reporting period then save as current
        if ($periods->count() == 1) {
            //$company->ReportingPeriod
            $company->reporting_period_id = $period->id;
            $company->save();
        }

        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Company $company)
    {
        abort_unless($this->authorize('update-payroll')  || $this->authorize('update-hr')  || $this->authorize('update-admin'), 403);

        //
        $request['company_id'] = 1;

        $attributes = $request->validate([
            'title' => 'required|max:100',
            'company_id' => '',
            'start_date' => 'date',
            'end_date' => 'date',
            'comment' => 'max:191'
        ]);

        $period = ReportingPeriod::create($attributes);

        //$company->reporting_period_id = $period->id;
        //$company->save();

        return back()->with('success', 'A new reporting period has been added.');
    }

    public function setReportingPeriod(Request $request)
    {
        abort_unless($this->authorize('update-payroll')  || $this->authorize('update-hr')  || $this->authorize('update-admin'), 403);

        $company = Company::find(1);

//        dd($request->reporting_period_id);
        $reporting = ReportingPeriod::find($request->reporting_period_id);
        //dd($reporting);
        $company->reporting_period_id = $request->reporting_period_id;
        $company->first_period_date = $reporting->start_date;
        $company->end_financial_date = $reporting->end_date;
        $company->update();

        return back()->with('success', 'The reporting period has been updated.');
    }
}
