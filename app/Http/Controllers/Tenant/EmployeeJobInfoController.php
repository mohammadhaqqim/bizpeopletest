<?php

namespace App\Http\Controllers\Tenant;

use App\Employee;
use App\EmployeeJob;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployeeJobInfoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function update(Request $request, Employee $employee)
    {
        //abort_unless($this->authorize('update-hr') || auth()->user()->employee_id == $employee->id, 403);
        abort_unless($this->authorize('update-hr') || app(Manager::class)->getUserAccess()->employee_id == $employee->id, 403);
        
        if (!$this->checkEmail($request->email_payslip_to) && $request['email_payslip'] == 'on') {
            if ($this->checkEmail($employee->work_email)) {
                $request['email_payslip_to'] = $employee->work_email;
                $request['email_payslip'] = "1";
            } elseif ($this->checkEmail($employee->home_email)) {
                $request['email_payslip_to'] = $employee->home_email;
                $request['email_payslip'] = "1";
            }
        }

        $attributes = $request->validate([
            'date_joined' => 'required|date',
            'working_week_id' => 'nullable|exists:tenant.working_weeks,id',
            'work_permit_expiry' => 'nullable|date',
             'labour_license_expiry' => 'nullable|date',
            'email_payslip_to' => 'nullable|email',
            'payment_mode_id' => 'nullable|exists:tenant.payment_modes,id',
            'bank_id' => '',
            'bank_account' => 'nullable|max:50',
            'tap_account' => 'nullable|max:50',
            'scp_account' => 'nullable|max:50',
            'epf_account' => 'nullable|max:50',
        ]);

        $attributes['email_payslip'] = $request['email_payslip'] == 'on' ? "1" : "0";
        $attributes['tap_member'] = $request['tap_member'] == 'on' ? "1" : "0";
        $attributes['scp_member'] = $request['scp_member'] == 'on' ? "1" : "0";
        $attributes['epf_member'] = $request['epf_member'] == 'on' ? "1" : "0";
        
        $employee->update($attributes);

        return back()->with('success', 'Updated employee job information: '.$employee->name);
    }

    private function checkEmail($email)
    {
        $find1 = strpos($email, '@');
        $find2 = strpos($email, '.');
        return ($find1 !== false && $find2 !== false && $find2 > $find1);
    }
}
