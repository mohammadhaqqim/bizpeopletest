<?php

namespace App\Http\Controllers\Tenant;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Relationship;

class RelationshipController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = $request->validate([
            'title' => 'required|max:100|unique:tenant.races,title',
        ]);

        Relationship::create($attributes);

        return back()->with('success', 'A new relationship was generated');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\race  $race
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Relationship $relationship)
    {
        //
        /*         if ($request->has('default')) {
                    $company = Company::find(1);
                    $company->relationship_id = $race->id;
                    $company->save();
                 }*/

        $relationship->title = $request->title;
        $relationship->save();

        return back()->with('success', 'The race was edited successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\race  $race
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $relationship = Relationship::find($id);
        $relationship->deleted_at = \Carbon\Carbon::now();
        $relationship->save();

        return back()->with('success', 'The relationship was removed successfully');
    }
}
