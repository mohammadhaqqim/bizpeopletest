<?php

namespace App\Http\Controllers\Tenant;

use App\Employee;
use App\EmployeeRelation;
use App\RelationExtras;
use App\Relationship;
use GuzzleHttp\Psr7\Request as Psr7Request;
//use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RelationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
//        $this->authorize('update-admin');
        abort_unless($this->authorize('update-hr') || $this->authorize('update-payroll'), 403);

        //dd($request);

        //
        $employee = Employee::findOrFail($id);
//        abort_unless(auth()->user()->hr || auth()->user()->employee_id == $employee->id, 403);

        $attributes = $request->validate([
            'name' => 'required|max:150',
            'relationship_id' => 'required|exists:tenant.relationships,id',
            'ic_number' => 'nullable|max:10',
            'phone_number' => 'max:20',
            'hand_phone' => 'max:20',
            'date_of_birth' => 'nullable|date'
        ]);

        $attributes['employee_id'] = $employee->id;
        $attributes['emergency'] = $request['emergency'] == 'emergency' ? "1" : "0";
        $attributes['dependant'] = $request['dependant'] == 'dependant' ? "1" : "0";
//        dd($attributes);

        $relation = EmployeeRelation::create($attributes);

        return back()->with('success', 'New relation add: '.$employee->name);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //        $this->authorize('update-hr');
        
        $employee = Employee::find($id);
        
        // abort unless authorised user is hr or the employee is connected to the user
        //abort_unless(auth()->user()->hr || auth()->user()->employee_id == $employee->id, 403);
//        abort_unless($this->authorize('update-hr') || $this->authorize('update-payroll'), 403);

        $relations = EmployeeRelation::select(
            'id',
            'name',
            'relationship_id',
            'ic_number',
            'hand_phone',
            'phone_number',
            'date_of_birth',
            'emergency',
            'dependant',
            'next_of_kin'
        )
            ->with('Relationship')
            ->where('employee_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();

        /*         $relationships = Relationship::select('id', 'title')
                    ->whereNull('deleted_at')
                    ->get(); */

        return view('employee.relations.edit')
            ->withEmployee($employee)
//            ->withRelationships($relationships)
            ->withRelations($relations);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update-admin');

        $relation = EmployeeRelation::find($id);
        abort_unless($this->authorize('update-hr') || $this->authorize('update-payroll'), 403);

//        abort_unless(auth()->user()->hr || auth()->user()->employee_id == $relation->employee_id, 403);

        $attributes = $request->validate([
            'name' => 'required|max:150',
            'relationship_id' => 'required|exists:tenant.relationships,id',
            'ic_number' => 'nullable|max:10',
            'phone_number' => 'max:20',
            'hand_phone' => 'max:20',
            'date_of_birth' => 'nullable|date'
        ]);

        // need to add passport update
        // need to add dependant / student pass details
        $this->updateRelationExtra($request, $id);
            
        $attributes['emergency'] = $request['emergency'] == 'emergency' ? "1" : "0";
        $attributes['dependant'] = $request['dependant'] == 'dependant' ? "1" : "0";

        $relation->update($attributes);

        return back()->with('success', 'Relation updated: '.$relation->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('update-admin');

        $relation = EmployeeRelation::find($id);
        $relation->deleted_at = \Carbon\Carbon::now();
        $relation->save();

        return redirect('settings/customise/relations')
            ->with('success', 'The relationship was deleted');
    }

    private function addRelationExtra(Request $request, $id)
    {
        if ((!empty($request['passport_number']) || !empty($request['passport_expiry'])
            || !empty($request['dependant_expiry']) || !empty($request['student_pass_expiry']))) {
            $attributes = $request->validate([
                'passport_number' => 'nullable|max:20',
                'passport_expiry' => 'nullable|date',
                'dependant_expiry' => 'nullable|date',
                'student_pass_expiry' => 'nullable|date'
            ]);

            RelationExtras::create($attributes);
        }
        return;
    }

    private function updateRelationExtra(Request $request, $id)
    {
        $relation = RelationExtras::find($id);

        // $relation not exist though some values exist
        if ($relation == null & (!empty($request['passport_number']) || !empty($request['passport_expiry'])
            || !empty($request['dependant_expiry']) || !empty($request['student_pass_expiry']))) {
            $this->addRelationExtra($request, $id);
        } elseif ((!empty($request['passport_number']) || !empty($request['passport_expiry'])
            || !empty($request['dependant_expiry']) || !empty($request['student_pass_expiry']))) {
            $attributes = $request->validate([
                'passport_number' => 'nullable|max:20',
                'passport_expiry' => 'nullable|date',
                'dependant_expiry' => 'nullable|date',
                'student_pass_expiry' => 'nullable|date'
            ]);

            $relation->update($attributes);
        }

        return;
    }
}
