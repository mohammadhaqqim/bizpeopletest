<?php

namespace App\Http\Controllers\Tenant;

use App\Company;
use App\Designation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DesignationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$request['company_id'] = Auth()->user()->company_id;
        $this->authorize('update-admin');

        $attributes = $request->validate([
            'title' => 'required|max:100|unique:tenant.designations,title',
            //'company_id' => 'required|exists:countries,id'
        ]);

        Designation::create($attributes);

        return back()->with('success', 'A new designation was generated successfully');
    }

    public function create()
    {
        $this->authorize('update-admin');

        return view('job.create');
//        dd('create new designation');
    }

    public function saveDesignation($attributes)
    {
        return Designation::create($attributes);
    }

    public function update(Designation $designation, Request $request)
    {
        $this->authorize('update-admin');

        if ($request->has('default')) {
            $company = Company::find(1);
            $company->designation_id = $designation->id;
            $company->save();
        }

        $designation->title = $request->title;
        $designation->save();

        return back()->with('success', 'The department was edited successfully');
    }


    public function CheckDesignation($title)
    {
        //abort_unless(auth()->user()->admin, 403);

        $job = Designation::where('title', '=', $title)
            ->first();

        if (is_null($job)) {
            // does not exist - generate
            $job = Designation::create([
                'title' => $title
            ]);
            $job->save();
        }

        return $job->id;
    }

    public function destroy(Designation $designation)
    {
        $this->authorize('update-admin');

        //        $this->authorize('update', $department);
        $designation->deleted_at = \Carbon\Carbon::now();
        $designation->save();

        return redirect('settings/customise/designations')
            ->with('success', 'The designation was deleted');
    }
}
