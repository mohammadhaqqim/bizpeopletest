<?php

namespace App\Http\Controllers\Tenant;

use App\Bank;
use App\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BankController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('update-admin');

        //
        //$request['company_id'] = 1;

        $attributes = $request->validate([
            'title' => 'required|max:100|unique:tenant.banks,title',
        //    'company_id' => 'required|exists:countries,id'
        ]);

        Bank::create($attributes);

        return back()->with('success', 'A new bank was generated');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function edit(Bank $bank)
    {
        $this->authorize('update-admin');

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bank $bank)
    {
        $this->authorize('update-admin');

        //
        //dd($bank);
        
        if ($request->has('default')) {
            $company = Company::find(1);
            $company->bank_id = $bank->id;
            $company->save();
        }

        $bank->title = $request->title;
        $bank->save();

        return back()->with('success', 'The bank was edited successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bank  $bank
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bank $bank)
    {
        $this->authorize('update-admin');
  
        $bank->deleted_at = \Carbon\Carbon::now();
        $bank->save();

        return redirect('settings/customise/banks');
    }
}
