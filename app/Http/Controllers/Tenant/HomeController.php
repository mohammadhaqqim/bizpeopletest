<?php

namespace App\Http\Controllers\Tenant;

use App\User;
use App\Company;
use App\Employee;
use App\TenantUser;
use App\BusinessUser;
use App\EmployeeLeave;
use App\Tenant\Manager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Symfony\Component\Console\Input\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_access = app(Manager::class)->getUserAccess();
        $leaves = null;
        
        if (app(Manager::class)->hasTenant()) {
            $this->checkUser();
        }
        
        if (app(Manager::class)->hasTenant()) {

            // if user is admin / hr then load all leave
            if ($this->authorize('update-hr') || $this->authorize('update-admin')) {
                $leaves = EmployeeLeave::whereNull('approved_at')
                    ->whereNull('deny_at')
                    ->whereNull('rejected_at')
                    ->whereNull('deleted_at')
                    ->whereNotNull('employee_id')
                    ->with(['Employee'])
                    ->get();
            } else {
                $emp_id = app(Manager::class)->getUserAccess()->employee_id;
                $leaves = EmployeeLeave::whereNull('approved_at')
                    ->whereNull('deny_at')
                    ->whereNull('rejected_at')
                    ->where('employee_id', '=', $emp_id)
                    ->with(['Employee'])
                    ->get();
            }

            $company = Company::find(1);
            //dd($company);

            return view('home')
                ->withCompany($company)
                ->withLeaves($leaves);
        }
//        dd($leaves[0]->Employee->name);


        // if user is only connected to one business and no tenant set
        /*         if (!app(Manager::class)->hasTenant() && auth()->user()->access->count() == 1) {
                    return redirect(route('tenant.switch', ['business' => auth()->user()->companies[0]->id]));
                } */

        return view('home');
    }

    // check if the user is copied into tenant_users table for reference
    private function checkUser()
    {
        $user = TenantUser::where('user_id', auth()->user()->id)->first();

        if ($user == null) {
            TenantUser::create([
                'user_id' => auth()->user()->id,
                'name' => auth()->user()->name,
                'block' => 0
            ]);
        }
    }

    
    private function loadLeaveRequests()
    {
        //all active employeess
        $emp = Employee::whereNull('deleted_at')
            ->whereNull('stopped_date')
            ->get();

        //filter the list ??payroll??
        if (!auth()->user()->hr && !auth()->user()->admin) {
            $emp = $emp->filter(function ($e) {
                if (!is_null(auth()->user()->employee_id)) {
                    // if user is the reported to person or the employee
                    if ($e->reports_to_id == auth()->user()->employee_id || $e->id == auth()->user()->employee_id) {
                        return true;
                    }
                    
                    // if the user is a HOD and employee department == users department
                    if ($e->getCurrentDepartment()->id == auth()->user()->Employee->getCurrentDepartment()->id && auth()->user()->hod) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            });
        }
 
        //if hr / admin
        $leave = EmployeeLeave::with(['Employee', 'LeaveType'])
            ->whereNull('deleted_at')
            ->whereNull('deny_date')
            ->whereNull('approved_date')
            ->whereIn('employee_id', $emp->map->id)
//            ->where('employee_id', '<>', auth()->user()->id)
            ->whereNotNull('leave_type_id')
            ->OrderByDesc('created_at')
            ->get();

        /*         $leave = EmployeeLeave::with(['Employee' => function ($query) {
                    $query->where('reports_to_id', '=', 13);
                }, 'LeaveType'])
                    ->whereNull('deleted_at')
                    ->whereNull('deny_date')
                    ->whereNull('approved_date')
                    ->where('employee_id', '<>', auth()->user()->id)
                    ->whereNotNull('leave_type_id')
                    ->get();
         */
        //dd($leave);
        return $leave;
    }

    private function checkDailyUpdate()
    {
    }

    public function message(Request $request)
    {
        $data = $request->all();
        $msg = "This is a simple message. ".$data['start_date'];


        return response()->json(array('msg'=> $msg), 200);
    }
}
