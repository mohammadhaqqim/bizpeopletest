<?php

namespace App\Http\Controllers\Tenant;

use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AllowanceController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $allowances = Transaction::whereNull('deleted_at')
            ->where('active', true)
            ->get();

        return view('settings.allowances')
            ->withAllowances($allowances);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        abort_unless(auth()->user()->hr || auth()->user()->payroll, 403);
        //dd('allowance store');
    }
}
