<?php

namespace App\Http\Controllers\Tenant;

use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allowances = Transaction::where('active', true)
            ->get();

        return view('settings.allowances')
            ->withAllowances($allowances);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('update-personal');
        //dd($request);

        $request['addition'] = $request['addded'] == '1' ? "1" : "0";

        if ($request['titleAdd']) {
            $request['title'] = $request['titleAdd'];
//            $request['addition'] = $request['additionAdd'] == '1' ? "1" : "0";
            $request['addition'] = $request['addded'] == '1' ? "1" : "0";
            $request['calculated'] = $request['calculatedAdd'] == 'on' ? "1" : "0";
            $request['fixed'] = $request['fixedAdd'] == 'on' ? "1" : "0";
        }
        
        $request['active'] = true;
        
        $attributes = $request->validate([
            'title' => 'required|max:100',
            'active' => 'boolean',
            'addition' => 'boolean',
            'fixed' => 'boolean',
            'calcualated' => 'boolean',
        ]);

        Transaction::create($attributes);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $this->authorize('update-personal');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * Transaction $transaction
     */
    public function update(Request $request, Transaction $transaction)
    {
        $this->authorize('update-personal');

        //dd($request->has('calculatedEdit'));
//        dd($request);

//        if ($request['calculatedEdit'] || $request['fixedEdit']) {
//            $request['addition'] = $request['additionAdd'] == '1' ? "1" : "0";
        $request['active'] = $request->has('activeEdit') ? "1" : "0";
        $request['calculated'] = $request->has('calculatedEdit') ? "1" : "0";
        $request['fixed'] = $request->has('fixedEdit') ? "1" : "0";
//        }

        $attributes = $request->validate([
            'title' => 'required|max:100',
            'active' => 'boolean',
            //'addition' => 'boolean',
            'fixed' => 'boolean',
            'calculated' => 'boolean',
        ]);

        //dd($attributes);
        $transaction->update($attributes);

        return back()->with('success', 'The transaction was edited successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $this->authorize('update-person');
    }
}
