<?php

namespace App\Http\Controllers\Tenant;

use App\Employee;
use App\EmployeeLeave;
use App\LeaveType;
use App\LeavePolicy;
use App\EmployeeLeavePolicy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class EmployeeLeavePolicyController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Employee $employee, LeaveType $leavetype)
    {
        abort_unless($this->authorize('update-hr') || $this->authorize('update-admin'), 403);

        //load current policy
        $current_policy = $this->CheckCurrentPolicy($employee->id, $leavetype->id);

        if (!is_null($current_policy)) {
//            dd('some a');
            if ($request['start_from_date'] <= $current_policy->start_from_date) {
                //update current policy
                $attributes = $request->validate([
                    'leave_policy_id' => 'required|exists:tenant.leave_policies,id',
                    'start_from_date' => 'required|date'
                ]);

                $current_policy->update($attributes);
                $current_policy->getRecalculateTotals();
                
                return redirect('/timeoff/'.$employee->id);
            }
        }
        //dd('some');

        $request['employee_id'] = $employee->id;
        $request['active'] = true;

        $attributes = $request->validate([
            'leave_policy_id' => 'required|exists:tenant.leave_policies,id',
            'employee_id' => 'required|exists:tenant.employees,id',
            'start_from_date' => 'required|date',
            'active' => 'required'
        ]);

        $policy = EmployeeLeavePolicy::create($attributes);
        $policy->getRecalculateTotals();
        
        return redirect('/timeoff/'.$employee->id);
    }


    public function update(Request $request, Employee $employee, LeaveType $leavetype)
    {
        abort_unless($this->authorize('update-hr') || $this->authorize('update-admin'), 403);

        //load current policy
        $current_policy = $this->CheckCurrentPolicy($employee->id, $leavetype->id);

        //edit
        if (!is_null($current_policy)) {
//            if ($request['start_from_date'] != $current_policy->start_from_date) {

            //update current policy
            $attributes = $request->validate([
                'leave_policy_id' => 'required|exists:tenant.leave_policies,id',
                'start_from_date' => 'required|date'
            ]);
                
            $current_policy->update($attributes);
            $current_policy->getRecalculateTotals();
            //          }
            
            return redirect('/people/timeoff/'.$employee->id);
        }

        // add new
        $request['employee_id'] = $employee->id;
        $request['active'] = true;

        $attributes = $request->validate([
            'leave_policy_id' => 'required|exists:tenant.leave_policies,id',
            'employee_id' => 'required|exists:tenant.employees,id',
            'start_from_date' => 'required|date',
            'active' => 'required'
        ]);

        $policy = EmployeeLeavePolicy::create($attributes);
        $policy->getRecalculateTotals();
        
        return redirect('/people/timeoff/'.$employee->id);
    }



    private function CheckCurrentPolicy($employee_id, $leave_type_id)
    {
        $policy_id = DB::connection('tenant')->table('employee_leave_policies')
            ->join('leave_policies', 'leave_policies.id', '=', 'employee_leave_policies.leave_policy_id')
            ->where('employee_leave_policies.employee_id', '=', $employee_id)
            ->where('leave_policies.leave_type_id', '=', $leave_type_id)
            ->where('employee_leave_policies.active', '=', 1)
            ->select('employee_leave_policies.id as id')
            ->first();

        //dd($policy_id);

        if (is_null($policy_id)) {
            return null;
        }

        $current_policy = EmployeeLeavePolicy::find($policy_id->id);

        return $current_policy;
    }

    public function create(Request $request, Employee $employee, LeaveType $leavetype)
    {
        abort_unless($this->authorize('update-hr') || $this->authorize('update-admin'), 403);

        // if policy is allocated return

        $leavePolicies = LeavePolicy::where('leave_type_id', '=', $leavetype->id)
            ->whereNull('deleted_at')
            ->get();

        $currentPolicy = $this->CheckCurrentPolicy($employee->id, $leavetype->id);
    
        return view('employee.leave_policy.create')
            ->withEmployee($employee)
            ->withCurrentPolicy($currentPolicy)
            ->withLeavePolicies($leavePolicies)
            ->withLeavetype($leavetype);
    }

    // not sure this is used
    public function getCurrentLeavePolicy($employee_id, $leave_type_id)
    {
        $policy_id = DB::connection('tenant')->table('employee_leave_policies')
            ->join('leave_policies', 'leave_policies.id', '=', 'employee_leave_policies.leave_policy_id')
            ->where('employee_id', '=', $employee_id)
            ->where('leave_type_id', '=', $leave_type_id)
            ->where('start_from_date', '<', \Carbon\Carbon::now())
            ->orderBy('start_from_date')
            ->get();

        dd($policy_id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    public function getRecalculateBalances($id)
    {
        $policy = EmployeeLeavePolicy::find($id);
        //dd($policy);
        $policy->getRecalculateTotals();
        //dd($policy);

        return redirect()->route('people.timeoff', ['id' => $policy->employee_id]);
    }
}
