<?php

namespace App\Http\Controllers\Tenant;

use App\race;
use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class RaceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$request['company_id'] = Auth()->user()->company_id;

        $attributes = $request->validate([
            'title' => 'required|max:100|unique:tenant.races,title',
        //    'company_id' => 'required|exists:countries,id'
        ]);

        Race::create($attributes);

        return back()->with('success', 'A new race was generated');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\race  $race
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, race $race)
    {
        //
        if ($request->has('default')) {
            $company = Company::find(1);
            $company->race_id = $race->id;
            $company->save();
        }

        $race->title = $request->title;
        $race->save();

        return back()->with('success', 'The race was edited successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\race  $race
     * @return \Illuminate\Http\Response
     */
    public function destroy(race $race)
    {
        $race->deleted_at = \Carbon\Carbon::now();
        $race->save();

        return back()->with('success', 'The race was removed successfully');
    }
}
