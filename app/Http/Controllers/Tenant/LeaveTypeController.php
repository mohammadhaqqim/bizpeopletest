<?php

namespace App\Http\Controllers\Tenant;

use App\LeaveType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LeaveTypeController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->authorize('update-admin');

        $request['active'] = true;
        $request['paid_time_off'] = $request->has('paid_time_off');

        $attributes = $request->validate([
            'title' => 'required|max:100',
            'paid_time_off' => 'required|boolean',
            'track_time' => 'required|max:1',
            'active' => 'boolean'
        ]);

        $leavetype = LeaveType::create($attributes);

        return back()->with('success', 'A new leave policy was successfully added to the system.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LeaveType $leavetype)
    {
        $this->authorize('update-admin');

        $request['active'] = $request->has('active') ? 1 : 0;

        $attributes = request()->validate([
            'title' => 'required|max:100',
            'track_time' => 'required|max:1',
            'active' => 'boolean'
        ]);

        $leavetype->update(
            $attributes
        );

        return back()->with('success', 'The leave policy was successfully updated.');
    }

    public function destroy(LeaveType $leavetype)
    {
        $this->authorize('update-admin');

        $leavetype->deleted_at = \Carbon\Carbon::now();
        $leavetype->save();

        return back()->with('success', 'The leave policy was successfully removed.');
    }
}
