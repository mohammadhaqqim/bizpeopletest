<?php

namespace App\Http\Controllers\Tenant;

use Auth;
use App\Employee;
use App\Allowance;
use Carbon\Carbon;
use App\Transaction;
use App\Tenant\Manager;
use App\EmployeeAllowance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class EmployeeAllowanceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $allowances = Transaction::where('active', true)
            ->get();

        return view('settings.allowances')
            ->withAllowances($allowances);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort_unless($this->authorize('update-hr'), 403);

        $transaction = Transaction::find($request['allowance_id']);
        $request['title'] = $transaction->title;
        $request['addition'] = $transaction->addition;

        $attributes = $request->validate([
            'employee_id' => 'required',
            'allowance_id' => 'required|exists:tenant.transactions,id',
            'title' => '',
            'amount' => 'required',
            'addition' => '',
            'effective_date' => 'required',
            'remarks' => ''
        ]);

        EmployeeAllowance::create($attributes);

        return back()->with('success', 'Success: New transaction entered');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\c  $c
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeeAllowance $allowance)
    {
        abort_unless($this->authorize('update-hr'), 403);

        $allowance->update(request([
            'amount', 'remarks'
        ]));

        //update log of change

        return back()->with('success', 'Update of transaction successful');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $employee = Employee::find($id);
        abort_unless($this->authorize('update-hr') || app(Manager::class)->getUserAccess()->employee_id == $employee->id, 403);
     
        /*         $transactions = Transaction::select('id', 'title', 'addition', 'calculated', 'fixed')
                    ->whereNull('deleted_at')
                    ->get(); */
        
        $allowances = EmployeeAllowance::where('employee_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();

        return view('allowances.edit')
            //->withTransactions($transactions)
            ->withAllowances($allowances)
            ->withEmployee($employee);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(EmployeeAllowance $allowance)
    {
        abort_unless($this->authorize('update-hr'), 403);

        // update log for auditing

        //$allowance->delete();
        $allowance->deleted_at = Carbon::now();
//        $allowance->deleted_by = Auth::user()->id;

        $allowance->update();

        return back();
    }
}
