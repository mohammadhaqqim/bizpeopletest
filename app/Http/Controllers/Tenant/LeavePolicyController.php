<?php

namespace App\Http\Controllers\Tenant;

use App\Company;
use App\LeaveType;
use App\LeavePolicy;
use App\LeavePolicySchedule;
use App\WorkingWeek;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;

class LeavePolicyController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('update-admin');

        //leavetype
        $leavetypes = LeaveType::whereNull('deleted_at')
            ->get();

        return view('employee.timeoff_policy.create')
            ->withLeavetypes($leavetypes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('update-admin');

        $attributes = $request->validate([
            'policy_name' => 'required|max:100',
            'leave_type_id' => 'required|exists:tenant.leave_types,id',
            'first_accrual' => 'required|max:1',
            'carryover' => 'required|max:1',

            'carryover_day_id' => 'required|numeric|min:0|max:31',
            'carryover_month_id' => 'required|numeric|min:0|max:12',

            'yearly_amount' => 'numeric',
            'carryover_type' => 'required|max:1',
            'max_carryover' => '',
            
            'accrual_occurs' => 'required|max:1'
        ]);

        // Policy
        LeavePolicy::create($attributes);

        return redirect('settings/timeoff');
    }

    public function update(Request $request, $id)
    {
        $this->authorize('update-admin');

        $policy = LeavePolicy::find($id);
        $attributes = $request->validate([
            'policy_name' => 'required|max:100',
            'leave_type_id' => 'required|exists:tenant.leave_types,id',
            'first_accrual' => 'required|max:1',
            'carryover' => 'required|max:1',
            'carryover_day_id' => 'required|max:1',
            'carryover_month_id' => 'required|max:1',

            'yearly_amount' => 'numeric',
            'carryover_type' => 'required|max:1',
            'max_carryover' => '',
            
            'accrual_occurs' => 'required|max:1'
        ]);

        $policy->update($attributes);
        
        return redirect('/settings/timeoff');
    }

    public function edit(Request $request, $id)
    {
        $this->authorize('update-admin');
 
        $policy = LeavePolicy::find($id);

        $leavetypes = LeaveType::whereNull('deleted_at')
            ->get();

        return view('employee.timeoff_policy.edit')
            ->withLeavetypes($leavetypes)
            ->withPolicy($policy);
    }
}
