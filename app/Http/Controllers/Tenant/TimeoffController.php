<?php

namespace App\Http\Controllers\Tenant;

use App\LeaveType;
use App\Tenant\Manager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TimeoffController extends Controller
{
    public function index()
    {
        abort_unless(app(Manager::class)->getUserAccess()->update_admin, 403);

        /*         $leavetypes = LeaveType::whereNull('deleted_at')
                    ->with(['LeavePolicies'])
                    ->get(); */

        return view('settings.timeoff');
//            ->withleavetypes($leavetypes);
    }
}
