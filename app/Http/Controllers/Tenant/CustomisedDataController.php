<?php

namespace App\Http\Controllers\Tenant;

use App\Bank;
use App\Race;
use App\Company;
use App\Religion;
use App\Department;
use App\Designation;
use App\PaymentMode;
use App\PaymentType;
use App\Relationship;
use App\CustomisedData;
use App\EndEmploymentReason;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Tenant\Manager;

class CustomisedDataController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        dd(app(Manager::class)->getTenant());
//        dd(app(Manager::class)->getUserAccess());
        abort_unless(app(Manager::class)->getUserAccess()->update_admin, 403);

//        $this->authorize('update-admin');

        $segment = 'departments';
        //dd($segment);

        //load all customised data
        $menu = CustomisedData::whereNull('deleted_at')
            ->get();

        //dd($menu);
        $selected = CustomisedData::where('table', '=', $segment)
            ->first();
        //dd($selected);

        $entries = $this->getSegment($segment);
        //dd($entries);

        /*         $entries = DB::table($segment)
                    ->select('id', 'title')
                    ->whereNull('deleted_at')
                    ->get(); */

        //dd($selected);
        //dd($menu);
        $defaultid = $selected->identifier;
        //$company = Company::find(Auth()->user()->company_id);
        $company = Company::find(1);
        $defaultid = $company[$defaultid];
        //dd($defaultid);

        return view('settings.customised.index')
            ->withMenu($menu)
            ->withSegment($segment)
            ->withEntries($entries)
            ->withDefaultid($defaultid)
            ->withSelected($selected);
    }

    public function display(Request $request)
    {
        $this->authorize('update-admin');

        $segment = $request->segment(3);

        //load all customised data
        $menu = CustomisedData::whereNull('deleted_at')
            ->get();

        // load customised data entry
        $selected = CustomisedData::where('table', '=', $segment)
            ->first();
        //dd($segment);
        $entries = $this->getSegment($segment);
        $defaultid = $selected->identifier;
        $company = Company::find(1);
        $defaultid = $company[$defaultid];

        return view('settings.customised.index')
            ->withMenu($menu)
            ->withSegment($segment)
            ->withEntries($entries)
            ->withDefaultid($defaultid)
            ->withSelected($selected);
    }

    private function getSegment($segment)
    {
        switch ($segment) {
            case 'departments':
                return Department::whereNull('deleted_at')->get();
                break;
            case 'designations':
                return Designation::whereNull('deleted_at')->get();
                break;
            case 'banks':
                return Bank::whereNull('deleted_at')->get();
                break;
            case 'payment_modes':
                return PaymentMode::whereNull('deleted_at')->get();
                break;
            case 'races':
                return Race::whereNull('deleted_at')->get();
                break;
            case 'religions':
                return Religion::whereNull('deleted_at')->get();
                break;
            case 'relationships':
                return Relationship::whereNull('deleted_at')->get();
                break;
            case 'payment_types':
                return PaymentType::whereNull('deleted_at')->get();
                break;
            case 'end_employment_reasons':
                return EndEmploymentReason::whereNull('deleted_at')->get();
                break;
            
        }
    }
}
