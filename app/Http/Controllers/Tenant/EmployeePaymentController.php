<?php

namespace App\Http\Controllers\Tenant;

use App\Company;
use App\Country;
use App\Employee;
use App\EmployeePayment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class EmployeePaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Employee $employee, Request $request)
    {
        abort_unless($this->authorize('update-hr'), 403);

        $request['employee_id'] = $employee->id;
        $request['payment_mode_id'] = ($employee->payment_mode_id == null ? Company::find(1)->payment_mode_id : $employee->payment_mode_id);
        
        EmployeePayment::create(
            $this->validation($request)
        );

        return back()->with('success', 'New payment added: '.$employee->name);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\c  $c
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmployeePayment $employeePayment)
    {
        abort_unless($this->authorize('update-hr'), 403);

        $employee = Employee::find($employeePayment->employee_id);
        $request['employee_id'] = $employee->id;
        $request['payment_mode_id'] = ($employee->payment_mode_id == null ? Company::find(1)->payment_mode_id : $employee->payment_mode_id);

        $attributes = $this->validation($request);
        $employeePayment->update(
            $attributes
        );

        /* $employeePayment->update(request([
            'effective_date',
            'basic_salary',
            'payment_type_id',
            'overtime',
            'comment'
            ])); */

        return back();
    }

    public function edit(Request $request, EmployeePayment $employeePayment)
    {
        $employee = Employee::find($employeePayment->employee_id);
        $currency = Country::find($employeePayment->currency_id);
        
        return view('payroll.employee.edit', compact('employeePayment'))
            ->withEmployee($employee);
    }

    public function validation(Request $request)
    {
        return $request->validate([
            'employee_id' => 'required|exists:tenant.employees,id',
            'effective_date' => 'required|date',
            'basic_salary' => 'numeric|min:0',
            'currency_id' => 'required|exists:tenant.countries,id',
            'overtime' => 'numeric|min:0',
            'payment_mode_id' => 'required|exists:tenant.payment_modes,id',
            'comment' => 'nullable|string',
        ]);
    }
}
