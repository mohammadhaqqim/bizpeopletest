<?php

namespace App\Http\Controllers\Tenant;

use App\Employee;
use App\Jobs\EmailPayslip;
use App\Jobs\GeneratePayslip;
use Carbon\Carbon;
use App\PaymentMode;
use App\PayrollMaster;
use App\PayrollEmployee;
use App\ReportingPeriod;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Mail\ApprovedPayslip;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class PayrollApprovalController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        abort_unless($this->authorize('update-payroll'), 403);

        //get the payroll
        $payroll = PayrollMaster::findOrFail($id);

        //get payroll employees
        $employeePayrolls = PayrollEmployee::where('payroll_master_id', $id)
            ->whereNull('deleted_at')
            ->get();

        $paymentModes = PaymentMode::select('id', 'title')
            ->whereNull('deleted_at')
            ->get();

        return view('payroll.approval.index')
            ->withPayroll($payroll)
            ->withEmployeePayrolls($employeePayrolls)
            ->withPaymentModes($paymentModes);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\r  $r
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        abort_unless($this->authorize('update-payroll'), 403);

        //get the payroll -> lock it down for approval
        $payroll = PayrollMaster::findOrFail($id);
        $payroll->approved_by = auth()->user()->id;
        $payroll->locked_datetime = Carbon::now();
        $payroll->save();

        // set all employee payrolls to approved
        DB::connection('tenant')->table('payroll_employees')
            ->whereNull('deleted_at')
            ->where('payroll_master_id', $payroll->id)
            ->update(['approved' => 1]);

        // get payroll employees
        $employees_payroll = PayrollEmployee::where('payroll_master_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();

        // schedule each payslip to be printed and emailed
        foreach ($employees_payroll as $employee) {
            // send print payslip to queue
            GeneratePayslip::dispatchNow($employee->id);
            $emp = Employee::find($employee->employee_id);

            if ($emp->email_payslip == 1 && $emp->email_payslip_to != null) {
                EmailPayslip::dispatchNow($payroll->id, $employee->id);
            }
        }
        // take user to historical periods - see the approved payroll
        return back()->with('success', 'Approval finalised.');
    }

    public function printAllPayslips($id)
    {
        $employees_payroll = PayrollEmployee::where('payroll_master_id', '=', $id)
            ->whereNull('deleted_at')
            ->where('approved', 1)
            ->get();
        
        //dd($employees_payroll[0]->id);

        // schedule each payslip to be printed and emailed
        foreach ($employees_payroll as $employee) {
            // send print payslip to queue
            GeneratePayslip::dispatchNow($employee->id);
            $emp = Employee::find($employee->employee_id);
            if ($emp->email_payslip && $emp->email_payslip_to) {
                EmailPayslip::dispatchNow($employee->id, $emp);
            }
        }

        //dd(Carbon::now());
    }

    private function email()
    {
        /*         $to_name = 'RECEIVER_NAME';
                $to_email = 'RECEIVER_EMAIL_ADDRESS';
                $data = array('name'=>'Cloudways (sender_name)', 'body' => 'A test mail');

                Mail::send('mail', $data, function ($message) use ($to_name, $to_email) {
                    $message->to($to_email, $to_name)
                    ->subject('Laravel Test Mail');
                    $message->from('SENDER_EMAIL_ADDRESS', 'Test Mail');
                });

                Mail::to('regan@bizitsolutions.biz')->send(
                    new ApprovedPayslip();
                ); */
    }

    public function batch(Request $request, $id)
    {
        $array = [];

        foreach ($request->all() as $key => $value) {
            if (Str::startsWith($key, 'approve')) {
                $val = str_replace('approve', '', $key);
                array_push($array, $val);
            }
        }

        DB::connection('tenant')->table('payroll_employees')
            ->whereIn('id', $array)
            ->update(['approved' => 1]);

        

        return back();
    }
}
