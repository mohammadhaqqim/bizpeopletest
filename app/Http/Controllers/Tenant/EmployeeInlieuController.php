<?php

namespace App\Http\Controllers\Tenant;

use App\Employee;
use Carbon\Carbon;
use App\EmployeeLeave;
use App\EmployeeDaysOff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Validation\ValidationException;

class EmployeeInlieuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Employee $employee, Request $request)
    {
        //abort_unless($this->authorize('update-hr') || $employee->id == auth()->user()->id, 403);
        
        $policy = $request->exists('policy') ? $request->policy : -2;
        $employee = Employee::find($employee->id);
        
        $events = EmployeeLeave::where('start_date', '>=', Carbon::now())
            ->where('start_date', '<', Carbon::now()->addMonth(1))
            ->orderBy('start_date')
            ->get();

        return view('employee.inlieu.create')
            ->withEvents($events)
            ->withPolicy($policy)
            ->withEmployee($employee);
    }

    public function store(Employee $employee, Request $request)
    {
        //dd($employee);
        abort_unless($this->authorize('update-hr'), 403);
        
        $request['created_by_id'] = auth()->user()->id;
        $request['company_id'] = 1;
        $request['employee_id'] = $employee->id;

        //dd(app(EmployeeDaysOffController::class)->getWorkingWeek($employee->id, true);
        //dd($employee->WorkingWeek);

        $avg = $employee->WorkingWeek->hours_day;
        $hours = $this->sumDaysOff($request);

        $request['hours_off'] = -1*$hours;
        $request['days_off'] = -1*$hours/$avg;

        // validate employee_leave
        $attributesLeave = $request->validate([
            //'company_id' => 'required|exists:tenant.companies,id',
            'employee_id' => 'required|exists:tenant.employees,id',
            'leave_type_id' => 'required|exists:tenant.leave_types,id',
            'created_by_id' => 'required|exists:users,id',
            'start_date' => 'required|date',
            'end_date' => 'required|date',
            'hours_off' => '',
            'days_off' => '',
            'comment' => 'max:200'
        ]);

        DB::beginTransaction();

        try {
            $newLeave = EmployeeLeave::create($attributesLeave);
            $newLeave->employee;
            $newLeave->leavetype;
            //dd($newLeave->leavetype);
        } catch (ValidationException $e) {
            DB::rollback();
            return back()->with('error', 'Issue with Employee Leave entry.');
        } catch (\Exception $e) {
            DB::rollBack();
            return back()->with('error', 'Issue with Employee Leave entry:');
        }

        //dd('stage 1');

        try {
            $startDate = new \Carbon\Carbon($request['start_date']);
            $working = $employee->WorkingWeek;

            for ($i = 0; $i < $this->getDaysOff($request) + 1; $i++) {
                //dd($startDate->format('N')-1);

                //get hours worked for the day
                EmployeeDaysOff::create([
                    'employee_leave_id' => $newLeave->id,
                    'off_day' => $startDate,
                    'work_hours' => $working[$startDate->format('N')-1],
                    'off_hours' => $request['day'.$i],
                    'day_off' => ($working[$startDate->format('N')-1] == 0 ? 0 : $request['day'.$i]/$working[$startDate->format('N')-1])
                ]);

                $startDate->addDay(1);
            }
        } catch (ValidationException $e) {
            DB::rollback();
            return back()->with('error', 'Issue with one Day Off - Validation');
        } catch (\Exception $e) {
            DB::rollBack();
            return back()->with('error', 'Issue with one Day off - Exception '.$e);
        }

        DB::commit();

        //email policy to line manager and employee
        //$this->SendEmail($employee->id, $newLeave);
        $newLeave->getCalculatePolicyTotals();

        return redirect('/people/timeoff/'.$employee->id)->with('success', 'Leave request has been generated');
    }

    private function sumDaysOff($request)
    {
        $days = $this->getDaysOff($request) + 1;

        $hours = 0;
        for ($i = 0; $i < $days; $i++) {
            $hours += is_null($request['day'.$i]) ? 0 : $request['day'.$i];
        }

        return $hours;
    }

    private function getDaysOff($request)
    {
        $start = new \Carbon\Carbon($request['start_date']);
        $end = new \Carbon\Carbon($request['end_date']);

        return $start->diffInDays($end);
    }
}
