<?php

namespace App\Http\Controllers\Tenant;

use App\Employee;
use App\EmployeeLeave;
use App\EmployeeDaysOff;
use App\EmployeeLeavePolicy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\LeavePolicy;
use App\LeaveType;

class EmployeeDaysOffController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function daysoff(Request $request)
    {
        //dd($request);
        $data = $request->all();

        $working = $this->getWorkingWeek($data['id']);
        
        $start_date = \Carbon\Carbon::parse($data['start_date']);
        $end_date = \Carbon\Carbon::parse($data['end_date']);
        $i = 0;
        $total_hours = 0;
        $days = 0;
        $add_days = '';

        while ($start_date <= $end_date) {
            //$add_days += $this->getDaysOff($start_date, $i, $data, $working);
            $add_days = $add_days.$this->getDaysOff($start_date, $i, $data, $working);

            // is the date a public holiday
            if ($this->isHoliday($start_date->format('Y-m-d')) != 1) {
                $hours = $this->getHoursOff($data['id'], $start_date->format('Y-m-d'), $data['leaveID']);
                
                if (is_null($hours)) {
                    if ($working[$start_date->format('N')-1]>0) {
                        $days += 1;
                    }
                    $total_hours += $working[$start_date->format('N')-1];
                }
            }

            $start_date = $start_date->addDay();
            $i++;
        }

        if ($data['type'] == "0") {
            return response()->json(array('msg'=>$add_days, 'balance'=>0, 'policy'=>'', 'track'=>0, 'days'=>0), 200);
        }

        $type_id = $data['type'];
        $employee_id = $data['id'];

        // employee_id or type_id are null then
        if (is_null($employee_id) || is_null($type_id)) {
            $balance = 0;
        } else {
            $balance = $this->getLeaveBalance($employee_id, $type_id);
        }

        $leave = LeaveType::find($type_id);
        $empPolicy = $leave->getCurrentEmployeePolicy($employee_id);
        $policy = LeavePolicy::find($empPolicy->leave_policy_id);

        $scheduled = $empPolicy->scheduled;

        if ($data['leaveID'] != "-1") {
            $remove = $this->getHoursRemove($employee_id, $data['leaveID'], $leave->track_time);
   
            $scheduled -= $remove;
        }

        $msg2 = $this->getValues($leave->track_time, $scheduled, $policy->yearly_amount, $empPolicy->taken, $data, $empPolicy->carryover);

        return response()->json(array('msg'=>$add_days, 'balance'=>$balance, 'policy'=>$msg2, 'track'=>$leave->track_time, 'days'=>$days), 200);
    }

    public function inlieu(Request $request)
    {
        //dd($request);
        $data = $request->all();

        $working = $this->getWorkingWeek($data['id']);
        
        $start_date = \Carbon\Carbon::parse($data['start_date']);
        $end_date = \Carbon\Carbon::parse($data['end_date']);
        $i = 0;
        $total_hours = 0;
        $days = 0;
        $add_days = '';

        
        while ($start_date <= $end_date) {
            //$add_days += $this->getDaysOff($start_date, $i, $data, $working);
            $add_days = $add_days.$this->getDaysOff($start_date, $i, $data, $working);

            // is the date a public holiday
            /*             if ($this->isHoliday($start_date->format('Y-m-d')) != 1) {
                            $hours = $this->getHoursOff($data['id'], $start_date->format('Y-m-d'), $data['leaveID']);

                            if (is_null($hours)) {
                                if ($working[$start_date->format('N')-1]>0) {
                                    $days += 1;
                                }
                                $total_hours += $working[$start_date->format('N')-1];
                            }
                        } */

            $start_date = $start_date->addDay();
            $i++;
        }

        if ($data['type'] == "0") {
            return response()->json(array('msg'=>$add_days, 'balance'=>0, 'policy'=>'', 'track'=>0, 'days'=>0), 200);
        }

        $type_id = $data['type'];
        $employee_id = $data['id'];

        // employee_id or type_id are null then
        if (is_null($employee_id) || is_null($type_id)) {
            $balance = 0;
        } else {
            $balance = $this->getLeaveBalance($employee_id, $type_id);
        }

        $leave = LeaveType::find($type_id);
        $empPolicy = $leave->getCurrentEmployeePolicy($employee_id);
        $policy = LeavePolicy::find($empPolicy->leave_policy_id);

        $scheduled = $empPolicy->scheduled;

        if ($data['leaveID'] != "-1") {
            $remove = $this->getHoursRemove($employee_id, $data['leaveID'], $leave->track_time);
   
            $scheduled -= $remove;
        }

        $msg2 = $this->getValues($leave->track_time, $scheduled, $policy->yearly_amount, $empPolicy->taken, $data, $empPolicy->carryover);

        return response()->json(array('msg'=>$add_days, 'balance'=>$balance, 'policy'=>$msg2, 'track'=>$leave->track_time, 'days'=>$days), 200);
    }


    /*     public function daysofff(Request $request)
        {
            $data = $request->all();
            $working = $this->getWorkingWeek($data['id']);

            $start = '<div class="row p-2 justify-content-center">';
            $labelDay = '<small for="" class="my-auto ml-2" style="width:75px;">';
            $inputdate = '</small><input type="date" class="form-control custom-select-sm mr-1 date-timeoff" id="date_from" name="date_from" style="width:130px;" placeholder="dd/mm/yyyy" value="';
            $inputhours = '<input type="number" style="width:60px;" class="form-control custom-select-sm d-inline" onblur="addDaysOff()" value="';
            $label = '<small for="" class="my-auto pl-2" style="width:60px">Hours</small></div>';

            $start_date = \Carbon\Carbon::parse($data['start_date']);
            $end_date = \Carbon\Carbon::parse($data['end_date']);
            $type_id = $data['type'];
            $employee_id = $data['id'];

            //return response()->json(array('msg'=>$data['leaveID']), 200);

            $add_days = '';
            $msg = '';
            $i = 0;
            $total_hours = 0;
            $days = 0;
            $remove_hours = 0;

            while ($start_date <= $end_date) {
                $add_days = $add_days
                    .$start
                    .$labelDay
                    .$start_date->format('l')
                    .$inputdate
                    .$start_date->format('Y-m-d')
                    .'" disabled>'
                    .$inputhours;

                // is the date a public holiday
                if ($this->isHoliday($start_date->format('Y-m-d')) == 1) {
                    $add_days = $add_days.'0'
                    .'" name="day'.$i.'" id="day'.$i.'" disabled>';
                } else {
                    // check if there is time off already
                    $hours = $this->getHoursOff($data['id'], $start_date->format('Y-m-d'), $data['leaveID']);

                    if (is_null($hours)) {
                        $add_days = $add_days
                            .$working[$start_date->format('N')-1]
                            .'" name="day'.$i.'" id="day'.$i.'">';
                        if ($working[$start_date->format('N')-1]>0) {
                            $days += 1;
                        }
                        $total_hours += $working[$start_date->format('N')-1];
                    } else {
                        $add_days = $add_days
                            .'0" name="day'.$i.'" id="day'.$i.'">';
                    }
                }

                // add the item to the list
                $add_days = $add_days
                    .$label;

                $start_date = $start_date->addDay();
                $i++;
            }

            $msg = $add_days;


            if (is_null($employee_id) || is_null($type_id)) {
                $balance = 0;
            } else {
                $balance = $this->getLeaveBalance($employee_id, $data['type']);
            }


            $avg = $this->getAverageDay($data['id']);

            $leave = LeaveType::find($data['type']);
            $msg2 = $this->getValues($balance, $leave->track_time, $avg, 0, 0, 0);

            return response()->json(array('msg'=>$msg, 'days'=>$days, 'hours'=>$total_hours, 'balance'=>$balance, 'AvgHours'=>$avg, 'track'=>$leave->track_time ), 200);
        } */

    private function getValues($track_time, $scheduled, $yearly_max, $taken, $data, $carryover)
    {
        if (is_null($data['id']) || is_null($data['type'])) {
            $balance = 0;
        } else {
            $balance = $this->getLeaveBalance($data['id'], $data['type']);
        }

        $average = $this->getAverageDay($data['id']);

        $bal = 'bal <small id="availableBalance">'.$balance.' </small>';
        $track = 'track <small id="track">'.$track_time.'</small>';
        $avg = 'avg <small id="avg" class="mr-2" >'.$average.'</small>';
        $schedule = 'sched- <small id="scheduled" class="mr-2" >'.$scheduled.'</small>';
        $max = 'max- <small id="max" class="mr-2" >'.$yearly_max.'</small>';
        $tkn = 'taken- <small id="taken" class="mr-2" >'.$taken.'</small>';
        $over = 'carry- <small id="carry" class="mr-2" >'.$carryover.'</small>';
        
        return $bal.$track.$avg.$schedule.$max.$tkn.$over;
    }

    private function getDaysOff($start_date, $index, $data, $working)
    {
        $start = '<div class="row p-2 justify-content-center">';
        $labelDay = '<small for="" class="my-auto ml-2" style="width:75px;">';
        $inputdate = '</small><input type="date" class="form-control custom-select-sm mr-1 date-timeoff" id="date_from" name="date_from" style="width:130px;" placeholder="dd/mm/yyyy" value="';
        $inputhours = '<input type="number" style="width:60px;" class="form-control custom-select-sm d-inline" onblur="addDaysOff()" value="';
        $label = '<small for="" class="my-auto pl-2" style="width:60px">Hours</small></div>';
        
        $add_days =
            $start
            .$labelDay
            .$start_date->format('l')
            .$inputdate
            .$start_date->format('Y-m-d')
            .'" disabled>'
            .$inputhours;

        //$add_days = $add_days.'0'.'" name="day'.$index.'" id="day'.$index.'" disabled>';

        // is the date a public holiday
        if ($this->isHoliday($start_date->format('Y-m-d')) == 1) {
            $add_days = $add_days.'0'
                .'" name="day'.$index.'" id="day'.$index.'" disabled>';
        } else {
            // check if there is time off already
            $hours = $this->getHoursOff($data['id'], $start_date->format('Y-m-d'), $data['leaveID']);

            if (is_null($hours)) {
                $add_days = $add_days
                .$working[$start_date->format('N')-1]
                .'" name="day'.$index.'" id="day'.$index.'">';
            } else {
                $add_days = $add_days
                .'0" name="day'.$index.'" id="day'.$index.'">';
            }
        }
        $add_days = $add_days.$label;

        return $add_days;
    }

    private function getLeaveBalance($employee_id, $type_id)
    {
        // find EmployeeLeavePolicy
        $policy_id = DB::connection('tenant')->table('employee_leave_policies')
            ->join('leave_policies', 'leave_policies.id', '=', 'employee_leave_policies.leave_policy_id')
            ->where('employee_leave_policies.employee_id', '=', $employee_id)
            ->where('leave_policies.leave_type_id', '=', $type_id)
            ->where('employee_leave_policies.active', '=', 1)
            ->select('employee_leave_policies.id as id')
            ->first();

        if (is_null($policy_id)) {
            return null;
        }

        $current_policy = EmployeeLeavePolicy::find($policy_id->id);
        // return carryover + available - taken
        return $current_policy->balance;
    }

    private function getHoursOff($employee_id, $date, $leaveID)
    {
        //read employee days off

        $hoursOff = DB::connection('tenant')->table('employee_days_offs')
            ->join('employee_leaves', 'employee_leaves.id', '=', 'employee_days_offs.employee_leave_id')
            ->where('employee_id', '=', $employee_id)
            ->where('off_day', '=', $date)
            ->where('employee_days_offs.employee_leave_id', '!=', $leaveID)
            ->select('off_hours')
            ->first();

        return $hoursOff;
        // EmployeeDaysOff::find()
    }

    private function getHoursRemove($employee_id, $leaveID, $track)
    {
        //read employee days off

        $hoursOff = DB::connection('tenant')->table('employee_days_offs')
            ->join('employee_leaves', 'employee_leaves.id', '=', 'employee_days_offs.employee_leave_id')
            ->where('employee_id', '=', $employee_id)
            ->where('employee_days_offs.employee_leave_id', '=', $leaveID)
            ->select('off_hours', 'day_off')
            ->get();

        if ($track == 'h') {
            return $hoursOff->sum('off_hours');
        } else {
            return $hoursOff->sum('day_off');
        }
        // EmployeeDaysOff::find()
    }

    public function isHoliday($date)
    {
        $holiday = EmployeeLeave::whereNull('employee_id')
            ->where('start_date', '<=', $date)
            ->where('end_date', '>=', $date)
            ->get();

        return $holiday->count();
    }

    public function getHolidays($start, $end)
    {
        $holidays = EmployeeLeave::whereNull('employee_id')
            ->where(function ($query) use ($start) {
                $query->where('start_date', '<=', $start)
                    ->where('end_date', '>=', $start);
            })->orWhere(function ($query) use ($end) {
                $query->where('start_date', '<=', $end)
                    ->where('end_date', '>=', $end);
            });

        return $holidays;
    }

    public function getWorkingWeek($id)
    {
        $employee = Employee::find($id);

        $working = [8,8,8,8,8,0,0];

        if (is_null($employee->WorkingWeek)) {
            $working = [
                $employee->WorkingWeek->mon,
                $employee->WorkingWeek->tue,
                $employee->WorkingWeek->wed,
                $employee->WorkingWeek->thu,
                $employee->WorkingWeek->fri,
                $employee->WorkingWeek->sat,
                $employee->WorkingWeek->sun];
        }

        return $working;
    }

    private function getAverageDay($id)
    {
        $employee = Employee::find($id);

        $days = 0;
        
        if ($employee->WorkingWeek->mon > 0) {
            $days++;
            $avg = $employee->WorkingWeek->mon;
        }

        if ($avg += $employee->WorkingWeek->tue > 0) {
            $days++;
            $avg += $employee->WorkingWeek->tue;
        }

        if ($avg += $employee->WorkingWeek->wed > 0) {
            $days++;
            $avg += $employee->WorkingWeek->wed;
        }
        
        if ($avg += $employee->WorkingWeek->thu > 0) {
            $days++;
            $avg += $employee->WorkingWeek->thu;
        }

        $avg = $avg / $days;
        $employee->WorkingWeek->avg = $avg;

        return $employee->WorkingWeek->avg;
    }
}
