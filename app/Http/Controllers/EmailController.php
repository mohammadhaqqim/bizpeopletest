<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmail;

class EmailController extends Controller
{
    public function index ()
    {

    $isi_email = [
      'title' => 'Biz Mailing',
      'body' => 'Welcome to Biz People'
    ];

    $tujuan = 'samplecodeprogram@gmail.com';

    Mail::to($tujuan)->send(new SendEmail($isi_email));


        return 'Email has been Successfully Sent!';
    }
}
