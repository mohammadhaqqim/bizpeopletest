<?php

namespace App\Http\Controllers\Auth;

use App\Tenant\Manager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    protected function authenticated(Request $request, $user)
    {
        if (!app(Manager::class)->hasTenant()) {
            return redirect(RouteServiceProvider::HOME);
            //return route('home');
        }
    }

    public function redirectPath()
    {
        // if user is only connected to one business and set tenant automatically
        if (!app(Manager::class)->hasTenant() && auth()->user()->access->count() == 1) {
            return redirect(route('tenant.switch', ['business' => auth()->user()->companies[0]->id]));
        }
        
        // if multiple or no tenant than show home view
        if (!app(Manager::class)->hasTenant()) {
            return route('home');
        }
    }
}
