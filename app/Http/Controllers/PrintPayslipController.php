<?php

namespace App\Http\Controllers;

use PDF;
use App\Company;
use App\PayrollEmployee;
use App\PayrollTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PrintPayslipController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, $id)
    {
        $payslip = PayrollEmployee::findOrFail($id);
        //dd($payslip);

        // abort unless you are a payroll officer or the payslip item is your payslip
        abort_unless(auth()->user()->payroll || $payslip->employee_id == auth()->user()->employee_id, 403);

        $path = $this->generatePayslip($id);
        $path = storage_path($path);

        return response()->file($path);

        return view('payroll.payslip')
            ->withPath($path);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        abort_unless(auth()->user()->payroll, 403);

        // generate the file
        $filename = $this->generatePayslip($id);

        // copy the file to public folder
        //$file = storage_path('payslips/'.$id.'/'.$filename);
        //$destination = public_path('files/payslips/'.$id.'/'.$filename);

        //Storage::copy($file, $destination);

        //return view('payroll.payslip')
        //    ->withPath($destination);
    }

    public function payslipExists($id)
    {
    }

    public function generatePayslip($id)
    {
        abort_unless(auth()->user()->payroll, 403);

        $payslip = PayrollEmployee::findOrFail($id);
        abort_unless(auth()->user()->payroll || $payslip->employee_id == auth()->user()->employee_id, 403);

        //get data
        $res = DB::connection('tenant')->table('payroll_employees')
            ->leftJoin('employees', 'employees.id', '=', 'payroll_employees.employee_id')
            ->leftjoin('employee_jobs', 'employee_jobs.id', '=', 'payroll_employees.employee_job_id')
            ->leftjoin('designations', 'designations.id', '=', 'employee_jobs.designation_id')
            ->leftJoin('payroll_masters', 'payroll_masters.id', '=', 'payroll_employees.payroll_master_id')
            ->leftJoin('payment_modes', 'payroll_employees.payment_mode_id', 'payment_modes.id')
            ->select(
                'payroll_employees.id as id',
                'employees.name as emp_name',
                'employees.employee_code',
                'employees.id as emp_id',
                'designations.title as designation',
                'payroll_employees.tap as tap',
                'payroll_employees.scp as scp',
                'payroll_employees.epf as epf',
                'payroll_employees.tap_ee as tap_ee',
                'payroll_employees.scp_ee as scp_ee',
                'payroll_employees.epf_ee as epf_ee',
                'payroll_employees.base_salary as basic_salary',
                'payroll_employees.addition_amount as add_amt',
                'payroll_employees.deduction_amount as ded_amt',
                'payroll_employees.overtime_amount as ot_amt',
                'payroll_employees.doubletime_amount as dt_amt',
                'payroll_employees.unpaid_amount as up_amt',
                'employees.nationality_id as nationality_id',
                'payment_modes.title as payment_mode',
                'employees.date_joined as date_joined',
                'payroll_masters.start_date as created_at',
                'payroll_masters.start_date as start_date',
                'payroll_masters.end_date as end_date',
                'payroll_masters.company_id'
            )
            ->where('payroll_employees.id', '=', $id)
            ->get();

        // get fixed, deductions, additions data and company
        $alterations = PayrollTransaction::where('payroll_employees_id', '=', $id)
            ->whereNull('deleted_at')
            ->get();
        $fixed = $alterations->where('fixed', '=', true)->where('deleted_at', 'IS', null);
        $deductions = $alterations->where('addition', '=', false)->where('deleted_at', 'IS', null);
        $additions = $alterations->where('addition', '=', true)->where('fixed', '=', false)->where('deleted_at', 'IS', null);
        $company = Company::find($res[0]->company_id);

        //generate pdf file
        $ps = $res[0];
        $pdf = PDF::loadView('payslip', compact('ps', 'fixed', 'deductions', 'additions', 'company'))->setPaper('a4', 'Portrait');

        $payperiod = $res[0]->created_at;
        if (is_null($res[0]->employee_code)) {
            $fname = $res[0]->id.'-'.date('M-Y', strtotime($payperiod)).'.pdf';
        } else {
            $fname = $res[0]->employee_code.'-'.date('M-Y', strtotime($payperiod)).'.pdf';
        }

        //File::makeDirector(storage_path('payslips/'.$payslip->payroll_master_id));
        $exists = Storage::disk('local')->has('payslips'.$payslip->payroll_master_id);

        if (!$exists) {
            Storage::disk('local')->makeDirectory('payslips/'.$payslip->payroll_master_id);
        }

        $filename = storage_path('app/payslips/'.$payslip->payroll_master_id.'/'.$fname);
        $fname = 'app/payslips/'.$payslip->payroll_master_id.'/'.$fname;

        $pdf->save($filename);

        return $fname;
    }
}
