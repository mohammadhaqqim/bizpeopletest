<?php

namespace App\Http\ViewComposers;

use App\LeaveType;
use Illuminate\View\View;

class LeaveTypesViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $leaveTypes = LeaveType::whereNull('deleted_at')
                ->where('active', '=', true)
                ->with(['LeavePolicies'])
                ->get();
    
            $view->withLeaveTypes($leaveTypes);
        }
    }
}
