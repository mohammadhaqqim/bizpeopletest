<?php

namespace App\Http\ViewComposers;

use App\Department;
use Illuminate\View\View;

class DepartmentsViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $departments = Department::select('id', 'title')
                ->whereNull('deleted_at')
                ->get();

            $view->withDepartments($departments);
        }
    }
}
