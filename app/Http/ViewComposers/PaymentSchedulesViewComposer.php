<?php

namespace App\Http\ViewComposers;

use App\PaymentSchedule;
use Illuminate\View\View;

class PaymentSchedulesViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $paymentschedules = PaymentSchedule::select('id', 'title')
                ->whereNull('deleted_at')
                ->get();

            $view->withPaymentSchedules($paymentschedules);
        }
    }
}
