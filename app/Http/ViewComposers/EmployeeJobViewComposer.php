<?php

namespace App\Http\ViewComposers;

use App\EmployeeJob;
use Illuminate\View\View;

class EmployeeJobViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $employeeJobs = EmployeeJob::select('id', 'effective_date', 'department_id', 'division_id', 'designation_id', 'report_to_id')
                ->where('employee_id', '=', $id)
                ->whereNull('deleted_at')
                ->get()
                ->sortByDesc('effective_date');
    
            $view->withEmployeeJobs($employeeJobs);
        }
    }
}
