<?php

namespace App\Http\ViewComposers;

use App\PaymentType;
use Illuminate\View\View;

class PaymentTypesViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $paymentTypes = PaymentType::select('id', 'title')
                ->whereNull('deleted_at')
                ->get();

            $view->withPaymentTypes($paymentTypes);
        }
    }
}
