<?php

namespace App\Http\ViewComposers;

use App\Designation;
use Illuminate\View\View;

class DesignationsViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $designations = Designation::select('id', 'title')
                ->whereNull('deleted_at')
                ->get();

            $view->withDesignations($designations);
        }
    }
}
