<?php

namespace App\Http\ViewComposers;

use App\IcColor;
use Illuminate\View\View;

class ICColorsViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $icColors = IcColor::select('id', 'title')
                ->whereNull('deleted_at')
                ->get();
            
            $view->withIcColors($icColors);
        }
    }
}
