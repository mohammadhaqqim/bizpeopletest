<?php

namespace App\Http\ViewComposers;

use App\EmploymentStatus;
use Illuminate\View\View;

class EmploymentStatusesViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $employmentStatuses = EmploymentStatus::select('id', 'title')
                ->whereNull('deleted_at')
                ->orderBy('title')
                ->get();

            $view->withEmploymentStatuses($employmentStatuses);
        }
    }
}
