<?php

namespace App\Http\ViewComposers;

use App\PayrollMaster;
use Illuminate\View\View;

class PayrollMasterViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $payroll = PayrollMaster::find($id);

            $view->withPayroll($payroll);
        }
    }
}
