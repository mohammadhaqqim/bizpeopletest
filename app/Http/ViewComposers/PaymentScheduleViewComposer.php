<?php

namespace App\Http\ViewComposers;

use App\PaymentSchedule;
use Illuminate\View\View;

class PaymentScheduleViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $paySchedules = PaymentSchedule::select('id', 'title', 'periods_per_year', 'first_period_date')
                ->whereNull('deleted_at')
                ->get();

            $view->withPaySchedules($paySchedules);
        }
    }
}
