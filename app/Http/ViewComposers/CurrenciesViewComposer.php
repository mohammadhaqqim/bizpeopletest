<?php

namespace App\Http\ViewComposers;

use App\Country;
use Illuminate\View\View;

class CurrenciesViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $currencies = Country::select('id', 'currency_code')
                ->where('use_currency', '=', true)
                ->get();

            $view->withCurrencies($currencies);
        }
    }
}
