<?php

namespace App\Http\ViewComposers;

use App\Country;
use Illuminate\View\View;

class CountriesViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $countries = Country::select('id', 'currency_code', 'country', 'nationality')
//                ->where('use_currency', '=', true)
                ->whereNull('deleted_at')
                ->get();

            $view->withCountries($countries);
        }
    }
}
