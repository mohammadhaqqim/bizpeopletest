<?php

namespace App\Http\ViewComposers;

use App\Relationship;
use Illuminate\View\View;

class RelationshipsViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $relationships = Relationship::select('id', 'title')
                ->whereNull('deleted_at')
                ->get();

            $view->withRelationships($relationships);
        }
    }
}
