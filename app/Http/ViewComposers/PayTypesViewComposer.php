<?php

namespace App\Http\ViewComposers;

use App\Company;
use App\PayType;
use Illuminate\View\View;

class PayTypesViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $paytypes = PayType::select('id', 'title')
                ->whereNull('deleted_at')
                ->orderBy('title')
                ->get();

            $view->withPayTypes($paytypes);
        }
    }
}
