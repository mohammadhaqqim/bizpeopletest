<?php

namespace App\Http\ViewComposers;

use App\Bank;
use Illuminate\View\View;

class BanksViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $banks = Bank::select('id', 'title')
                ->whereNull('deleted_at')
                    ->get();

            $view->withBanks($banks);
        }
    }
}
