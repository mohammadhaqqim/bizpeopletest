<?php

namespace App\Http\ViewComposers;

use App\Transaction;
use Illuminate\View\View;

class TransactionsViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $transactions = Transaction::select('id', 'title', 'addition', 'calculated', 'fixed')
                ->whereNull('deleted_at')
                ->get();

            $view->withTransactions($transactions);
        }
    }
}
