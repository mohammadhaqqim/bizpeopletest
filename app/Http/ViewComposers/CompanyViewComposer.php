<?php

namespace App\Http\ViewComposers;

use App\Company;
use Illuminate\View\View;

class CompanyViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            //dd(cache());

            $company = Company::find(1);
            /*             $company = cache()->remember('company', 24*60, function () {
                            return Company::find(1);
                        }); */
            /*             $company = Company::all()
                            ->first();
             */
    
            $view->withCompany($company);
            //end_fianancial_date
        }
    }
}
