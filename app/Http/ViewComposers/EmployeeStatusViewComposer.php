<?php

namespace App\Http\ViewComposers;

use App\EmploymentStatus;
use Illuminate\View\View;
use Illuminate\Support\Facades\DB;

class EmployeeStatusViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $employmentStatus = EmploymentStatus::select('id', 'title')
                ->whereNull('deleted_at')
                ->get();
    
            $view->withEmployeeStatus($employmentStatus);
        }
    }
}
