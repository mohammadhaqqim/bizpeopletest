<?php

namespace App\Http\ViewComposers;

use App\LeaveType;
use Illuminate\View\View;

class LeaveViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $leavetypes = LeaveType::whereNull('deleted_at')
                ->with(['LeavePolicies'])
                ->get();

            $view->withLeavetypes($leavetypes);
        }
    }
}
