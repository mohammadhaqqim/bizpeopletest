<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\EndEmploymentReason;

class EndEmploymentViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $endemployments = EndEmploymentReason::select('id', 'title')
                ->whereNull('deleted_at')
                ->get();

            $view->withEndEmployments($endemployments);
        }
    }
}
