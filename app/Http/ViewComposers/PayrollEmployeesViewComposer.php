<?php

namespace App\Http\ViewComposers;

use App\PayrollEmployee;
use Illuminate\View\View;

class PayrollEmployeesViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $employeePayrolls = PayrollEmployee::where('payroll_master_id', $id)
                ->whereNull('deleted_at')
                ->with('Employee')
                ->get();
            
            $view->withEmployeePayrolls($employeePayrolls);
        }
    }
}
