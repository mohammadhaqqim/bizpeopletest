<?php

namespace App\Http\ViewComposers;

use App\Gender;
use Illuminate\View\View;

class GenderViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $genders = Gender::all();

            $view->withGenders($genders);
        }
    }
}
