<?php

namespace App\Http\ViewComposers;

use App\Employee;
use Illuminate\View\View;

class ManagersViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $managers = Employee::whereNull('deleted_at')
                ->where(function ($query) {
                    $query->where('hod', true)
                        ->orWhere('manager', true);
                })->get();

            $view->withManagers($managers);
        }
    }
}
