<?php

namespace App\Http\ViewComposers;

use App\Religion;
use Illuminate\View\View;

class ReligionsViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $religions = Religion::select('id', 'title')
                ->whereNull('deleted_at')
                ->orderBy('title')
                ->get();
                
            $view->withReligions($religions);
        }
    }
}
