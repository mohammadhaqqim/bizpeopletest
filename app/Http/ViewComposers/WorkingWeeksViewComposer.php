<?php

namespace App\Http\ViewComposers;

use App\WorkingWeek;
use Illuminate\View\View;

class WorkingWeeksViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $workingWeeks = WorkingWeek::select('id', 'title')
                ->whereNull('deleted_at')
                ->get();

            $view->withWorkingWeeks($workingWeeks);
        }
    }
}
