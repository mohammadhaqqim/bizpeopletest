<?php

namespace App\Http\ViewComposers;

use App\MaritalStatus;
use Illuminate\View\View;

class MaritalStatusViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $maritalStatus = MaritalStatus::select('id', 'title')
                ->whereNull('deleted_at')
                ->get();

            $view->withMaritalStatus($maritalStatus);
        }
    }
}
