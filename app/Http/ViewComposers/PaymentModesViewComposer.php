<?php

namespace App\Http\ViewComposers;

use App\PaymentMode;
use Illuminate\View\View;

class PaymentModesViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $paymentmodes = PaymentMode::select('id', 'title')
                ->whereNull('deleted_at')
                ->get();

            $view->withPaymentmodes($paymentmodes);
        }
    }
}
