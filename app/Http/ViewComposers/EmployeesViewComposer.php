<?php

namespace App\Http\ViewComposers;

use App\Employee;
use Illuminate\View\View;

class EmployeesViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $employees = Employee::select('id', 'name', 'employee_code')
                ->whereNull('deleted_at')
                ->with(['currentjob','currentStatus'])
                ->get();
            
            $view->withEmployees($employees);
        }
    }
}
