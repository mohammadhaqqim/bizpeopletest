<?php

namespace App\Http\ViewComposers;

use App\ReportingPeriod;
use Illuminate\View\View;

class PeriodsViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $periods = ReportingPeriod::whereNull('deleted_at')
                ->get();

            $view->withPeriods($periods);
        }
    }
}
