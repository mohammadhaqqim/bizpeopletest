<?php

namespace App\Http\ViewComposers;

use App\Company;
use Illuminate\View\View;

class CustomDataViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $company = Company::all()
                ->first();

            $view->withCompany($company);
        }
    }
}
