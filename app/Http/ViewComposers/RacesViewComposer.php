<?php

namespace App\Http\ViewComposers;

use App\Race;
use Illuminate\View\View;

class RacesViewComposer
{
    /**
     * Bind Data to view
     */
    public function compose(View $view)
    {
        if (auth()->check()) {
            $races = Race::select('id', 'title')
                ->whereNull('deleted_at')
                ->orderBy('title')
                ->get();

            $view->withRaces($races);
        }
    }
}
