<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Http\ViewComposers\BanksViewComposer;
use App\Http\ViewComposers\LeaveViewComposer;
use App\Http\ViewComposers\RacesViewComposer;
use App\Http\ViewComposers\GenderViewComposer;
use App\Http\ViewComposers\CompanyViewComposer;
use App\Http\ViewComposers\PeriodsViewComposer;
use App\Http\ViewComposers\ICColorsViewComposer;
use App\Http\ViewComposers\ManagersViewComposer;
use App\Http\ViewComposers\PayTypesViewComposer;
use App\Http\ViewComposers\CountriesViewComposer;
use App\Http\ViewComposers\EmployeesViewComposer;
use App\Http\ViewComposers\ReligionsViewComposer;
use App\Http\ViewComposers\CurrenciesViewComposer;
use App\Http\ViewComposers\LeaveTypesViewComposer;
use App\Http\ViewComposers\NavigationViewComposer;
use App\Http\ViewComposers\UserAccessViewComposer;
use App\Http\ViewComposers\DepartmentsViewComposer;
use App\Http\ViewComposers\EmployeeJobViewComposer;
use App\Http\ViewComposers\LeavePolicyViewComposer;
use App\Http\ViewComposers\DesignationsViewComposer;
use App\Http\ViewComposers\PaymentModesViewComposer;
use App\Http\ViewComposers\PaymentTypesViewComposer;
use App\Http\ViewComposers\TransactionsViewComposer;
use App\Http\ViewComposers\WorkingWeeksViewComposer;
use App\Http\ViewComposers\EndEmploymentViewComposer;
use App\Http\ViewComposers\MaritalStatusViewComposer;
use App\Http\ViewComposers\PayrollMasterViewComposer;
use App\Http\ViewComposers\RelationshipsViewComposer;
use App\Http\ViewComposers\EmployeeStatusViewComposer;
use App\Http\ViewComposers\PaymentScheduleViewComposer;
use App\Http\ViewComposers\PaymentSchedulesViewComposer;
use App\Http\ViewComposers\PayrollEmployeesViewComposer;
use App\Http\ViewComposers\EmploymentStatusesViewComposer;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        View::composer(
            ['settings.timeoff'],
            LeaveViewComposer::class
        );

        View::composer(
            ['settings.company',
            'job.edit',
            'payroll.approval.index',
            'payroll.details.edit',
            'components._nav',
            ],
            CompanyViewComposer::class
        );
        
        View::composer(
            ['job.edit', ],
            EndEmploymentViewComposer::class
        );

        View::composer(
            ['settings.company'],
            PeriodsViewComposer::class
        );

        View::composer(
            ['settings.company',
            'employee.create',
            'employee.edit',],
            CountriesViewComposer::class
        );

        View::composer(
            ['employee.timeoff.index',
            'employee.timeoff.create',
            'employee.inlieu.create',
            'employee.timeoff.edit'],
            LeaveTypesViewComposer::class
        );

        View::composer(
            [
            'payroll.employee.index',
            'settings.useraccess',
        //    'payroll.details.edit'
        ],
            EmployeesViewComposer::class
        );

        View::composer(
            ['settings.company',
            'job.edit', ],
            PaymentScheduleViewComposer::class
        );
        
        View::composer(
            ['settings.company'],
            CountriesViewComposer::class
        );

        View::composer(
            ['employee.create'],
            PayTypesViewComposer::class
        );

        View::composer(
            ['components.modal.edit_employee_job',
            'components.modal.edit_employee_job',
            'components.employee_job',
            'employee.create',
            'job.edit',
        ],
            DepartmentsViewComposer::class
        );

        View::composer(
            ['components.modal.edit_employee_job',
            'components.modal.edit_employee_job',
            'components.employee_job',
            'employee.create',
            'job.edit',
        ],
            DesignationsViewComposer::class
        );

        View::composer(
            ['components.modal.edit_employee_job',
            'components.modal.edit_employee_job',
            'components.employee_job',
            'employee.create',
            'job.edit',
        ],
            ManagersViewComposer::class
        );
        
        View::composer(
            ['components.modal.add_employee_payment',
            'components.modal.edit_employee_payment',
            'employee_job',
            'employee.create',
            'job.edit',
        ],
            PaymentTypesViewComposer::class
        );

        View::composer(
            ['components.modal.add_employee_payment',
            'components.modal.edit_employee_payment',
            'employee_job',
            'job.edit',
        ],
            CurrenciesViewComposer::class
        );

        View::composer(
            ['components._nav',
            'components.nav', ],
            NavigationViewComposer::class
        );

        View::composer(
            ['employee.create',
            'job.edit',
            'payroll.details.edit'
        ],
            BanksViewComposer::class
        );

        View::composer(
            ['employee.create',
            'job.edit', ],
            PaymentSchedulesViewComposer::class
        );

        View::composer(
            ['employee.create',
            'job.edit',
            'payroll.employee.index',
            'payroll.details.edit'],
            PaymentModesViewComposer::class
        );

        View::composer(
            ['employee.edit',
            'employee.create',
            'job.edit',
        ],
            EmploymentStatusesViewComposer::class
        );

        View::composer(
            ['employee.edit',
            'employee.create',],
            RacesViewComposer::class
        );

        View::composer(
            ['employee.edit',
            'employee.create',],
            ICColorsViewComposer::class
        );

        View::composer(
            ['employee.edit',
            'job.edit',
            'employee.timeoff.index',
            ],
            WorkingWeeksViewComposer::class
        );

        View::composer(
            ['employee.edit',
            'employee.create',],
            ReligionsViewComposer::class
        );

        View::composer(
            'settings.useraccess',
            UserAccessViewComposer::class
        );

        View::composer(
            ['components.modal.edit_employee_status',
            'components.modal.edit_employee_status',
            'components.modal.employee_status',
        ],
            EmployeeStatusViewComposer::class
        );

        View::composer(
            ['components.modal.add_leave_type',
            'components.modal.leave_summary', ],
            LeavePolicyViewComposer::class
        );

        View::composer(
            ['components.modal.add_relation', 'components.modal.edit_relation'],
            RelationshipsViewComposer::class
        );

        View::composer(
            ['components.modal.add_transaction_add',
            'components.modal.add_transaction_ded',
            'payroll.transactions.edit'],
            TransactionsViewComposer::class
        );

        View::composer(
            ['employee.create',
            'employee.edit'],
            GenderViewComposer::class
        );

        View::composer(
            ['employee.create',
            'employee.edit'],
            MaritalStatusViewComposer::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
