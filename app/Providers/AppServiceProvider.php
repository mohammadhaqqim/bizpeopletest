<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Http\ViewComposers\NavigationViewComposer;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        /*         View::composers(
                    [
                        'App\Http\Views\Composers\AnimalComposer' => ['animal.edit', ' animal.create']
                    ]);
         */

/*         View::composer(
            'components._nav',
            NavigationViewComposer::class
        );

        View::composer(
            'settings.useraccess',
            UserAccessViewComposer::class
        ); */
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        View::composer(['employee.index', 'payroll.employee.index'], EmployeesViewComposer::class);
//        View::composer('payroll.index', PayrollMastersViewComposer::class);
        
        //
        
        Schema::defaultStringLength(191);
    }
}
