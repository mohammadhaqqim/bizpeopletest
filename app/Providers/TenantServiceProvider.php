<?php

namespace App\Providers;

use App\Tenant\Manager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use App\Console\Commands\Tenant\Migrate;
use App\Tenant\Database\DatabaseManager;

class TenantServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Migrate::class, function ($app) {
            return new Migrate($app->make('migrator'), $app->make(DatabaseManager::class));
        });

        /*         $this->app->singleton(MigrateRollback::class, function ($app) {
                    return new MigrateRollback($app['migrator'], $app[Dispatcher::class], $app[DatabaseManager::class]);
                }); */
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(Manager::class, function () {
            return new Manager();
        });

        Request::macro('tenant', function () {
            return app(Manager::class)->getTenant();
        });

        Request::macro('access', function () {
            return app(Manager::class)->getUserAccess();
        });

        Blade::if('tenant', function () {
            return app(Manager::class)->hasTenant();
        });
    }
}
