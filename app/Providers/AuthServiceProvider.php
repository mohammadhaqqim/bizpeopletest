<?php

namespace App\Providers;

use App\User;
use App\Employee;
use App\Tenant\Manager;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        'App\Employee' => 'App\Policies\EmployeePolicy',
        'App\EmployeeStatus' => 'App\Policies\EmployeeStatusPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function () {
            if (!is_null(app(Manager::class)->getUserAccess())) {
                if (app(Manager::class)->getUserAccess()->admin) {
                    return true;
                }
            }
        });

        /*         Gate::before(function (User $user) {
                    if ($user->admin) {
                        return true;
                    }
                });
         */
        Gate::define('connected-database', function () {
            return is_null(session()->get('tenant'));
        });
        
        Gate::define('update-personal', function () {
            return (app(Manager::class)->getUserAccess()->payroll
            || app(Manager::class)->getUserAccess()->hr
            || app(Manager::class)->getUserAccess()->admin);
        });
        
        Gate::define('update-payroll', function () {
            return app(Manager::class)->getUserAccess()->payroll;
        });

        Gate::define('update-hr', function () {
            return app(Manager::class)->getUserAccess()->hr;
        });

        Gate::define('update-admin', function () {
            if (is_null(app(Manager::class)->getUserAccess())) {
                return false;
            }

            return app(Manager::class)->getUserAccess()->admin;
        });

        /*         Gate::define('update-admin', function () {
                    return auth()->user()->admin;
                });
         */
        Gate::define('update-manager', function (Employee $employee) {
            //reports to current user - manages employee
//            app(Manager::class)->getUserAccess()->employee_id
//            if ($employee->ReportsTo->is(auth()->user()->employee) || app(Manager::class)->getUserAccess()->hr) {
            if ($employee->ReportsTo->is(app(Manager::class)->getUserAccess()->employee_id)
                || app(Manager::class)->getUserAccess()->hr) {
                return true;
            }

            // NEEDS TESTING
            //HOD and Same Department
            if (app(Manager::class)->getUserAccess()->hod && $employee->getDepartment() == app(Manager::class)->getUserAccess()->employee->getDepartment()) {
                return true;
            }

            //
            return false;
        });
    }
}
