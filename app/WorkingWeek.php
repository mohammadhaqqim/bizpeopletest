<?php

namespace App;

use Illuminate\Support\Collection;
use PhpParser\ErrorHandler\Collecting;
use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class WorkingWeek extends Model
{
    use ForTenants;

    //
    protected $guarded = [];
        
    public function getTotal()
    {
        return $this->mon+$this->tue+$this->wed+$this->thu+$this->fri+$this->sat+$this->sun;
    }

    public function getDaysOff()
    {
        $days = new Collection();
        
        if ($this->mon == 0) {
            $days->push(1);
        }

        if ($this->tue == 0) {
            $days->push(2);
        }

        if ($this->wed == 0) {
            $days->push(3);
        }

        if ($this->thu == 0) {
            $days->push(4);
        }

        if ($this->fri == 0) {
            $days->push(5);
        }

        if ($this->sat == 0) {
            $days->push(6);
        }

        if ($this->sun == 0) {
            $days->push(0);
        }

        return $days;
    }

    public function getAverageDay()
    {
        return $this->avg;
    }
}
