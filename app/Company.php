<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;
use App\Tenant\Tenant;
use App\Tenant\Traits\isTenant;

class Company extends Model
{
    use ForTenants;

    protected $guarded = [];
    
    public function Owner()
    {
        return $this->belongsTo('App\User', 'owner_id');
    }

    public function Country()
    {
        return $this->belongsTo('App\Country', 'address_country_id', 'id');
    }

    public function PayPeriod()
    {
        return $this->belongsTo('App\PaymentSchedule', 'payment_period_id', 'id');
    }

    public function ReportingPeriod()
    {
        return $this->belongsTo('App\ReportingPeriod', 'reporting_period_id', 'id');
    }

    public function PaymentType()
    {
        return $this->belongsTo('App\PaymentType', 'payment_type_id', 'id');
    }

    public function getLogo()
    {
        return $this->logo;
    }
}
