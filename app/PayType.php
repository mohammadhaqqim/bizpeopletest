<?php

namespace App;

use App\Tenant\Traits\ForTenants;
use Illuminate\Database\Eloquent\Model;

class PayType extends Model
{
    use ForTenants;

    //
    protected $guarded = [];
}
