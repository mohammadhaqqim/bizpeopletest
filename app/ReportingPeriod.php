<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class ReportingPeriod extends Model
{
    public function getConnectionName()
    {
        return 'tenant';
    }
//    protected $table = 'reporting_periods';

    //
    protected $guarded = [];
}
