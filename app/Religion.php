<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class Religion extends Model
{
    use ForTenants;

    //
    protected $guarded = [];
}
