<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class Bank extends Model
{
    use ForTenants;

    //
    protected $guarded = [];
}
