<?php

namespace App;

use App\Tenant\Traits\isTenant;
use Illuminate\Database\Eloquent\Model;

class MainCompany extends Model
{
    use isTenant;

    //protected $table = 'companies_main';

    protected $guarded = [];
}
