<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class EmployeeRelation extends Model
{
    use ForTenants;

    //
    protected $guarded = [];
        
    public function Employee()
    {
        return $this->belongsTo('App\Employee');
    }
    
    public function Relationship()
    {
        return $this->belongsTo('App\Relationship');
    }

    public function RelationExtras()
    {
        return $this->hasOne('App\RelationExtras', 'relation_id', 'id');
    }
}
