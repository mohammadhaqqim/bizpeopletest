<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class RelationExtras extends Model
{
    use ForTenants;

    //
    protected $guarded = [];

    public function Relation()
    {
        return $this->belongsTo('App\EmployeeRelation', 'relation_id');
    }
}
