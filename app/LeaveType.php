<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class LeaveType extends Model
{
    use ForTenants;

    //
    protected $guarded = [];

    public function LeavePolicies()
    {
        return $this->hasMany('App\LeavePolicy', 'leave_type_id');
    }

    public function getCurrentEmployeePolicy($employee_id)
    {
        // get id
        $policy_id = DB::connection('tenant')->table('employee_leave_policies')
            ->join('leave_policies', 'leave_policies.id', '=', 'employee_leave_policies.leave_policy_id')
            ->where('employee_leave_policies.employee_id', '=', $employee_id)
            ->where('leave_policies.leave_type_id', '=', $this->id)
            ->where('start_from_date', '<=', \Carbon\Carbon::now())
            ->select('employee_leave_policies.id as id')
            ->orderByDesc('start_from_date')
            ->first();

        if (is_null($policy_id)) {
            return null;
        }

        $current_policy = EmployeeLeavePolicy::where('id', '=', $policy_id->id)
            ->first();
        
        return $current_policy;
    }

    /*     public function getEmployeePolicyAvailable($employee_id)
        {
            $policy_id = $this->getCurrentEmployeePolicy($employee_id);

            if (is_null($policy_id)) {
                return null;
            }

            $current_policy = EmployeeLeavePolicy::where('id', '=', $policy_id->id)
                ->first();

            return $current_policy->available;
        } */
}
