<?php

namespace App;

use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class EmployeeLeavePolicy extends Model
{
    use ForTenants;
    
    //
    protected $guarded = [];
    
    public function Employee()
    {
        return $this->belongsTo('App\Employee');
    }

    public function LeavePolicy()
    {
        return $this->belongsTo('App\LeavePolicy');
    }

    public function updateAvailable()
    {
    }

    public function setLeaveValues()
    {
        $schedule = $this->LeavePolicy->Schedules;

        $days = $this->getDays($this->start_from_date);

        // find the number of months
        if ($schedule[0]->amount_accrued_type == 'm') {
            $days = floor($days / 30);
        }

        $this->available = $days * $schedule[0]->amount_accrued;

        if ($this->available > $schedule[0]->max_accrual && $schedule[0]->max_accrual > 0) {
            $this->available = $schedule[0]->max_accrual;
        }

        $this->getBalance();

        return;
    }

    // update the balance
    private function getBalance()
    {
        $this->balance = $this->carryover + $this->available - $this->taken;
        $this->save();

        return $this->balance;
    }

    private function getDays($start_date)
    {
        $datetime2 = \Carbon\Carbon::now();
        $days = $datetime2->diffInDays($start_date);

        return $days;
    }

    // update the scheduled time for the policy
    public function getScheduledTime()
    {
        // start date
        $start = $this->start_from_date;

        $company = Company::find(1);

        $end_period = new \Carbon\Carbon($company->end_financial_date);
        $start_period = new \Carbon\Carbon($company->end_financial_date);
        $start_period->addDay();
        $start_period->addYear(-1);

        if ($start < $start_period) {
            $start = $start_period;
        }

        $leave = DB::connection('tenant')->table('employee_days_offs')
            ->join('employee_leaves', 'employee_leaves.id', '=', 'employee_days_offs.employee_leave_id')
            ->where('employee_leaves.leave_type_id', '=', $this->LeavePolicy->leave_type_id)
            ->where('off_day', '>', $start)
            ->where('off_day', '<=', $end_period)
            ->where('employee_id', '=', $this->employee_id)
            ->whereNull('employee_leaves.approved_by_id')
            ->whereNull('employee_leaves.rejected_by_id')
            ->whereNull('employee_leaves.deleted_at')
            ->select(
                'employee_days_offs.off_day',
                'employee_days_offs.work_hours',
                'employee_days_offs.off_hours',
                'employee_days_offs.day_off'
            )
            ->get();

        $type = LeaveType::find($this->LeavePolicy->leave_type_id);
        
        if ($type->track_time == 'h') {
            $this->scheduled = $leave->sum('off_hours');
        } else {
            $this->scheduled = $leave->sum('day_off');
        }

        return $this->scheduled;
    }

    public function getRecalculateTotals()
    {
        //calculate carryover amount
        $this->getCarryOverAmount();
//        dd('one');
        //calculate available amount
        $this->getAvailableTime();
        //calculate take amount - approved and in past
        $this->totals();
//        $this->getTakenTime();
        //calculate scheduled amount - all future time off
//        $this->getScheduledTime();
        // calaulate the balance
        $this->getBalance();
    }

    public function totals()
    {
        // start date
        $start = $this->start_from_date;

        $company = Company::find(1);

        $end_period = new \Carbon\Carbon($company->end_financial_date);
        $start_period = new \Carbon\Carbon($company->end_financial_date);
        $start_period->addDay();
        $start_period->addYear(-1);

        if ($start < $start_period) {
            $start = $start_period;
        }

        $leave = DB::connection('tenant')->table('employee_days_offs')
            ->join('employee_leaves', 'employee_leaves.id', '=', 'employee_days_offs.employee_leave_id')
            ->where('employee_leaves.leave_type_id', '=', $this->LeavePolicy->leave_type_id)
            ->where('off_day', '>', $start_period)
            ->where('off_day', '<=', $end_period)
            ->where('employee_id', '=', $this->employee_id)
            ->whereNull('employee_leaves.deleted_at')
            //taken
            ->selectRaw('sum( case when approved_by_id > 0 then off_hours end) as approved_h')
            ->selectRaw('sum( case when approved_by_id > 0 then day_off end) as approved_d')
            //scheduled
            ->selectRaw('sum( case when approved_by_id is null and rejected_by_id is null then off_hours end) as scheduled_h')
            ->selectRaw('sum( case when approved_by_id is null and rejected_by_id is null then day_off end) as scheduled_d')
            ->get();

//        dd($leave[0]);

        $type = LeaveType::find($this->LeavePolicy->leave_type_id);
        
        if ($type->track_time == 'h') {
            $this->taken = $leave[0]->approved_h;
            $this->scheduled = $leave[0]->scheduled_h;
        } else {
            $this->taken = $leave[0]->approved_d;
            $this->scheduled = $leave[0]->scheduled_d;
        }

        $this->balance = $this->carryover + $this->available - $this->taken;
    
        return $this->taken;
    }


    public function getTakenTime()
    {
        // start date
        $start = $this->start_from_date;

        $company = Company::find(1);

        $end_period = new \Carbon\Carbon($company->end_financial_date);
        $start_period = new \Carbon\Carbon($company->end_financial_date);
        $start_period->addDay();
        $start_period->addYear(-1);

        if ($start < $start_period) {
            $start = $start_period;
        }

        $leave = DB::connection('tenant')->table('employee_days_offs')
            ->join('employee_leaves', 'employee_leaves.id', '=', 'employee_days_offs.employee_leave_id')
            ->where('employee_leaves.leave_type_id', '=', $this->LeavePolicy->leave_type_id)
            ->where('off_day', '>', \Carbon\Carbon::now())
            ->where('off_day', '<=', $end_period)
            ->where('employee_id', '=', $this->employee_id)
            ->where('employee_leaves.approved_by_id', '>', 0)
            ->whereNull('employee_leaves.deleted_at')
            ->select(
                'employee_days_offs.off_day',
                'employee_days_offs.work_hours',
                'employee_days_offs.off_hours',
                'employee_days_offs.day_off'
            )
            ->get();

        $type = LeaveType::find($this->LeavePolicy->leave_type_id);
        
        if ($type->track_time == 'h') {
            $this->taken = $leave->sum('off_hours');
        } else {
            $this->taken = $leave->sum('day_off');
        }
    
        return $this->taken;
    }

    public function getAvailableTime()
    {
        $company = Company::find(1);

        $first_period = $company->first_period_date;//request()->session->get('first_period_date');
        $periods = $company->periods_per_year;//request()->session->get('periods_per_year');

        $hired = $this->Employee->date_joined;

        // if the hired date is after first period then calculate available from
        // hired date not the first period
        if ($hired > $first_period) {
            $first_period = $hired;
        }

        $period_no = $this->getNoPeriods(\Carbon\Carbon::now(), $first_period, $periods);

        if ($hired > $first_period && $this->LeavePolicy->first_accrual == 'p') {
            $s_date = $this->getFirstDatePeriod($hired, $first_period, $periods);
            $e_date = $this->getLastDatePeriod($hired, $first_period, $periods);
            $daysOff = $this->getWorkingDaysOff();

            $days = $this->getWorkDays($s_date, $e_date, $daysOff);
            $worked = $this->getWorkDays($hired, $e_date, $daysOff);

            $period_no = $period_no - (1 - $worked / $days);
        }

        // if accrual occurs at begining of month
        if ($this->LeavePolicy->accrual_occurs == 'b') {
            $period_no++;
        }

        $amount = $period_no * $this->LeavePolicy->yearly_amount / $periods;
        
        //get negative from days_off table - added days from admin ie In Lieu days
        $additional = $this->getNegativeHours();
        $amount -= $additional;

        $this->available = round($amount, 2);

        return round($amount, 2);
    }

    private function getNegativeHours()
    {
        $company = Company::find(1);

        $leave = DB::connection('tenant')->table('employee_days_offs')
            ->join('employee_leaves', 'employee_leaves.id', '=', 'employee_days_offs.employee_leave_id')
            ->where('employee_leaves.leave_type_id', '=', $this->LeavePolicy->leave_type_id)
            ->where('off_day', '>', $company->first_period_date)
            ->where('off_day', '<=', $company->end_financial_date)
            ->where('employee_id', '=', $this->employee_id)
            ->where('off_hours', '<', 0)
            ->where('employee_leaves.approved_by_id', '>', 0)
            ->whereNull('employee_leaves.deleted_at')
            ->select(
                'employee_days_offs.off_day',
                'employee_days_offs.work_hours',
                'employee_days_offs.off_hours',
                'employee_days_offs.day_off'
            )
            ->get();

        if (is_null($leave)) {
            return 0;
        }
                
        return $leave->sum('off_hours');
    }

    public function getCarryOverAmount()
    {
        $start = $this->start_from_date;

        $company = Company::find(1);
        //dd($company);

        $end_period = new \Carbon\Carbon($company->end_financial_date);
        $start_period = new \Carbon\Carbon($company->end_financial_date);
        $start_period->addDay();
        $start_period->addYear(-1);

        if ($start < $start_period) {
            $start = $start_period;
        }

        $leave = DB::connection('tenant')->table('employee_days_offs')
            ->join('employee_leaves', 'employee_leaves.id', '=', 'employee_days_offs.employee_leave_id')
            ->where('employee_leaves.leave_type_id', '=', $this->LeavePolicy->leave_type_id)
            ->where('off_day', '>', $start)
            ->where('off_day', '<=', $end_period)
            ->where('carryover', '=', 1)
            ->where('employee_id', '=', $this->employee_id)
            ->where('employee_leaves.approved_by_id', '>', 0)
            ->whereNull('employee_leaves.deleted_at')
            ->select(
                'employee_days_offs.off_day',
                'employee_days_offs.work_hours',
                'employee_days_offs.off_hours',
                'employee_leaves.days_off',
            )
            ->get();

        $type = LeaveType::find($this->LeavePolicy->leave_type_id);
        
        if ($type->track_time == 'h') {
            $this->carryover = $leave->sum('off_hours');
        } else {
            $this->carryover = $leave->sum('day_off');
        }
    
        return $this->carryover;
    }

    // get reporting times for company
    public function getReportingPeriod()
    {
        $company = Company::find(1);
        //dd($company);
        //session(['company' => $company]);
        session(['use_tap' => $company->use_tap]);
        session(['use_scp' => $company->use_scp]);
        session(['use_epf' => $company->use_epf]);
        session(['company_name' => $company->company_name]);
        session(['logo' => $company->logo]);
        session(['address_country_id' => $company->address_country_id]);
        session(['first_period_date' => $company->first_period_date]);
        session(['end_financial_date' => $company->end_financial_date]);
        session(['periods_per_year' => $company->periods_per_year]);
        session(['race_id' => $company->race_id]);
        session(['religion_id' => $company->religion_id]);
        session(['nationality_id' => $company->nationality_id]);
        session(['payment_mode_id' => $company->payment_mode_id]);
        session(['bank_id' => $company->bank_id]);
        session(['employment_status_id' => $company->employment_status_id]);
        session(['payment_type_id' => $company->payment_type_id]);
        session(['designation_id' => $company->designation_id]);
        session(['department_id' => $company->department_id]);
    }

    // get the working days from - start, to - end, with array of days off
    // mon - 1, sat - 6, sun -7
    public function getWorkDays($start, $end, $daysOff)
    {
        $workdays = 0;

        for ($i = $start; $i <= $end; $i = \Carbon\Carbon::parse($i)->addDay(1)) {
            //$day = date("w", $i);
            $day = \Carbon\Carbon::parse($i)->format('N');
            if (!in_array($day, $daysOff)) {
                $workdays++;
            }
        }

        return $workdays;
    }

    // good
    // get an array of days that are not worked for employee of this policy
    public function getWorkingDaysOff()
    {
        $week = $this->employee->WorkingWeek;
        //return $week;

        $daysOff = [];

        if ($week->mon == 0) {
            array_push($daysOff, '1');
        }
        if ($week->tue == 0) {
            array_push($daysOff, '2');
        }
        if ($week->wed == 0) {
            array_push($daysOff, '3');
        }
        if ($week->thu == 0) {
            array_push($daysOff, '4');
        }
        if ($week->fri == 0) {
            array_push($daysOff, '5');
        }
        if ($week->sat == 0) {
            array_push($daysOff, '6');
        }
        if ($week->sun == 0) {
            array_push($daysOff, '7');
        }

        return $daysOff;
    }

    private function getPartialPeriod($hired, $first_date, $periods)
    {
        $hired_period_start = $this->getFirstDatePeriod($hired, $first_date, $periods);
        $days_worked = $hired->diffInDays($hired_period_start);
        $days_per_period = 365 / $periods;

        $part = $days_worked/$days_per_period;

        // daily / weekly / monthly
        switch ($this->LeavePolicy->Schedules[0]->amount_accural_type) {
            case 'd':
                //$part = $part *
                break;

            case 'w':
                break;
            
            case 'm':
                break;

        }
    }

    // good
    // find the period starting date for current_date,
    // first_date - first day of financial year
    // periods - periods for the year
    private function getFirstDatePeriod($current_date, $first_date, $periods)
    {
        /*         $dt = Carbon::parse($current_date);
                //dd($first_date);
                dd($dt->diff($first_date));
         */
        // days between date and first_date
        $int = Carbon::parse($current_date)->diff($first_date);

        if ($periods == 12) {
            $period_no = $int->m;
            $period_days = floor(365/12);
        } else {
            $period_days = $this->getDaysForPeriod($periods);
            $days = $int->d;
               
            //$period_days = $periods == 52 ? 7 : 14;
            $period_no = floor($days / $period_days);
        }

        $days = $period_no * $period_days - 1;

        return \Carbon\Carbon::parse($first_date)->addDay($days);
    }

    // get the last day of the period for the current_date
    // first_date - first day of financial year
    // periods - periods for the year
    public function getLastDatePeriod($current_date, $first_date, $periods)
    {
        // days between date and first_date
        $int = Carbon::parse($current_date)->diff($first_date);
        $days = $int->d;

        if ($periods == 12) {
            $period_no = $int->m;
            $period_days = floor(365/12);
        } else {
            $period_days = $this->getDaysForPeriod($periods);
               
            //$period_days = $periods == 52 ? 7 : 14;
            $period_no = floor($days / $period_days);
        }

        $period_no++;

        $days = $period_no * $period_days - 2;

        return \Carbon\Carbon::parse($first_date)->addDay($days);
    }

    // good
    // get the average number of days for a given period
    private function getDaysForPeriod($periods)
    {
        $period_days = 30;

        switch ($periods) {
            case 13:
                $period_days = 28;
                break;
            case 24:
                $period_days = 15;
                break;
            case 26:
                $period_days = 14;
                break;
            case 52:
                $period_days = 7;
                break;
        }

        return $period_days;
    }

    //good
    // get the no of periods from current_date to first_date with #periods
    public function getNoPeriods($current_date, $first_date, $periods)
    {
        $int = Carbon::parse($current_date)->diff($first_date);

        if ($periods == 12) {
            $period_no = $int->m;
        } else {
            // days between date and first_date
            $days = $int->d;
            $period_days = $this->getDaysForPeriod($periods);

            //$period_days = $periods == 52 ? 7 : 14;
            $period_no = floor($days / $period_days);
        }

        return $period_no;
    }
}
