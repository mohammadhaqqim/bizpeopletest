<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class EmployeeDaysOff extends Model
{
    use ForTenants;

    //
    protected $guarded = [];
}
