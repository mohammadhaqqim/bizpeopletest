<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class EmployeeJob extends Model
{
    use ForTenants;
    
    //
    protected $guarded = [];
    
    public function Department()
    {
        return $this->belongsTo('App\Department');
    }

    public function Designation()
    {
        return $this->belongsTo('App\Designation');
    }

    //???
    public function ReportsTo()
    {
        return $this->belongsTo('App\Employee', 'report_to_id');
    }
}
