<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class EmploymentType extends Model
{
    use ForTenants;
    //
}
