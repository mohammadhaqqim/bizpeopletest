<?php

namespace App;
use Carbon\Carbon;
use DateTime;


use Illuminate\Database\Eloquent\Model;

class UserLoginAudit extends Model
{
    //
    protected $guarded = [];

    public function Employee()
    {
        return $this->belongsTo('App\Employee');
    }

    public function User()
    {
        return $this->belongsTo('App\User');
    }

    
}
