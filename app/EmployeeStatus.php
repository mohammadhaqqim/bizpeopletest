<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class EmployeeStatus extends Model
{
    use ForTenants;

    //
    protected $guarded = [];

    public function StatusTitle()
    {
        return $this->belongsTo('App\EmploymentStatus', 'employment_status_id');
    }

    public function status()
    {
        return $this->belongsTo('App\EmploymentStatus', 'employment_status_id', 'id');
    }
}
