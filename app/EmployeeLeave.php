<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class EmployeeLeave extends Model
{
    use ForTenants;

    //
    protected $guarded = [];
    
    /*     public function Policy()
        {
            return $this->belongsTo('App\EmployeeLeavePolicy', 'employee_leave_policy_id');
        } */

    /*     public function Policy()
        {
            return $this->belongsTo('App\EmployeeLeavePolicy', 'employee_leave_policy_id');
        } */

    public function LeaveType()
    {
        return $this->belongsTo('App\LeaveType', 'leave_type_id');
    }

    public function Employee()
    {
        return $this->belongsTo('App\Employee', 'employee_id');
    }

    public function ApprovedBy()
    {
        //return $this->setConnection('mysql')->belongsTo('App\User', 'approved_by_id');

        return $this->belongsTo('App\TenantUser', 'approved_by_id');
    }

    public function DeniedBy()
    {
        return $this->belongsTo('App\TenantUser', 'rejected_by_id');
    }

    public function RejectedBy()
    {
        return $this->belongsTo('App\TenantUser', 'rejected_by_id');
    }

    public function getCalculatePolicyTotals()
    {
        $policy_id = DB::connection('tenant')->table('employee_leave_policies')
            ->join('leave_policies', 'leave_policies.id', '=', 'employee_leave_policies.leave_policy_id')
            ->where('employee_leave_policies.employee_id', '=', $this->employee_id)
            ->where('leave_policies.leave_type_id', '=', $this->leave_type_id)
            ->where('employee_leave_policies.active', '=', 1)
            ->select('employee_leave_policies.id as id')
            ->first();

        if (is_null($policy_id)) {
            return null;
        }

        $current_policy = EmployeeLeavePolicy::find($policy_id->id);
        //return $current_policy;
        $current_policy->getRecalculateTotals();

        return $current_policy;
    }
}
