<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class LeavePolicySchedule extends Model
{
    use ForTenants;

    //
    protected $guarded = [];

    public function LeavePolicy()
    {
        return $this->belongsTo('App\LeavePolicy', 'leave_policy_id');
    }
}
