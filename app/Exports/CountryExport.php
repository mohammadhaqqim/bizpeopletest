<?php

namespace App\Exports;

use App\Country;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class CountryExport implements
    FromCollection,
    WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $res = DB::connection('tenant')->table('countries')
            ->select(
                'country',
                'nationality',
                'use_currency',
                'currency_code'
            )
            ->where('id', '=', -1)
            ->get();
    
        return $res;
    }

    public function headings(): array
    {
        return [
            "country",
            "nationality",
            "use_currency",
            "currency_code"
        ];
    }
}
