<?php

namespace App\Exports;

use App\designation;
use Maatwebsite\Excel\Concerns\FromCollection;

class DesignationExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return designation::all();
    }
}
