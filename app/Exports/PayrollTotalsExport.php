<?php

namespace App\Exports;

use App\User;
use App\PayrollMaster;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx\PageSetup;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

class PayrollTotalsExport implements FromCollection, WithCustomStartCell, WithHeadings, ShouldAutoSize, WithTitle, WithEvents, WithColumnFormatting, WithColumnWidths
{
    protected $payroll_id;
    protected $reportTitle;
    protected $rowcount;

    public function __construct($title, $id)
    {
        $this->reportTitle = $title;
        $this->payroll_id = $id;
    }

    public function collection()
    {
        $res = DB::connection('tenant')->table('payroll_employees')
            ->join('employees', 'employees.id', '=', 'payroll_employees.employee_id')
            ->join('payment_modes', 'payment_modes.id', '=', 'payroll_employees.payment_mode_id')
            ->select(
                'name',
                'base_salary as basic_salary',
                DB::RAW('unpaid_amount*-1'),
                'overtime_amount as overtime',
                'remarks as bonus',
                'addition_amount as additions',
                DB::RAW('(deduction_amount)*-1'),
                'remarks',
                'tap_ee',
                'scp',
                'scp_ee',
                'epf_ee',
                'scp_ee as scp2',
                'title as bank_mode'
            )
            ->where('payroll_master_id', '=', $this->payroll_id)
            ->get();

        $this->rowcount = $res->count();

        return $res;
    }

    
    public function startCell(): string
    {
        return 'B7';
    }
    
    public function headings(): array
    {
        return [
            "Name",
            "Basic Salary",
            "Unpaid Leave",
            "Overtime",
            "Bonus", "Add", "Ded", "Other Desc", "TAP", "SCP", "SCP EE", "EPF EE", "Allowance", "Bank"
        ];
    }

    public function title(): String
    {
        return 'Payroll Totals Report';
    }

    public function columnWidths(): array
    {
        return [
            'A' => 5
        ];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $endrow = $this->rowcount + 7;
                $lastrow = $this->rowcount + 8;
            
                $event->sheet->getDelegate()->getColumnDimension('D')->setAutoSize(false);
                $event->sheet->getDelegate()->getColumnDimension('A')->setWidth(3);
                //
                $event->sheet
                    ->getPageSetup()
                    ->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
                $event->sheet->getPageSetup()->setScale(70);

                //$cellRange = 'A8:N8'; // All headers
                //$event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $this->SetReportTitle($event);
                $this->SetTableHeaders($event);
                $this->InsertFormula($event, $lastrow);
                $this->FormatHeaderBorders($event, $endrow);
                $this->SetBorders($event, $lastrow);
                $this->IncludePayrollTotals($event, $endrow);
                $this->SetPayrollFooter($event, $lastrow);
            },
        ];
    }

    public function SetPayrollFooter($event, $row)
    {
        $row=$row + 3;
        $event->sheet->setCellValue('B'.$row, 'Prepared by :');
        $event->sheet->setCellValue('E'.$row, 'Approved by :');

        $row += 3;

        $event->sheet->setCellValue('B'.$row, 'Name :');
        $event->sheet->setCellValue('E'.$row, 'Name :');

        $this->SheetBorderTop($event, 'B'.$row, false);
        $this->SheetBorderTop($event, 'E'.$row.':I'.$row, false);

        $row++;
        $event->sheet->setCellValue('B'.$row, 'Date :');
        $event->sheet->setCellValue('E'.$row, 'Date :');

        $row++;
        $row++;

        $event->sheet->setCellValue('B'.$row, 'Note :');
        $row++;
        $event->sheet->setCellValue('B'.$row, 'EE - Employee');
        $row++;
        $event->sheet->setCellValue('B'.$row, 'ER - Employer');
    }

    private function IncludePayrollTotals($event, $endrow)
    {
        //$sheet->setCellValue('A'.$row, '');
        $row = $endrow + 1;
        $event->sheet->setCellValue('B'.$row, 'TOTAL');

        $formula = '=ROUND(SUM(C8:C'.$endrow.'),2)';
        $event->sheet->setCellValue('C'.$row, $formula);
        $formula = '=ROUND(SUM(D8:D'.$endrow.'),2)';
        $event->sheet->setCellValue('D'.$row, $formula);
        $formula = '=ROUND(SUM(E8:E'.$endrow.'),2)';
        $event->sheet->setCellValue('E'.$row, $formula);
        $formula = '=ROUND(SUM(F8:F'.$endrow.'),2)';
        $event->sheet->setCellValue('F'.$row, $formula);
        $formula = '=ROUND(SUM(G8:G'.$endrow.'),2)';
        $event->sheet->setCellValue('G'.$row, $formula);
        $formula = '=ROUND(SUM(H8:H'.$endrow.'),2)';
        $event->sheet->setCellValue('H'.$row, $formula);
        $formula = '=ROUND(SUM(I8:I'.$endrow.'),2)';
        $event->sheet->setCellValue('I'.$row, $formula);
        $formula = '=ROUND(SUM(J8:J'.$endrow.'),2)';
        $event->sheet->setCellValue('J'.$row, $formula);
        $formula = '=ROUND(SUM(K8:K'.$endrow.'),2)';
        $event->sheet->setCellValue('K'.$row, $formula);
        $formula = '=ROUND(SUM(L8:L'.$endrow.'),2)';
        $event->sheet->setCellValue('L'.$row, $formula);
        $formula = '=ROUND(SUM(M8:M'.$endrow.'),2)';
        $event->sheet->setCellValue('M'.$row, $formula);
        $formula = '=ROUND(SUM(N8:N'.$endrow.'),2)';
        $event->sheet->setCellValue('N'.$row, $formula);

        $event->sheet->getStyle('C'.$row.':N'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

        $event->sheet->getStyle('D8:D'.$row)->getNumberFormat()->setFormatCode('#,##0.00;[Red](#,##0.00)');
        $event->sheet->getStyle('H8:H'.$row)->getNumberFormat()->setFormatCode('#,##0.00;[Red](#,##0.00)');

        $event->sheet->getStyle('C'.$row.':N'.$row)->getFont()->setBold(true);
    }

    private function FormatHeaderBorders($event, $endrow)
    {
        $this->SheetBorderThin($event, 'A8:O'.$endrow, false);

        $this->SheetBorderThin($event, 'A5:O7', true);
        $this->SheetBorderThin($event, 'C5:O6', false);
    }

    private function SheetBorderTop($event, $cells, $center)
    {
        $style = [
            'borders' => [
                'top' => [
                    'borderStyle' => Border::BORDER_THIN,
                ],
            ]
        ];

        $event->sheet->getStyle($cells)->applyFromArray($style);
    }
    
    private function SheetBorderThin($event, $cells, $center)
    {
        $style = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                    //'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    //'color' => ['argb' => '000000'],
                ],
            ]
        ];

        $event->sheet->getStyle($cells)->applyFromArray($style);
    }

    private function SheetBorderDotted($event, $cells, $center)
    {
        $style = [
            'borders' => [
                'horizontal' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                    //'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    //'color' => ['argb' => '000000'],
                ],
            ]
        ];

        $event->sheet->getStyle($cells)->applyFromArray($style);
    }

    private function SetBorders($event, $lastrow)
    {
        $this->SheetBorderThin($event, 'C5:C'.$lastrow, false);
        $this->SheetBorderThin($event, 'E5:E'.$lastrow, false);
        $this->SheetBorderThin($event, 'G5:G'.$lastrow, false);
        $this->SheetBorderThin($event, 'I5:I'.$lastrow, false);
        $this->SheetBorderThin($event, 'K5:K'.$lastrow, false);
        $this->SheetBorderThin($event, 'M5:M'.$lastrow, false);
        $this->SheetBorderThin($event, 'N5:N'.$lastrow, false);
        $this->SheetBorderThin($event, 'O5:O'.$lastrow, false);

        $this->SheetBorderThin($event, 'C'.$lastrow.':M'.$lastrow, false);
        $row = $lastrow-1;
        $this->SheetBorderDotted($event, 'A8:O'.$row, false);
    }

    private function InsertFormula($event, $lastrow)
    {
        for ($i = 1; $i < $this->rowcount+1; $i++) {
            $row = 7 + $i;
            $formula = '=ROUND(SUM(C'.$row.':H'.$row.'),2)';
            $event->sheet->setCellValue('I'.$row, $formula);
         
            $formula = '=ROUND(SUM(I'.$row.'-J'.$row.'-L'.$row.'-M'.$row.'),2)';
            $event->sheet->setCellValue('N'.$row, $formula);
        }

        //Bold totals
        $cellRange = 'I8:I'.$lastrow;
        $event->sheet->getStyle($cellRange)->getFont()->setBold(true);
        $cellRange = 'N8:N'.$lastrow;
        $event->sheet->getStyle($cellRange)->getFont()->setBold(true);
    }



    private function SetReportTitle($event)
    {
        $payroll = PayrollMaster::find($this->payroll_id);

        $event->sheet->setCellValue('B1', env('COMPANY_NAME'));
        $event->sheet->getStyle('B1')->getFont()->setBold(true);
        $event->sheet->setCellValue('B2', $this->reportTitle);
        $event->sheet->getStyle('B2')->getFont()->setBold(true);
        $event->sheet->setCellValue('B3', $payroll->title);
        $event->sheet->getStyle('B3')->getFont()->setBold(true);
    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'D' => NumberFormat::builtInFormatCode(40),
            'E' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'F' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'G' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'H' => NumberFormat::builtInFormatCode(40),
            'I' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'J' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'K' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'L' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'M' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'N' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
        ];
    }

    private function SetTableHeaders($event)
    {
        $event->sheet->setCellValue('A6', 'No');
        $event->sheet->getStyle('A6')->getFont()->setBold(true);

        $event->sheet->setCellValue('B6', 'Name');
        $event->sheet->getStyle('B6')->getFont()->setBold(true);
        $event->sheet->setCellValue('B7', '');

        $event->sheet->setCellValue('C5', 'Basic');
        $event->sheet->setCellValue('C6', 'Salary');
        $event->sheet->getStyle('C5:C6')->getFont()->setBold(true);
        $event->sheet->setCellValue('C7', 'B$');

        $event->sheet->setCellValue('D5', 'Unpaid');
        $event->sheet->setCellValue('D6', 'Leave');
        $event->sheet->getStyle('D5:D6')->getFont()->setBold(true);
        $event->sheet->setCellValue('D7', 'B$');

        $event->sheet->setCellValue('E5', 'Overtime');
        $event->sheet->getStyle('E5')->getFont()->setBold(true);
        $event->sheet->setCellValue('E7', 'B$');

        $event->sheet->setCellValue('F5', 'Bonus');
        $event->sheet->getStyle('F5')->getFont()->setBold(true);
        $event->sheet->setCellValue('F7', 'B$');

        $event->sheet->setCellValue('G5', 'Allowance');
        $event->sheet->getStyle('G5:G6')->getFont()->setBold(true);
        $event->sheet->setCellValue('G6', '');
        $event->sheet->setCellValue('G7', 'B$');

        $event->sheet->setCellValue('H5', 'Deduction');
        $event->sheet->getStyle('H5:H6')->getFont()->setBold(true);
        $event->sheet->setCellValue('H6', '');
        $event->sheet->setCellValue('H7', 'B$');

        $event->sheet->setCellValue('I5', 'Gross');
        $event->sheet->setCellValue('I6', 'Salary');
        $event->sheet->getStyle('I5:I6')->getFont()->setBold(true);
        $event->sheet->setCellValue('I7', 'B$');

        $event->sheet->setCellValue('J5', 'TAP');
        $event->sheet->setCellValue('J6', 'ER / EE');
        $event->sheet->getStyle('J5:J6')->getFont()->setBold(true);
        $event->sheet->setCellValue('J7', 'B$');

        $event->sheet->setCellValue('K5', 'SCP');
        $event->sheet->setCellValue('K6', 'Emp.r');
        $event->sheet->getStyle('K5:K6')->getFont()->setBold(true);
        $event->sheet->setCellValue('K7', 'B$');

        $event->sheet->setCellValue('L5', 'SCP');
        $event->sheet->setCellValue('L6', 'Emp.e');
        $event->sheet->getStyle('L5:L6')->getFont()->setBold(true);
        $event->sheet->setCellValue('L7', 'B$');

        $event->sheet->setCellValue('M5', 'EPF');
        $event->sheet->setCellValue('M6', 'ER / EE');
        $event->sheet->getStyle('M5:M6')->getFont()->setBold(true);
        $event->sheet->setCellValue('M7', 'B$');

        $event->sheet->setCellValue('N5', 'Net');
        $event->sheet->setCellValue('N6', 'Salary');
        $event->sheet->getStyle('N5:N6')->getFont()->setBold(true);
        $event->sheet->setCellValue('N7', 'B$');

        $event->sheet->setCellValue('O5', 'Payment');
        $event->sheet->setCellValue('O6', 'Mode');
        $event->sheet->setCellValue('O7', '');
        $event->sheet->getStyle('O5:O6')->getFont()->setBold(true);
    }
}
