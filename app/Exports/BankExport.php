<?php

namespace App\Exports;

use App\User;
use App\PayrollMaster;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class BankExport implements
    FromCollection,
    WithHeadings,
    ShouldAutoSize,
    WithColumnFormatting /*     WithCustomStartCell,
    WithHeadings,
    WithEvents,
    WithColumnFormatting,
    ShouldAutoSize */

//, WithCustomStartCell, WithHeadings, ShouldAutoSize, WithTitle, WithColumnFormatting, WithColumnWidths
{
    protected $payroll_id;
    protected $reportTitle;

    public function __construct($title, $id)
    {
        $this->reportTitle = $title;
        $this->payroll_id = $id;
    }

    public function collection()
    {
        $res = DB::connection('tenant')->table('payroll_employees')
            ->join('employees', 'employees.id', '=', 'payroll_employees.employee_id')
            ->join('banks', 'banks.id', '=', 'payroll_employees.bank_id')
            ->join('payment_modes', 'payment_modes.id', '=', 'payroll_employees.payment_mode_id')
            ->select(
                'name',
                'payment_modes.title as mode',
                'banks.title as bank',
                'payroll_employees.bank_account',
                DB::RAW('(base_salary + addition_amount 
                    - deduction_amount + overtime_amount + doubletime_amount
                    - tap_ee - scp_ee - epf_ee
                ) as Net')
            )
            ->where('payroll_master_id', '=', $this->payroll_id)
            ->get();

        //dd($res);
        $this->rowcount = $res->count();

        return $res;
    }

    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_NUMBER,
            'E' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
        ];
    }

    public function headings(): array
    {
        return [
            "Name",
            "Payment Mode",
            "Bank",
            "Account No.",
            "Net Salary",
        ];
    }
}
