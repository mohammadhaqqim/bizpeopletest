<?php

namespace App\Exports;

use App\User;
use App\PayrollMaster;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class ActivityExport implements
    FromCollection,
    WithCustomStartCell,
    WithHeadings,
    WithEvents,
    WithColumnFormatting,
    ShouldAutoSize

//, WithCustomStartCell, WithHeadings, ShouldAutoSize, WithTitle, WithColumnFormatting, WithColumnWidths
{
    protected $payroll_id;
    protected $reportTitle;
    protected $rowcount;

    public function __construct($title, $id)
    {
        $this->reportTitle = $title;
        $this->payroll_id = $id;
    }

    public function collection()
    {
        $res = DB::connection('tenant')->table('payroll_employees')
            ->join('employees', 'employees.id', '=', 'payroll_employees.employee_id')
            ->join('payment_modes', 'payment_modes.id', '=', 'payroll_employees.payment_mode_id')
            ->select(
                'name',
                'base_salary as basic_salary',
                'old_salary',
                DB::RAW('(basic_salary - old_salary) as diff'),
                DB::RAW('(SELECT sum(amount) FROM payroll_transactions 
                    WHERE payroll_employees_id=payroll_employees.id 
                    and addition=true
                    and deleted_at is null
                    and title like "%over%time%") as overtime'),
                DB::RAW('(SELECT sum(amount) FROM payroll_transactions 
                    WHERE payroll_employees_id=payroll_employees.id 
                    and addition=true
                    and deleted_at is null
                    and title like "%double%time%") as doubletime'),
                DB::RAW('(SELECT sum(amount) FROM payroll_transactions 
                    WHERE payroll_employees_id=payroll_employees.id 
                    and addition=true
                    and deleted_at is null
                    and title like "%transport%") as transport'),
                DB::RAW('(SELECT sum(amount) FROM payroll_transactions 
                    WHERE payroll_employees_id=payroll_employees.id 
                    and addition=true
                    and deleted_at is null
                    and title like "%housing%") as housing'),
                DB::RAW('(SELECT sum(amount) FROM payroll_transactions 
                    WHERE payroll_employees_id=payroll_employees.id 
                    and addition=true
                    and deleted_at is null
                    and title like "%meal%") as meal'),
                DB::RAW('(SELECT sum(amount) FROM payroll_transactions 
                    WHERE payroll_employees_id=payroll_employees.id 
                    and addition=true
                    and deleted_at is null
                    and title not like "%meal%"
                    and title not like "%housing%"
                    and title not like "%transport%"
                    and title not like "%double%time%"
                    and title not like "%over%time%"
                    ) as other'),
                DB::RAW('(SELECT sum(amount) FROM payroll_transactions 
                    WHERE payroll_employees_id=payroll_employees.id 
                    and addition=true
                    and deleted_at is null) as additions'),
                DB::RAW('(SELECT sum(amount) FROM payroll_transactions 
                    WHERE payroll_employees_id=payroll_employees.id     
                    and addition=false
                    and deleted_at is null
                    and title like "%unpaid%leave%") as unpaid'),
                DB::RAW('(SELECT sum(amount) FROM payroll_transactions 
                    WHERE payroll_employees_id=payroll_employees.id     
                    and addition=false
                    and deleted_at is null
                    and title like "%personal%") as personal'),
                'payroll_employees.deleted_at as desc1',
                DB::RAW('(SELECT sum(amount) FROM payroll_transactions 
                    WHERE payroll_employees_id=payroll_employees.id     
                    and addition=false
                    and deleted_at is null
                    and title like "%other%") as xyz'),
                'payroll_employees.deleted_at as desc2',
                DB::RAW('(SELECT sum(amount) FROM payroll_transactions 
                    WHERE payroll_employees_id=payroll_employees.id     
                    and addition=false
                    and deleted_at is null) as deductions')
            )
            ->where('payroll_master_id', '=', $this->payroll_id)
            ->get();

        $this->rowcount = $res->count();

        return $res;
    }

    public function startCell(): string
    {
        return 'B7';
    }

    public function headings(): array
    {
        return [
            "Name",
            "Basic ",
            "Old",
            "Diff",
            "Overtime",
            "Double",
            "Transport",
            "Housing",
            "Meal",
            "Other",
            "Additions",
            "Unpaid",
            "Personal",
            "Personal",
            "Other",
            "Other",
            "Deductions"
        ];
    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
//            'D' => NumberFormat::builtInFormatCode(40),
            'D' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'E' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'F' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'G' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
//            'H' => NumberFormat::builtInFormatCode(40),
            'H' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'I' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'J' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'K' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'L' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'M' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'N' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'O' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'P' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'Q' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'R' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
        ];
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $endrow = $this->rowcount + 7;
                $lastrow = $this->rowcount + 8;
            
                $this->SetReportTitle($event);
                $this->SetActivityHeaders($event);
                $this->FormatHeaderBorders($event, $endrow);
                $this->SetBorders($event, $lastrow);
                $this->IncludeActivityTotals($event, $lastrow);
                $this->HeightLightColumns($event, $lastrow);
            },
        ];
    }

    private function HeightLightColumns($event, $row)
    {
        $this->SheetBorderThick($event, 'E5:E'.$row, false);
        $this->SheetBorderThick($event, 'L5:L'.$row, false);
        $this->SheetBorderThick($event, 'R5:R'.$row, false);
    }

    private function IncludeActivityTotals($event, $row)
    {
        $count = $row - 1;
        $event->sheet->setCellValue('B'.$row, 'TOTAL');

        $formula = '=ROUND(SUM(C8:C'.$count.'),2)';
        $event->sheet->setCellValue('C'.$row, $formula);
        $formula = '=ROUND(SUM(D8:D'.$count.'),2)';
        $event->sheet->setCellValue('D'.$row, $formula);
        $formula = '=ROUND(SUM(E8:E'.$count.'),2)';
        $event->sheet->setCellValue('E'.$row, $formula);
        $formula = '=ROUND(SUM(F8:F'.$count.'),2)';
        $event->sheet->setCellValue('F'.$row, $formula);
        $formula = '=ROUND(SUM(G8:G'.$count.'),2)';
        $event->sheet->setCellValue('G'.$row, $formula);
        $formula = '=ROUND(SUM(H8:H'.$count.'),2)';
        $event->sheet->setCellValue('H'.$row, $formula);
        $formula = '=ROUND(SUM(I8:I'.$count.'),2)';
        $event->sheet->setCellValue('I'.$row, $formula);
        $formula = '=ROUND(SUM(J8:J'.$count.'),2)';
        $event->sheet->setCellValue('J'.$row, $formula);
        $formula = '=ROUND(SUM(K8:K'.$count.'),2)';
        $event->sheet->setCellValue('K'.$row, $formula);
        $formula = '=ROUND(SUM(L8:L'.$count.'),2)';
        $event->sheet->setCellValue('L'.$row, $formula);

        $formula = '=ROUND(SUM(M9:M'.$count.'),2)';
        $event->sheet->setCellValue('M'.$row, $formula);
        $formula = '=ROUND(SUM(N9:N'.$count.'),2)';
        $event->sheet->setCellValue('N'.$row, $formula);
        $formula = '=ROUND(SUM(P9:P'.$count.'),2)';
        $event->sheet->setCellValue('P'.$row, $formula);
        $formula = '=ROUND(SUM(R9:R'.$count.'),2)';
        $event->sheet->setCellValue('R'.$row, $formula);

        $event->sheet->getStyle('C'.$row.':R'.$row)->getFont()->setBold(true);
        $event->sheet->getStyle('C'.$row.':R'.$row)->getNumberFormat()->setFormatCode('#,##0.00');
    }


    private function SetBorders($event, $lastrow)
    {
        $this->SheetBorderThin($event, 'C5:C'.$lastrow, false);
        $this->SheetBorderThin($event, 'E5:E'.$lastrow, false);
        $this->SheetBorderThin($event, 'G5:G'.$lastrow, false);
        $this->SheetBorderThin($event, 'I5:I'.$lastrow, false);
        $this->SheetBorderThin($event, 'K5:K'.$lastrow, false);
        $this->SheetBorderThin($event, 'M5:M'.$lastrow, false);
        $this->SheetBorderThin($event, 'N5:N'.$lastrow, false);
        $this->SheetBorderThin($event, 'P5:P'.$lastrow, false);
        $this->SheetBorderThin($event, 'R5:R'.$lastrow, false);

        $this->SheetBorderThin($event, 'C'.$lastrow.':R'.$lastrow, false);
        $row = $lastrow-1;
        $this->SheetBorderDotted($event, 'A8:R'.$row, false);
    }

    private function FormatHeaderBorders($event, $endrow)
    {
        $this->SheetBorderThin($event, 'A5:R'.$endrow, true);
        $this->SheetBorderThin($event, 'A5:R7', true);
        $this->SheetBorderThin($event, 'C5:R6', false);
    }

    private function SetReportTitle($event)
    {
        $payroll = PayrollMaster::find($this->payroll_id);

        $event->sheet->setCellValue('B1', env('COMPANY_NAME'));
        $event->sheet->getStyle('B1')->getFont()->setBold(true);
        $event->sheet->setCellValue('B2', $this->reportTitle);
        $event->sheet->getStyle('B2')->getFont()->setBold(true);
        $event->sheet->setCellValue('B3', $payroll->title);
        $event->sheet->getStyle('B3')->getFont()->setBold(true);
    }


    private function SetActivityHeaders($event)
    {
        /*         $event->sheet->setColumnFormat(array(
                    'C' => '#,##0.00',
                    'D' => '#,##0.00',
                    'E' => '#,##0.00',
                    'F' => '#,##0.00',
                    'G' => '#,##0.00',
                    'H' => '#,##0.00',
                    'I' => '#,##0.00',
                    'J' => '#,##0.00',
                    'K' => '#,##0.00',
                    'L' => '#,##0.00',
                    'N' => '#,##0.00',
                    'M' => '#,##0.00',
                    'P' => '#,##0.00',
                    'R' => '#,##0.00'
                )); */

        $event->sheet->setCellValue('A6', 'No');
        $event->sheet->getStyle('A6')->getFont()->setBold(true);

        $event->sheet->setCellValue('B6', 'Name');
        $event->sheet->getStyle('B6')->getFont()->setBold(true);
        $event->sheet->setCellValue('B7', '');

        $event->sheet->setCellValue('C5', 'Basic');
        $event->sheet->setCellValue('C6', 'Salary');
        $event->sheet->getStyle('C5:C6')->getFont()->setBold(true);
        $event->sheet->setCellValue('C7', 'B$');

        $event->sheet->setCellValue('D5', 'Old');
        $event->sheet->setCellValue('D6', 'Salary');
        $event->sheet->getStyle('D5:D6')->getFont()->setBold(true);
        $event->sheet->setCellValue('D7', 'B$');

        $event->sheet->setCellValue('E5', 'Salary');
        $event->sheet->setCellValue('E6', 'Diff.');
        $event->sheet->getStyle('E5:E6')->getFont()->setBold(true);
        $event->sheet->setCellValue('E7', 'B$');

        $event->sheet->setCellValue('F5', 'Overtime');
        $event->sheet->setCellValue('F6', 'Amt');
        $event->sheet->getStyle('F5:F6')->getFont()->setBold(true);
        $event->sheet->setCellValue('F7', 'B$');

        $event->sheet->setCellValue('G5', 'Doubletime');
        $event->sheet->setCellValue('G6', 'Amt');
        $event->sheet->getStyle('G5:G6')->getFont()->setBold(true);
        $event->sheet->setCellValue('G7', 'B$');

        $event->sheet->setCellValue('H5', 'Transport');
        $event->sheet->getStyle('H5')->getFont()->setBold(true);
        $event->sheet->setCellValue('H7', 'B$');

        $event->sheet->setCellValue('I5', 'Housing');
        $event->sheet->getStyle('I5')->getFont()->setBold(true);
        $event->sheet->setCellValue('I7', 'B$');

        $event->sheet->setCellValue('J5', 'Meal');
        $event->sheet->getStyle('J5')->getFont()->setBold(true);
        $event->sheet->setCellValue('J7', 'B$');

        $event->sheet->setCellValue('K5', 'Other');
        $event->sheet->getStyle('K5')->getFont()->setBold(true);
        $event->sheet->setCellValue('K7', 'B$');

        $event->sheet->setCellValue('L5', 'Additions');
        $event->sheet->getStyle('L5')->getFont()->setBold(true);
        $event->sheet->setCellValue('L7', 'B$');

        $event->sheet->setCellValue('M5', 'Unpaid');
        $event->sheet->setCellValue('M6', 'Leave');
        $event->sheet->getStyle('M5:M6')->getFont()->setBold(true);
        $event->sheet->setCellValue('M7', 'B$');

        $event->sheet->setCellValue('N5', 'Personal');
        $event->sheet->setCellValue('N6', 'Amt');
        $event->sheet->getStyle('N5:N6')->getFont()->setBold(true);
        $event->sheet->setCellValue('N7', 'B$');

        $event->sheet->setCellValue('O5', 'Personal');
        $event->sheet->setCellValue('O6', 'Desc.');
        $event->sheet->getStyle('O5:O6')->getFont()->setBold(true);
        $event->sheet->setCellValue('O7', '');

        $event->sheet->setCellValue('P5', 'Other');
        $event->sheet->setCellValue('P6', 'Amt');
        $event->sheet->getStyle('P5:P6')->getFont()->setBold(true);
        $event->sheet->setCellValue('P7', 'B$');

        $event->sheet->setCellValue('Q5', 'Other');
        $event->sheet->setCellValue('Q6', 'Desc.');
        $event->sheet->getStyle('Q5:Q6')->getFont()->setBold(true);
        $event->sheet->setCellValue('Q7', '');

        $event->sheet->setCellValue('R5', 'Deductions');
        $event->sheet->getStyle('R5')->getFont()->setBold(true);
        $event->sheet->setCellValue('R7', 'B$');

        //$this->SheetBorderThin($event->sheet, 'A5:R7', true);
        //$this->SheetBorderThin($event->sheet, 'C5:R6', false);
    }

    private function SheetBorderThin($event, $cells, $center)
    {
        $style = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THIN,
                    //'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    //'color' => ['argb' => '000000'],
                ],
            ]
        ];

        $event->sheet->getStyle($cells)->applyFromArray($style);
    }

    private function SheetBorderThick($event, $cells, $center)
    {
        $style = [
            'borders' => [
                'outline' => [
                    'borderStyle' => Border::BORDER_THICK,
                    //'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    //'color' => ['argb' => '000000'],
                ],
            ]
        ];

        $event->sheet->getStyle($cells)->applyFromArray($style);
    }

    private function SheetBorderDotted($event, $cells, $center)
    {
        $style = [
            'borders' => [
                'horizontal' => [
                    'borderStyle' => Border::BORDER_DOTTED,
                    //'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    //'color' => ['argb' => '000000'],
                ],
            ]
        ];

        $event->sheet->getStyle($cells)->applyFromArray($style);
    }
}
