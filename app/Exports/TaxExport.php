<?php

namespace App\Exports;

use App\PayrollMaster;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\WithEvents;
use PhpOffice\PhpSpreadsheet\Style\Border;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithColumnWidths;

class TaxExport implements
    FromCollection,
    WithHeadings,
    ShouldAutoSize,
    WithColumnFormatting /*     WithCustomStartCell,
    WithHeadings,
    WithEvents,
    WithColumnFormatting,
    ShouldAutoSize */

//, WithCustomStartCell, WithHeadings, ShouldAutoSize, WithTitle, WithColumnFormatting, WithColumnWidths
{
    protected $payroll_id;
    protected $reportTitle;

    public function __construct($title, $id)
    {
        $this->reportTitle = $title;
        $this->payroll_id = $id;
    }

    public function collection()
    {
        $res = DB::connection('tenant')->table('payroll_employees')
            ->join('employees', 'employees.id', '=', 'payroll_employees.employee_id')
            ->select(
                'ic_number',
                'name',
                'base_salary',
                'tap',
                'tap_ee',
                'scp',
                'scp_ee',
                'epf',
                'epf_ee'
            )
            ->where('payroll_master_id', '=', $this->payroll_id)
            ->get();

        //dd($res);
        $this->rowcount = $res->count();

        return $res;
    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'D' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'E' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'F' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'G' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'H' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'I' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
        ];
    }

    public function headings(): array
    {
        return [
            'IC Number',
            'Name',
            'Salary',
            'TAP',
            'TAP EE',
            'SCP',
            'ECP EE',
            'EPF',
            'EPF EE'
        ];
    }
}
