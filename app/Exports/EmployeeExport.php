<?php

namespace App\Exports;

use App\Employee;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class EmployeeExport implements
    FromCollection,
    WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        //    return Employee::all();
        $res = DB::connection('tenant')->table('employees')
        ->select(
            'employee_code',
            'name'
        )
        ->where('id', '=', -1)
        ->get();

        return $res;
    }

    public function headings(): array
    {
        return [
            'code',
            'name',
            'preferred_name',
            'work_email',
            'date_of_birth',
            'gender',
            'nationality',
            'religion',
            'ic_number',
            'date_joined',
            'employment_status',
            'designation',
            'department',
            'basic_salary',
            'payment_mode',
            'payment_type'
        ];

        // payment mode = Bank / Cash / Cheque
        // Payment type = Weekly / Monthly / Bi-Monthly / One-Off ...
        // employment_status = Part-time / Full time / Intern / Probation ...
    }
}
