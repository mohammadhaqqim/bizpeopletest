<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class PayrollMaster extends Model
{
    use ForTenants;
    
    //
    protected $guarded = [];

    public function CreatedBy()
    {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function Company()
    {
        return $this->belongsTo('App\Company');
    }

    public function Employees()
    {
        return $this->hasMany('App\PayrollEmployee', 'payroll_master_id');
    }

    public function ApprovedBy()
    {
        return $this->belongsTo('App\User', 'approved_by');
    }

    public function getPayout()
    {
        $payroll = PayrollEmployee::where('payroll_master_id', $this->id)->get();

        $payout = 0;
        foreach ($payroll as $p) {
            $payout += $p->getPayout();
        }

        return  $payout;
    }

    public function getGrossPayout()
    {
        $payroll = PayrollEmployee::where('payroll_master_id', $this->id)->get();

        $payout = 0;
        foreach ($payroll as $p) {
            $payout += $p->getGross();
        }

        return  $payout;
    }
    
    public function getNetPayout()
    {
        $payroll = PayrollEmployee::where('payroll_master_id', $this->id)->get();

        $payout = 0;
        foreach ($payroll as $p) {
            $payout += $p->getNet();
        }

        return  $payout;
    }
}
