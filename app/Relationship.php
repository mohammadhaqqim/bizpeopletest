<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class Relationship extends Model
{
    use ForTenants;

    //
    protected $guarded = [];
}
