<?php

namespace App;

use Carbon\Carbon;
use Faker\Provider\ar_SA\Payment;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class Employee extends Model
{
    use ForTenants;
    
    //
    protected $guarded = [];
    
    public function gender()
    {
        return $this->belongsTo('App\Gender');
    }

    public function maritalStatus()
    {
        return $this->belongsTo('App\MaritalStatus');
    }

    public function nationality()
    {
        return $this->belongsTo('App\Country');
    }

    public function residenceCountry()
    {
        return $this->belongsTo('App\Country', 'address_country_id');
    }

    public function jobs()
    {
        return $this->hasMany('App\EmployeeJob', 'employee_id', 'id');
    }

    public function payments()
    {
        return $this->hasMany('App\EmployeePayment', 'employee_id', 'id');
    }

    public function race()
    {
        return $this->belongsTo('App\Race');
    }

    public function religion()
    {
        return $this->belongsTo('App\Religion');
    }

    public function statusHistory()
    {
        return $this->hasMany('App\EmployeeStatus', 'employment_id', 'id');
    }

    //works
    public function employmentHistory()
    {
        return $this->hasMany('App\EmployeeStatus', 'employee_id', 'id');
    }

    //??
    public function employmentStatus()
    {
        return $this->belongsTo('App\EmploymentStatus', 'employment_type_id', 'id');
    }

    //??
    public function currentjob()
    {
        return $this->belongsTo('App\Designation', 'designation_id', 'id');
    }

    //??
    public function currentStatus()
    {
        return $this->belongsTo('App\EmploymentStatus', 'employment_status_id', 'id');
    }

    public function iccolor()
    {
        return $this->belongsTo('App\IcColor', 'ic_color_id');
    }

    public function Company()
    {
        return $this->belongsTo('App\Company');
    }

    public function Allowances()
    {
        return $this->hasMany('App\EmployeeAllowances', 'employee_id', 'id');
    }

    public function EndService()
    {
        return $this->hasMany('App\EmployeeEndService', 'id', 'employee_end_service_id');
    }

    public function Payment()
    {
        return $this->belongsTo('App\PaymentMode', 'payment_mode_id');
    }

    public function User()
    {
        return $this->belongsTo('App\Users', 'id', 'employee_id');
    }

    public function ReportsTo()
    {
        return $this->belongsTo('App\Employee', 'reports_to_id', 'id');
    }

    public function WorkingWeek()
    {
        return $this->belongsTo('App\WorkingWeek', 'working_week_id', 'id');
    }

    public function statuses()
    {
        return $this->hasMany('App\EmployeeStatus', 'employee_id', 'id');
    }

    public function payrolls()
    {
        return $this->hasMany('App\PayrollEmployee', 'employee_id', 'id')
            ->whereNull('deleted_at');
    }

    public function getPayrollEmployees()
    {
        return $this->hasMany('App\PayrollEmployee', 'employee_id', 'id')
            ->whereNull('deleted_at');
    }

    /*     public function endService()
        {
            return $this->belongsTo('App\EndEmploymentReason', 'end_reason_id', 'id');
        } */

    public function getServiceYearsAttribute()
    {
        $diff = $this->getDateDiffAttribute();
        $years = floor($diff / (365*60*60*24));

        return $years;
    }

    public function getServiceMonthsAttribute()
    {
        $diff = $this->getDateDiffAttribute();
        $years = floor($diff / (365*60*60*24));
        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
        
        return $months;
    }

    public function getDateDiffAttribute()
    {
        $date1 = strtotime($this->date_joined);
        $date2 = strtotime(Carbon::now());

        $diff = abs($date2 - $date1);

        return $diff;
    }

    //??
    public function getCurrentStatus()
    {
        $status = EmployeeStatus::where('employee_id', '=', $this->id)
            ->whereDate('effective_date', '<', Carbon::now())
            ->orderByDesc('effective_date')
            ->first();

        return $status;
    }

    //??
    public function getCurrentJob()
    {
        $job = EmployeeJob::where('employee_id', '=', $this->id)
            ->whereDate('effective_date', '<', Carbon::now())
            ->orderByDesc('effective_date')
            ->first();
        
        return $job;
    }

    //??
    public function getCurrentDepartment()
    {
        $department = DB::connection('tenant')->table('vw_employee_jobs')
            ->join('departments', 'departments.id', '=', 'vw_employee_jobs.department_id')
            ->where('employee_id', '=', $this->id)
            ->first();

        /*         $department = EmployeeJob::where('employee_id', '=', $this->id)
                    ->whereDate('effective_date', '<', Carbon::now())
                    ->orderByDesc('effective_date')
                    ->first(); */
        
        return $department;
    }

    public function getDepartmentID()
    {
        $this->getCurrentDepartment()->id;
    }

    public function getCurrentDesignation()
    {
        $designation = DB::connection('tenant')->table('vw_employee_jobs')
            ->join('designations', 'designations.id', '=', 'vw_employee_jobs.designation_id')
            ->where('employee_id', '=', $this->id)
            ->first();

        /*         $designation = EmployeeJob::where('employee_id', '=', $this->id)
                    ->whereDate('effective_date', '<', Carbon::now())
                    ->orderByDesc('effective_date')
                    ->first(); */
        
        return $designation->title;
    }

    public function getCurrentPayment()
    {
        $payment = EmployeePayment::where('employee_id', '=', $this->id)
            ->whereDate('effective_date', '<', Carbon::now())
            ->orderByDesc('effective_date')
            ->first();

        if (is_null($payment)) {
            return 0;
        }
            
        return $payment;
    }

    public function getCurrentPaymentType()
    {
        $payment = EmployeePayment::where('employee_id', '=', $this->id)
            ->whereDate('effective_date', '<', Carbon::now())
            ->orderByDesc('effective_date')
            ->first();

        if (is_null($payment)) {
            return 0;
        }
            
        return $payment->PaymentType;
    }


    public function getCurrentPay()
    {
        $payment = EmployeePayment::where('employee_id', '=', $this->id)
        ->whereDate('effective_date', '<', Carbon::now())
        ->orderByDesc('effective_date')
        ->first();

        if (is_null($payment)) {
            return 0;
        }
        
        return $payment->basic_salary;
    }

    //**** */
    public function getJob()
    {
        $employee_jobs = DB::connection('tenant')->table('vw_employee_jobs')
            ->select('designations.*')
            ->join('designations', 'designations.id', '=', 'vw_employee_jobs.designation_id')
            ->where('employee_id', '=', $this->id)
            ->first();
        
        return $employee_jobs;
    }

    //**** */
    public function getDepartment()
    {
        $department = DB::connection('tenant')->table('vw_employee_jobs')
            ->select('departments.*')
            ->join('departments', 'departments.id', '=', 'vw_employee_jobs.department_id')
            ->where('employee_id', '=', $this->id)
            ->first();

        /*         $department = DB::table('vw_employee_jobs')
                    ->join('departments', 'departments.id', '=', 'vw_employee_jobs.department_id')
                    ->where('employee_id', '=', $this->id)
                    ->first(); */

        if ($department == null) {
            return null;
        }
        
        return Department::find($department->id);
    }

    //**** */
    public function getPayment()
    {
        
        /*         $payment = DB::table('vw_employee_payments')
                    ->where('employee_id', '=', $this->id)
                    ->first();
         */
        //dd($this->Payment->title);
        //dd($this->payment_mode_id);
        $payment = DB::connection('tenant')->table('payment_modes')
            ->select('title')
            ->where('id', '=', $this->payment_mode_id)
            ->first();

        return $payment;
        
        $payment = PaymentMode::find($this->payment_mode_id);
        //dd($this->passport_number);
//        dd($payment->title);
        return $payment->title;
        //return $payment;
    }

    //**** */
    public function getBasePay()
    {
        $payment = DB::connection('tenant')->table('vw_employee_payments')
            ->where('employee_id', '=', $this->id)
            ->first();

        return $payment->basic_salary;
    }
    
    public function getOvertime()
    {
        $payment = DB::connection('tenant')->table('vw_employee_payments')
            ->where('employee_id', '=', $this->id)
            ->first();

        return $payment->overtime;
    }

    public function getAdditions()
    {
        $amount = DB::connection('tenant')->table('employee_allowances')
            ->join('transactions', 'employee_allowances.allowance_id', '=', 'transactions.id')
            ->select('employee_allowances.id', 'amount')
            ->whereNull('transactions.deleted_at')
            ->where('transactions.addition', '=', true)
            ->where('employee_allowances.employee_id', '=', $this->id)
            ->get();

        return $amount->sum('amount');
    }

    public function getDeductions()
    {
        $amount = DB::connection('tenant')->table('employee_allowances')
        ->join('transactions', 'employee_allowances.allowance_id', '=', 'transactions.id')
        ->select('employee_allowances.id', 'amount')
        ->whereNull('transactions.deleted_at')
        ->where('transactions.addition', '=', false)
        ->where('employee_allowances.employee_id', '=', $this->id)
        ->get();

        return $amount->sum('amount');
    }

    public function getInitals()
    {
        $names = explode(' ', $this->name);
        $first = str_split($names[0]);

        $lst = count($names) - 1;
        $last = str_split($names[$lst]);

        return $first[0].$last[0];
    }
}
