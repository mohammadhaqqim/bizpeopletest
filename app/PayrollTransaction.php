<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class PayrollTransaction extends Model
{
    use ForTenants;
    
    //
    protected $guarded = [];

    public function PayrollEmployee()
    {
        return $this->belongsTo('App\PayrollEmployee', 'payroll_employees_id');
    }

    public function Transaction()
    {
        return $this->belongsTo('App\Transaction');
    }

    public function EmployeeID()
    {
        $payroll_employee = $this->PayrollEmployee();

        return $payroll_employee->employee_id;
    }

    public function getPayrollMaster()
    {
        $payroll = PayrollEmployee::find($this->payroll_employees_id);

        return $payroll->payroll_master_id;
        // /        return $payroll->payroll_master_id;
//        return $this->belongsTo('App\PayrollEmployee', 'payroll_employees_id')->payroll_master_id;
//        return $this->PayrollEmployee()->payroll_master_id;
    }
}
