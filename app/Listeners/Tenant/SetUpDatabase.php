<?php

namespace App\Listeners\Tenant;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\Tenant\TenantDatabaseCreated;

class SetUpDatabase
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  TenantDatabseCreated  $event
     * @return void
     */
    public function handle(TenantDatabaseCreated $event)
    {
        Artisan::call('tenants:migrate', [
            '--tenants' => [$event->tenant->id]
        ]);
    }
}
