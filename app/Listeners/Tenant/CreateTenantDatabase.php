<?php

namespace App\Listeners\Tenant;

use App\Tenant\Manager;
use Illuminate\Support\Facades\DB;
use App\Events\Tenant\TenantWasCreated;
use App\Tenant\Database\DatabaseCreator;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Events\Tenant\TenantDatabaseCreated;

class CreateTenantDatabase
{
    protected $databaseCreator;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(DatabaseCreator $databaseCreator)
    {
        $this->databaseCreator = $databaseCreator;
    }

    /**
     * Handle the event.
     *
     * @param  TenantIdentified  $event
     * @return void
     */
    public function handle(TenantWasCreated $event)
    {
        //dd($event->tenant);
        if (!$this->databaseCreator->create($event->tenant)) {
            throw new \Exception('Database failed to be created.');
        }

        // migrate the database and seed if needs be
        event(new TenantDatabaseCreated($event->tenant));

        $sql = array('title' => session()->get('fin_year'),
            'start_date' => session()->get('start_date'),
            'end_date' => session()->get('end_date'));
        $reporting_id = DB::connection('tenant')->table('reporting_periods')->insertGetId($sql);

        /*         $sql = "update companies set company_name = '".$event->tenant->name."',  "
                    ."reporting_period_id = ".$reporting_id.", "
                    ."first_period_date='".session()->get('start_date')."'"; */

        $sql = array('company_name' => $event->tenant->name,
            'reporting_period_id' => $reporting_id,
            'periods_per_year' => 12,
            'first_period_date' => session()->get('start_date'),
            'end_financial_date' => session()->get('end_date'),
            'payment_type_id' => 4, // monthly
            'working_week_id' => 1, // standard week
        );
        DB::connection('tenant')->table('companies')
            ->where('id', 1)
            ->update($sql);

        //dd(Session::get('fin_year'));
        //$sql = "insert into reporting_periods(title, start_date, end_date) ".
        //    "values('".session()->get('fin_year')."', '".session()->get('start_date')."', '".session()->get('end_date')."')";
    }
}
