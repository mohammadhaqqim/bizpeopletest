<?php

namespace App\View\Components;

use App\Employee;
use Illuminate\View\Component;

class employee_header extends Component
{
    public $name;
    public $jobTitle;
    public $empid;
    public $empp;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name, $jobTitle, $empid)
    {
        //
        //$this->empp = Employee::find($empid)->get();
        $this->empp = "Test";

        $this->name = $name;
        $this->jobTitle = $jobTitle;
        $this->empid = $empid;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.employee_header');
    }
}
