<?php

namespace App\Policies;

use App\EmployeeStatus;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeeStatusPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User  $user
     * @param  \App\EmployeeStatus  $employeeStatus
     * @return mixed
     */
    public function view(User $user, EmployeeStatus $employeeStatus)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User  $user
     * @param  \App\EmployeeStatus  $employeeStatus
     * @return mixed
     */
    public function update(User $user, EmployeeStatus $employeeStatus)
    {
        //
        return $user->employee_id == $employeeStatus->employee_id || $user->hr;
    }
}
