<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class LeavePolicy extends Model
{
    use ForTenants;

    //
    protected $guarded = [];

    public function LeaveType()
    {
        return $this->belongsTo('App\LeaveType', 'leave_type_id');
    }

    //???
    public function Schedules()
    {
        return $this->hasMany('App\LeavePolicySchedule', 'leave_policy_id');
    }

    public function getEmployeeLeavePolicy($employee_id)
    {
        $policy_id = DB::connection('tenant')->table('employee_leave_policies')
            ->where('employee_leave_policies.employee_id', '=', $employee_id)
            ->where('employee_leave_policies.leave_policy_id', '=', $this->id)
            ->where('start_from_date', '<=', \Carbon\Carbon::now())
            ->select('employee_leave_policies.id as id')
            ->orderByDesc('start_from_date')
            ->first();

        if (is_null($policy_id)) {
            return null;
        }

        $current_policy = EmployeeLeavePolicy::where('id', '=', $policy_id->id)
            ->first();

        return $current_policy->available;
    }

    public function getCurrentEmployeeLeavePolicy($employee_id)
    {
        $policy_id = DB::connection('tenant')->table('employee_leave_policies')
            ->where('employee_leave_policies.employee_id', '=', $employee_id)
            ->where('employee_leave_policies.leave_policy_id', '=', $this->id)
            ->where('start_from_date', '<=', \Carbon\Carbon::now())
            ->select('employee_leave_policies.id as id')
            ->orderByDesc('start_from_date')
            ->first();

        if (is_null($policy_id)) {
            return null;
        }

        $current_policy = EmployeeLeavePolicy::where('id', '=', $policy_id->id)
            ->first();

        return $current_policy;
    }
}
