<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tenant\Traits\ForTenants;

class EmployeePayment extends Model
{
    use ForTenants;

    //
    protected $guarded = [];

    public function Employee()
    {
        return $this->belongsTo('App\Employee');
    }

    public function Currency()
    {
        return $this->belongsTo('App\Country', 'currency_id');
    }

    // bank , cash, cheque
    public function PaymentMode()
    {
        return $this->belongsTo('App\PaymentMode', 'payment_mode_id');
    }

    // pay periods
    public function PaymentSchedule()
    {
        return $this->belongsTo('App\PaymentSchedule', 'payment_schedule_id');
    }

    // what $ amount represents
    public function PaymentType()
    {
        return $this->belongsTo('App\PaymentType', 'payment_type_id');
    }
}
