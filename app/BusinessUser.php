<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessUser extends Model
{
    protected $table = 'business_user';
    
    //
    protected $guarded = [];
    //protected $fillable = ['business_id', 'user_id', 'payroll', 'hr', 'hod', 'manager', 'admin', ''];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function business()
    {
        return $this->belongsTo('App\Business');
    }
}
