<?php

namespace App\Jobs;

use App\Employee;
use App\PayrollMaster;
use App\PayrollEmployee;
use App\Mail\ApprovedPayslip;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\File;

class EmailPayslip implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $payroll_employee_id;
    protected $payroll_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($payroll_id, $payroll_employee_id)
    {
        //
        $this->payroll_id = $payroll_id;
        $this->payroll_employee_id = $payroll_employee_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $payroll = PayrollMaster::findOrFail($this->payroll_id);
        $payroll_emp = PayrollEmployee::findOrFail($this->payroll_employee_id);
        $emp = Employee::findOrFail($payroll_emp->employee_id);

        $filename = $this->checkFileExists($payroll->start_date, $payroll->id, $emp->id, $emp->employee_code);

        if ($filename == null) {
            return;
        }
        Log::info('Emailing payslip '.$emp->email_payslip_to);

        $payroll['filename'] = $filename;
        $payroll['payslip_id'] = $this->payroll_employee_id;
        $payroll['name'] = $emp->name;

        Mail::to($emp->email_payslip_to)->send(
            new ApprovedPayslip($payroll)
        );
    }

    private function checkFileExists($start_date, $payroll_id, $emp_id, $emp_code)
    {
        $part_file = date('M-Y', strtotime($start_date)).'.pdf';

        $filename = $emp_id.'-'.$part_file;
        $filename = 'app/payslips/'.$payroll_id.'/'.$filename;
        $exists = File::exists(storage_path($filename));

        if (!$exists) {
            $filename = $emp_code.'-'.$part_file;
            $filename = 'app/payslips/'.$payroll_id.'/'.$filename;
            $exists = File::exists(storage_path($filename));
        } else {
            return $filename;
        }

        if ($exists) {
            return $filename;
        } else {
            return null;
        }
    }
}
