<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Log;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class GeneratePayslip implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $payroll_employee_id;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($payroll_employee_id)
    {
        //
        $this->payroll_employee_id = $payroll_employee_id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        //Log::info('Somehting');
        app('App\Http\Controllers\PrintPayslipController')->generatePayslip($this->payroll_employee_id);
    }
}
