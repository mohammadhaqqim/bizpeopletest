<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TenantConnection extends Model
{
    protected $fillable = ['business_id', 'database', 'db_user', 'db_password'];
}
