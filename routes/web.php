<?php

use App\PayType;
use App\PayrollEmployee;
use App\Exports\TaxExport;
use App\Jobs\EmailPayslip;
use App\Exports\BankExport;
use App\Exports\CountryExport;
use App\Exports\ActivityExport;
use App\Exports\EmployeeExport;
use App\Exports\PayrollTotalsExport;
use App\Http\Controllers\EmailController;
use App\Http\Controllers\LocalizationController;
use App\Http\Controllers\TenantController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Mail;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Tenant\HomeController@index');
Auth::routes();


// ??
Route::get('/person', function () {
    return view('personnel.create');
});

Route::get('/welcome', function () {
    dd(app(Manager::class)->getUserAccess());
    return view('welcome');
});


Route::post('/getmsg', 'Tenant\HomeController@message');

// Real controllers
//??

// ?? Job information ....
Route::get('employee/{id}/job/edit', 'Tenant\EmployeeJobController@edit');


// ???
Route::patch('run/payroll/{id}', 'PayrollMasterController@runpayroll');
Route::patch('add/payroll/{id}', 'PayrollMasterController@addEmployeeToPayroll');


//Route::patch('taxaccounts/{company}', 'CompanyController@accounts');
//Route::patch('workweek/{company}', 'CompanyController@workweek');

//Route::post('workweek', 'WorkingWeekController@store');
//Route::patch('workweek/{id}', 'WorkingWeekController@update');
//Route::delete('workweek', 'WorkingWeekController@destroy');
//Route::get('delete/workweek/{id}', 'WorkingWeekController@destroy')->name('workweek-delete');

//Route::patch('workweek/{company}', 'WorkweekController@update');
//Route::get('delete/workweek/{company}', 'WorkweekController@destroy')->name('workweek-delete');

/*
Route::get('settings/week/edit', function () {
    return view('settings.week');
});

Route::get('settings/access/edit', function () {
    return view('settings.access');
});
 */


// Personal



//Settings
// ?? -- All need to add new Controllers
//Route::get('settings/company', 'SettingsController@company');
//Route::get('settings/holidays', 'SettingsController@holidays');
//Route::get('settings/holidays', 'SettingsController@holidays');
//Route::get('settings/week', 'SettingsController@week');
//Route::get('settings/access', 'SettingsController@access');
//Route::get('settings/allowance', 'SettingsController@allowance');

//Route::get('settings/timeoff/', 'SettingsController@timeoff');
//Route::get('settings/timeoff/', 'SettingsController@timeoff');


// ?? -- All need to add new Controllers
/* Route::get('settings/{id}/company', 'SettingsController@company');
Route::get('settings/{id}/holidays', 'SettingsController@holidays');
Route::get('settings/{id}/week', 'SettingsController@week');
Route::get('settings/{id}/access', 'SettingsController@access');
 */

/* Route::post('leavetype', 'LeaveTypeController@store');
Route::patch('leavetype/{leavetype}', 'LeaveTypeController@update');
Route::delete('leavetype/{leavetype}', 'LeaveTypeController@destroy');
Route::get('delete/leavetype/{leavetype}', 'LeaveTypeController@destroy')->name('leavetype-delete'); */

//Route::get('payroll/historical', 'PayrollMasterController@historical');
//Route::get('payroll/employees', 'PayrollMasterController@employees');
//Route::get('payroll/reports', '');

//
Route::resource('businesses', 'BusinessController');
Route::get('/tenant/{business}', 'TenantController@switch')->name('tenant.switch');


// Small options
Route::resource('countries', 'Tenant\CountryController');
Route::resource('employmentstatuses', 'EmploymentStatusController');


// Employee items
//Route::get('people', 'EmployeeController@index');
//Route::get('people/{employee}/edit', 'EmployeeController@edit');

//Route::resource('employee', 'EmployeeController');                              //secure+



// ??
//Route::patch('employee/{employee}/jobinfo', 'EmployeeJobController@jobinformation');
Route::patch('employee/{employee}/jobinfo', 'EmployeeJobInfoController@update'); //finalied

// Employee Allowances
Route::get('employee/{employeeid}/allowances/{allowanceid}/edit', 'Tenant\EmployeeAllowanceController@edit');

// Employee Time Off
//Route::resource('people/timeoff', 'EmployeeTimeOffController');



Route::get('settings/timeoff/policy', 'LeavePolicyController@create');
Route::get('settings/timeoff/policy/{id}/edit', 'LeavePolicyController@edit')->name('policy-edit');
//Route::post('timeoffpolicy', 'LeavePolicyController@store');
//Route::patch('timeoffpolicy/{id}', 'LeavePolicyController@update');

/* Route::get('timeoff/approval/{id}', 'TimeOffApproval@show');
Route::patch('timeoff/approval/{id}', 'TimeOffApproval@approve');
Route::get('timeoff/deny/{id}', 'TimeOffApproval@deny')->name('timeoff-deny');
Route::patch('timeoff/deny/{id}', 'TimeOffApproval@deny');
 */
//Route::post('timeoff/policy', '');


Route::post('user', 'RegistrationController@store');
Route::patch('user/{id}', 'RegistrationController@update');

Route::post('business_user', 'BusinessUserController@store');

Route::get('print_payroll_totals/{id}', 'PrintPayrollTotalsController@index');

// Employee JobP


//Route::patch('companies/{company}/logo', 'Tenant\CompanyController@logo');
//Route::resource('banks', 'BankController');

Route::get('designations/create', 'Tenant\DesignationController@create')->name('designations/create');

//Route::resource('designations', 'DesignationController');

///Route::resource('transaction', 'TransactionController');
//Route::post('transaction/', 'TransactionController@store');
//Route::post('transaction/', 'TransactionController@store');


//Route::post('company/{company}/period', 'ReportingPeriodController@create');
//Route::patch('company/{company}/period', 'ReportingPeriodController@update');
//Route::patch('company/{company}/reportingperiod', 'ReportingPeriodController@setReportingPeriod');

Route::get('password/change', 'ChangePasswordController@index');
Route::post('password/change', 'ChangePasswordController@store')->name('change.password');

Route::get('change-password', 'ChangePasswordController@index');
Route::post('change-password', 'ChangePasswordController@store')->name('change.password');


Route::get('download_totals/{payroll}', 'Tenant\ReportController@export');

//Route::get('settings/holidays/edit', 'PublicHolidaysController@index');
//Route::post('holiday', 'PublicHolidaysController@store');
//Route::patch('holiday/{id}', 'PublicHolidaysController@update');
//Route::delete('holiday/{id}', 'PublicHolidaysController@destroy');

//Route::post('getDaysOff', 'EmployeeDaysOffController@daysoff');
//Route::get('getDaysOffEdit', 'EmployeeDaysOffController@daysoff');

//Route::get('employeeleavepolicy/calculatetotals/{employeeleavepolicy}', 'EmployeeLeavePolicyController@getRecalculateBalances');

/* Route::get('employee/{employee}/leavetype/{leavetype}', 'EmployeeTimeOffController@UpdateLeavePolicy');
 */


//Route::get('settings/allowance', 'CustomisedDataController@index');

Route::get('employee-avatar/{id}', 'EmployeeAvatarController@edit');
Route::get('employee-avatar', 'EmployeeAvatarController@create');

//Route::get('employee-import', 'EmployeeController@importExportView');
//Route::post('employee-import', 'EmployeeController@import')->name('employee-import');

Route::get('employee-export', function () {
    //return dd('export file');
    return Excel::download(new EmployeeExport, 'employees.xlsx');
//    return Excel::download(new CountryExport, 'countries.xlsx');
})->name('employee-export');

//Route::get('export', 'Employees@export')->name('export');
//Route::post('import', 'Employees@import')->name('import');


//Documentation Route Controller
Route::resource('/documentations', 'DocumentationController');

//Sidebar Documentation Individual for View Only
Route::get('doctopic', 'DocumentationController@doctopic');
Route::get('docgettingstarted', 'DocumentationController@docgettingstarted');
Route::get('docpayroll', 'DocumentationController@docpayroll');
Route::get('docpeople', 'DocumentationController@docpeople');
Route::get('docreport', 'DocumentationController@docreport');

Route::get('docuser', 'DocumentationController@docuser');
Route::get('docsetting', 'DocumentationController@docsetting');
Route::get('docerror', 'DocumentationController@docerror');
Route::get('docterm', 'DocumentationController@docterm');
Route::get('docpolicy', 'DocumentationController@docpolicy');

//Documentation Content Individual for View Only
Route::get('doclogin', 'DocumentationController@doclogin');
Route::get('docforgetpassword', 'DocumentationController@docforgetpassword');
Route::get('docdepartment', 'DocumentationController@docdepartment');
Route::get('docemployee', 'DocumentationController@docemployee');

//Documentation Learn Individual for View Only
Route::get('doclearnbizgetstart', 'DocumentationController@doclearnbizgetstart');
Route::get('doclearnbizgetdashboard', 'DocumentationController@doclearnbizgetdashboard');
Route::get('doclearnbizgetchangepassword', 'DocumentationController@doclearnbizgetchangepassword');

Route::get('doclearnbizpayrollfeature', 'DocumentationController@doclearnbizpayrollfeature');
Route::get('doclearnbizpayrolltransaction', 'DocumentationController@doclearnbizpayrolltransaction');
Route::get('doclearnbizpayrollemployee', 'DocumentationController@doclearnbizpayrollemployee');
Route::get('doclearnbizpayrollhistory', 'DocumentationController@doclearnbizpayrollhistory');
Route::get('doclearnbizpayrollreport', 'DocumentationController@doclearnbizpayrollreport');

Route::get('doclearnbizpeople', 'DocumentationController@doclearnbizpeople');
Route::get('doclearnbizpeopleprofile', 'DocumentationController@doclearnbizpeopleprofile');
Route::get('doclearnbizpeopleprofilepersonal', 'DocumentationController@doclearnbizpeopleprofilepersonal');
Route::get('doclearnbizpeopleprofilejob', 'DocumentationController@doclearnbizpeopleprofilejob');
Route::get('doclearnbizpeopleprofileallowance', 'DocumentationController@doclearnbizpeopleprofileallowance');
Route::get('doclearnbizpeopleprofiletimeoff', 'DocumentationController@doclearnbizpeopleprofiletimeoff');
Route::get('doclearnbizpeopleprofilefamily', 'DocumentationController@doclearnbizpeopleprofilefamily');

Route::get('doclearnbizreport', 'DocumentationController@doclearnbizreport');


//Language Switcher Localization
Route::get("locale/{lang}",[LocalizationController::class,'setLang']);

//Errors for View Only
Route::get("errors/errorpage", 'ErrorController@errorpage');
Route::get("errors/errorpage404", 'ErrorController@errorpage404');

//Test Mail
Route::get('emails/email',[EmailController::class,'index'] );
