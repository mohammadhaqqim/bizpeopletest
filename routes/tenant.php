<?php

use App\Tenant\Manager;
use App\Exports\TaxExport;
use App\Exports\BankExport;
use App\Exports\ActivityExport;
use Illuminate\Support\Facades\DB;
use App\Exports\PayrollTotalsExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Tenant\ProjectController;
use App\Http\Controllers\Tenant\CustomisedDataController;

Route::get('/home', [App\Http\Controllers\Tenant\HomeController::class, 'index'])->name('home');

//Route::get('/home', 'HomeController@index')->name('home');
//Route::resource('projects', ProjectController::class);

Route::patch('payroll/{payroll_id}/employee', 'Tenant\PayrollEmployeeController@store');

Route::patch('run/payroll/{id}', 'Tenant\PayrollMasterController@runpayroll');
Route::patch('add/payroll/{id}', 'Tenant\PayrollMasterController@addEmployeeToPayroll');
Route::get('payroll/historical', 'Tenant\PayrollMasterController@historical');
Route::get('payroll/employees', 'Tenant\PayrollMasterController@employees');

Route::post('employee/{employee}/endservice', 'Tenant\EmployeeController@endService'); //need to test
Route::get('employee/delete_end_service/{employee}', 'Tenant\EmployeeController@clearEndService'); //need to test

// Payroll
Route::resource('payroll', 'Tenant\PayrollMasterController');                      //secure+
Route::resource('payrolltransaction', 'Tenant\PayrollTransactionController');          //secure
Route::resource('payrollemployee', 'Tenant\PayrollEmployeeController');                //secure
Route::resource('end_employment_reasons', 'Tenant\EndEmploymentReasonController');

Route::get('payroll/{id}/employees', 'Tenant\PayrollController@employees');
Route::get('payroll/historical/{id}/employees', 'Tenant\PayrollController@employees');
Route::get('payroll/{id}/transactions', 'Tenant\PayrollController@transactions');
Route::get('payroll/historical/{id}/transactions', 'Tenant\PayrollController@transactions');

Route::get('payroll/{id}/approval', 'Tenant\PayrollApprovalController@index');
Route::get('payroll/historical/{id}/approval', 'Tenant\PayrollApprovalController@index');
//??
Route::get('payroll/{id}/approvals', 'Tenant\PayrollApprovalController@index');
Route::patch('payroll/{id}/approval', 'Tenant\PayrollApprovalController@update');

Route::patch('payroll/{id}/batch-approval', 'Tenant\PayrollApprovalController@batch');
//Route::patch('payroll-approval/{id}', 'PayrollController@approve');
Route::post('payroll/{id}/transaction', 'Tenant\PayrollTransactionController@store');
Route::get('payroll-transactions', 'Tenant\PayrollController@transactions');

Route::get('printpayslip/{id}', 'Tenant\PrintPayslipController@index');
Route::get('printpayslip/payrolls/{id}', 'Tenant\PrintPayslipController@print_all_payslips');

Route::get('/test', function () {

//    return redirect()->route('tenant.switch', ['business' => auth()->user()->companies[0]->id]);
    //dd(app(Manager::class)->getUserAccess());
});

Route::get('/single', function () {
    return redirect()->route('tenant.switch', ['business' => auth()->user()->companies[0]->id]);
//    dd(app(Manager::class)->getUserAccess());
});

//views
Route::get('settings/customise', 'Tenant\CustomisedDataController@index');
Route::get('settings/customise/{data}', 'Tenant\CustomisedDataController@display');

Route::resource('settings/access', 'BusinessUserController');
Route::resource('settings/user_access', 'BusinessUserController');
Route::resource('settings/week', 'Tenant\WorkingWeekController');
Route::resource('workweek', 'Tenant\WorkingWeekController');

// N/A
//Route::resource('settings/allowance', 'Tenant\EmployeeAllowanceController');

//Route::resource('allowance', 'Tenant\AllowanceController');
Route::resource('allowances', 'Tenant\EmployeeAllowanceController');                   //finalised
Route::resource('transaction', 'Tenant\TransactionController');
//Route::resource('timeoff', 'Tenant\TimeoffController');
//Route::resource('timeoff', 'Tenant\EmployeeTimeOffController');                   //finalised


//Route::get('settings/week', 'SettingsController@week');

Route::resource('departments', 'Tenant\DepartmentController');
Route::resource('companies', 'Tenant\CompanyController');
Route::resource('designations', 'Tenant\DesignationController');
Route::resource('banks', 'Tenant\BankController');
Route::resource('races', 'Tenant\RaceController');
Route::resource('religions', 'Tenant\ReligionController');
Route::resource('payment_types', 'Tenant\PaymentTypeController');
Route::resource('relationships', 'Tenant\RelationshipController');
Route::resource('holidays', 'Tenant\PublicHolidayController');
Route::resource('leavetypes', 'Tenant\LeaveTypeController');
Route::resource('timeoffpolicy', 'Tenant\LeavePolicyController');
Route::resource('people', 'Tenant\EmployeeController');
Route::resource('employee', 'Tenant\EmployeeController');
Route::resource('people/jobs', 'Tenant\EmployeeJobController');
Route::resource('allowances', 'Tenant\EmployeeAllowanceController');                   //finalised
Route::resource('people/allowances', 'Tenant\EmployeeAllowanceController');                   //finalised
// Employee Family
Route::resource('people/relations', 'Tenant\RelationsController');
Route::resource('relations', 'Tenant\RelationsController');

Route::get('settings/company', 'Tenant\CompanyController@index');
Route::get('settings/allowance', 'Tenant\TransactionController@index');
Route::get('settings/timeoff/policy', 'Tenant\LeavePolicyController@create');
Route::get('settings/timeoff/', 'Tenant\TimeoffController@index');
//Route::get('settings/holidays/edit', 'Tenant\PublicHolidayController@index');
Route::get('settings/holidays', 'Tenant\PublicHolidayController@index');
Route::get('settings/contributions', 'Tenant\ContributionController@index');


Route::post('company/period', 'Tenant\ReportingPeriodController@create');
//Route::patch('company/reportingperiod', 'ReportingPeriodController@setReportingPeriod');
Route::patch('company/logo', 'Tenant\CompanyController@logo');
Route::post('company/logo', 'Tenant\CompanyController@logo');
//Route::patch('companies/{company}/logo', 'Tenant\CompanyController@logo');

Route::patch('company/reportingperiod', 'Tenant\ReportingPeriodController@setReportingPeriod');

Route::get('delete/leavetype/{leavetype}', 'Tenant\LeaveTypeController@destroy')->name('leavetype-delete');
Route::get('settings/timeoff/policy/{id}/edit', 'Tenant\LeavePolicyController@edit')->name('policy-edit');

Route::patch('taxaccounts', 'Tenant\CompanyController@accounts');


// deleting via anchors
Route::get('delete/departments/{department}', 'Tenant\DepartmentController@destroy');
Route::get('delete/designations/{designation}', 'Tenant\DesignationController@destroy');
Route::get('delete/banks/{bank}', 'Tenant\BankController@destroy');
Route::get('delete/payment_modes/{id}', 'Tenant\PaymentModeController@destroy');
Route::get('delete/payment_types/{id}', 'Tenant\PaymentTypeController@destroy');
Route::get('delete/races/{race}', 'Tenant\RaceController@destroy');
Route::get('delete/relations/{id}', 'Tenant\RelationsController@destroy');
Route::get('delete/religions/{religion}', 'Tenant\ReligionController@destroy');
Route::get('delete/workweek/{id}', 'Tenant\WorkingWeekController@destroy')->name('workweek-delete');
Route::get('delete/relationships/{id}', 'Tenant\RelationshipController@destroy');

Route::get('people/personal/{employee}/edit', 'Tenant\EmployeeController@edit');

//Route::get('people/jobs/{id}/edit', 'EmployeeJobController@edit');
//Route::patch('people/jobs/{employee_job}', 'EmployeeJobController@update');
//Route::post('people/jobs/', 'EmployeeJobController@store');

Route::get('jobs/{id}/edit', 'Tenant\EmployeeJobController@edit');
Route::patch('jobs/{employee_job}', 'Tenant\EmployeeJobController@update');
Route::post('jobs/', 'Tenant\EmployeeJobController@store');

Route::patch('employee/{employee}/jobinfo', 'Tenant\EmployeeJobInfoController@update'); //finalied
Route::patch('statuses/{employee_status}', 'Tenant\EmployeeStatusController@update');
Route::post('statuses/', 'Tenant\EmployeeStatusController@store');
Route::patch('payments/{employee_payment}', 'Tenant\EmployeePaymentController@update');
Route::post('payments/', 'Tenant\EmployeePaymentController@store');

Route::delete('people/timeoff/{id}', 'Tenant\EmployeeTimeOffController@destroy')->name('timeoff-delete');
Route::get('people/timeoff/{id}/', 'Tenant\EmployeeTimeOffController@index')->name('people.timeoff');
Route::post('people/timeoff/{id}/', 'Tenant\EmployeeTimeOffController@store');

Route::get('employeeleavepolicy/calculatetotals/{employeeleavepolicy}', 'Tenant\EmployeeLeavePolicyController@getRecalculateBalances');

Route::get('amend/timeoff/{id}/edit', 'Tenant\AmendTimeoffController@edit');

Route::get('timeoff/{id}/', 'Tenant\EmployeeTimeOffController@index');
Route::post('timeoff/{id}/', 'Tenant\EmployeeTimeOffController@store');
Route::get('timeoff/{id}/edit', 'Tenant\EmployeeTimeOffController@edit');
Route::patch('timeoff/{id}', 'Tenant\EmployeeTimeOffController@update');

Route::get('timeoff/approval/{id}', 'Tenant\TimeOffApproval@show');
Route::patch('timeoff/approval/{id}', 'Tenant\TimeOffApproval@approve');
Route::get('timeoff/deny/{id}', 'Tenant\TimeOffApproval@deny')->name('timeoff-deny');
Route::patch('timeoff/deny/{id}', 'Tenant\TimeOffApproval@deny');

Route::get('employee/{employee}/timeoff/create', 'Tenant\EmployeeTimeOffController@create');
Route::get('timeoff/employee/{employee}/create', 'Tenant\EmployeeTimeOffController@create');

Route::get('inlieu/employee/{employee}/create', 'Tenant\EmployeeInlieuController@create');
Route::post('inlieu/employee/{employee}', 'Tenant\EmployeeInlieuController@store');


Route::get('employee/{employee}/leavepolicy/{leavetype}', 'Tenant\EmployeeLeavePolicyController@create');
//Route::patch('employee/{employee}/leavepolicy/{leavetype}', 'Tenant\EmployeeLeavePolicyController@store');
Route::patch('employee/{employee}/leavepolicy/{leavetype}', 'Tenant\EmployeeLeavePolicyController@update');

Route::post('getDaysOff', 'Tenant\EmployeeDaysOffController@daysoff');
Route::post('getInlieu', 'Tenant\EmployeeDaysOffController@inlieu');
Route::get('getDaysOffEdit', 'Tenant\EmployeeDaysOffController@daysoff');

// typo ??
Route::resource('payment-modes', 'Tenant\PaymentModeController');
Route::resource('employee-types', 'EmployeeTypeController');

//Route::get('departments/create', 'DepartmentController@create')->name('departments/create');

Route::get('employee-import', 'Tenant\EmployeeController@importExportView');
Route::post('employee-import', 'Tenant\EmployeeController@import')->name('employee-import');

Route::post('employee/{id}/relation', 'Tenant\RelationsController@store');            //
Route::patch('employee/{id}/relation', 'Tenant\RelationsController@update');            //

Route::get('reports', 'Tenant\ReportController@index');
Route::get('payroll/reports/audit', 'Tenant\ReportController@payroll_audit');

Route::post('employee/{employee}/status', 'Tenant\EmployeeStatusController@store');    //finalised
Route::patch('employee/{employee_status}/status', 'Tenant\EmployeeStatusController@update');    //finalised
Route::post('employee/{employee}/job', 'Tenant\EmployeeJobController@store');          //finalised
Route::post('employee/{employee}/payment', 'Tenant\EmployeePaymentController@store');  //finalised

Route::get('contribution/{type}', 'Tenant\ContributionController@edit');
Route::patch('contribution/{type}', 'Tenant\ContributionController@update');
//Route::post('contribution/{type}', 'Tenant\ContributionController@store');

// Reports
Route::get('report/payroll_totals/{payroll}', function ($payroll) {
    //return Excel::create(new PayrollTotalsExport('Payroll Totals Report', $payroll), 'PayrollTotals.pdf')->export('pdf');
    //return Excel::download(new PayrollTotalsExport('Payroll Totals Report', $payroll), 'PayrollTotals.pdf');
    return Excel::download(new PayrollTotalsExport('Payroll Totals Report', $payroll), 'PayrollTotals.xlsx');
});
Route::get('report/activity/{payroll}', function ($payroll) {
    return Excel::download(new ActivityExport('Activity Report', $payroll), 'Activity.xlsx');
});
Route::get('report/bank/{payroll}', function ($payroll) {
    return Excel::download(new BankExport('Bank Report', $payroll), 'Bank.xlsx');
});
Route::get('report/tax/{payroll}', function ($payroll) {
    return Excel::download(new TaxExport('Tax Report', $payroll), 'Tax.xlsx');
});


// look up and check ...

// N/A
//Route::resource('personnel', 'PersonnelController');



//Settings
// ?? -- All need to add new Controllers
//Route::get('settings/company', 'SettingsController@company');

// auth - individual
