@extends('documentationlayouts.app')

@section('content')

<h1 style="text-align: center">Biz People Documentation</h1>

<hr>

<div class="row">
    <div class="col-sm-12" style="text-align:right;">
        <a  href="/documentations/" class="btn btn-secondary">Go Back To Previous Page</a>
    </div>
</div>

<br>

<div class="row list-group-item">

    <div class="row">
        <div class="col-sm-12">
            <h2>Detail of the Document</h2>
        </div>
    </div>

    <hr>

    <!-- Documentation Detail -->
    <div class="row">
        <div class="col-sm-1">
            <small>ID :</small>
        </div>
        <div class="col-sm-1">
            <h5>{{ $documentation->id }}</h5>
        </div>
        <div class="col-sm-2">
            <small>TITLE :</small>
        </div>
        <div class="col-sm-4">
            <h5>{{ $documentation->documentation_name }}</h5>
        </div>
        <div class="col-sm-4">
            <small>DESCRIPTION :</small>
          </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-sm-4">
            <small>IMAGE FILENAME :</small>
        </div>
        <div class="col-sm-8">
            <h5>{{ $documentation->image_link }}</h5>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-sm-4">
            <small>IMAGE :</small>
        </div>
        <div class="col-sm-8">
            <img src="/storage/image_link/{{$documentation->image_link}}" class="img-responsive" style="max-height:200px; max-width:300px;" alt="" srcset=""/>
        </div>
    </div>

   </div>

   <hr>

    <div class="row">
        <div class="col-sm-12">
             <p style="text-align: right">{{ $documentation->documentation_name }} created on {{ $documentation->created_at }}</p>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-3" style="text-align: right">
        <!-- Edit Link -->
        <a href="/documentations/{{$documentation->id}}/edit" class="btn btn-secondary">Edit Document</a>
        </div>

        <div class="col-sm-3" style="text-align: right">
        <!-- Delete Function -->
        <form action="/documentations/{{  $documentation->id  }}" method="POST">
            @method('DELETE')
            @csrf
            <button type="submit" class="btn btn-danger">Delete Document</button>
        </form>
        </div>

    </div>

 @endsection






