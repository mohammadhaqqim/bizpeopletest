@include('documentationlayouts.app')

@extends('documentationinclude.appcontent')

@section('doccontent')


<h1 style="text-align: left;">
  <img src="/images/icons/iconreport.png" width="40px" height="40px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
 @lang('public.docreport')
</h1>

<br>


<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/doclearnbizreport" style="font-size: 25px;">@lang('public.wheretofind')</a>
    </div>
</div>

@endsection
