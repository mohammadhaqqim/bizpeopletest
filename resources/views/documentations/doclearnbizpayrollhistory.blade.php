@include('documentationlayouts.app')

@extends('documentationinclude.appcontent')

@section('doccontent')

<h1 style="text-align: left;">
  <img src="/images/icons/iconlearn.png" width="40px" height="40px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
  @lang('public.wheretocheck')</a>
</h1>

<hr>

<br>

<div class="row">
    <div class="col-sm-12">
      <h3 style="font-weight: bolder">@lang('public.topicpayroll')</h3>
      </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12">
        <h3>@lang('public.readusermanual')</h3>
      </div>
</div>

<br>

<div class="row" style="text-align: justify;">
    <div class="col-sm-12">
       <h3>
         @lang('public.mainpayrollhistory')
       </h3>
    </div>
</div>

<br><br>

<div class="row">
    <div class="col-sm-12">
      <h3 style="font-weight: bolder">@lang('public.mainpayrollhistorylist')</h3>
      </div>
</div>

<br>

<div class="row" style="text-align: center">
    <div class="col-sm-12">
       <img src="/images/docs/BizPayrollHistorical.jpg"class="bd-placeholder-img" width="100%" height="100%">
    </div>
</div>

<br>

<div class="row" style="text-align: justify;">
    <div class="col-sm-12">
       <h3>
         @lang('public.readhistorylist')
       </h3>
    </div>
</div>

<br><br>

<div class="row">
    <div class="col-sm-12">
      <h3 style="font-weight: bolder">@lang('public.mainpayrollhisotrycreate')</h3>
      </div>
</div>

<br>

<div class="row" style="text-align: center">
    <div class="col-sm-12">
       <img src="/images/docs/BizPayrollHistoricalCreate.jpg"class="bd-placeholder-img" width="100%" height="100%">
    </div>
</div>

<br>


<div class="row" style="text-align: justify;">
    <div class="col-sm-12">
       <h3>
         @lang('public.readpayrollhistorycreate')
       </h3>
    </div>
</div>


<br>

<hr>

<div class="row" style="text-align: left;">
    <div class="col-sm-12">
       <h4 style="font-weight: bolder">
          @lang('public.continuebasic')
       </h4>

       <br>

       <a href="http://127.0.0.1:8000/doclearnbizpayrollfeature" style="font-size: 20px;">@lang('public.whatfeature')</a>

       <br><br>

       <a href="http://127.0.0.1:8000/doclearnbizpayrolltransaction" style="font-size: 20px;">@lang('public.howcheck')</a>

       <br><br>

       <a href="http://127.0.0.1:8000/doclearnbizpayrollemployee" style="font-size: 20px;">@lang('public.wherecan')</a>

       <br><br>

       <a href="http://127.0.0.1:8000/doclearnbizpayrollhistory" style="font-size: 20px;">@lang('public.wheretocheck')</a>

       <br><br>

       <a href="http://127.0.0.1:8000/doclearnbizpayrollreport" style="font-size: 20px;">@lang('public.wheretodownload')</a>

    </div>
</div>

@endsection
