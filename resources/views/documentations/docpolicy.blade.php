@include('documentationlayouts.app')

@extends('documentationinclude.appcontent')

@section('doccontent')

<h1 style="text-align: left;">
  <img src="/images/icons/iconpolicy.png" width="40px" height="40px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
  Privacy Policy
</h1>

<br>

  <hr>

  <h5 style="font-weight: bolder">
   Information on Privacy Policy from BizIT Solution System are really certified :
  </h5>

 <p>
  This Privacy Policy explains how BizIT Solution Sdn Bhd (“BizIT”, “us”, “we” or “our”) and its affiliates collect, use, disclose, transfer, protect, store
  and otherwise process your information when using our Services.
 </p>

 <p>
  BizIT Solution Sdn Bhd is committed to the privacy and secure processing of the personal data it maintains for its User or Customers in an open and transparent manner.
 </p>

<p>
It is also committed to the collection and processing of the personal data in full compliance
</p>

<p>
With the General Regulation on the Protection of Personal Data of the BizIT Solution (Regulation 2021)
(hereafter referred to as "the Regulation") and the legislation in force that governs the collection and processing of Personal Data of individuals.
</p>

<p>This Privacy Policy was updated on 28 October 2021.</p>

<hr>

@endsection
