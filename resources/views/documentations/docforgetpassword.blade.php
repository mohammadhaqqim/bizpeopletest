@include('documentationlayouts.app')

@extends('documentationinclude.appcontent')

@section('doccontent')

<h1 style="text-align: left;">
  <img src="/images/icons/iconunlock.png" width="40px" height="40px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
  Documentation : Forgot Password
</h1>


<div class="row">
 <div class="col-sm-12">
   <h3>User Manual for Forget Password</h3>
 </div>
</div>

<div class="row">
 <div class="col-sm-12">
   <h3>3. Login to BizPeople : Forgot Password</h3>
   </div>
</div>

<br>

<div class="row" style="text-align: justify; font-size:20px;">
    <div class="col-sm-12">
      <p>
         System will provide a process for the ‘Forgot Password’,
         User need to follow the instruction that stated on the Figure 3 Login Page - Forgot Password.
       </p>
     </div>
</div>

<div class="row" style="text-align: center">
    <div class="col-sm-12">
       <img src="/images/docs/docforgotpassword.png" class="bd-placeholder-img" width="100%" height="100%">
    </div>
</div>

<br>

<div class="row" style="text-align: justify; font-size:20px;">
    <div class="col-sm-12">
       <p>
          User must insert their registered ‘Email’ account into the ‘Textbox’ field,
          and then user need to click on the ‘Continue’ button,
          to finalize the process in this page.
       </p>
       <p>
         If it is ‘Correct Email’, system will show the further process for User to ‘Reset Password’.
       </p>
    </div>
</div>


@endsection
