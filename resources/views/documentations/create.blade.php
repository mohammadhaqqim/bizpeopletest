@extends('documentationlayouts.app')

@section('content')

<h1 style="text-align: center">Biz People Documentation</h1>

<hr>

<div class="row">
    <div class="col-sm-12" style="text-align:right;">
        <a  href="/documentations" class="btn btn-secondary">Go Back To Previous Page</a>
    </div>
</div>

<br>

<div class="row list-group-item">

    <div class="row">
        <div class="col-sm-12">
        <h3>Form : Create New Documentation</h3>
        </div>
        <div class="col-sm">
        <h6>Please fill in all the Empty Field</h6>
        </div>
    </div>

    <hr>

    <form action="/documentations" method="POST" enctype="multipart/form-data">
    @csrf

    <div class="row">
    <div class="form-group col-sm-12">
        <label for="title">Document Name</label>
        <input class="form-control" type="text" name="documentation_name" id="documentation_name" placeholder="How to Get Started? , Read System Step etc.">
    </div>
    </div>

    <div class="row">
        <div class="form-group col-sm-12">
            <label for="title">Document Text</label>
            <input class="form-control" type="text" name="documentation_text" id="documentation_text" placeholder="Tutorial 1, Lesson 1 etc.">
        </div>
    </div>

    <div class="row">
        <div class="form-group col-sm-12">
            <label for="title">Upload Document Image</label>
            <input type="file" id="image_link" name="image_link" class="form-control">
        </div>
    </div>

    <div class="row">
        <div class="form-group col-sm-12" style="text-align: right">
            <button type="submit" class="btn btn-primary">Add / Submit Document</button>
        </div>
    </div>

    </form>

</div>


@endsection
