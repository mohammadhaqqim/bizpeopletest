@include('documentationlayouts.app')

@extends('documentationinclude.appcontent')

@section('doccontent')


<h1 style="text-align: left;">
  <img src="/images/icons/iconuser.png" width="40px" height="40px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
  @lang('public.docuser')
</h1>

<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/doclogin" style="font-size: 25px;">@lang('public.howtolog')</a>
    </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/docforgetpassword" style="font-size: 25px;">@lang('public.howtoreset')</a>
    </div>
</div>

@endsection
