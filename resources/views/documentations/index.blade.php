@extends('documentationlayouts.app')

@section('content')

<h1 style="text-align: center;">Biz People Documentation</h1>

<hr>

<div class="row">
    <div class="col-sm-6">
        <h2>Available Document</h2>
    </div>
    <div class="col-sm-6" style="text-align:right;">
        <a class="btn btn-success" href="http://127.0.0.1:8000/documentations/create">Add New Document</a>
    </div>
</div>

<br>

<div class="table-responsive-sm">
    <table class="table table-sm table-striped table-hover" style="text-align: center;">
        <thead>
            <tr>
                <th>No.</th>
                <th>Image</th>
                <th>Documentation Name</th>
                <th>Documentation Description</th>
                <th>Created</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @if(count($documentations) > 0)
            @foreach($documentations as $documentation)
            <tr>
                <td>{{ $documentation->id }}</td>
                <td><img src="/storage/image_link/{{ $documentation->image_link }}" class="img-responsive rounded" style="max-height:100px; max-width:100px;" alt="" srcset="" /></td>
                <td>{{ $documentation->documentation_name }}</td>
                <td>{{ $documentation->documentation_text }}</td>
                <td>{{ $documentation->created_at }}</td>
                <td><a href="/documentations/{{ $documentation->id }}">View {{ $documentation->documentation_name }}</a></td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="row">
        <div class="col-sm-12">
          <br>
           {{ $documentations->links( "pagination::bootstrap-4") }}
        </div>
      </div>
          @else
          <div class="row">
              <div class="list-group-item col-sm-12">
                  <p style="padding-top: 10px;">No Document Found</p>
              </div>
          </div>
      @endif
    </div>

@endsection
