@include('documentationlayouts.app')

@extends('documentationinclude.appcontent')

@section('doccontent')

<h1 style="text-align: left;">
    <img src="/images/icons/iconterm.png" width="40px" height="40px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
    Terms of Use
</h1>

<br>

<hr>

<h5 style="font-weight: bolder">For the User or Employee that are responsible to use these provided System must follow the term that stated below :</h5>

<p>
 These Terms of use which also include the Privacy Policy, Cookies Policy,
 Data Processing Addendum and Terms of Third-Party Integration ("Terms")
 set out the terms and conditions for your use of Services offered by BizIT Solution Sdn Bhd.
</p>

<p>
 By clicking “Accept” or using any of the Services, you acknowledge that you have read and understood these Terms,
 and that you have agreed to these Terms on behalf of any person or entity for whom you are using the Services.
 If you do not agree to any of these Terms, then you should not use the Services.
</p>

<p>
 All rights not expressly granted in these Terms are reserved by BizIT Solution.
 We may change these Terms from time to time by posting updates to the Website.
 You are advised to check the Terms from time to time for any updates or changes that may impact you
 and if you do not accept such amendments, you must cease using the Services.
</p>

<p>These Terms of Use were updated on 28 October 2021.</p>

<hr>

<h5 style="font-weight: bolder">Use of your Account</h5>

<p>
BizIT Solution grants you a limited, non-exclusive, non-transferable, revocable licence to
use the Services for the purposes of with the Customer's business and in accordance with these Terms.
</p>

<p>
You are responsible for any conduct or activity undertaken in your account, including any changes made, and
the input or modification of or access to any data or information in your account, by any of your Authorised Users.
</p>

<p>
You control each Authorised User’s level of access to the Services at all times
and can revoke or change an Authorised User’s access, or level of access,
at any time and for any reason, in which case that person or entity will cease to be an Authorised User
or shall have that different level of access, as the case may be.
</p>

<p>
If:
</p>
<p>
(a) you are an Authorised User using or accessing the Services for the benefit of an Customer, then you are
responsible for ensuring you have the right to do so from the relevant Customer
(including all necessary authorisations to access, amend or remove data, or make changes to the Customer's account); or
</p>
<p>
(b) you are a Customer, then you are responsible for authorising use or access of the Services by any Authorised User
and will indemnify Cavius against any claims or loss relating to any Authorised User's use of or access to your account.
</p>

<p>
You acknowledge and agree that, if there is any dispute between a Customer and an Authorised User regarding access to Services,
the Customer shall decide what access or level of access to the relevant Data or Services that Authorized User shall have,
if any. We will have no responsibility to anyone other than the Customer, and the Services and these Terms are intended solely
for the benefit of the Customer and not for any Authorised User(s).
</p>

<br>

<hr>

<h5 style="font-weight: bolder">Cancellation of accounts</h5>

<p>
We can cancel or suspend your account at any time on written notice to you. Such termination will be effective at the end of your
then-current paid-up subscription period. We may also cancel or suspend your account immediately if, in our sole discretion:
</p>

<p>
You have committed a material or persistent breach of these Terms or any other terms applying to your use of the Services;
</p>

<p>
We consider that provision of the Services, or use of them, in your territory or jurisdiction will or is likely to breach any applicable law;
</p>

<p>
We decide to withdraw the Services from your territory or jurisdiction altogether; or
you fail to comply with any limits or restrictions (e.g, on the number of users, stores or transactions) applicable to your plan;
</p>

<p>
Your use of the account is creating a security or availability risk for BizIT Solution or our other customers,
is impacting (or may impact) the stability or performance of our systems, or is requiring disproportionate resource to deliver
 (eg, in terms of storage or processing requirements, support requests or helpdesk queries).
</p>

<p>
You are entitled to cancel your account with BizIT Solution at any time. You can request to cancel your account by sending an email to BizITSolution@Biz.
</p>

<p>
 If you cancel your account before the end of your current paid up month or year, your cancellation will take effect immediately and
 you will not be entitled to any refund of Fees paid in advance (unless we agree otherwise).
</p>

<p>
If your account is cancelled, your account will be deactivated and all of your rights granted under these Terms will immediately come to an end
(except to the extent that it is necessary for them to continue in respect of our ongoing storage of your data up to the date of permanent deletion).
We are not liable for any loss or damage following, or as a result of, cancellation of your account, and it is your responsibility to ensure that any
content or data which you require is exported, backed-up or replicated before cancellation.
</p>


@endsection
