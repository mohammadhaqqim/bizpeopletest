@include('documentationlayouts.app')

@extends('documentationinclude.appcontent')

@section('doccontent')

<h1 style="text-align: left;">
  <img src="/images/icons/iconshare.png" width="40px" height="40px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
  @lang('public.docpeople')
</h1>

<br>

<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/doclearnbizpeople" style="font-size: 25px;">@lang('public.howtoadd')</a>
    </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/doclearnbizpeopleprofile" style="font-size: 25px;">@lang('public.whatfunction')</a>
    </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/doclearnbizpeopleprofilepersonal" style="font-size: 25px;">@lang('public.wherepersonal')</a>
    </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/doclearnbizpeopleprofilejob" style="font-size: 25px;">@lang('public.wherejob')</a>
    </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/doclearnbizpeopleprofileallowance" style="font-size: 25px;">@lang('public.whereallowance')</a>
    </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/doclearnbizpeopleprofiletimeoff" style="font-size: 25px;">@lang('public.wheretime')</a>
    </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/doclearnbizpeopleprofilefamily" style="font-size: 25px;">@lang('public.wherefamily')</a>
    </div>
</div>

@endsection
