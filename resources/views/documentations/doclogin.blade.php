@include('documentationlayouts.app')

@extends('documentationinclude.appcontent')

@section('doccontent')

<h1 style="text-align: left;">
  <img src="/images/icons/iconsecure.png" width="40px" height="40px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
  Documentation : Login
</h1>


<div class="row">
    <div class="col-sm-12">
      <h3>User Manual for Login</h3>
      </div>
</div>

<div class="row">
 <div class="col-sm-12">
   <h3>1. Login to BizPeople : Credential Access</h3>
   </div>
</div>

<br>

<div class="row" style="text-align: justify; font-size:20px;">
    <div class="col-sm-12">
      <p>For Users, when you run the BizPeople Website.
         You will see the welcoming screen and system features that was developed.
         It will show and listed accordingly in a numbered below;
       </p>
     </div>
</div>

<div class="row" style="text-align: center">
    <div class="col-sm-12">
       <img src="/images/docs/doclogincredential.png"class="bd-placeholder-img" width="100%" height="100%">
    </div>
</div>

<br>

<div class="row" style="text-align: justify; font-size:20px;">
    <div class="col-sm-12">
       <p>To Login into the BizPeople System, User Credential is strictly required for individual ‘Users’.
          It is only for ‘Registered Account’ that was provided by the Developer.
       </p>
       <p>User need to insert their exact ‘Email Address’ and ‘Password’ that was registered and the system will automatically allow the user to access the system.
          If user accidentally insert ‘Wrong Credential’, System will give a ‘Pop Out’ notice, which will be show on Figure 2 Login Page - Wrong Credential.
       </p>
    </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12">
      <h3>2. Login to BizPeople : Wrong Credential Access</h3>
      </div>
   </div>

   <br>

   <div class="row" style="text-align: justify; font-size:20px;">
       <div class="col-sm-12">
         <p>If user insert ‘Incorrect Account’ or ‘Wrong Email and Password’, the system will appearing a ‘Failure Message’ below the ‘Email Text Box’.
            User need to fill in the ‘Correct Credential’ to Access the BizPeople System.
        </p>
        </div>
   </div>

   <div class="row" style="text-align: center">
       <div class="col-sm-12">
          <img src="/images/docs/docloginwrongcredential.png" class="bd-placeholder-img" width="100%" height="100%">
       </div>
   </div>

   <br>

   <div class="row" style="text-align: justify; font-size:20px;">
       <div class="col-sm-12">
          <p>For the User, that have forgotten their account credential ‘Password’, the system has provide for User a ‘Link’ that named as ‘Forgot Password’ above the ‘Login Button’ that show on Figure 2 Login Page - Wrong Credential.
          </p>
          <p>User need to insert their exact ‘Email Address’ and ‘Password’ that was registered and the system will automatically allow the user to access the system.
             If user accidentally insert ‘Wrong Credential’, System will give a ‘Pop Out’ notice, which will be show on Figure 2 Login Page - Wrong Credential.
          </p>
        <p>
            If User click the ‘Forgot Password’ link, it will automatically navigate to Figure 3 Login Page - Forgot Password website.
        </p>
       </div>
   </div>

@endsection
