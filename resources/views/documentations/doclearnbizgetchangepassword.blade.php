@include('documentationlayouts.app')

@extends('documentationinclude.appcontent')

@section('doccontent')

<h1 style="text-align: left;">
  <img src="/images/icons/iconlearn.png" width="40px" height="40px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
  @lang('public.howtochangepassword')</a>
</h1>

<hr>

<br>

<div class="row">
    <div class="col-sm-12">
      <h3 style="font-weight: bolder">@lang('public.topicgetstart')</h3>
      </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12">
      <h3>@lang('public.readusermanual')</h3>
      </div>
</div>

<br>

<div class="row">
 <div class="col-sm-12">
   <h3>
    @lang('public.homepagedashboard')
   </h3>
   </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12">
      <h3 style="font-weight: bolder">@lang('public.useraccounticon')</h3>
      </div>
</div>

<br>

<div class="row" style="text-align: center">
    <div class="col-sm-12">
       <img src="/images/docs/BizDashboardNavigation.jpg" class="bd-placeholder-img" width="100%" height="100%">
    </div>
</div>

<br>

<div class="row" style="text-align: justify;">
    <div class="col-sm-12">
       <h3>@lang('public.showtwooption')</h3>

        <br>

        <br>

        <div class="row">
            <div class="col-sm-12">
            <h3 style="font-weight: bolder">@lang('public.passworddashboard') </h3>
            </div>
        </div>

        <br>

       <div class="row" style="text-align: center">
        <div class="col-sm-12">
           <img src="/images/docs/BizDashboardNavigationChangePassword.jpg" class="bd-placeholder-img" width="100%" height="100%">
        </div>
       </div>

       <br>

       <h3>@lang('public.passwordform')</h3>

        <br>

        <h3>@lang('public.updatepassword') </h3>

       <br>

    </div>
</div>

<br>

<hr>

<div class="row" style="text-align: left;">
    <div class="col-sm-12">
       <h4 style="font-weight: bolder">
        @lang('public.continuebasic')
       </h4>

       <br>

       <a href="http://127.0.0.1:8000/doclearnbizgetstart" style="font-size: 20px;">@lang('public.howgetstart')</a>

       <br><br>

       <a href="http://127.0.0.1:8000/doclearnbizgetdashboard" style="font-size: 20px;">@lang('public.finishlogin')</a>

       <br><br>

       <a href="http://127.0.0.1:8000/doclearnbizgetchangepassword" style="font-size: 20px;">@lang('public.howtochangepassword')</a>

       <br>

    </div>
</div>

@endsection
