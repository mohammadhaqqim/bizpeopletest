@include('documentationlayouts.app')

@extends('documentationinclude.appcontent')

@section('doccontent')


<h1 style="text-align: left;">
  <img src="/images/icons/iconpayment.png" width="40px" height="40px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
  @lang('public.docpayroll')
</h1>

<br>

<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/doclearnbizpayrollfeature" style="font-size: 25px;">@lang('public.whatfeature')</a>
    </div>
</div>

<br>

<br>

<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/doclearnbizpayrolltransaction" style="font-size: 25px;">@lang('public.howcheck')</a>
    </div>
</div>

<br>

<br>

<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/doclearnbizpayrollemployee" style="font-size: 25px;">@lang('public.wherecan')</a>
    </div>
</div>

<br>

<br>

<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/doclearnbizpayrollhistory" style="font-size: 25px;">@lang('public.wheretocheck')</a>
    </div>
</div>

<br>

<br>

<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/doclearnbizpayrollreport" style="font-size: 25px;">@lang('public.wheretodownload')</a>
    </div>
</div>

@endsection
