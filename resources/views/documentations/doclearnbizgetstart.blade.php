@include('documentationlayouts.app')

@extends('documentationinclude.appcontent')

@section('doccontent')

<h1 style="text-align: left;">
  <img src="/images/icons/iconlearn.png" width="40px" height="40px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
  @lang('public.howgetstart')</a>
</h1>

<hr>

<br>

<div class="row">
    <div class="col-sm-12">
      <h3 style="font-weight: bolder">@lang('public.topicgetstart')</h3>
      </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12">
        <h3>@lang('public.readusermanual')</h3>
      </div>
</div>

<br>

<div class="row">
 <div class="col-sm-12">
   <h3>
    <h3>@lang('public.installbiz')</h3>
   </h3>

   <br>

   <h3 style="font-weight: bolder">
    @lang('public.readnote')
   </h3>

   <h3>
    @lang('public.readbiztype')
    </h3>

    <br>

    <h3>
    @lang('public.readvarious')
   </h3>
   </div>
</div>

<br>

<div class="row" style="text-align: justify;">
    <div class="col-sm-12">
      <h3>@lang('public.readafterinstall')
      </h3>
      <br>
      <h3>
        @lang('public.readmustlive')
      </h3>
      <br>
      <h3 style="font-weight: bolder">@lang('public.forexamplewebsite')</h3>  <h3>@lang('public.bizpeople')</h3>
      <br>

     </div>
</div>

<br>

<div class="row" style="text-align: center">
    <div class="col-sm-12">
       <img src="/images/docs/doclogincredential.png"class="bd-placeholder-img" width="100%" height="100%">
    </div>
</div>

<br>

<div class="row" style="text-align: justify;">
    <div class="col-sm-12">
       <h3>
         @lang('public.readuserneed')
       </h3>
    </div>
</div>

<br>

<hr>

<div class="row" style="text-align: left;">
    <div class="col-sm-12">
       <h4 style="font-weight: bolder">
          @lang('public.continuebasic')
       </h4>

       <br>

       <a href="http://127.0.0.1:8000/doclearnbizgetstart" style="font-size: 20px;">@lang('public.howgetstart')</a>

       <br><br>

       <a href="http://127.0.0.1:8000/doclearnbizgetdashboard" style="font-size: 20px;">@lang('public.finishlogin')</a>

    </div>
</div>

@endsection
