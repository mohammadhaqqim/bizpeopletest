@extends('documentationlayouts.app')

@section('content')

<div class="card">

{{-- <h1 style="text-align: left;">
  <img src="/images/icons/icondoc.png" width="40px" height="40px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
   Documentation : User System Guideline
   @lang('public.documentationusersystemguideline')
</h1> --}}

<br>


<div class="row" style="text-align: center">
    <div class="col-sm-12">
       <img src="/images/logos/BizLogoOutlineWhite.png" width="150px" height="150px">
       <br><br>
       <img src="/images/logos/BizPeopleBlack.png" width="400px" height="80px">
    </div>
</div>


 <br>

<div class="row">
    <div class="col-sm-12">
      <h2 style="text-align: center; color:#000; font-weight:bolder;">
        {{-- Choose Your Documentation Topic --}}
        @lang('public.choosetopic')
        {{-- <img src="/images/icons/iconview.png" width="35px" height="35px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;"> --}}
      </h2>
    </div>
</div>

<br>

<div class="row">
  <div class="icon-bar">

    <div class="col">
    <a href="http://127.0.0.1:8000/docgettingstarted">
      <br>
      <img src="/images/icons/iconlaunch.png" width="65px" height="65px" style="text-align: center; margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
      <br><br>
        {{-- Getting Started --}}
      @lang('public.docgettingstarted')
      <br><br>
    </a>
    </div>

    <div class="col">
        <a href="http://127.0.0.1:8000/docpayroll">
            <br>
            <img src="/images/icons/iconpayment.png" width="65px" height="65px" style="text-align: center; margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
            <br><br>
            {{-- Payroll --}}
            @lang('public.docpayroll')
            <br><br>
          </a>
        </div>

        <div class="col">
          <a href="http://127.0.0.1:8000/docpeople">
            <br>
            <img src="/images/icons/iconpeople.png" width="65px" height="65px" style="text-align: center; margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
            <br><br>
            {{-- People --}}
            @lang('public.docpeople')
            <br><br>
          </a>
        </div>

        <div class="col">
          <a href="http://127.0.0.1:8000/docreport">
            <br>
            <img src="/images/icons/iconreport.png" width="65px" height="65px" style="text-align: center; margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
            <br><br>
            {{-- Report 3 --}}
            @lang('public.docreport')
            <br><br>
          </a>
        </div>


    <!-- Row 2 -->

    <div class="col">
        <a href="http://127.0.0.1:8000/docuser">
          <br>
          <img src="/images/icons/iconuser.png" width="65px" height="65px" style="text-align: center; margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
          <br><br>
          {{-- Users --}}
          @lang('public.docuser')
          <br><br>
        </a>
      </div>

    <div class="col">
      <a href="http://127.0.0.1:8000/docsetting">
        <br>
        <img src="/images/icons/iconsetup.png" width="65px" height="65px" style="text-align: center; margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
        <br><br>
        {{-- Setting --}}
        @lang('public.docsetting')
        <br><br>
      </a>
    </div>

    <div class="col">
      <a href="http://127.0.0.1:8000/docpolicy">
        <br>
        <img src="/images/icons/iconpolicy.png" width="65px" height="65px" style="text-align: center; margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
        <br><br>
        {{-- Policy --}}
        @lang('public.docpolicy')
        <br><br>
      </a>
    </div>

    <div class="col">
        <a href="http://127.0.0.1:8000/doclogin">
          <br>
          <img src="/images/icons/iconsecure.png" width="65px" height="65px" style="text-align: center; margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
          <br><br>
          {{-- Login --}}
          @lang('public.doclogin')
          <br><br>
        </a>
        </div>

        <div class="col">
        <a href="http://127.0.0.1:8000/docforgetpassword">
           <br>
           <img src="/images/icons/iconunlock.png" width="65px" height="65px" style="text-align: center; margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
           <br><br>
           {{-- Forget Password --}}
           @lang('public.docforgetpassword')
           <br><br>
        </a>
        </div>

        {{-- <div class="col">
          <a href="http://127.0.0.1:8000/docdepartment">
           <br>
           <img src="/images/icons/icondepartment.png" width="65px" height="65px" style="text-align: center; margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
           <br><br>
            Department
           @lang('public.docdepartment')
           <br><br>
          </a>
        </div> --}}

        {{-- <div class="col">
            <a href="http://127.0.0.1:8000/docemployee">
              <br>
              <img src="/images/icons/iconemployee.png" width="65px" height="65px" style="text-align: center; margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
              <br><br>
               Employee
              @lang('public.docemployee')
              <br><br>
            </a>
         </div> --}}

    <div class="col">
        <a href="http://127.0.0.1:8000/docerror">
         {{-- <a href="http://127.0.0.1:8000/errors/errorpage"> --}}
          <br>
          <img src="/images/icons/iconerror.png" width="65px" height="65px" style="text-align: center; margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
          <br><br>
          {{-- Errors --}}
          @lang('public.docerror')
          <br><br>
        </a>
      </div>

      <div class="col">
         <a href="http://127.0.0.1:8000/errors/errorpage">
          <br>
          <img src="/images/icons/iconerrorpage.png" width="65px" height="65px" style="text-align: center; margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
          <br><br>
          {{-- Page Error --}}
          @lang('public.errorpage')
          <br><br>
        </a>
      </div>

      <div class="col">
         <a href="http://127.0.0.1:8000/errors/errorpage404">
          <br>
          <img src="/images/icons/iconerrorpage404.png" width="65px" height="65px" style="text-align: center; margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
          <br><br>
          {{-- Page Error 404 --}}
          @lang('public.errorpage404')
          <br><br>
        </a>
      </div>

  </div>
</div>

<br>

<p style="text-align: center;"><b>@lang('public.readtopic')</b></p>

</div>

@endsection
