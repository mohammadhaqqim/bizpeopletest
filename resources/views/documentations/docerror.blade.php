@include('documentationlayouts.app')

@extends('documentationinclude.appcontent')

@section('doccontent')


<h1 style="text-align: left;">
  <img src="/images/icons/iconerror.png" width="40px" height="40px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
  Errors
</h1>

<br>

<h3>Type of Errors or Page Failure that could be appear on the System will be explain below:<h3>

<hr>

<h4 style="font-weight: bolder;">Client Error<h4>

<hr>

<h5 style="font-weight: bolder;">400 Error Page<h5>

<p>Bad request! The server doesn’t understand. Clearing the cache should help.</p>

<p>Example:</p>

<p>Click the given link : <a href="http://127.0.0.1:8000/errors/errorpage"> @lang('public.errorpage')</a>.</p>



<br>

<h5 style="font-weight: bolder;">401 Error Page<h5>

<p>Unauthorised access. You don’t have the correct access rights, user name or password</p>

<br>

<h5 style="font-weight: bolder;">402 Error Page<h5>

<p>Payment is required for access</p>

<br>

<h5 style="font-weight: bolder;">400 Error Page<h5>

<p>Forbidden! The file does not have permission that allows the pages to be viewed on the web</p>

<br>

<h5 style="font-weight: bolder;">404 Error Page<h5>

<p>Page Not found. For whatever reason, the web page is not available. This could be that you’ve entered the wrong web address</p>

<p>Example:</p>

<p>Click the given link : <a href="http://127.0.0.1:8000/errors/errorpage404"> @lang('public.errorpage404')</a>.</p>



<br>

<hr>

<h4 style="font-weight: bolder;">Server Error<h4>

<hr>

<h5 style="font-weight: bolder;">500 Error Page</h5>
<p>Server error, Usually caused by a server that has not been configured properly</p>

<br>

<h5 style="font-weight: bolder;">501 Error Page</h5>
<p>Not implemented</p>

<br>

<h5 style="font-weight: bolder;">502 Error Page</h5>

<p>Bad gateway. This means that the server you're contacting is a gateway or proxy (substitute) server.
   It is not receiving a valid response from the main server that should actually be dealing with the request.
</p>

<br>

<h5 style="font-weight: bolder;">503 Error Page</h5>

<p>Out of resources. This means that the system is overloaded</p>

<br>

<h5 style="font-weight: bolder;">504 Error Page</h5>

<p>Gateway timed-out. A slow internet connection can be caused by multiple things,
   such as defective Ethernet cables, old switches, or general poor service.
   It could also mean the server is in need of maintenance
</p>

<br>

<h5 style="font-weight: bolder;">505 Error Page</h5>

<p>HTTP version is not supported</p>


@endsection
