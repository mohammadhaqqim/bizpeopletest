@extends('documentationlayouts.app')

@section('content')

<h1 style="text-align: center">Biz People Documentation</h1>

<hr>

<div class="row">
    <div class="col-sm-12" style="text-align:right;">
        <a  href="/documentations/{{ $documentation->id }}/" class="btn btn-secondary">Go Back To Previous Page</a>
    </div>
</div>

<br>

<div class="row list-group-item">

    <div class="row">
        <div class="col-sm-12">
             <h3>Edit Detail of the Documentation</h3>
        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-sm-3">
            <label>ID NUMBER :</label>
        </div>
        <div class="col-sm-1">
             <h5>{{ $documentation->id }}</h5>
        </div>
        <div class="col-sm-3">
            <label>DOCUMENT NAME :</label>
       </div>
       <div class="col-sm-5">
        <h5>{{ $documentation->documentation_name }}</h5>
   </div>
    </div>

    <hr>

    <!-- Edit Input Document -->
    <form action="/documentations/{{ $documentation->id }}" method="POST" enctype="multipart/form-data">
     @method('patch')
     @csrf
        <div class="row">
            <div class="form-group col-sm-12">
                <label for="title">Document Name</label>
                <input class="form-control" type="text" name="documentation_name" id="documentation_name" placeholder="How to Get Started? , Read System Step etc." value="{{  $documentation->documentation_name }}">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-sm-12">
                <label for="title">Document Description</label>
                <textarea class="form-control" type="text" name="documentation_text" id="documentation_text" placeholder="Tutorial 1, Lesson 1 etc.">{{  $documentation->documentation_text }}</textarea>
            </div>
        </div>

        <hr>

        <div class="row">
            <div class="form-group col-sm-12">
                <label for="title">Upload Document Image</label>
                <hr>
                <h5>Image Filename : {{ $documentation->image_link }}</h5>
                <hr>
                <img src="/storage/image_link/{{ $documentation->image_link }}" class="img-responsive" style="max-height:400px; max-width:500px;" alt="" srcset=""/>
                <br>
                <hr>
                <br>
                <input type="file" id="image_link" name="image_link" class="form-control" value="{{ $documentation->image_link }}">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-sm-12" style="text-align: right">
                <button type="submit" class="btn btn-primary">Save Changes</button>
            </div>
        </div>

    </form>

</div>

@endsection
