@include('documentationlayouts.app')

@extends('documentationinclude.appcontent')

@section('doccontent')

<h1 style="text-align: left;">
 <img src="/images/icons/iconlaunch.png" width="40px" height="40px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
  @lang('public.documentationgettingstarted')
</h1>

<br>

<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/doclearnbizgetstart" style="font-size: 25px;">@lang('public.howgetstart')</a>
    </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/doclearnbizgetdashboard" style="font-size: 25px;">@lang('public.finishlogin')</a>
    </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12">
        <a href="http://127.0.0.1:8000/doclearnbizgetchangepassword" style="font-size: 25px;">@lang('public.howtochangepassword')</a>
    </div>
</div>


<br>

<a href="http://127.0.0.1:8000/doctopic" style="text-align: right;" class="backbtn">
    <b>@lang('public.gettingdocumentationtopic')</b>
</a>

@endsection
