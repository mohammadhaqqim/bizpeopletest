@include('documentationlayouts.app')

@extends('documentationinclude.appcontent')

@section('doccontent')

<h1 style="text-align: left;">
  <img src="/images/icons/iconlearn.png" width="40px" height="40px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;">
  @lang('public.wherejob')</a>
</h1>

<hr>

<br>

<div class="row">
    <div class="col-sm-12">
      <h3 style="font-weight: bolder">@lang('public.topicpeople')</h3>
      </div>
</div>

<br>

<div class="row">
    <div class="col-sm-12">
        <h3>@lang('public.readusermanual')</h3>
      </div>
</div>

<br>

<div class="row" style="text-align: justify;">
    <div class="col-sm-12">
       <h3>
         @lang('public.mainpeopleprofilejob')
       </h3>
    </div>
</div>

<br><br>

<div class="row">
    <div class="col-sm-12">
      <h3 style="font-weight: bolder">@lang('public.mainpeopleprofilejobform')</h3>
      </div>
</div>

<br>

<div class="row" style="text-align: center">
    <div class="col-sm-12">
       <img src="/images/docs/BizPeopleProfileJob.jpg"class="bd-placeholder-img" width="100%" height="100%">
    </div>
</div>

<br>

<div class="row" style="text-align: justify;">
    <div class="col-sm-12">
       <h3>
         @lang('public.readpeopleprofilejobform')
       </h3>
    </div>
</div>

<br>

<div class="row" style="text-align: center">
    <div class="col-sm-12">
       <img src="/images/docs/BizPeopleProfileJob1.jpg"class="bd-placeholder-img" width="100%" height="100%">
    </div>
</div>

<br>

<div class="row" style="text-align: justify;">
    <div class="col-sm-12">
       <h3>
         @lang('public.readpeopleprofilejobformedit')
       </h3>
    </div>
</div>

<br>

<hr>

<div class="row" style="text-align: left;">
    <div class="col-sm-12">
       <h4 style="font-weight: bolder">
          @lang('public.continuebasic')
       </h4>

       <br>

       <a href="http://127.0.0.1:8000/doclearnbizpeople" style="font-size: 20px;">@lang('public.howtoadd')</a>

      <br><br>

      <a href="http://127.0.0.1:8000/doclearnbizpeopleprofile" style="font-size: 20px;">@lang('public.whatfunction')</a>

      <br><br>

      <a href="http://127.0.0.1:8000/doclearnbizpeopleprofilepersonal" style="font-size: 20px;">@lang('public.wherepersonal')</a>

      <br><br>

      <a href="http://127.0.0.1:8000/doclearnbizpeopleprofilejob" style="font-size: 20px;">@lang('public.wherejob')</a>

      <br><br>

      <a href="http://127.0.0.1:8000/doclearnbizpeopleprofileallowance" style="font-size: 20px;">@lang('public.whereallowance')</a>

    </div>
</div>

@endsection
