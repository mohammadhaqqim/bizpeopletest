<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
 
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">

</head>

<body class="vh-100 d-flex m-0 justify-content-center align-items-top align-items-sm-center grad" style=""> 
    
    @yield('content')

<style>
    main{
        height: 100% !important;
    }

    .biz-login{
        width: 400px;
        height: 495px;
    }

    .grad {
        background-image: linear-gradient(to bottom right, #751A75, #B12D82);
    }

</style>
</body>