<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <style>
      .menu-color{
        background-color: #38465D !important;
      }

      .sidebar {
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        z-index: 100;
        padding: 48px 0 0;
        box-shadow: inset -1px 0 0 rgba(0, 0, 0, .1);
    }

    .nav-link i.fa{
      width: 35px;
      font-size: 21px;
    }
    </style>
</head>

<body>

    <nav class="navbar navbar-dark fixed-top  flex-md-nowrap p-0 shadow menu-color">
      <a class="navbar-brand col-sm-3 col-md-2 mr-0 text-center" href="#">BizPeople v3</a>
      <ul class="navbar-nav px-3">
        <li class="nav-item text-nowrap">
          <a class="nav-link" href="#">Sign out</a>
        </li>
      </ul>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-2 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">
            <div class="row text-center my-3">
              <h3 class="w-100 mb-0 ">Superkleen</h3>
              <a class="nav-link w-100 py-0" href="#">Logout</a>
            </div>
            <ul class="nav flex-column">
              <li class="nav-item">
                <a class="nav-link active" href="#">
                  Dashboard <span class="sr-only">(current)</span>
                  <i class="fa fa-tv float-right text-center"></i>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">`
                  Employees
                  <i class="fa fa-users float-right text-center"></i>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  Payroll
                  <i class="fa fa-dollar float-right text-center"></i>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">                  
                  Leave
                  <i class="fa fa-plane float-right text-center"></i>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  Reports
                  <i class="fa fa-bar-chart float-right text-center"></i>
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">
                  Settings
                  <i class="fa fa-cogs float-right text-center"></i>
                </a>
              </li>
            </ul>  
          </div>
        </nav>      
      </div>
    </div>

</body>
</html>
