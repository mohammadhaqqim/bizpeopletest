<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

        <style>
            html, body {
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .page-title, .page-title-menu{
                 padding-left: 255px; 
            }

            .side-profile{
                width: 245px;
            }

            .side-profile-image{
                top: 20px;
                left: 45px;
                width: 180px;
                height: 180px;
                border: 2px solid blue;
            }

            .side-profile-image i{
                font-size: 120px;
            }

            .bg-biz{
/*                background-color: #41AF4B; */
                background-color: #0074E8;
            }


            .color-biz{
                color: #41AF4B;
            }

            .text-biz-blue{
                color: #0074E8;
            }
            .text-biz-green{
                color: #41AF4B;
            }

            .bg-light-biz{
                background-color: #F1F1F3;
            }

            .nav .nav-item:hover{
                background-color: white;
            }

            .nav-tabs .nav-link:hover{
                color: green !important;
            }

            .second-menu .nav-link{
                color: white;    
            }

            .second-menu .nav-link:hover{
                color: #0074E8;
                border-radius: 25px;
            }


            .second-menu .nav-link.active{
                color: #0074E8;
                background-color: white;
                
            }


            .side-display, .main-content{
                height: calc(100vh - 210px);
            }

            .detail-content{
                width: calc(100% - 255px);
            }

            .nav-tabs{
                /* border-bottom-color: #41AF4B; */
            }

            .staff-image, .staff-image i{
                width: 175px;
                height: 175px;                
            }

            .staff-image i{

            }

            .header-image{
                width: 255px;
            }

            .fill{
                height: calc(100vh-80px) !important;
            }

            .biz{
                min-height: calc(100%-90px) !important;
            }

            main{
                min-height: ;
            }
        </style>
    </head>
    <body> 
        <div class=" d-flex flex-column biz">

                <nav class="px-3 siteHeader bg-light-biz shadow sticky-top">
                    <a class="navbar-brand align-items-stretch side-profile mr-0" href="#">
                        <i class="fa fa-id-badge" style="font-size: 22px;"></i>
                        {{ config('app.companyname', 'Laravel') }}
                    </a>            
                    <div class="navbar-brand mr-auto py-0 align-items-stretch">
                        <ul class="nav ">
                            <li class="nav-item active">
                                <a class="nav-link color-biz" href="#">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="personnel">Personal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Payroll</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">People</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Reports</a>
                            </li>
                        </ul>
                    </div>
                    <div class="navbar-brand float-right py-0">
                      <ul class="nav">
                        <li class="nav-item ">
                            <a class="nav-link px-2" href="#" data-toggle="tooltip" title = "Logout">
                            <i class="fa fa-power-off  text-center"></i>              
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link px-2" href="#">
                            <i class="fa fa-cog text-center" data-toggle="tooltip" title = "Settings"></i>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link px-2" href="#">
                            <i class="fa fa-sign-out text-center" data-toggle="tooltip" title = "Logout"></i>              
                            </a>
                        </li>
                      </ul>
                    </div>                
                </nav>

{{--                 <nav class="px-3 py-2 bg-biz second-menu" hidden>
                    <a class="navbar-brand text-center side-profile mr-0 text-white" href="#">
                        Regan Nagorcka..
                    </a>   
                    <div class="navbar-brand py-0">
                        <ul class="nav nav-pills">
                            <li class="nav-item">
                                <a class="nav-link active" href="/personnel/{{ $employee->id }}/edit">Personal</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/job/{{ $employee->id }}/edit">Job</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Leave</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Emergency</a>
                            </li>
                        </ul>
                    </div>
                </nav> --}}

        
{{--                 <div class="p-2 bg-biz position-relative" style="height: 162px;">
                    <div class="bg-white side-profile-image position-absolute rounded d-flex align-items-center justify-content-center" style="">
                        <i class="fa fa-user-o" ></i> 
                    </div>
                    <div class=" header-menu h-100">
                        <div class="mt-5 page-title" >
                            <h2 class="pb-4 text-white">Regan Nagorcka..</h2>
                        </div>
                        <div class="page-title-menu ">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                  <a class="nav-link active" href="/personnel/{{ $employee->id }}/edit">Personal</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link text-white" href="/job/{{ $employee->id }}/edit">Job</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link text-white" href="#">Time Off</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link text-white" href="#">Emergency</a>
                                </li>
                              </ul>
                        </div>
                    </div>
                </div> --}}

{{--                 <div class="p-2 d-flex min-vh-100">                    
                    <div class=" side-profile float-left flex-grow-1 px-2">
                        <div class="flex-column h-100 bg-light-biz px-1 text-center text-secondary pt-5">
                            <i class="fa fa-envelope " data-toggle="tooltip" title = "Logout"></i>
                             regan@bizitsolutions.biz
                        </div>
                    </div> --}}
                    
                    <main class="container-fluid overflow-auto biz px-0" style="border: 1px solid yellow;">
                        @yield('content')
                    </main>
{{--                 </div>
 --}}
        </div>

    </body>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
</html>
