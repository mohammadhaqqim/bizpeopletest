<x-layout>

    @include('components.employee_header') 
{{--         <x-employee_header name="{{ $employee->name }}" jobTitle="{{ $employee->currentjob->designation->title }}" empid="{{ $employee->id }}" /> --}}

    <div class="main-content mt-2">

        @include('components.employee_aside')

        <div class=" pr-3">
        
            <div class="row border-bottom pb-3 biz-title">
                <i class="fa fa-briefcase my-auto px-2"></i> 
                <h2 class="mb-0">Allowances</h2>
            </div>

            Transaction allowances are additions and deductions that are generated at the time a payment period is generated.

            @if (optional(request()->access())->hr)
                <a href="" data-toggle="modal" data-target="#allowanceAddModal"><span class="badge badge-primary">Add</span></a>
            @endif

            <div class="row pt-4">
                <h4>Current Additions and Deductions </h4> 

            @if (optional(request()->access())->hr)
                <div class="col text-right text-biz-blue">
                    <i class="fa fa-plus-circle my-auto px-2" style="font-size: 18px;" data-toggle="modal" data-target="#additionModal"></i> 
                    <a href="" data-toggle="modal" data-target="#additionModal"><label class="text-right">New Addition</label></a>
                    <label for="" class="px-2"> / </label>
                    <i class="fa fa-plus-circle my-auto px-2" style="font-size: 18px;" data-toggle="modal" data-target="#deductionModal"></i> 
                    <a href="" data-toggle="modal" data-target="#deductionModal"><label class="text-right">New Deduction</label></a>
                </div>
            @endif

                <table id="tblAllowances" class="table border">
                    <thead class="tbl-dark">
                        <tr>
                            <th scope="col" hidden>#</th>
                            <th scope="col">Allowance Type</th>
                            <th scope="col" class="d-none d-sm-table-cell">Effective From</th>
                            <th scope="col" hidden>Effective From</th>
                            <th scope="col" hidden>amount</th>
                            <th scope="col" class="text-right">
                                <i class="fa fa-plus-circle my-auto px-2 text-success" ></i> Addition $
                            </th>
                            <th scope="col" class="text-right">                    
                                <i class="fa fa-minus-circle my-auto px-2 text-danger" ></i> Deduction $
                            </th>
                            <th scope="col" class="d-none d-lg-table-cell">Remarks</th>
                            
                        @if (optional(request()->access())->hr)
                            <th>Change</th>
                        @endif

                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($allowances as $allowance)
                        <tr>
                            <td hidden>{{ $allowance->id }}</td>
                            <td>{{ $allowance->title }}</td>
                            <td hidden>{{ $allowance->effective_date }}</td>
                            <td class="d-none d-sm-table-cell">{{ date('d-m-Y', strtotime($allowance->effective_date)) }}</td>
                            <td class="text-right" hidden>{{ $allowance->amount }}</td>
                        @if( $allowance->addition )
                            <td class="text-right">{{ number_format($allowance->amount, 2) }}</td>
                            <td class="text-right">-</td>
                        @else
                            <td class="text-right">-</td>
                            <td class="text-right">{{ number_format($allowance->amount, 2) }}</td>
                        @endif
                            <td class="d-none d-lg-table-cell">{{ $allowance->remarks }}</td>

                        @if (optional(request()->access())->hr)
                            <td>
                                <a href="#" onclick="transactionEdit({{ $allowance->id }}); return false;"><i class="fa fa-pencil my-auto px-2 text-primary" style="font--size: 18px;" data-toggle="" data-target=""></i></a>
                                <a href="#" onclick="confirmDelete({{ $allowance->id }}); return false;"><i class="fa fa-times-circle my-auto px-2 text-danger" style="font--size: 18px;" data-toggle="" data-target=""></i></a>
                            </td>
                        @endif

                        </tr>
                            
                        @endforeach

                        <tr class="" style="background-color: #F3F3F3;">
                            <td></td>
                            <td class="text-right font-weight-bold d-none d-sm-table-cell"">Totals</td>
                            <td class="text-right font-weight-bold">{{ number_format($allowances->where('addition', 1)->sum('amount'), 2) }}</td>
                            <td class="text-right font-weight-bold">{{ number_format($allowances->where('addition', 0)->sum('amount'), 2) }}</td>
                            <td class="d-none d-lg-table-cell"></td>

                        @if (optional(request()->access())->hr)
                            <td></td>
                        @endif
                        
                        </tr> 
                    </tbody>
                </table>
                
            </div>
        </div>

        <div class="modal" tabindex="-1"  id="deleteModal">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                <form id="deleteTransaction" method="POST" action="/allowances/">
                @method('DELETE')
                @csrf                        
    
                <div class="modal-header">
                  <i class="fa fa-file-text-o pt-1"></i>
                  <h5 class="modal-title pl-2">Delete Trasnaction</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>

                <div class="modal-body">
                    <input id="employee_id" name="employee_id" value="{{ $employee->id }}" hidden>

                    <div class="row">
                        <div class="form-group col text-center">
                            <h5 ="">Are you sure you would like to delete the following transaction?</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                        <label for="date_joined">Effective Date</label>
                        <div class="input-group">
                            <input type="date" class="form-control" id="deleteDate" name="deleteDate" placeholder="dd/mm/yyyy" 
                                value="{{ date('Y-m-d', strtotime('0 day')) }}" disabled>
                        </div>    
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                            <label for="amount">Allowance Type</label>
                            <input type="" class="form-control" id="deleteTitle" name="deleteTitle" placeholder="0.00" value="" disabled>
                        </div>   
                        
                        <div class="form-group col-6">
                            <label for="deleteAmount">Amount $</label>
                            <input type="number" class="form-control" id="deleteAmount" name="deleteAmount" placeholder="0.00" disabled>
                        </div>  
                    </div>
                    <div class="row">
                        <div class="form-group col">
                            <label for="deleteRemarks">Remark</label>
                            <textarea type="text" class="form-control" rows="3" id="deleteRemarks" name="deleteRemarks" placeholder="Enter any comments" maxlength="200" disabled></textarea>
                        </div>                        
                    </div>
                    
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                  <button type="submit" class="btn btn-danger">Yes</button>
                </div>
                </form>
              </div>
            </div>
        </div>

    @if (optional(request()->access())->hr)
        @include('components.modal.add_transaction_add')
        @include('components.modal.add_transaction_ded')
        @include('components.modal.edit_transaction')    
        @include('components.modal.add_allowance')
    @endif

    <script>
        function confirmDelete(id){
            document.getElementById("deleteTransaction").action = "/allowances/" + id;

            var oTable = document.getElementById("tblAllowances");
            for (i = 1; i < oTable.rows.length; i++){
                var oCells = oTable.rows.item(i).cells;

                if (oCells.item(0).innerHTML == id){
                    document.getElementById("deleteDate").value = oCells.item(2).innerHTML;
                    document.getElementById("deleteTitle").setAttribute("value", oCells.item(1).innerHTML);
                    document.getElementById("deleteAmount").setAttribute("value", Number(oCells.item(4).innerHTML).toFixed(2));
                    document.getElementById("deleteRemarks").value = oCells.item(7).innerHTML;
                }
            }

            $("#deleteModal").modal('show');
        }

        function transactionEdit(id){
            document.getElementById("editTransaction").action = "/allowances/" + id;

            var oTable = document.getElementById("tblAllowances");
            for (i = 1; i < oTable.rows.length; i++){
                var oCells = oTable.rows.item(i).cells;

                if (oCells.item(0).innerHTML == id){
                    document.getElementsByClassName("editDate")[0].value = oCells.item(2).innerHTML;
                    document.getElementsByClassName("editTitle")[0].value = oCells.item(1).innerHTML; 
                    document.getElementsByClassName("editAmount")[0].value = Number(oCells.item(4).innerHTML).toFixed(2);                        
                    document.getElementsByClassName("editRemarks")[0].value = oCells.item(7).innerHTML;
                }
            }

            $("#editModal").modal('show');
        }

        function checkAdditionSelection(obj){
            if (obj.value == 0){
                $('#additionModal').modal('toggle');
                var e = document.getElementById('addded');
                e.options[1].selected = true;
                $("#allowanceAddModal").modal('show');
            }
        }

        function checkDeductionSelection(obj){
            if (obj.value == 0){
                $('#deductionModal').modal('toggle');
                //addded
                var e = document.getElementById('addded');
                e.options[2].selected = true;
                $("#allowanceAddModal").modal('show');
            }
        }

    </script>
    
            
</x-layout>