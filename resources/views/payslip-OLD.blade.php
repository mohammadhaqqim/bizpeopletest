@php($joined = date('j F Y', strtotime($ps->date_joined)))
@php($payperiod = date('F Y', strtotime($ps->created_at)))
@php($designation = $ps->designation)
@php($companyName = $company->company_name)
@php($totaltaxes = number_format($ps->tap + $ps->scp + $ps->epf,2))
<style>
    .row{margin-left:-15px;margin-right:-15px}
    .col-xs-12{float:left}
    .col-xs-12{width:100%}
    .col-xs-12{position:relative;min-height:1px;padding-left:15px;padding-right:15px}
    .text-left{text-align:left}
    .pull-right{float:right!important}
</style>
<style>
    @page { 
        margin-top: 0.5cm;
        margin-bottom: 0.5cm;
    }

    .sold {
        border-style: solid;
        border-width: 1px;
    }

    .dott {
        border-style: dotted;
        border-width: 1px;
    }

    tr{
        height: 20px;
    }

    td{
        padding-left: 5px;
    }

    table {
        border-collapse: collapse;
        border-spacing: 0;
    }

    .rotate {
        position: absolute;
        transform: rotate(-90.0deg); 
    }

    .e2{
        top: 725px;
        left: -30px;
    }

    .e1{
        top: 210px;
        left: -30px;   
    }

    .d1{
        top: 319px;
        left: -39px;
    }

    .a1{
        top: 410px;
        left: -33px;
    }
    
    .d2{
        top: 837px;
        left: -39px;
    }
    
    
    .a2{
        top: 927px;
        left: -32px;
    }
    
    .rotated1 {
        position: absolute;
        top: 180px;
        left: 0px;
        transform: rotate(-90.0deg); 
    }    

</style>

<div class="row" style="font-family: 'Times New Roman', Times, serif">
    <div class="row" style="font-size: 17px;">
        <div class="col-xs-12">
            <span class="pull-left" style="">
                <strong> {{ $companyName }} ..</strong>
            </span>
            <span class="pull-right" style="font-size: 20px;">
                <strong>SALARY SLIP</strong>
            </span>
        </div>
    </div>

    <span style="font-size: 5px; color: white;">.</span>
    <div class="rotate e1" style="font-size: 13px;"> EARNINGS </div> 
    <div class="rotate d1" style="font-size: 13px;"> DEDUCTIONS </div> 
    <div class="rotate a1" style="font-size: 13px;"> ADDITIONS </div> 
   
    <div class="row" style="font-size: 14px;">
        <table class="col-xs-12" style="">
            <tr style="border: 1px solid grey;">
                <td style="width: 20%;">Name </td>
                <td style="width: 2%;">:</td>
                <td class="text-left" style="width: 42%; border-bottom: 1px solid grey;">{{ $ps->emp_name }}</td>
                <td style="width: 3%;">&nbsp;</td>    
                <td style="width: 15%;">&nbsp;</td>    
                <td style="width: 2%;">&nbsp;</td>
                <td class="text-left" style="width: 17%;">&nbsp;</td>
            </tr>
            <tr style="">
                <td style="width: 20%;">Position </td>
                <td style="width: 2%;">:</td>
                <td class="text-left" style="width: 42%; border-bottom: 1px solid grey;"> {{ $ps->designation }}</td>
                <td style="width: 3%;">&nbsp;</td>    
                <td style="width: 15%;">Period</td>    
                <td style="width: 2%;">:</td>
                <td class="text-left" style="width: 17%; border-bottom: 1px solid grey;">{{ $payperiod }}</td>            
            </tr>
            <tr style="">
                <td style="width: 20%;">Date Joined</td>
                <td style="width: 2%;">:</td>
                <td class="text-left" style="width: 42%; border-bottom: 1px solid grey;">{{ $joined }}</td>
                <td style="width: 3%;"></td>    
                <td style="width: 15%;">Payment Mode</td>    
                <td style="width: 2%;">:</td>
                <td class="text-left" style="width: 17%; border-bottom: 1px solid grey;">{{ $ps->payment_mode == 'Bank' ? 'Bank Transfer' : $ps->payment_mode }}</td>
            </tr>
            <tr style="">
                <td style="width: 20%;">&nbsp;</td>
                <td style="width: 2%;">&nbsp;</td>
                <td class="text-left" style="width: 42%;">&nbsp;</td>
                <td style="width: 3%;">&nbsp;</td>    
                <td style="width: 15%;">&nbsp;</td>    
                <td style="width: 2%;">&nbsp;</td>
                <td class="text-left" style="width: 17%;">&nbsp;</td>
            </tr>
        </table>
    </div>
    
    <span style="font-size: 7px; color: white;">.</span>

    <div class="row" style="font-size: 14px;">
        <table class="col-xs-12" style="margin-bottom: 0px; padding-bottom: 0px;">
            <tr style="">
                <td style="width: 5%; "></td>
                <td style="width: 46%; border: 1px solid black; text-align: center;"><span> <strong> Description </strong> </span></td>
                <td style="width: 13%; border: 1px solid black; text-align: center; ">
                    <span> <strong> BND </strong> </span>
                </td>
                <td style="width: 3%;">&nbsp;</td>
                <td style="width: 20%;">&nbsp;</td>
                <td style="width: 14%;text-align: right">&nbsp;</td>            
            </tr>
            {{-- Fixed --}}
            <tr style="">
                <td style="width: 5%; border-top: 1px solid black; border-left: 1px solid black;">
                
                </td>
                <td style="width: 46%; border: 1px solid black;"><span> Basic Salary </span></td>
                <td style="width: 13%; border: 1px solid black; text-align: right">
                    <span> {{ number_format($ps->basic_salary,2) }} </span>
                </td>
                <td style="width: 3%;">&nbsp;</td>
                <td style="width: 20%;">&nbsp;</td>
                <td style="width: 14%;text-align: right">
                    &nbsp;
                </td>            
            </tr>
            @foreach($fixed as $fix)
            <tr style="">
                <td style="width: 2%; border-left: 1px solid black;"></td>
                <td style="width: 46%; border: 1px solid black;"> <span> {{ $fix->title }} </span></td>
                <td style="width: 13%; border: 1px solid black; text-align: right">
                    @if ( $fix->amount == 0)
                        <span> - </span>
                    @else
                        <span> {{ number_format($fix->amount,2) }} </span>
                    @endif
                 </td>
                 <td style="width: 3%;">&nbsp;</td>
                 <td style="width: 20%;">&nbsp;</td>
                 <td style="width: 14%; text-align: right">
                     &nbsp;
                 </td>            
            </tr>
            @endforeach 
            {{-- enter blank rows  --}}
            @for ($i = $fixed->count()+1; $i < 5; $i++)
            <tr style="">
                <td style="width: 2%; border-left: 1px solid black;"></td>
                <td style="width: 46%; border: 1px solid black;">&nbsp;</td>
                <td style="width: 13%; border: 1px solid black; text-align: right">
                    <span>&nbsp;</span>
                </td>
                <td style="width: 3%;">&nbsp;</td>
                <td style="width: 20%; ">&nbsp;</td>
                <td style="width: 14%; text-align: right">
                    &nbsp;
                </td>            
            </tr>
            @endfor

            <tr style="">
                <td style="width: 5%; border-bottom: 1px solid black; border-left: 1px solid black;">
                    
                </td>
                <td style="width: 46%; border: 2px solid black;"> <span> GROSS PAY </span></td>
                <td style="width: 13%; border: 2px solid black; text-align: right">
                    <span> {{ number_format($ps->basic_salary + $fixed->sum('amount'),2) }} </span>
                </td>
                <td style="width: 3%;">&nbsp;</td>
                <td style="width: 20%; ">&nbsp;</td>
                <td style="width: 14%; text-align: right">
                    &nbsp;
                </td>            
            </tr>

        @if($ps->nationality_id === $company->address_country_id)
            {{-- National staff deductions --}}    
            <tr style="">
                <td style="width: 2%; border-left: 1px solid black;"></td>
                <td style="width: 46%; border: 1px solid black;">TAP - Employee</td>
                <td style="width: 13%; border: 1px solid black; text-align: right">
                    @if ( $ps->tap == 0)
                        <span> - </span>
                    @else
                        <span> {{ number_format($ps->tap_ee,2) }} </span>
                    @endif
                </td>
                <td style="width: 3%;">&nbsp;</td>
                <td style="width: 20%; border: 1px solid black;"><span> Employer's TAP </span></td>
                <td style="width: 14%; border: 1px solid black; text-align: right">
                    @if ( $ps->tap == 0)
                        <span> - </span>
                    @else
                        <span> {{ number_format($ps->tap,2) }} </span>
                    @endif
                </td>
            </tr>
            <tr style="">
                <td style="width: 2%; border-left: 1px solid black;"></td>
                <td style="width: 46%; border: 1px solid black;"> SCP - Employee</td>
                <td style="width: 13%; border: 1px solid black; text-align: right">
                    @if ( $ps->scp == 0)
                        <span> - </span>
                    @else
                        <span> {{ number_format($ps->scp_ee,2) }} </span>
                    @endif
                </td>
                <td style="width: 3%;">&nbsp;</td>
                <td style="width: 20%; border: 1px solid black;"><span> Employer's SCP </span></td>
                <td style="width: 14%; border: 1px solid black; text-align: right">
                    @if ( $ps->scp == 0)
                        <span> - </span>
                    @else
                        <span> {{ number_format($ps->scp,2) }} </span>
                    @endif
                </td>            
            </tr>

            @php($start = 0)
            @foreach($deductions as $ded)
            <tr style="">
                <td style="width: 2%; border-left: 1px solid black;"></td>
                <td style="width: 46%; border: 1px solid black;"> <span> {{ $ded->title }} </span></td>
                <td style="width: 13%; border: 1px solid black; text-align: right">
                    @if ( $ded->amount == 0)
                        <span> - </span>
                    @else
                        <span> {{ number_format($ded->amount,2) }} </span>
                    @endif
                 </td>
                 <td style="width: 3%;">&nbsp;</td>
                 @if( $start > 0)
                    <td style="width: 20%;"></td>
                    <td style="width: 14%; text-align: right"></td>
                 @else
                    <td style="width: 20%; border: 2px solid black;"><span><strong> Total Contribution </strong> </span></td>
                    <td style="width: 14%;  border: 2px solid black; text-align: right">
                        <span> {{ number_format($ps->tap + $ps->scp + $ps->epf,2) }} </span>
                    </td>            
                 @endif
                @php($start = 1)
            </tr>
            @endforeach 

{{-- enter blank rows  --}}
            @for ($i = $deductions->count()+1; $i < 3; $i++)
            <tr style="">
                <td style="width: 2%; border-left: 1px solid black;"></td>
                <td style="width: 46%; border: 1px solid black;">&nbsp;</td>
                <td style="width: 13%; border: 1px solid black; text-align: right">
                    <span>&nbsp;</span>
                </td>
                <td style="width: 3%;">&nbsp;</td>                
            @if ( $start == 1 )
                <td style="width: 20%; ">&nbsp;</td>
                <td style="width: 14%; text-align: right">
                    &nbsp;
                </td>            
            @else
                <td style="width: 20%; border: 2px solid black;"><span><strong> Total Contribution </strong> </span></td>
                <td style="width: 14%;  border: 2px solid black; text-align: right">
                    <span> {{ number_format($ps->tap + $ps->scp + $ps->epf,2) }} </span>
                </td>   
                @php($start = 1)         
             @endif
            </tr>
            @endfor

        @else        
        {{-- EXPAT deductions EPF--}}
            @if($totaltaxes > 0)
            <tr style="" >
                <td style="width: 2%; border-left: 1px solid black;"></td>
                <td style="width: 46%; border: 1px solid black;">EPF - Employee</td>
                <td style="width: 13%; border: 1px solid black; text-align: right">
                    <span> {{ $ps->epf_ee == 0 ? '' : number_format($ps->epf_ee,2) }} </span>
                </td>
                <td style="width: 3%;">&nbsp;</td>
                <td style="width: 20%; border: 1px solid black;"><span> Employer's EPF </span></td>
                <td style="width: 14%; border: 1px solid black; text-align: right">
                    <span> {{ number_format($ps->epf,2) }} </span>
                </td>
            </tr>   
            @endif

            {{-- if there are no deductions place in epf total --}}
            @if ($deductions->count() == 0 && $totaltaxes > 0)
            <tr style="">
                <td style="width: 2%; border-left: 1px solid black;"></td>
                <td style="width: 46%; border: 1px solid black;"></td>
                <td style="width: 13%; border: 1px solid black; text-align: right"></td>
                <td style="width: 3%;">&nbsp;</td>
                <td style="width: 20%; border: 2px solid black;"><span> <strong> Total Contribution </strong> </span></td>
                <td style="width: 14%; border: 2px solid black; text-align: right">
                    <span> <strong>{{ $ps->epf == 0 ? '' : number_format($ps->epf,2) }}</strong> </span>
                </td>
            </tr>  
            @endif

            @php($start = 0)
            @foreach($deductions as $ded)
            <tr style="">
                <td style="width: 2%; border-left: 1px solid black;"></td>
                <td style="width: 46%; border: 1px solid black;"> <span> {{ $ded->title }} </span></td>
                <td style="width: 13%; border: 1px solid black; text-align: right">
                    <span> {{ number_format($ded->amount,2) }} </span>
                 </td>
                 <td style="width: 3%;">&nbsp;</td>
                 {{-- --}}
                @if ( $totaltaxes == 0 || $start > 0)
                    <td style="width: 20%;">&nbsp;</td>
                    <td style="width: 14%; text-align: right">&nbsp;</td>
                @else
                    <td style="width: 20%; border: 2px solid black;"><span> <strong> Total Contribution </strong></span></td>
                    <td style="width: 14%; border: 2px solid black; text-align: right">
                        <span> <strong>{{ number_format($ps->epf,2) }}</strong> </span>
                    </td>                 
                @endif
                @php($start = 1)
            </tr>
            @endforeach 
            
{{-- enter blank rows  --}}

            @if( $totaltaxes > 0 && $deductions->count() == 0)
                @php($start = 2)
            @else
                @php($start = 1)
            @endif

            @if( $totaltaxes == 0)
                @php($start = 0)
            @endif

            @for ($i = $deductions->count()+$start; $i < 4; $i++)
            <tr style="">
                <td style="width: 2%; border-left: 1px solid black;"></td>
                <td style="width: 46%; border: 1px solid black;">&nbsp;</td>
                <td style="width: 13%; border: 1px solid black; text-align: right">
                    <span>&nbsp;</span>
                </td>
                <td style="width: 3%;">&nbsp;</td>
                <td style="width: 20%; ">&nbsp;</td>
                <td style="width: 14%; text-align: right">
                    &nbsp;
                </td>            
            </tr>
            @endfor

        @endif
            <tr style="">
                <td style="width: 2%; border-bottom: 1px solid black; border-left: 1px solid black;">
                </td>
                <td style="width: 46%; border: 2px solid black;"> TOTAL DEDUCTIONS </td>
                <td style="width: 13%; border: 2px solid black; text-align: right">
                    <span> {{ number_format($deductions->sum('amount') + $ps->scp_ee + $ps->tap_ee + $ps->epf_ee,2) }} </span>
                    {{-- <span> {{ number_format($ps->ded_up_amt + $ps->scp_ee + $ps->tap_ee + $ps->epf_ee + $ps->ded_others_amt,2) }} </span> --}}
                </td>
                <td style="width: 3%;">&nbsp;</td>
                <td style="width: 20%; ">&nbsp;</td>
                <td style="width: 14%;  text-align: right">
                    &nbsp;
                </td>
            </tr>

            {{-- Additions --}}
            @foreach($additions as $add)
            <tr style="">
                <td style="width: 2%; border-left: 1px solid black;"></td>
                <td style="width: 46%; border: 1px solid black;"> <span> {{ $add->title }} </span></td>
                <td style="width: 13%; border: 1px solid black; text-align: right">
                    @if ( $add->amount == 0)
                        <span> - </span>
                    @else
                        <span> {{ number_format($add->amount,2) }} </span>
                    @endif
                 </td>
                 <td style="width: 3%;">&nbsp;</td>
                 <td style="width: 20%;">&nbsp;</td>
                 <td style="width: 14%; text-align: right">
                     &nbsp;
                 </td>            
            </tr>
            @endforeach 

            {{-- enter blank rows  --}}
            @for ($i = $additions->count()+1; $i < 4; $i++)
            <tr style="">
                <td style="width: 2%; border-left: 1px solid black;"></td>
                <td style="width: 46%; border: 1px solid black;">&nbsp;</td>
                <td style="width: 13%; border: 1px solid black; text-align: right">
                    <span>&nbsp;</span>
                </td>
                <td style="width: 3%;">&nbsp;</td>
                <td style="width: 20%; ">&nbsp;</td>
                <td style="width: 14%; text-align: right">
                    &nbsp;
                </td>            
            </tr>
            @endfor

            <tr style="">
                <td style="width: 2%; border-left: 1px solid black;">
                </td>
                <td style="width: 46%; border: 2px solid black;"> TOTAL ADDITIONS </td>
                <td style="width: 13%; border: 2px solid black; text-align: right">
                    <span> {{ number_format($additions->sum('amount'),2) }} </span>    
                </td> 
                <td style="width: 3%;">&nbsp;</td>
                <td style="width: 20%;">&nbsp;</td>
                <td style="width: 14%;">&nbsp;</td>
            </tr>        
            <tr style="">
                <td style="width: 2%; border-left: 2px solid black; border-top: 2px solid black; border-bottom: 2px solid black;"></td>
                <td style="width: 46%; text-align: right; border-bottom: 2px solid black; border-top: 2px solid black; right-padding: 10px;"> <strong> NET PAY </strong> </td>
                <td style="width: 13%; border: 2px solid black; border-bottom: 2px solid black; text-align: right">
                    @php( $total_add = $ps->ot_amt + $ps->dt_amt + $ps->add_amt) 
                    @php( $total_ded = $ps->up_amt + $ps->ded_amt)
                    <span> <strong> {{ number_format($ps->basic_salary - ($ps->tap_ee + $ps->scp_ee) - ($ps->epf_ee) + $fixed->sum('amount') + $additions->sum('amount') - $deductions->sum('amount'),2) }}</strong> </span>
                </td>
                <td style="width: 3%;">&nbsp;</td>
                <td style="width: 20%; ">&nbsp;</td>
                <td style="width: 14%; ">&nbsp;</td>
            </tr>                        
        </table>
    </div>
    <br>
    <span style="font-size: 7px; color: white;">.</span>
    <hr>

@if( $ps->payment_mode != 'Bank Transfer')
        <div class="row" style="font-size: 17px;" {{ $ps->payment_mode == 'Bank Transfer' ? 'hidden' : '' }}>
            <div class="col-xs-12">
                <span class="pull-left">
                    <strong>{{ $companyName }} </strong>
                </span>
                <span class="pull-right" style="font-size: 20px;">
                    <strong>SALARY SLIP</strong>
                </span>
            </div>
        </div>
    <span style="font-size: 7px; color: white;">.</span>
    <div class="rotate e2" {{ $ps->payment_mode != 'Bank Transfer' ? '' : 'hidden' }} style="font-size: 13px;"> EARNINGS </div> 
    <div class="rotate d2" {{ $ps->payment_mode != 'Bank Transfer' ? '' : 'hidden' }} style="font-size: 13px;"> DEDUCTIONS </div> 
    <div class="rotate a2" {{ $ps->payment_mode != 'Bank Transfer' ? '' : 'hidden' }} style="font-size: 13px;"> ADDITIONS </div>     

        <div class="row" style="font-size: 14px;" {{ $ps->payment_mode != 'Bank Transfer' ? '' : 'hidden' }}>
            <table class="col-xs-12" style="">
                <tr style="border: 1px solid grey;">
                    <td style="width: 20%;">Name </td>
                    <td style="width: 2%;">:</td>
                    <td class="text-left" style="width: 42%; border-bottom: 1px solid grey;">{{ $ps->emp_name }}</td>
                    <td style="width: 3%;">&nbsp;</td>    
                    <td style="width: 15%;">&nbsp;</td>    
                    <td style="width: 2%;">&nbsp;</td>
                    <td class="text-left" style="width: 17%;">&nbsp;</td>
                </tr>
                <tr style="">
                    <td style="width: 20%;">Position </td>
                    <td style="width: 2%;">:</td>
                    <td class="text-left" style="width: 42%; border-bottom: 1px solid grey;"> {{ $ps->designation }}</td>
                    <td style="width: 3%;">&nbsp;</td>    
                    <td style="width: 15%;">Period</td>    
                    <td style="width: 2%;">:</td>
                    <td class="text-left" style="width: 17%; border-bottom: 1px solid grey;">{{ $payperiod }}</td>            
                </tr>
                <tr style="">
                    <td style="width: 20%;">Date Joined</td>
                    <td style="width: 2%;">:</td>
                    <td class="text-left" style="width: 42%; border-bottom: 1px solid grey;">{{ $joined }}</td>
                    <td style="width: 3%;"></td>    
                    <td style="width: 15%;">Payment Mode</td>    
                    <td style="width: 2%;">:</td>
                    <td class="text-left" style="width: 17%; border-bottom: 1px solid grey;">{{ $ps->payment_mode == 'Bank Transfer' ? 'Bank Transfer' : $ps->payment_mode }}</td>
                </tr>
                <tr style="">
                    <td style="width: 20%;">&nbsp;</td>
                    <td style="width: 2%;">&nbsp;</td>
                    <td class="text-left" style="width: 42%;">&nbsp;</td>
                    <td style="width: 3%;">&nbsp;</td>    
                    <td style="width: 15%;">&nbsp;</td>    
                    <td style="width: 2%;">&nbsp;</td>
                    <td class="text-left" style="width: 17%;">&nbsp;</td>
                </tr>
            </table>
        </div>
    <span style="font-size: 7px; color: white;">.</span>
        <div class="row" style="font-size: 14px;" {{ $ps->payment_mode != 'Bank Transfer' ? '' : 'hidden' }}>
            <table class="col-xs-12" style="" >
                <tr style="">
                    <td style="width: 5%; "></td>
                    <td style="width: 46%; border: 1px double black; text-align: center; "><span> <strong> Description </strong> </span></td>
                    <td style="width: 13%; border: 1px solid black; text-align: center;">
                        <span> <strong> BND </strong> </span>
                    </td>
                    <td style="width: 3%;">&nbsp;</td>
                    <td style="width: 20%;">&nbsp;</td>
                    <td style="width: 14%;text-align: right">
                        &nbsp;
                    </td>            
                </tr>
                {{-- Fixed --}}
                <tr style="">
                    <td style="width: 5%; border-top: 1px solid black; border-left: 1px solid black;">
                    
                    </td>
                    <td style="width: 46%; border: 1px solid black;"><span> Basic Salary </span></td>
                    <td style="width: 13%; border: 1px solid black; text-align: right">
                        <span> {{ number_format($ps->basic_salary,2) }} </span>
                    </td>
                    <td style="width: 3%;">&nbsp;</td>
                    <td style="width: 20%;">&nbsp;</td>
                    <td style="width: 14%;text-align: right">
                        &nbsp;
                    </td>            
                </tr>
                @foreach($fixed as $fix)
                <tr style="">
                    <td style="width: 2%; border-left: 1px solid black;"></td>
                    <td style="width: 46%; border: 1px solid black;"> <span> {{ $fix->title }} </span></td>
                    <td style="width: 13%; border: 1px solid black; text-align: right">
                        @if ( $fix->amount == 0)
                            <span> - </span>
                        @else
                            <span> {{ number_format($fix->amount,2) }} </span>
                        @endif
                     </td>
                     <td style="width: 3%;">&nbsp;</td>
                     <td style="width: 20%;">&nbsp;</td>
                     <td style="width: 14%; text-align: right">
                         &nbsp;
                     </td>            
                </tr>
                @endforeach 
                {{-- enter blank rows  --}}
                @for ($i = $fixed->count()+1; $i < 5; $i++)
                <tr style="">
                    <td style="width: 2%; border-left: 1px solid black;"></td>
                    <td style="width: 46%; border: 1px solid black;">&nbsp;</td>
                    <td style="width: 13%; border: 1px solid black; text-align: right">
                        <span>&nbsp;</span>
                    </td>
                    <td style="width: 3%;">&nbsp;</td>
                    <td style="width: 20%; ">&nbsp;</td>
                    <td style="width: 14%; text-align: right">
                        &nbsp;
                    </td>            
                </tr>
                @endfor
    
                <tr style="">
                    <td style="width: 5%; border-bottom: 1px solid black; border-left: 1px solid black;">
                        
                    </td>
                    <td style="width: 46%; border: 2px solid black;"> <span> GROSS PAY </span></td>
                    <td style="width: 13%; border: 2px solid black; text-align: right">
                        <span> {{ number_format($ps->basic_salary + $fixed->sum('amount'),2) }} </span>
                    </td>
                    <td style="width: 3%;">&nbsp;</td>
                    <td style="width: 20%; ">&nbsp;</td>
                    <td style="width: 14%; text-align: right">
                        &nbsp;
                    </td>            
                </tr>
    
            @if($ps->nationality_id === $company->address_country_id)
                {{-- National staff deductions --}}    
                <tr style="">
                    <td style="width: 2%; border-left: 1px solid black;"></td>
                    <td style="width: 46%; border: 1px solid black;">TAP - Employee</td>
                    <td style="width: 13%; border: 1px solid black; text-align: right">
                        @if ( $ps->tap == 0)
                            <span> - </span>
                        @else
                            <span> {{ number_format($ps->tap_ee,2) }} </span>
                        @endif
                    </td>
                    <td style="width: 3%;">&nbsp;</td>
                    <td style="width: 20%; border: 1px solid black;"><span> Employer's TAP </span></td>
                    <td style="width: 14%; border: 1px solid black; text-align: right">
                        @if ( $ps->tap == 0)
                            <span> - </span>
                        @else
                            <span> {{ number_format($ps->tap,2) }} </span>
                        @endif
                    </td>
                </tr>
                <tr style="">
                    <td style="width: 2%; border-left: 1px solid black;"></td>
                    <td style="width: 46%; border: 1px solid black;"> SCP - Employee</td>
                    <td style="width: 13%; border: 1px solid black; text-align: right">
                        @if ( $ps->scp == 0)
                            <span> - </span>
                        @else
                            <span> {{ number_format($ps->scp_ee,2) }} </span>
                        @endif
                    </td>
                    <td style="width: 3%;">&nbsp;</td>
                    <td style="width: 20%; border: 1px solid black;"><span> Employer's SCP </span></td>
                    <td style="width: 14%; border: 1px solid black; text-align: right">
                        @if ( $ps->scp == 0)
                            <span> - </span>
                        @else
                            <span> {{ number_format($ps->scp,2) }} </span>
                        @endif
                    </td>            
                </tr> 
    
                @php($start = 0)
                @foreach($deductions as $ded)
                <tr style="">
                    <td style="width: 2%; border-left: 1px solid black;"></td>
                    <td style="width: 46%; border: 1px solid black;"> <span> {{ $ded->title }} </span></td>
                    <td style="width: 13%; border: 1px solid black; text-align: right">
                        @if ( $ded->amount == 0)
                            <span> - </span>
                        @else
                            <span> {{ number_format($ded->amount,2) }} </span>
                        @endif
                     </td>
                     <td style="width: 3%;">&nbsp;</td>
                     @if( $start > 0)
                        <td style="width: 20%;"></td>
                        <td style="width: 14%; text-align: right"></td>
                     @else
                        <td style="width: 20%; border: 2px solid black;"><span><strong> Total Contribution </strong> </span></td>
                        <td style="width: 14%;  border: 2px solid black; text-align: right">
                            <span> {{ number_format($ps->tap + $ps->scp + $ps->epf,2) }} </span>
                        </td>            
                     @endif
                    @php($start = 1)
                </tr>
                @endforeach           
    
    {{-- enter blank rows  --}}
                @for ($i = $deductions->count()+1; $i < 3; $i++)
                <tr style="">
                    <td style="width: 2%; border-left: 1px solid black;"></td>
                    <td style="width: 46%; border: 1px solid black;">&nbsp;</td>
                    <td style="width: 13%; border: 1px solid black; text-align: right">
                        <span>&nbsp;</span>
                    </td>
                    <td style="width: 3%;">&nbsp;</td>                
                @if ( $start == 1 )
                    <td style="width: 20%; ">&nbsp;</td>
                    <td style="width: 14%; text-align: right">
                        &nbsp;
                    </td>            
                @else
                    <td style="width: 20%; border: 2px solid black;"><span><strong> Total Contribution </strong> </span></td>
                    <td style="width: 14%;  border: 2px solid black; text-align: right">
                        <span> {{ number_format($ps->tap + $ps->scp + $ps->epf,2) }} </span>
                    </td>   
                    @php($start = 1)         
                @endif
                </tr>
                @endfor
    
            @else        
            {{-- EXPAT deductions EPF--}}
                @if($totaltaxes > 0)
                <tr style="" >
                    <td style="width: 2%; border-left: 1px solid black;"></td>
                    <td style="width: 46%; border: 1px solid black;">EPF - Employee</td>
                    <td style="width: 13%; border: 1px solid black; text-align: right">
                        <span> {{ $ps->epf_ee == 0 ? '' : number_format($ps->epf_ee,2) }} </span>
                    </td>
                    <td style="width: 3%;">&nbsp;</td>
                    <td style="width: 20%; border: 1px solid black;"><span> Employer's EPF </span></td>
                    <td style="width: 14%; border: 1px solid black; text-align: right">
                        <span> {{ number_format($ps->epf,2) }} </span>
                    </td>
                </tr>   
                @endif
    
                {{-- if there are no deductions place in epf total --}}
                @if ($deductions->count() == 0 && $totaltaxes > 0)
                <tr style="">
                    <td style="width: 2%; border-left: 1px solid black;"></td>
                    <td style="width: 46%; border: 1px solid black;"></td>
                    <td style="width: 13%; border: 1px solid black; text-align: right"></td>
                    <td style="width: 3%;">&nbsp;</td>
                    <td style="width: 20%; border: 2px solid black;"><span> <strong> Total Contribution </strong> </span></td>
                    <td style="width: 14%; border: 2px solid black; text-align: right">
                        <span> <strong>{{ $ps->epf == 0 ? '' : number_format($ps->epf,2) }}</strong> </span>
                    </td>
                </tr>  
                @endif
    
                @php($start = 0)
                @foreach($deductions as $ded)
                <tr style="">
                    <td style="width: 2%; border-left: 1px solid black;"></td>
                    <td style="width: 46%; border: 1px solid black;"> <span> {{ $ded->title }} </span></td>
                    <td style="width: 13%; border: 1px solid black; text-align: right">
                        <span> {{ number_format($ded->amount,2) }} </span>
                     </td>
                     <td style="width: 3%;">&nbsp;</td>
                     {{-- --}}
                    @if ( $totaltaxes == 0 || $start > 0)
                        <td style="width: 20%;">&nbsp;</td>
                        <td style="width: 14%; text-align: right">&nbsp;</td>
                    @else
                        <td style="width: 20%; border: 2px solid black;"><span> <strong> Total Contribution </strong></span></td>
                        <td style="width: 14%; border: 2px solid black; text-align: right">
                            <span> <strong>{{ number_format($ps->epf,2) }}</strong> </span>
                        </td>                 
                    @endif
                    @php($start = 1)
                </tr>
                @endforeach 
                
    {{-- enter blank rows  --}}
    
                @if( $totaltaxes > 0 && $deductions->count() == 0)
                    @php($start = 2)
                @else
                    @php($start = 1)
                @endif
    
                @if( $totaltaxes == 0)
                    @php($start = 0)
                @endif
    
                @for ($i = $deductions->count()+$start; $i < 4; $i++)
                <tr style="">
                    <td style="width: 2%; border-left: 1px solid black;"></td>
                    <td style="width: 46%; border: 1px solid black;">&nbsp;</td>
                    <td style="width: 13%; border: 1px solid black; text-align: right">
                        <span>&nbsp;</span>
                    </td>
                    <td style="width: 3%;">&nbsp;</td>
                    <td style="width: 20%; ">&nbsp;</td>
                    <td style="width: 14%; text-align: right">
                        &nbsp;
                    </td>            
                </tr>
                @endfor
    
            @endif
                <tr style="">
                    <td style="width: 2%; border-bottom: 1px solid black; border-left: 1px solid black;">
                    </td>
                    <td style="width: 46%; border: 2px solid black;"> TOTAL DEDUCTIONS </td>
                    <td style="width: 13%; border: 2px solid black; text-align: right">
                        <span> {{ number_format($deductions->sum('amount') + $ps->scp_ee + $ps->tap_ee + $ps->epf_ee,2) }} </span>
                        {{-- <span> {{ number_format($ps->ded_up_amt + $ps->scp_ee + $ps->tap_ee + $ps->epf_ee + $ps->ded_others_amt,2) }} </span> --}}
                    </td>
                    <td style="width: 3%;">&nbsp;</td>
                    <td style="width: 20%; ">&nbsp;</td>
                    <td style="width: 14%;  text-align: right">
                        &nbsp;
                    </td>
                </tr>
    
                {{-- Additions --}}
                @foreach($additions as $add)
                <tr style="">
                    <td style="width: 2%; border-left: 1px solid black;"></td>
                    <td style="width: 46%; border: 1px solid black;"> <span> {{ $add->title }} </span></td>
                    <td style="width: 13%; border: 1px solid black; text-align: right">
                        @if ( $add->amount == 0)
                            <span> - </span>
                        @else
                            <span> {{ number_format($add->amount,2) }} </span>
                        @endif
                     </td>
    
                     <td style="width: 3%;">&nbsp;</td>
    
                     @if ($loop->index == 0)
                     <td style="width: 20%;">Received by</td>
                     @else
                     <td style="width: 20%;">&nbsp;</td>
                     @endif
    
                     <td style="width: 14%; text-align: right">
                         &nbsp;
                     </td>            
                </tr>
                @endforeach 
    
                {{-- enter blank rows  --}}
                @for ($i = $additions->count()+1; $i < 4; $i++)
                <tr style="">
                    <td style="width: 2%; border-left: 1px solid black;"></td>
                    <td style="width: 46%; border: 1px solid black;">&nbsp;</td>
                    <td style="width: 13%; border: 1px solid black; text-align: right">
                        <span>&nbsp;</span>
                    </td>
                    <td style="width: 3%;">&nbsp;</td>
    
                    @if ($i == 1)
                    <td style="width: 20%;">Received by</td>
                    @else
                    <td style="width: 20%;">&nbsp;</td>
                    @endif
    
                    <td style="width: 14%; text-align: right">
                        &nbsp;
                    </td>            
                </tr>
                @endfor
    
                <tr style="">
                    <td style="width: 2%; border-left: 1px solid black;">
                    </td>
                    <td style="width: 46%; border: 2px solid black;"> TOTAL ADDITIONS </td>
                    <td style="width: 13%; border: 2px solid black; text-align: right">
                        <span> {{ number_format($additions->sum('amount'),2) }} </span>    
                    </td> 
                    <td style="width: 3%;">&nbsp;</td>
                    <td style="width: 20%; border-bottom: 1px solid black;">&nbsp;</td>
                    <td style="width: 14%; border-bottom: 1px solid black;">&nbsp;</td>
                </tr>        
                <tr style="">
                    <td style="width: 2%; border-left: 2px solid black; border-top: 2px solid black; border-bottom: 2px solid black;"></td>
                    <td style="width: 46%; text-align: right; border-bottom: 2px solid black; border-top: 2px solid black; right-padding: 10px;"> <strong> NET PAY </strong> </td>
                    <td style="width: 13%; border: 2px solid black; border-bottom: 2px solid black; text-align: right">
                        <span> <strong> {{ number_format($ps->basic_salary - ($ps->tap_ee + $ps->scp_ee) - ($ps->epf_ee) + $fixed->sum('amount') + $additions->sum('amount') - $deductions->sum('amount'),2) }}</strong> </span>
                    </td>
                    <td style="width: 3%;">&nbsp;</td>
                    <td style="width: 20%; ">Date:</td>
                    <td style="width: 14%; ">&nbsp;</td>
                </tr>                        
    
            </table>
        </div>
    </div>
@endif