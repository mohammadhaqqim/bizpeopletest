<!doctype html>
<html lang='en'>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Biz-People</title>

  <link rel="icon" href="/images/logos/BizLogoOutlineWhite.png" type="image/png">

  <link rel="stylesheet" href="{{ asset('css/appbootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('css/appbootstrap.min.css') }}">

  <script src="{{ asset('js/appbootstrap.js') }}"></script>
  <script src="{{ asset('js/appbootstrap.min.js') }}"></script>

  <!-- Add icon library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>

 <style>

    body {
    min-height: 100vh;
    max-width: 100%;
    margin: 0 auto;
    font-family:'Roboto','Noto Sans',Arial,Verdana,sans-serif;;
    backdrop-filter: blur(25px);
    /* background: url(images/backgrounds/BizColorDot.jpg) no-repeat center center fixed; */
    -webkit-background-size: cover;
    -moz-background-size: cover;
    -o-background-size: cover;
      background-size: cover;
      transition: background-color .5s;
  }

  /* Fixed Sidebar */
  .sidebar {
    height: 100%;
    width: 0;
    position: fixed;
    z-index: 1;
    top: 0;
    left: 0;
    background-color: #fff;
    overflow-x: hidden;
    transition: 0.5s;
    box-shadow: 1px 1px 5px #000;
  }

  .sidebar a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 18px;
    color: ;
    display: block;
    transition: 0.3s;
  }

  .sidebar a:hover {
    color: ;
    background: #DCDCDC;
  }

  .sidebar .closebtn {
    position: absolute;
    font-size: 36px;
  }

 /* Open Close Navigation Button */
  .openbtn {
    font-size: 8px;
    cursor: pointer;
    padding: 10px 10px;
    color: #000;
    margin-top: 5px;
  }

  .openbtn:hover {
    background-color: #dcdcdc;
    color: #000;
  }

  #main {
    transition: margin-left .5s;
    background: #fff;
  }


/* On smaller screens,
where height is less than 450px,
change the style of the sidenav
(less padding and a smaller font size) */
@media screen and (max-height: 450px) {
  .sidebar {padding-top: 15px;}
  .sidebar a {font-size: 18px;}
}

/* Dropdown Navigation in Sidebar */
.dropbtn {
    background-color: #444;
    color: #fff;
    cursor: pointer;
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 18px;
    display: block;
    transition: 0.3s;
}

/* Dropdown button on hover & focus */
 .dropbtn:hover {
    background-color: #dcdcdc;
    color: #6495ed;
}


/* Sticky Footer */
.footer {
   background-color: #444;
   color: #fff;
   text-align: center;
}

/* Link Button */
.backbtn {
  color:#000;
}

.backbtn:hover {
    color:#6495ed;
}

.changebtn {
  color:#000;
}

.changebtn:hover {
    color:#6495ed;
}

/* Scroll Button */
.scrollTop {
    position: fixed;
    bottom: 800px;
    right: 50px;
    height: 45px;
    width: 50px;
    background: url("images/icons/iconupwhite.png");
    border-radius: 50%;
    background-size: 33px;
    background-position: center;
    background-repeat: no-repeat;
    cursor: pointer;
    z-index: 100000;
    visibility: hidden;
    opacity: 0;
    transition: 0.5s;
}

.scrollTop.active {
   bottom: 100px;
   visibility: visible;
   opacity: 1;
}

.scrollTop:hover {
    background: #dcdcdc url("images/icons/iconupwhite.png");
    background-position: center;
    background-repeat: no-repeat;
}

  /* Icon Bar DocTopic */
  .icon-bar {
  width: 100%; /* Full-width */
  background-color:;
  overflow: auto; /* Overflow due to float */
  border-radius: 10px;
  padding:1px;
  /* box-shadow: 1px 1px 2px 2px #444; */
  /* background: linear-gradient(90deg, rgba(162,0,255,1) 0%, rgba(157,21,152,1) 50%, rgba(162,0,255,1) 100%); */
}

.icon-bar a {
  float: left; /* Float links side by side */
  text-align: center; /* Center-align text */
  width: 21%; /* Equal width (5 icons with 20% width each = 100%) */
  height: 20%;
  margin: 10px; /* Some top and bottom padding */
  transition: all 0.3s ease; /* Add transition for hover effects */
  color: #000 ; /* White text color */
  border-radius: 2px;
  box-shadow: 0 0 3px #000;
}

/* Add a hover color */
.icon-bar a:hover {
  color: #6495ed;
  box-shadow: 0 0 3px #6495ed;
}

</style>


{{-- Button Navbar JS --}}
<script>
function openNav() {
  document.getElementById("mySidebar").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
  document.getElementById("mySidebar").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
}
</script>

{{-- Dropdown Navigation Sidebar Buttonr JS --}}
<script>
/* When the user clicks on the button,
toggle between hiding and showing the dropdown content */
function myFunction() {
  document.getElementById("myDropdown").classList.toggle("show");
}
// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
       if (openDropdown.classList.contains('show')) {
           openDropdown.classList.remove('show');
       }
     }
  }
}
</script>

{{-- Button JS ScrollToTop Animation --}}
<script type="text/javascript">
   window.addEventListener('scroll', function(){
       var scroll = document.querySelector('.scrollTop');
       scroll.classList.toggle("active", window.scrollY > 500)
   })

   function scrollToTop(){
       window.scrollTo({
           top: 0,
           behavior: 'smooth'
       })
   }
</script>

</head>

<body>
    @include('documentationinclude.navbar')

   <div class="container">

        @include('documentationinclude.scroller')

        @include('documentationinclude.messages')

        @yield('content')

        @include('documentationinclude.footer')

   </div>


</body>

</html>
