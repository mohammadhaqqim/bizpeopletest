<x-layout>

    <style>
        .biz-title i.fa{
            font-size: 25px;
        }
        .form-group label{
            margin-bottom: 0px;
            color: lightslategray;
        }
        .row.form-group{
            margin-bottom: 0px;
        }
    </style>
    
    <div class="container vh-100 d-flex align-items-center">
        
        <div class="card w-100">
            <form method="POST" action="/departments">
            @csrf
    
            <div class="card-header biz-header">
                <div class="col my-auto">
                    <h4 class="my-auto"> <span class="fa fa-user mr-2"></span>Add New Department</h4> 
                </div>
            </div>
    
            <div class="card-body">
                <form method="POST" action="/statuses">
                    @csrf
    
                    <div class="row">
                        <div class="form-group col">
                            <label for="title">Department Title</Title></label><label class="text-danger"> *</label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Department Title">
                        </div>
                    </div>
            
                    <div class="row float-right">
                        <div class="form-group col">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="self.close()">Close</button>
                            <button type="submit" class="btn btn-primary">Save Designation</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    
    
    </div>
    
    </x-layout>