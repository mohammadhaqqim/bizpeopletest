@php($reportName = 'Payroll Totals Report')
@php($companyName = $company->company_name)
<style>
    .row{margin-left:-15px;margin-right:-15px}
    .row{width:100%}

    .col-xs-12{float:left}
    .col-xs-12{width:100%}
    .col-12{width:100%}
    .col-3{width:25%}
    .col-1{width:8%}

    .col-xs-12{position:relative;min-height:1px;padding-left:15px;padding-right:15px}
    .text-left{text-align:left}
    .pull-right{float:right!important}
</style>
<style>
    @page { 
        margin-top: 0.5cm;
        margin-bottom: 0.5cm;
    }

.border{
    border: 1px solid black;
}
.bold{
    font-weight: 700;
}
.sides{
     border-left: 1px solid black;
     border-right: 1px solid black;
}
.text-right{
    text-align: right;
}
.dotted-bottom{
    border-bottom: 1px dotted black;
}
.border-top{
    border-top: 1px solid black;
}
.text-red{
    color: red;
}
.body{
    font-family: Verdana, Arial, sans-serif;
}

    .sold {
        border-style: solid;
        border-width: 1px;
    }

    .dott {
        border-style: dotted;
        border-width: 1px;
    }

    tr{
        height: 20px;
    }

    td{
        padding-left: 5px;
    }

    table {
        border-collapse: collapse;
        border-spacing: 0;
    }

    .rotate {
        position: absolute;
        transform: rotate(-90.0deg); 
    }

    .e2{
        top: 725px;
        left: -30px;
    }

    .e1{
        top: 210px;
        left: -30px;   
    }

    .d1{
        top: 319px;
        left: -39px;
    }

    .a1{
        top: 410px;
        left: -33px;
    }
    
    .d2{
        top: 837px;
        left: -39px;
    }
    
    
    .a2{
        top: 927px;
        left: -32px;
    }
    
    .rotated1 {
        position: absolute;
        top: 180px;
        left: 0px;
        transform: rotate(-90.0deg); 
    }    

</style>


<div class="row body" style="font-family: 'Calibri', Times, serif">
    <div class="col" >
        <strong> {{ $companyName }} </strong>
    </div>
    <div class="col" style="font-size: 13px;">
        <strong> {{ $reportName }} </strong>
    </div>
    <div class="col" style="font-size: 13px;">
        <strong> {{ $payperiod->title }} </strong>
    </div>

    <br>

    <div style="font-size: 11px; position: relative;">
        <table class="col-xs-12" style="">
            <thead>
                <tr style="border: 1px solid grey;">
                    <td class="border bold" style="width: 2%;" rowspan="2">No </td>
                    <td class="border bold" style="width: 20%;" rowspan="2">Name</td>
                    <td class="border bold" style="width: 7%;" >                        
                        Basic <br>
                        Salary
                    </td>
                    <td class="border bold" style="width: 5%;">
                        Unpaid <br>
                        Leave
                    </td>    
                    <td class="border bold" style="width: 5%;">
                        Overtime
                    </td>    
                    <td class="border bold" style="width: 5%;">
                        Bonus
                    </td>    
                    <td class="border bold" style="width: 5%;">
                        Allowance                        
                    </td>    
                    <td class="border bold" style="width: 5%;">
                        Deduction                        
                    </td>    
                    <td class="border bold" style="width: 7%;">
                        Gross <br>
                        Salary
                    </td>    
                    <td class="border bold" style="width: 5%;">
                        TAP <br>
                        ER / EE
                    </td> 
                    <td class="border bold" style="width: 5%;">
                        SCP <br>
                        Emp.r
                    </td> 
                    <td class="border bold" style="width: 5%;">
                        SCP <br>
                        Emp.e
                    </td> 
                    <td class="border bold" style="width: 5%;">
                        EPF <br>
                        EE / ER
                    </td> 
                    <td class="border bold" style="width: 7%;">
                        Net <br>
                        Salary
                    </td> 
                    <td class="border bold" style="width: 10%;" rowspan="2">
                        Payment <br>
                        Mode
                    </td> 

                    <td></td>   
                </tr>
                <tr>
                    <td class="border">$B</td>
                    <td class="border">$B</td>
                    <td class="border">$B</td>
                    <td class="border">$B</td>
                    <td class="border">$B</td>
                    <td class="border">$B</td>
                    <td class="border">$B</td>
                    <td class="border">$B</td>
                    <td class="border">$B</td>
                    <td class="border">$B</td>
                    <td class="border">$B</td>
                    <td class="border">$B</td>
                    <td></td>
                </tr>
            </thead>
            <tbody>
            @foreach($ps as $emp)
                <tr>
                    <td class="sides dotted-bottom">{{ $emp->id }}</td>
                    <td class="dotted-bottom">{{ $emp->name }}</td>
                    <td class="sides text-right dotted-bottom">{{ number_format((float)$emp->basic_salary, 2) }}</td>
                    <td class="text-right dotted-bottom text-red">
                        {{ $emp->unpaid == 0 ? '' : '('.number_format((float)$emp->unpaid*-1, 2).')' }}
                    </td>
                    <td class="sides text-right dotted-bottom">
                        {{ $emp->overtime == 0 ? '' : number_format((float)$emp->overtime, 2) }}
                    </td>
                    <td class="text-right dotted-bottom">
                        {{ $emp->bonus == 0 ? '' : number_format((float)$emp->bonus, 2) }}
                    </td>
                    <td class="sides text-right dotted-bottom">{{ number_format((float)$emp->additions, 2) }}</td>
                    <td class="text-right dotted-bottom text-red" >
                        {{ $emp->deduction == 0 ? '' : '('.number_format((float)$emp->deduction*-1, 2).')' }}
                    </td>
                    <td class="sides text-right dotted-bottom bold">
                        {{ number_format((float)$emp->gross, 2) }}
                    </td>
                    <td class="text-right dotted-bottom">
                        {{ $emp->tap == 0 ? '' : number_format((float)$emp->tap, 2) }}
                    </td> 
                    <td class="sides text-right dotted-bottom">
                        {{ $emp->scp == 0 ? '' : number_format((float)$emp->scp, 2) }}
                    </td> 
                    <td class="text-right dotted-bottom">
                        {{ $emp->scp_ee == 0 ? '' : number_format((float)$emp->scp_ee, 2) }}
                    </td> 
                    <td class="sides text-right dotted-bottom">
                        {{ $emp->epf_ee == 0 ? '' : number_format((float)$emp->epf_ee, 2) }}
                    </td> 
                    <td class="text-right dotted-bottom bold">
                        {{ number_format((float) $emp->gross - $emp->tap - $emp->scp_ee - $emp->epf_ee,  2) }}
                    </td> 
                    <td class="sides dotted-bottom">{{ $emp->bank_mode }}</td> 
                    <td style="border-left: 1px solid black;"></td>
                </tr>
            @endforeach
                <tr>
                    <td class="border-top"></td>
                    <td class="border-top">TOTALS</td>
                    <td class="border text-right">{{ number_format($ps->sum('basic_salary'),2) }}</td>
                    <td class="border text-right text-red">{{ '('.number_format($ps->sum('unpaid')*-1,2).')' }}</td>
                    <td class="border text-right">{{ number_format($ps->sum('overtime'),2) }}</td>
                    <td class="border text-right">{{ number_format($ps->sum('bonus'),2) }}</td>
                    <td class="border text-right">{{ number_format($ps->sum('additions'),2) }}</td>
                    <td class="border text-right text-red">{{ '('.number_format($ps->sum('deduction')*-1,2).')' }}</td>
                    <td class="border text-right bold">{{ number_format($ps->sum('gross'),2) }}</td>
                    <td class="border text-right">{{ number_format($ps->sum('tap'),2) }}</td>
                    <td class="border text-right">{{ number_format($ps->sum('scp'),2) }}</td>
                    <td class="border text-right">{{ number_format($ps->sum('scp_ee'),2) }}</td>
                    <td class="border text-right">{{ number_format($ps->sum('epf_ee'),2) }}</td>
                    <td class="border text-right bold">
                        {{ number_format($ps->sum('gross') - $ps->sum('tap') - $ps->sum('scp_ee') - $ps->sum('epf_ee'),  2) }}
                    </td>
                    <td class="border-top"></td>
                </tr>
                <tr>
                    <td class=""></td>
                    <td class=""></td>
                    <td class="border-top"></td>
                    <td class="border-top"></td>
                    <td class="border-top"></td>
                    <td class="border-top"></td>
                    <td class="border-top"></td>
                    <td class="border-top"></td>
                    <td class="border-top"></td>
                    <td class="border-top"></td>
                    <td class="border-top"></td>
                    <td class="border-top"></td>
                    <td class="border-top"></td>
                    <td class="border-top"></td>
                </tr>
                <tr><td><br></td></tr>
                <tr><td><br></td></tr>
                <tr><td><br></td></tr>
                <tr>
                    <td class=""></td>
                    <td class="" colspan="2">Prepared By:</td>
                    <td class=""></td>
                    <td class="" colspan="5">Approved By:</td>
                    <td class="" colspan="6"></td>
                </tr>
                <tr><td><br></td></tr>
                <tr><td><br></td></tr>
                <tr><td><br></td></tr>
                <tr>
                    <td class=""></td>
                    <td class="border-top" colspan="2">Name</td>
                    <td class=""></td>
                    <td class="border-top" colspan="5">Name</td>
                    <td class="" colspan="6"></td>
                </tr>
                <tr>
                    <td class=""></td>
                    <td class="" colspan="2">Date</td>
                    <td class=""></td>
                    <td class="" colspan="5">Date</td>
                    <td class="" colspan="6"></td>
                </tr>
                <tr><td><br></td></tr>
                <tr><td><br></td></tr>
                <tr>
                    <td class=""></td>
                    <td class="" colspan="2">Note: {{ $today }}
                        <br> EE - Employee 
                        <br> ER - Employer</td>
                    <td class=""></td>
                    <td class="" colspan="5"></td>
                    <td class="" colspan="6"></td>
                </tr>
            </tbody>
        </table>
    </div>


</div>