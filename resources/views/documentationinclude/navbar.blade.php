 <!-- Sidebar Left Navigation Button -->
 <div id="mySidebar" class="sidebar">

  <!-- Sidebar Navigation List -->
  <ul class="list-group">

    <!-- Sidebar Close Button / BizLogo -->
    <li class="list-group-item">
     <div class="row">
      <a class="text-align:left;"  style="background: #fff" href="javascript:void(0)" class="closebtn" onclick="closeNav()">
       <b style="font-size:20px; color:#000; margin:5px;">&#10006</b>
       <img src="/images/logos/BizPeopleColour.png" alt="" style="width:140px; height:32px;">
      </a>
     </div>
    </li>
    <!-- End of Sidebar Close Button / BizLogo -->

    <!-- Documentation Navigation List -->

     <li>
      <a class="dropbtn" href="http://127.0.0.1:8000/doctopic">
        <b>@lang('public.documentationtopic')</b>
      </a>
    </li>

    <li>
    <a href="http://127.0.0.1:8000/docgettingstarted">
        @lang('public.docgettingstarted')
     </a>
    </li>

    <li>
        <a href="http://127.0.0.1:8000/docpayroll">
          @lang('public.docpayroll')
         </a>
    </li>

    <li>
        <a href="http://127.0.0.1:8000/docpeople">
          @lang('public.docpeople')
         </a>
    </li>

    <li>
     <a href="http://127.0.0.1:8000/docreport">
     @lang('public.docreport')
     </a>
    </li>

    <li>
    <a class="dropdown-btn dropbtn">
        <b>@lang('public.docusermanual')</b>
        <i class="fa fa-caret-down"></i>
      </a>
      <div class="dropdown-container">
        <a href="http://127.0.0.1:8000/docuser">@lang('public.docuser')</a>
        <a href="http://127.0.0.1:8000/docsetting">@lang('public.docsetting')</a>
        <a href="http://127.0.0.1:8000/docerror">@lang('public.docerror')</a>
      </div>
    </li>

    <!-- Link Divider -->
    <li>
     <a class="dropbtn" href="http://127.0.0.1:8000/">
       <b>@lang('public.bizpeople')</b>
     </a>
    </li>

    <li>
     <a href="http://127.0.0.1:8000/docterm">
      @lang('public.docterm')
     </a>
    </li>

    <li>
    <a href="http://127.0.0.1:8000/docpolicy">
        @lang('public.docpolicy')
    </a>
    </li>


    {{-- <li> --}}
      {{-- <a href="http://127.0.0.1:8000/doclogin"> --}}
       {{-- <img src="/images/icons/iconsecure.png" width="30px" height="30px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;"> --}}
       {{-- Login --}}
      {{--  @lang('public.login') --}}
     {{--  </a> --}}
    {{-- </li> --}}

    {{-- <li> --}}
      {{-- <a href="http://127.0.0.1:8000/docforgetpassword"> --}}
       {{-- <img src="/images/icons/iconunlock.png" width="30px" height="30px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;"> --}}
       {{-- Forget Password --}}
       {{-- @lang('public.forgetpassword') --}}
      {{-- </a> --}}
   {{--  </li> --}}

    {{-- <li> --}}
      {{-- <a href="http://127.0.0.1:8000/docdepartment"> --}}
       {{-- <img src="/images/icons/icondepartment.png" width="30px" height="30px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;"> --}}
       {{-- Department --}}
      {{--  @lang('public.department') --}}
     {{--  </a> --}}
    {{-- </li> --}}

   {{--  <li> --}}
      {{-- <a href="http://127.0.0.1:8000/docemployee"> --}}
       {{-- <img src="/images/icons/iconshare.png" width="30px" height="30px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;"> --}}
       {{-- Employee --}}
      {{--  @lang('public.employee') --}}
     {{--  </a> --}}
    {{-- </li> --}}

    {{-- <li> --}}
        {{-- <a href="http://127.0.0.1:8000/errors/errorpage"> --}}
        {{--  <img src="/images/icons/iconerror.png" width="30px" height="30px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;"> --}}
         {{-- Employee --}}
        {{--  @lang('public.error') --}}
        {{-- </a> --}}
    {{-- </li> --}}

    {{-- <li> --}}
        {{-- <a href="http://127.0.0.1:8000/errors/errorpage"> --}}
          {{-- <img src="/images/icons/iconpolicy.png" width="30px" height="30px" style="margin: 4px; border-radius: 5px 5px 5px 5px; background-color:#fff;"> --}}
          {{-- Policy --}}
          {{-- @lang('public.policy') --}}
        {{--  </a> --}}
    {{--  </li> --}}

    <!-- End of Documentation Navigation List -->

  </ul>
  <!-- End of Sidebar Navigation List -->

</div>
<!-- End of Sidebar Left Navigation Button -->



<!-- Div BizPeople Guideline Center Top Navbar -->
<div id="main" class="container-fluid">

 <!-- Top Navbar -->

    <!-- Icon Open Sidebar Navigation Button -->
       <a class="openbtn navbar-brand float-sm-start" style="font-size:32px;" onclick="openNav()">
          <b>&#9776;</b>
          <!-- BizGuideline Logo -->
          <img src="@lang('public.bizlogo')" alt="" style="height:36px; width:190px;">
      </a>
    <!-- End of Icon Open Sidebar Navigation Button -->

       <!-- Back Navigation Button -->
       <ul class="nav nav-item nav-link float-sm-end">
        <li class="nav-item">
            <div class="navbar">
                <a  href="http://127.0.0.1:8000/" class="backbtn nav-link">
                <img src="images/icons/iconenter.png" width="20px" height="20px">
                {{--  Back to Login --}}
                @lang('public.bizpeoplelogin')
              </a>
            </div>
         </li>
       </ul>
       <!-- End of  Back Navigation Button -->

        <!-- Switch Language -->
        <ul class="nav nav-item nav-link float-sm-end">
           <li class="nav-item">
            <div class="navbar dropdown">
                <a class="nav-link dropdown-toggle changebtn"
                    href="#"
                    id="navbarDropdown"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">
                    <img src="images/icons/icontrans.png" width="20px" height="20px">
                    {{-- Change Language --}}
                    @lang('public.changelanguage')
                </a>
                <!-- Dropdown Link -->
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="locale/en">

                    <i><img src="images/flags/UsFlag.png" width="30px" height="30px"></i>
                    {{-- English (United States) --}}
                        @lang('public.english')
                    </a>

                    <a class="dropdown-item" href="locale/bn">
                    <i>
                        <img src="images/flags/BnFlag.png" width="30px" height="30px">
                    </i>
                    {{-- Malay (Brunei) --}}
                    @lang('public.malay')
                    </a>
                </div>
                <!-- End of Dropdown Link -->
            </div>
           </li>
        </ul>
       <!-- End of Switch Language -->

      <!-- Topic Link -->
      <ul class="nav nav-item nav-link float-sm-end">
          <li class="nav-item">
                <div class="navbar dropdown">
                    <a class="nav-link dropdown-toggle changebtn"
                        href="#"
                        id="navbarDropdown"
                        role="button"
                        data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        <img src="images/icons/icondoc.png" width="25px" height="25px">
                        @lang('public.documentation')
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" style="background: #dcdcdc;" href="http://127.0.0.1:8000/doctopic">
                            @lang('public.doctopiclist')
                        </a>

                        <a class="dropdown-item" href="http://127.0.0.1:8000/docgettingstarted">
                          @lang('public.docgettingstarted')
                        </a>

                        <a class="dropdown-item" href="http://127.0.0.1:8000/docpayroll">
                            @lang('public.docpayroll')
                        </a>

                        <a class="dropdown-item" href="http://127.0.0.1:8000/docpeople">
                            @lang('public.docpeople')
                        </a>

                        <a class="dropdown-item" href="http://127.0.0.1:8000/docreport">
                            @lang('public.docreport')
                        </a>
                    </div>
                </div>
           </li>
      </ul>
      <!-- End of Topic Link -->

</div>
<!-- End of Top Navbar -->

</div>
<!-- End of Div Biz Guideline Center Top Navbar -->

