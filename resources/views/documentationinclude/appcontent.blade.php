
<style>
/* Fixed sidecontent, full height */
.sidecontent {
    width: 200px;
  position: fixed;
  background-color: #fff;
  padding: 5px;

}

.sidecontent a {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 18px;
  color: #000;
  display: block;
}

.sidecontent a:hover {
  color: #6495ed;
  background: #d3d3d3;
}

/* Style the sidecontent links and the dropdown button */
.sidecontent a, .dropdown-btn {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 16px;
  /* color: #000; */
  display: block;
  border: none;
  /* background: #fff; */
  width: 100%;
  text-align: left;
  cursor: pointer;
  outline: none;
}

.dropdown-btn {
    color:#000;
    background: #fff;
}

/* On mouse-over */
.sidecontent a:hover, .dropdown-btn:hover {
  color: #6495ed;
  background: #dcdcdc;
  width: 100%;
}

/* Add an active class to the active dropdown button */
.active {
  background-color: #d3d3d3;
  color: #fff;
}

/* Dropdown container (hidden by default). Optional: add a lighter background color and some left padding to change the design of the dropdown content */
.dropdown-container {
  display: none;
  background-color: #fff;
  /* padding-left: 8px; */
}

/* Optional: Style the caret down icon */
.fa-caret-down {
  float: right;
  padding-right: 8px;
}

.maincontent {
  margin-left: 250px; /* Same as the width of the sidenav */
  background: #fff;
  text-align: justify;
}

</style>

<!-- Container -->
<div class="container" style="padding-bottom: 100px; margin-top:-100px;">

    <!--  Side Content -->
    <div class="sidecontent">
        <a href="http://127.0.0.1:8000/doctopic">
        <b>@lang('public.doctopiclist')</b>
        </a>

        <a href="http://127.0.0.1:8000/docgettingstarted">
        @lang('public.docgettingstarted')
        </a>

        <a href="http://127.0.0.1:8000/docpayroll">
        @lang('public.docpayroll')
        </a>

        <a href="http://127.0.0.1:8000/docpeople">
        @lang('public.docpeople')
        </a>

        <a href="http://127.0.0.1:8000/docreport">
        @lang('public.docreport')
        </a>


  <a class="dropdown-btn">
    <b>@lang('public.docusermanual')</b>
    <i class="fa fa-caret-down"></i>
  </a>
  <div class="dropdown-container">
    <a href="http://127.0.0.1:8000/docuser">@lang('public.docuser')</a>
    <a href="http://127.0.0.1:8000/docsetting">@lang('public.docsetting')</a>
    <a href="http://127.0.0.1:8000/docerror">@lang('public.docerror')</a>
  </div>

   <!-- Middle Link Divider -->
   <a href="http://127.0.0.1:8000/">
    <b>@lang('public.bizpeople')</b>
    </a>

    <a href="http://127.0.0.1:8000/docterm">
    @lang('public.docterm')
    </a>

    <a href="http://127.0.0.1:8000/docpolicy">
        @lang('public.docpolicy')
    </a>
 </div>
<!-- End of Side Content -->

  <!-- Main Content -->
  <div class="maincontent">

    <div class="card" style="padding: 30px;">

    @yield('doccontent')

    </div>

</div>
<!-- End of Main Content -->

</div>
<!-- End of Container -->



<script>
/* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
  this.classList.toggle("active");
  var dropdownContent = this.nextElementSibling;
  if (dropdownContent.style.display === "block") {
  dropdownContent.style.display = "none";
  } else {
  dropdownContent.style.display = "block";
  }
  });
}
</script>

