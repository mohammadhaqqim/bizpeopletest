<x-layout>
    
    @include('components.payroll_header') 
    
    <div class="main-content mt-0">
    
        <div class="main pr-3 ">

            <div class="row border-bottom my-3 pb-3 biz-title">
                <i class="fa fa-calendar my-auto px-2"></i> 
                <h2 class="mb-0">Payroll Details</h2>
            </div>
            
<!-- display the payroll period information -->

<!-- add employee to list -->



            <small>Some text information</small>
    
            <div class="row pt-1">
                <div class="col-6 pl-0">
                    <h4>October 2020.</h4>
                </div>
            </div>            
            <div class="row">
                <table class="table table-sm border mb-1" >
                    <thead class="tbl-dark">
                        <tr height="80 px">
                            <th scope="col" class="align-middle">#</th>
                            <th scope="col" class="align-middle">Employee Name</th>
                            <th scope="col" class="align-middle">Type</th>
                            <th scope="col" class="align-middle">Base Wage</th>
                            <th scope="col" class="align-middle">TAP</th>
                            <th scope="col" class="align-middle">SCP</th>
                            <th scope="col" class="align-middle">EFP</th>
                            <th scope="col" class="align-middle">Addition</th>
                            <th scope="col" class="align-middle">Deduction</th>
                        </tr>
                    </thead>
                    <tbody style="font-size: 12px;">
                        <tr style="">
                            <td class="align-middle">1</td>
                            <td class="align-middle" style=""><a href=""> Regan Nagorcka</a></td>
                            <td class="align-middle">Salary</td>
                            <td class="align-middle">$ 2,000.00</td>
                            <td class="align-middle">$ 0.00</td>
                            <td class="align-middle">$ 0.00</td>
                            <td class="align-middle">$ 0.00</td>
                            <td class="align-middle">$ 100.00</td>
                            <td class="align-middle">$ 0.00</td>
                        </tr>
                    </tbody>
                </table>
            </div>
    
        </div>
    </div>

</x-layout>
    