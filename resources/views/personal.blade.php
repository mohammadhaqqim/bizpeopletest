@extends('layouts.profilemain')

<style>
    .biz-title i.fa{
        font-size: 25px;
    }
</style>
@section('content')
<div class="container">
    <div class="row border-bottom my-2 pb-3 biz-title">
        <i class="fa fa-address-card my-auto px-2" data-toggle="tooltip" title = "Logout"></i> 
        <h3 class="mb-0">Personal</h3>
    </div>
    
    <div class="biz-section mb-3 border-bottom ">
        <div class="row">
            <h5>Standard Inforation</h5>
        </div>
        <div class="row">
            <div class="form-group col-3 pl-0">
                <label for="employee_id">Employee Code</label>
                <input type="text" class="form-control" id="employee_id" placeholder="Your Unique ID">
            </div>
            <div class="form-group col-2 pl-0">
                <label for="employment_status">Status</label>
                <select name="employment_status" id="" class="custom-select">
                    <option value="1">Active</option>
                    <option value="2">In-active</option>
                </select>
            </div>            
        </div>
        <div class="row">
            <div class="form-group col-6 pl-0">
                <label for="name">Employee Name</label>
                <input type="text" class="form-control" id="name" placeholder="Employee name">
            </div>
            <div class="form-group col-3 pl-0">
                <label for="preferred_name">Preferred Name</label>
                <input type="text" class="form-control" id="preferred_name" placeholder="Employee name">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-3 pl-0">
                <label for="date_of_birth">Date of Birth</label>
                <input type="date" class="form-control" id="date_of_birth" placeholder="Employee name">
            </div>
            <div class="form-group col-3 pl-0">
                <label for="gender">Gender</label>
                <select name="gender" id="gender" class="custom-select">
                    <option selected>Choose..</option>
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                </select>
            </div>
            <div class="form-group col-3 pl-0">
                <label for="martial_status_id">Marital Status</label>
                <select name="martial_status_id" id="martial_status_id" class="custom-select">
                    <option selected>Choose..</option>
                    <option value="1">Single</option>
                    <option value="2">Married</option>
                    <option value="3">Divorced</option>
                </select>
            </div>            
        </div>

    </div>


    <div class="biz-section mb-3 border-bottom ">
        <div class="row">
            <h5>Address</h5>
        </div>
        
        <div class="row">
            <div class="form-group col-5 pl-0 mb-2">
                <input type="text" class="form-control" id="address1" placeholder="Address 1">
            </div>
        </div>
    
        <div class="row">
            <div class="form-group col-5 pl-0 mb-2">
                <input type="text" class="form-control" id="address2" placeholder="Address 2">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-3 pl-0">
                <input type="text" class="form-control" id="city" placeholder="City">
            </div>
            <div class="form-group col-3 pl-0">
                <input type="text" class="form-control" id="area" placeholder="Area">
            </div>
            <div class="form-group col-3 pl-0">
                <input type="text" class="form-control" id="postcode" placeholder="Postcode">
            </div>
        </div>
    </div>
    

    <div class="biz-section mb-3 border-bottom ">
        <div class="row">
            <h5>Contact</h5>
        </div>
        <div class="row">
            <label for="">Phone</label>
        </div>
        <div class="row">
            <div class="form-group col-3 pl-0">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1"><i class="fa fa-building"></i></span>
                    </div>
                    <input type="text" id="work_phone" class="form-control" placeholder="Work Number" aria-label="Work Number" aria-describedby="basic-addon1">
                </div>
            </div>
            <div class="form-group col-1 pl-0">
                <input type="text" class="form-control" id="phone_ext" placeholder="Ext">
            </div>
            <div class="form-group col-3 pl-0">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1"><i class="fa fa-mobile"></i></span>
                    </div>
                    <input type="text" class="form-control" id="hand_phone" placeholder="Mobile Number">
                </div>
            </div>
            <div class="form-group col-3 pl-0">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1"><i class="fa fa-home"></i></span>
                    </div>
                    <input type="text" class="form-control" id="home_phone" placeholder="Home Number">
                </div>
            </div>
        </div>
        <div class="row">
            <label for="">Email</label>
        </div>
        <div class="row">
            <div class="form-group col-5 pl-0 mb-0">
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1"><i class="fa fa-building"></i></span>
                    </div>
                    <input type="text" id="work_phone" class="form-control" placeholder="Work Email" aria-label="Work Email" aria-describedby="basic-addon1">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-5 pl-0 mb-2">
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-home"></i></span>
                    </div>
                    <input type="text" class="form-control" id="home_phone" placeholder="Home Email">
                </div>
            </div>
        </div>
    </div>
    
    <div class="biz-section mb-3">
        <div class="row">
            <div class="col-6">
                <h5>Visa Information</h5>
            </div>
            <div class="col-6 text-right text-biz-blue">
                <i class="fa fa-plus-circle my-auto px-2" style="font-size: 18px;" data-toggle="tooltip" title = "Logout"></i> 
                <label for="" class="">Add New Visa</label>  
            </div>
        </div>

        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Date</th>
                    <th scope="col">Visa</th>
                    <th scope="col">Issuing Country</th>
                    <th scope="col">Issued</th>
                    <th scope="col">Expiration</th>
                    <th scope="col">Status</th>
                    <th scope="col">Note</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
    
    <div class="row">
        <div class="col-6">
            <h5>Visa Information</h5>
        </div>
        <div class="col-6 text-right text-biz-green">
            <i class="fa fa-plus-circle my-auto px-2" data-toggle="tooltip" title = "Logout"></i> 
            <label for="">Add New Visa</label>  
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <h5>Visa Information</h5>
        </div>
        <div class="col-6 text-right">
            <i class="fa fa-plus-circle my-auto px-2" data-toggle="tooltip" title = "Logout"></i> 
            <label for="">Add New Visa</label>  
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <h5>Visa Information</h5>
        </div>
        <div class="col-6 text-right">
            <i class="fa fa-plus-circle my-auto px-2" data-toggle="tooltip" title = "Logout"></i> 
            <label for="">Add New Visa</label>  
        </div>
    </div>    

</div>
@endsection
