@component('mail::message')
# Time Off Request - {{ $employee_leave->employee->name }}

A {{ $employee_leave->leavetype->title }} leave request for {{ $employee_leave->employee->name }} has been generated. 

@component('mail::panel')
Leave Request Type
# {{ $employee_leave->leavetype->title }}
@endcomponent

@component('mail::panel')
Dates
# {{ date('d-m-Y', strtotime($employee_leave->start_date)) }} to {{ date('d-m-Y', strtotime($employee_leave->end_date)) }}
@endcomponent

To process the leave request please click the link below.

@component('mail::button', ['url' => url('/timeoff/approval/'.$employee_leave->id)])
Process Request 
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
