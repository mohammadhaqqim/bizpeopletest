@component('mail::message')
# Pay Period: {{ $payroll->title }} 

Hi {{ $payroll->name }},

The pay period from 
{{ date('d F Y', strtotime($payroll->start_date)) }} to {{ date('d F Y', strtotime($payroll->end_date)) }} 
has been finalised and here is the link to your payslip for this pay period.

@component('mail::button', ['url' => url('printpayslip/'.$payroll->payslip_id)])
View Payslip
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
