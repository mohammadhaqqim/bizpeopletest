@component('mail::message')
# Time Off Request - Response {{ is_null($employee_leave->approved_by_id) ? 'Denied' : 'Approved' }}

@component('mail::panel')
Leave Request Type
# {{ $employee_leave->leavetype->title }}
@endcomponent

@component('mail::panel')
Dates
# {{ date('d-m-Y', strtotime($employee_leave->start_date)) }} to {{ date('d-m-Y', strtotime($employee_leave->end_date)) }}
@endcomponent

Your time off request has been 
# {{ is_null($employee_leave->approved_by_id) ? 'Denied' : 'Approved' }} by {{ is_null($employee_leave->approved_by_id) ? $employee_leave->DeniedBy->name : $employee_leave->ApprovedBy->name }}
on {{ is_null($employee_leave->approved_by_id) ? date('d-m-Y', strtotime($employee_leave->rejected_date)) : date('d-m-Y', strtotime($employee_leave->approved_date)) }} 

Thanks,<br>
{{ config('app.name') }}
@endcomponent
