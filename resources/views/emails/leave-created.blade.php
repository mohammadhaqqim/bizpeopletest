@component('mail::message')
# Leave Request - {{ $employee_leave->leavetype->title }} for {{ $employee_leave->employee->name }}

A leave request for {{ $employee_leave->employee->name }} has been generated. 

@component('mail::panel')
Leave Type: {{ $employee_leave->LeaveType->title }} ({{ $employee_leave->start_date }} to {{ $employee_leave->end_date }})
@endcomponent

To view the details of the leave request please click the link below.

@component('mail::button', ['url' => url('/timeoff/'.$employee_leave->id.'/edit')])
View Leave 
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
