<x-layout>

    @section('content')
    <div class="container">
        <div class="row pt-5">
            Home      
        </div>
        
    </div>
    @endsection

<style>
    .biz-chart{
        width: 400px;
    }
</style>

<div class="row container mt-3">
    <div class="col-4">
        Menu - Tenancy
    </div>
    <div class="col-8">
        <table id="tblAllowances" class="table border">
            <thead class="tbl-dark">
                <tr>
                    <th scope="col" hidden>#</th>
                    <th scope="col" colspan="2">Notifications as at {{ \Carbon\Carbon::now()->format('d-m-Y') }} </th>
                    <th scope="col" class="text-right" style="width: 175px;"><span class="badge badge-info text-white">
                        {{-- {{ $leaves->count() }} --}}
                        </span> Announcements
                    </th>
                </tr>        
            </thead>
            <tbody>

                @if( isset($leaves) )
                @foreach($leaves as $leave)
                <tr>
                    <td hidden>1</td>
                    <td class="text-center align-middle" style="width: 50px; font-size: 30px;">
                        <span><i class="fa fa-calendar"></i></span>
                    </td>
                    <td class="pl-0">

                        <div class="font-weight-bold">{{ $leave->Employee->name }} requested 
                            {{ $leave->LeaveType->track_time == 'd' ? $leave->days_off.' Days of ' : $leave->hours_off.' Hours of ' }} 
                            {{ $leave->LeaveType->title }}
                        </div> 
                        <div class="text-muted">{{ \Carbon\Carbon::parse($leave->start_date)->format('d-M') }} to 
                            {{ \Carbon\Carbon::parse($leave->end_date)->format('d-M') }} : 
                            Created {{ date_diff($leave->created_at, new \DateTime())->format("%d days ago") }}
                        </div>
                    </td>
                    <td class="text-right align-middle pr-4"> 
                        <a href="/timeoff/approval/{{ $leave->id }}">
                            <button class="btn btn-outline-primary btn-sm">View</button>
                        </a>
                    </td>
        
                </tr>
                @endforeach
                @endif
 
            </tbody>
        </table>
    
    </div>
</div>

<style>

</style>
</x-layout>
