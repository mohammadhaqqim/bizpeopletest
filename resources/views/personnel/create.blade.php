@extends('layouts.profilemain')

<style>
    .biz-title i.fa{
        font-size: 25px;
    }
    .form-group label{
        margin-bottom: 0px;
        color: lightslategray;
    }
    .row.form-group{
        margin-bottom: 0px;
    }
</style>
@section('content')
<div class="container">
    <form method="POST" action="/personnel">
        @csrf

    <div class="row border-bottom my-2 pb-3 biz-title">
        <i class="fa fa-address-card my-auto px-2" data-toggle="tooltip" title = "Logout"></i> 
        <h3 class="mb-0">Personal</h3>
    </div>
    
    <div class="biz-section mb-3 border-bottom ">
        <div class="row">
            <h5>Standard Inforation</h5>
        </div>
        <div class="row">
            <div class="form-group col-3 pl-0">
                <label for="employee_id" class="mb-0 text-secondary">Employee Code</label>
                <input type="text" class="form-control" id="employee_id" placeholder="Your Unique ID">
            </div>
            <div class="form-group col-2 pl-0">
                <label for="employment_status">Status</label>
                <select name="employment_status" id="" class="custom-select">
                    <option value="1">Active</option>
                    <option value="2">In-active</option>
                </select>
            </div>            
        </div>
        <div class="row">
            <div class="form-group col-6 pl-0">
                <label for="name">Employee Name</label>
                <input type="text" class="form-control" id="name" placeholder="Employee name">
            </div>
            <div class="form-group col-3 pl-0">
                <label for="preferred_name">Preferred Name</label>
                <input type="text" class="form-control" id="preferred_name" placeholder="Employee name">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-3 pl-0">
                <label for="date_of_birth">Date of Birth</label>
                <input type="date" class="form-control" id="date_of_birth" placeholder="Employee name">
            </div>
            <div class="form-group col-3 pl-0">
                <label for="gender">Gender</label>
                <select name="gender" id="gender" class="custom-select">
                    <option selected>Choose..</option>
                    <option value="1">Male</option>
                    <option value="2">Female</option>
                </select>
            </div>
            <div class="form-group col-3 pl-0">
                <label for="martial_status_id">Marital Status</label>
                <select name="martial_status_id" id="martial_status_id" class="custom-select">
                    <option selected>Choose..</option>
                    <option value="1">Single</option>
                    <option value="2">Married</option>
                    <option value="3">Divorced</option>
                </select>
            </div>            
        </div>

    </div>


    <div class="biz-section mb-3 border-bottom ">
        <div class="row">
            <h5>Address</h5>
        </div>
        
        <div class="row">
            <div class="form-group col-5 pl-0 mb-2">
                <input type="text" class="form-control" id="address1" placeholder="Address 1">
            </div>
        </div>
    
        <div class="row">
            <div class="form-group col-5 pl-0 mb-2">
                <input type="text" class="form-control" id="address2" placeholder="Address 2">
            </div>
        </div>

        <div class="row">
            <div class="form-group col-3 pl-0">
                <input type="text" class="form-control" id="city" placeholder="City">
            </div>
            <div class="form-group col-3 pl-0">
                <input type="text" class="form-control" id="area" placeholder="Area">
            </div>
            <div class="form-group col-3 pl-0">
                <input type="text" class="form-control" id="postcode" placeholder="Postcode">
            </div>
        </div>
    </div>
    

    <div class="biz-section mb-3 border-bottom ">
        <div class="row">
            <h5>Contact</h5>
        </div>
        <div class="row form-group">
            <label for="">Phone</label>
        </div>
        <div class="row">
            <div class="form-group col-3 pl-0">
                <div class="input-group ">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1"><i class="fa fa-building"></i></span>
                    </div>
                    <input type="text" id="work_phone" class="form-control" placeholder="Work Number" aria-label="Work Number" aria-describedby="basic-addon1">
                </div>
            </div>
            <div class="form-group col-1 pl-0">
                <input type="text" class="form-control" id="phone_ext" placeholder="Ext">
            </div>
            <div class="form-group col-3 pl-0">
                <div class="input-group ">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1"><i class="fa fa-mobile"></i></span>
                    </div>
                    <input type="text" class="form-control" id="hand_phone" placeholder="Mobile Number">
                </div>
            </div>
            <div class="form-group col-3 pl-0">
                <div class="input-group ">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1"><i class="fa fa-home"></i></span>
                    </div>
                    <input type="text" class="form-control" id="home_phone" placeholder="Home Number">
                </div>
            </div>
        </div>
        <div class="row form-group">
            <label for="">Email</label>
        </div>
        <div class="row">
            <div class="form-group col-5 pl-0 mb-0">
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <span class="input-group-text" id="basic-addon1"><i class="fa fa-building"></i></span>
                    </div>
                    <input type="text" id="work_phone" class="form-control" placeholder="Work Email" aria-label="Work Email" aria-describedby="basic-addon1">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-5 pl-0 mb-2">
                <div class="input-group mb-2">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-home"></i></span>
                    </div>
                    <input type="text" class="form-control" id="home_phone" placeholder="Home Email">
                </div>
            </div>
        </div>
    </div>
    
    <div class="biz-section mb-3">
        <div class="row">
            <div class="col-6">
                <h5 class="font-weight-bold">Work Visa Information</h5>
            </div>
            
            <div class="col-6 text-right text-biz-blue">
                <i class="fa fa-plus-circle my-auto px-2" style="font-size: 18px;" data-toggle="modal" data-target="#exampleModalCenter"></i> 
                <a for="" class="" onclick="AddNewVisa(); return false;" data-toggle="modal" data-target="#exampleModalCenter">Add New Work Visa</a>  
            </div>
        </div>

        <table class="table">
            <thead class="text-white bg-secondary">
                <tr>
                    <th scope="col" style="width: 5%;"></th>
                    <th scope="col" style="width: 15%;">Date</th>
                    <th scope="col" style="width: 20%;">Visa</th>
                    <th scope="col" style="width: 15%;">Issued</th>
                    <th scope="col" style="width: 15%;">Expiration</th>
                    <th scope="col" style="width: 15%;">Note</th>
                    <th scope="col" style="width: 15%;">Actions</th>
                </tr>
            </thead>
            <tbody>
                <tr class="no-data text-center">
                    <td colspan="7">No visa enteries are present</td>
                </tr>
            </tbody>
        </table>
    </div>
    
    </form>

    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form action="/visas/" method="POST"></form>
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Add New Visa</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-6 ">
                            <label for="date_entered" class="mb-1 text-secondary">Date</label>
                            <input type="date" class="form-control" id="date_entered" placeholder="dd/mm/yyyy">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                            <label for="visa_type" class="mb-1 text-secondary">Visa Type</label>
                            <select name="visa_type" id="visa_type" class="custom-select">
                                <option selected>Choose..</option>
                                <option value="1">Employment Visa</option>
                                <option value="2">IC</option>
                                <option value="3">Work Permit</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-6 ">
                            <label for="issued" class="mb-0 text-secondary">Issued Date</label>
                            <input type="date" class="form-control" id="issued" placeholder="dd/mm/yyyy">
                        </div>
                        <div class="form-group col-6 pl-0">
                            <label for="expiration" class="mb-0 text-secondary">Expiry Date</label>
                            <input type="date" class="form-control" id="expiration" placeholder="dd/mm/yyyy">
                        </div>         

                    </div>
                    <div class="row">
                        <div class="form-group col-12">
                            <label for="note" class="mb-0 text-secondary">Note on Visa</label>
                            <textarea input type="text" class="form-control" id="note" placeholder="Please enter any notes" rows="3"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </div>    
</div>
<script>
    function AddNewVisa(){
        $('#exampleModalCenter').modal(options)
    }
</script>
@endsection
