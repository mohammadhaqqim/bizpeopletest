<x-layout>

    @include('components.payroll_header') 
    
    <div class="min-content mt-0 ">
    
        <div class="main px-3 ">

            <div class="row border-bottom my-3 pb-3 biz-title">
                <i class="fa fa-calendar my-auto px-2"></i> 
                <h2 class="mb-0">Payroll Approval</h2>
            </div>
            
<!-- display the payroll period information -->
            @include('components.payroll_details')

            <div class="row pt-1 my-3">
                <div class="col-6 pl-0">
                    <h4 class="my-1">{{ $payroll->title }}</h4>
                </div>
{{--                 <div class="col-3 px-0">
                </div> --}}
                <div class="col-6 px-0">
                    <button type="button" class="btn btn-sm btn-outline-success" 
                    onclick="window.location='{{ url('printpayslip/payrolls/'.$payroll->id) }}'"><i class="fa fa-download"></i> Payslips</button>        
                    <input class="form-control form-control-sm col-6 float-right" id="myInput" type="text" placeholder="Search..">
                </div>
            </div>            

        @if( $payroll->approved_by == null )        
            <form method="POST" action="/payroll/{{ $payroll->id }}/batch-approval">            
            @csrf                        
            @method('PATCH')
        @endif

            <div class="row">

                <table id="payrollTable" class="table table-sm border mb-1" >
                    <thead class="tbl-dark">
                        <tr>
                            <th class="align-middle">#</th>
                            @if( $payroll->approved_by == null )
                            <th class="align-middle" >
                                Batch
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="customCheck" onchange="checkAll(this)">
                                    <label class="custom-control-label" for="customCheck">All</label>
                                </div>
                            </th>
                            @endif
                            <th class="align-middle" hidden>Name</th>
                            <th class="align-middle">Employee Name</th>
                            <th class="align-middle">Department</th>
                            <th class="align-middle" hidden>Base</th>
                            <th class="align-middle">Method</th>
                            <th class="align-middle text-right">Base Wage
                                <small class="d-block">$B</small> 
                            </th>
                            <th class="align-middle text-right">Unpaid Leave
                                <small class="d-block">$B</small> 
                            </th>
                            <th class="align-middle text-right" {{ $company->use_tap ? '' : 'hidden' }}>TAP
                                <small class="d-block">$B</small> 
                            </th>
                            <th class="align-middle text-right" {{ $company->use_scp ? '' : 'hidden' }}>SCP
                                <small class="d-block">$B</small> 
                            </th>
                            <th class="align-middle text-right" {{ $company->use_epf ? '' : 'hidden' }}>EFP
                                <small class="d-block">$B</small> 
                            </th>
                            <th class="align-middle text-right">Addition
                                <small class="d-block">$B</small> 
                            </th>
                            <th class="align-middle text-right">Deduction
                                <small class="d-block">$B</small> 
                            </th>
                            <th class="align-middle text-right">Net Pay
                                <small class="d-block">$B</small> 
                            </th>
                        </tr>
                    </thead>
                    <tbody id="myTable" name="myTable" style="font-size: 12px;">
                    @foreach($employeePayrolls as $emp_payroll)
                    <tr style="">
                        <td class="align-middle">{{ $emp_payroll->id }}</td>
                        @if( $payroll->approved_by == null )
                        <td class="align-middle">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input approvecheck" id="approve{{ $emp_payroll->id }}" name="approve{{ $emp_payroll->id }}" {{ $emp_payroll->approved  ? 'checked' : '' }}>
                                <label class="custom-control-label" for="approve{{ $emp_payroll->id }}"> </label>
                            </div>
                        </td>
                        @endif
                        <td hidden>{{ $emp_payroll->Employee->name }}</td>
                        <td class="align-middle" style="">  
                            <a href="/printpayslip/{{ $emp_payroll->id }}" target="_blank" style="font-size: 13px;">{{ $emp_payroll->Employee->name }}</a>
                            <small class="d-block">{{ $emp_payroll->EmployeeJob->Designation->title }}</small>
                        </td>
                        <td class="align-middle">{{ $emp_payroll->EmployeeJob->Department->title }}</td>
                        <td class="align-middle" hidden>{{ $emp_payroll->base_salary }}</td>
                        <td class="align-middle">
                            {{ $emp_payroll->paymentmode->title }}
                        </td>
                        <td class="align-middle text-right">{{ number_format($emp_payroll->base_salary, 2) }}</td>
                        <td class="align-middle text-right">{{ number_format($emp_payroll->unpaid_amount, 2) }}</td>
                        <td class="align-middle text-right" {{ $company->use_tap ? '' : 'hidden' }}>
                            {{ number_format($emp_payroll->tap_ee, 2)  }}
                        </td>
                        <td class="align-middle text-right" {{ $company->use_scp ? '' : 'hidden' }}>{{ number_format($emp_payroll->scp_ee, 2) }}</td>
                        <td class="align-middle text-right" {{ $company->use_epf ? '' : 'hidden' }}>{{ number_format($emp_payroll->epf_ee, 2) }}</td>
                        <td class="align-middle text-right">{{ number_format($emp_payroll->addition_amount, 2) }}</td>
                        <td class="align-middle text-right">{{ number_format($emp_payroll->deduction_amount, 2) }}</td>
                        <td class="align-middle text-right">
                            {{ number_format($emp_payroll->base_salary - $emp_payroll->unpaid_amount + $emp_payroll->addition_amount - $emp_payroll->deduction_amount 
                                - $emp_payroll->tap_ee - $emp_payroll->scp_ee - $emp_payroll->epf_ee, 2) }} 
                        </td>    
                    </tr>
                    @endforeach
                    <tr class="text-center" style="background-color: #F3F3F3; height: 50px;">
                        <td></td>
                        @if( $payroll->approved_by == null )
                        <td class="text-left">
                        </td>
                        @endif
                        <td></td>
                        <td></td>
                        <td class="font-weight-bold align-middle text-right">Totals $</td>
                        <td class="font-weight-bold align-middle text-right">{{ number_format($employeePayrolls->sum('base_salary'), 2) }}</td>
                        <td class="font-weight-bold align-middle text-right">{{ number_format($employeePayrolls->sum('unpaid_amount'), 2) }}</td>
                        <td class="font-weight-bold align-middle text-right" {{ $company->use_tap ? '' : 'hidden' }}>{{ number_format($employeePayrolls->sum('tap_ee'), 2) }}</td>
                        <td class="font-weight-bold align-middle text-right" {{ $company->use_scp ? '' : 'hidden' }}>{{ number_format($employeePayrolls->sum('scp_ee'), 2) }}</td>
                        <td class="font-weight-bold align-middle text-right" {{ $company->use_epf ? '' : 'hidden' }}>{{ number_format($employeePayrolls->sum('epf_ee'), 2) }}</td>
                        <td class="font-weight-bold align-middle text-right">{{ number_format($employeePayrolls->sum('addition_amount'), 2) }}</td>
                        <td class="font-weight-bold align-middle text-right">{{ number_format($employeePayrolls->sum('deduction_amount'), 2) }}</td>
                        <td class="font-weight-bold align-middle text-right">
                            {{ number_format($employeePayrolls->sum('base_salary') - $employeePayrolls->sum('unpaid_amount') + $employeePayrolls->sum('addition_amount') 
                                - $employeePayrolls->sum('deduction_amount') - $employeePayrolls->sum('tap_ee') 
                                - $employeePayrolls->sum('scp_ee') - $employeePayrolls->sum('epf_ee'), 2) }} 
                        </td>    
                    </tr> 

                    </tbody>
                </table>
            @if( $payroll->approved_by == null )        
                <button type="submit" class="btn btn-sm btn-primary">Approve Batch</button>
            @endif

            </div>
    @if( $payroll->approved_by == null )            
        </form>                        
    @endif

@if( $payroll->approved_by == null )        
        <form id="editTransaction" method="POST" action="/payroll/{{ $payroll->id }}/approval">
        @csrf                        
        @method('PATCH')
            <div class="row mt-3">
                <label class="my-auto mr-3">I have reviewed the preview information and are ready to approve this payroll</label>
                <button class="btn btn-primary" >Approve Payroll Date: {{ Carbon\Carbon::now()->format('d M Y') }}</button>
            </div>            
        </form>
@endif
        <div class="m-4">
            <label for=""></label>
        </div>

    </div>

@if( $payroll->approved_by == null )            
    <div class="modal" tabindex="-1"  id="editModal">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <form id="editPayrollTrans" method="POST" action="">
            @csrf                        
            @method('PATCH')
    
            <div class="modal-header">
              <i class="fa fa-money"></i>
              <h5 class="modal-title">Update Payroll Entry</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col">
                        <label for="name">Name</label>
                        <input type="text" class="form-control emp_name" id="name" name="name" disabled>
                    </div>    
                </div>
                <div class="row">            
                    <div class="form-group col">
                        <label for="department">Department</label>
                        <input type="text" class="form-control emp_department" id="department" name="department" disabled>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-6">
                        <label for="base_salary">Base Wage</label>
                        <input type="text" class="form-control emp_base_salary" id="base_salary" name="base_salary">                        
                    </div>
                    <div class="form-group col-6">
                        <label for="payment_mode_id">Payment Type</label>
                        <div class="input-group emp_payment_mode">
                            <select id="payment_mode_id" name="payment_mode_id" class="custom-select payment_method">
                            @foreach ($paymentModes as $mode)
                                <option value="{{ $mode->id }}" 
                                @if(isset($emp_payroll->payment_mode_id))
                                {{ $mode->id == $emp_payroll->payment_mode_id ? 'selected' :'' }}
                                @endif
                                >{{ $mode->title }}</option>

        {{--                         <option value="{{ $mode->id }}" {{ $mode->id == $emp_payroll->payment_mode_id ? 'selected' :'' }}>{{ $mode->title }}</option> --}}
                            @endforeach
                            </select>
                        </div>    
                    </div>                                    
                </div>
    
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
          </div>
        </div>
    </div>
    
    <div class="modal" tabindex="-1"  id="deleteModal">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <form id="deletePayrollTrans" method="POST" action="/payrollemployee/">
            @method('DELETE')
            @csrf                        

            <div class="modal-header">
              <i class="fa fa-file-text-o pt-1"></i>
              <h5 class="modal-title pl-2">Delete Employee Payroll Entry</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="form-group col text-center">
                        <h5>Are you sure you would like to delete the following employee payroll entry?</h5>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col">
                        <label for="name">Name</label>
                        <input type="text" class="form-control emp_name_delete" id="name" name="name" disabled>
                    </div>    
                </div>
                <div class="row">            
                    <div class="form-group col">
                        <label for="department">Department</label>
                        <input type="text" class="form-control emp_department_delete" id="department" name="department" disabled>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-6">
                        <label for="base_salary">Base Wage</label>
                        <input type="text" class="form-control emp_base_salary_delete" id="base_salary" name="base_salary" disabled>                        
                    </div>
                    <div class="form-group col-6">
                        <label for="payment_mode_id">Payment Type</label>
                        <div class="input-group emp_payment_mode">
                            <select id="payment_mode_id" name="payment_mode_id" class="custom-select payment_method_delete" disabled>
                            @foreach ($paymentModes as $mode)
                                <option value="{{ $mode->id }}" 
                                @if(isset($emp_payroll->payment_mode_id))
                                {{ $mode->id == $emp_payroll->payment_mode_id ? 'selected' :'' }}
                                @endif
                                >{{ $mode->title }}</option>
                            @endforeach
                            </select>
                        </div>    
                    </div>                                    
                </div>
                    
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="submit" class="btn btn-danger">Yes</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
            </div>
            </form>
          </div>
        </div>
    </div>
@endif 

<script>
$(document).ready(function(){
  $('#employee_id').editableSelect();

  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});

function deletePayrollTrans(id){

    document.getElementById("deletePayrollTrans").action = "/payrollemployee/" + id;

     var oTable = document.getElementById("payrollTable");
    for (i = 1; i < oTable.rows.length; i++){
        var oCells = oTable.rows.item(i).cells;

        if (oCells.item(0).innerHTML == id){
            document.getElementsByClassName("emp_name_delete")[0].value = oCells.item(1).innerHTML;
            document.getElementsByClassName("emp_department_delete")[0].value = oCells.item(3).innerHTML; 
            document.getElementsByClassName("emp_base_salary_delete")[0].value = Number(oCells.item(4).innerHTML).toFixed(2);                        
            document.getElementsByClassName("payment_method_delete")[0].value = document.getElementsByClassName("emp"+id)[0].value;
        }
    } 

    $("#deleteModal").modal('show');
}

function editPayrollTrans(id){
    document.getElementById("editPayrollTrans").action = "/payrollemployee/" + id;

     var oTable = document.getElementById("payrollTable");
    for (i = 1; i < oTable.rows.length; i++){
        var oCells = oTable.rows.item(i).cells;

        if (oCells.item(0).innerHTML == id){
            document.getElementsByClassName("emp_name")[0].value = oCells.item(1).innerHTML;
            document.getElementsByClassName("emp_department")[0].value = oCells.item(3).innerHTML; 
            document.getElementsByClassName("emp_base_salary")[0].value = Number(oCells.item(4).innerHTML).toFixed(2);                        
            document.getElementsByClassName("payment_method")[0].value = document.getElementsByClassName("emp"+id)[0].value;
        }
    } 

    $("#editModal").modal('show');
}

function checkSelection(a){
    var x = a.selectedIndex; //(a.value || a.options[a.selectedIndex].value);  //crossbrowser solution =)
    //alert(x);

    return;
    var selector = document.getElementById('employee_id');
    var x = document.getElementById('employee_id').value;
    alert(x);
    alert(selector.selectedIndex);

//    var value = selector[selector.selectedIndex].value;

    //alert("selection changed: ");
    // search table for user

/*     var oTable = document.getElementById("payrollTable");
    for (i = 1; i < oTable.rows.length; i++){
        var oCells = oTable.rows.item(i).cells;

        if (oCells.item(0).innerHTML == id){

        }
    }  */

}

function checkAll(bx) {
  var cbs = document.getElementsByClassName('approvecheck');
  for(var i=0; i < cbs.length; i++) {
      cbs[i].checked = bx.checked;
  }
}
</script>
</x-layout>
    