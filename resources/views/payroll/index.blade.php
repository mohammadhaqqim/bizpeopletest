<x-layout>
    
    @include('components.payroll_header') 
    
    <div class="mt-0">
    
        <div class="main px-3 ">

            <div class="row border-bottom my-3 pb-3 biz-title">
                <i class="fa fa-calendar my-auto px-2"></i> 
                <h2 class="mb-0">Run Payroll </h2>
            </div>
            <small>Available payrolls are displayed below. Please ensure the information is correct</small>

            <div class="row pt-1">
                <div class="col-6 pl-0">
                    <h4>Current Payrolls</h4>
                </div>
                <div class="col-6 text-right text-biz-blue">
{{--                     <i class="fa fa-plus-circle my-auto px-2" style="font-size: 18px;" ></i> 
                    <a href="/payroll/create" ><label class="text-right">Create new payroll</label></a> --}}
{{--                     <a href="" data-toggle="modal" data-target="#payrollMasterModal" > --}}
                    <a href="/payroll/create" >
                        <i class="fa fa-plus-circle my-auto px-2" style="font-size: 18px;" ></i> 
                        <label class="text-right">Create new payroll</label>
                    </a>
                </div>
            </div>            
            <div class="row">
                <table id="tblPayrolls" class="table table-sm border mb-1" >
                    <thead class="tbl-dark">
                        <tr >
                            <th scope="col" class="align-middle">ID</th>
                            <th scope="col" class="align-middle" hidden>Title </th>
                            <th scope="col" class="align-middle">Title </th>
                            <th scope="col" class="align-middle">Action</th>
                            <th scope="col" class="align-middle">Start Date</th>
                            <th scope="col" class="align-middle">End Date</th>
                            <th scope="col" class="align-middle">Work Days</th>
                            <th scope="col" class="align-middle">Due Date</th>
                            <th scope="col" class="align-middle">Pay Date</th>
                            <th hidden>Remarks</th>
                            <th hidden>start</th>
                            <th hidden>end</th>
                            <th hidden>due</th>
                            <th hidden>pay</th>
                        </tr>
                    </thead>
                    <tbody style="font-size: 13px;">
 
                        @foreach ($payrollMasters as $payroll)
                        <tr>
                            <td class="align-middle">{{ $payroll->id }}</td>
                            <td class="align-middle" hidden>{{ $payroll->title }}</td>
                            <td class="align-middle" style="">
                                <a href="#" onclick="payrollEdit({{ $payroll->id }}); return false;">{{ $payroll->title }}</a>
                            </td>
                            <td class="align-middle">
                            @if($payroll->run_payroll)
                                @if($payroll->approved_by == null)
                                <button type="button" class="btn btn-sm btn-success" onclick="window.location='{{ url('payroll/'.$payroll->id.'/employees') }}'">Employees</button>
                                <button type="button" class="btn btn-sm btn-success" onclick="window.location='{{ url('payroll/'.$payroll->id.'/transactions') }}'">Transactions</button>
                                <button type="button" class="btn btn-sm btn-success" onclick="window.location='{{ url('payroll/'.$payroll->id.'/approval') }}'">Approval</button>
                                @else 
                                <button type="button" class="btn btn-sm btn-success" onclick="window.location='{{ url('payroll/historical/'.$payroll->id.'/employees') }}'">Employees</button>
                                <button type="button" class="btn btn-sm btn-success" onclick="window.location='{{ url('payroll/historical/'.$payroll->id.'/transactions') }}'">Transactions</button>
                                <button type="button" class="btn btn-sm btn-success" onclick="window.location='{{ url('payroll/historical/'.$payroll->id.'/approval') }}'">Approval</button>
                                @endif
                            @else
                                <form method="POST" action="/run/payroll/{{ $payroll->id }}">
                                    @csrf
                                    @method('PATCH')
                                    <button type="submit" class="btn btn-sm btn-success">Run Payroll</button>    
                                </form>
                            @endif
                            </td>
                            <td class="align-middle">
                                {{ \Carbon\Carbon::parse($payroll->start_date)->format('d-M-Y') }}
                            </td>
                            <td class="align-middle">
                                {{ \Carbon\Carbon::parse($payroll->end_date)->format('d-M-Y') }}
                            </td>
                            <td class="align-middle">
                                {{ $payroll->working_days }}
                            </td>
                            <td class="align-middle">
                                {{ \Carbon\Carbon::parse($payroll->due_date)->format('d-M-Y') }}
                            </td>
                            <td class="align-middle">
                                {{ \Carbon\Carbon::parse($payroll->pay_date)->format('d-M-Y') }}
                            </td>
                            <td hidden>{{ $payroll->remarks }}</td>
                            <th hidden>{{ $payroll->start_date }}</th>
                            <th hidden>{{ $payroll->end_date }}</th>
                            <th hidden>{{ $payroll->due_date }}</th>
                            <th hidden>{{ $payroll->pay_date }}</th>
                        </tr>                            
                        @endforeach     
                    </tbody>
                </table>
            </div>
    
        </div>
    </div>


    <div class="modal" id="payrollMasterModal">
        <div class="modal-dialog modal-dialog-centered">
            <form method="POST" action="/payroll">
                @csrf
    
                <div class="modal-content ">
                    <div class="modal-header">
                        <i class="fa fa-user my-auto px-2"></i> 
                        <h5 class="modal-title">Create New Payroll Period</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col">
                                <label for="title">Payroll Title</label><label class="text-danger"> *</label>
                                <input type="text" class="form-control" id="title" name="title" 
                                    placeholder="Payroll Title" maxlength="100" required>
                            </div>       
                        </div>

                        <div class="row">
                            <div class="form-group col">
                                <label for="start_date">Start Date</label><label class="text-danger"> *</label>
                                <div class="input-group">
                                    <input type="date" class="form-control" id="start_date" name="start_date" placeholder="dd/mm/yyyy" value="{{ date('Y-m-d', strtotime('0 day')) }}" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>    
                            </div>
                            <div class="form-group col">
                                <label for="end_date">End Date</label><label class="text-danger"> *</label>
                                <div class="input-group">
                                    <input type="date" class="form-control" id="end_date" name="end_date" onchange="changeDuePay()" placeholder="dd/mm/yyyy" value="{{ date('Y-m-d', strtotime('0 day')) }}" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>    
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col">
                                <label for="due_date">Due Date</label>
                                <div class="input-group">
                                    <input type="date" class="form-control" id="due_date" name="due_date" placeholder="dd/mm/yyyy" value="{{ date('Y-m-d', strtotime('0 day')) }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>    
                            </div>
                            <div class="form-group col">
                                <label for="pay_date">Pay Date</label>
                                <div class="input-group">
                                    <input type="date" class="form-control" id="pay_date" name="pay_date" placeholder="dd/mm/yyyy" value="{{ date('Y-m-d', strtotime('0 day')) }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                                <label for="working_days">Working Days</label><label class="text-danger"> *</label>
                                <input type="number" class="form-control " id="working_days" name="working_days" 
                                    placeholder="Working days" required>
                            </div>       
                        </div>   

                        <div class="form-group">
                            <label for="remarks">Remarks</label>
                            <textarea class="form-control" name="remarks" id="remarks" rows="2" maxlength="2000"></textarea>
                        </div>
    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" >Save Changes</button>
                    </div>
                </div>
            </form>
        </div>

    </div>

    <div class="modal" id="editMasterModal">
        <div class="modal-dialog modal-dialog-centered">
            <form id="editPayroll" method="POST" action="/payroll">
                @csrf
                @method('PATCH')
    
                <div class="modal-content ">
                    <div class="modal-header">
                        <i class="fa fa-user my-auto px-2"></i> 
                        <h5 class="modal-title">Edit Payroll Period</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group col">
                                <label for="title">Payroll Title</label>
                                <input type="text" class="form-control editTitle" id="title" name="title" 
                                    placeholder="Payroll Title" maxlength="100" >
                            </div>       
                        </div>

                        <div class="row">
                            <div class="form-group col">
                                <label for="start_date">Start Date</label>
                                <div class="input-group">
                                    <input type="date" class="form-control editStartDate" id="start_date" name="start_date" placeholder="dd/mm/yyyy" value="{{ date('Y-m-d', strtotime('0 day')) }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>    
                            </div>
                            <div class="form-group col">
                                <label for="end_date">End Date</label>
                                <div class="input-group">
                                    <input type="date" class="form-control editEndDate" id="end_date" name="end_date" placeholder="dd/mm/yyyy" value="{{ date('Y-m-d', strtotime('0 day')) }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>    
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col">
                                <label for="due_date">Due Date</label>
                                <div class="input-group">
                                    <input type="date" class="form-control editDueDate" id="due_date" name="due_date" placeholder="dd/mm/yyyy" value="{{ date('Y-m-d', strtotime('0 day')) }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>    
                            </div>
                            <div class="form-group col">
                                <label for="pay_date">Pay Date</label>
                                <div class="input-group">
                                    <input type="date" class="form-control editPayDate" id="pay_date" name="pay_date" placeholder="dd/mm/yyyy" value="{{ date('Y-m-d', strtotime('0 day')) }}">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>    
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6">
                                <label for="working_days">Working Days</label><label class="text-danger"> *</label>
                                <input type="number" class="form-control editWorking" id="working_days" name="working_days" 
                                    placeholder="Working days" required>
                            </div>       
                        </div>   
                        <div class="form-group">
                            <label for="remarks">Remarks</label>
                            <textarea class="form-control editRemarks" name="remarks" id="remarks" rows="2" maxlength="2000"></textarea>
                        </div>
    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" >Save Changes</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <script>
        function payrollEdit(id){
            document.getElementById("editPayroll").action = "/payroll/" + id;

            var oTable = document.getElementById("tblPayrolls");
            
            for (i = 1; i < oTable.rows.length; i++){
                var oCells = oTable.rows.item(i).cells;

                if (oCells.item(0).innerHTML == id){
                    document.getElementsByClassName("editTitle")[0].value = oCells.item(1).innerHTML;
                    document.getElementsByClassName("editStartDate")[0].value = oCells.item(10).innerHTML;
                    document.getElementsByClassName("editEndDate")[0].value = oCells.item(11).innerHTML;
                    document.getElementsByClassName("editWorking")[0].value = oCells.item(6).innerHTML.trim();
                    document.getElementsByClassName("editDueDate")[0].value = oCells.item(12).innerHTML;
                    document.getElementsByClassName("editPayDate")[0].value = oCells.item(13).innerHTML;
                    document.getElementsByClassName("editRemarks")[0].value = oCells.item(9).innerHTML;
                }
            }

            $("#editMasterModal").modal('show');
        }

        function changeDuePay()
        {
            var endDate = document.getElementById("end_date").value; 
            var dueDate = document.getElementById("due_date").value;
            var payDate = document.getElementById("pay_date").value;

            var today = new Date();

            dd = String(today.getDate()).padStart('2', '0');
            mm = String(today.getMonth() + 1).padStart('2', '0');
            yyyy = today.getFullYear();
            today = yyyy + '-' + mm + '-' + dd;
            
            if(dueDate == today)
            {
                document.getElementById("due_date").value = endDate; 
            }

            if(payDate == today)
            {
                document.getElementById("pay_date").value = endDate; 
            }

        }
    </script>
</x-layout>
    