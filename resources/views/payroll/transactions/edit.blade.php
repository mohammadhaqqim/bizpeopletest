<x-layout>

    @include('components.payroll_header') 
    
    <div class="min-content mt-0 ">
    
        <div class="main px-3 ">

            <div class="row border-bottom my-3 pb-3 biz-title">
                <i class="fa fa-calendar my-auto px-2"></i> 
                <h2 class="mb-0">Payroll Transactions</h2>
            </div>
            
<!-- display the payroll period information -->
            @include('components.payroll_details')  
            
<!-- add employee to list -->
@if( $payroll->approved_by == null )
        <form method="POST" action="/payroll/{{ $payroll->id }}/transaction">
            @csrf                        
            
            <div class="row pt-3 pb-2">
                <div class="col-sm col-4-md px-0">
                    <label for="" class="mb-0">Employee Search</label> 
                    <select name="employee_id" id="employee_id" class="custom-select" onchange="findSalary()" onfocus="this.setSelectionRange(0, this.value.length)">

                    @foreach ($employees as $employee)
                    @if($employee->getPayrollEmployees->count() > 0)
                        @if(!$employee->getPayrollEmployees[0]->approved )
                        <option value="{{ $employee->id }}" >{{ $employee->name }}</option>
                        @endif
                    @endif
                    @endforeach

                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-6 col-md-8 p-0">
                    <div class="row">
                        <div class="col-md-6 px-0 pr-sm-2">
                            <label for="end_employment_id" class="mb-0"> Transaction Type </label>        
                            <select id="add_ded_id" name="add_ded_id" class="custom-select custom-select mb-3" onchange="typeChanged(this)">
                                <option value="1">Addition</option>
                                <option value="2" selected>Deduction</option>
                            </select>
                        </div>
                        <div class="col-md-6 col px-0 pr-sm-2">
                            <label id="label_add_ded" for="end_employment_id" class="mb-0">Addition / Deduction Type </label>  
                            <select id="additions" name="additions" class="custom-select custom-select mb-3 deductions" onchange="checkOvertime(this)">
                                <option selected>Select type</option>
                            @foreach($transactions as $transaction)
                                @if ( $transaction->addition )
                                <option value="{{ $transaction->id }}" data-calculated="{{ $transaction->calculated }}">{{ $transaction->title }}</option>
                                @endif
                            @endforeach
                            </select>

                            <select id="deductions" name="deductions" class="custom-select custom-select mb-3 deductions" onchange="checkSelection()">
                                <option selected>Select type</option>
                            @foreach($transactions as $transaction)
                                @if ( !$transaction->addition )
                                <option value="{{ $transaction->id }}" data-calculated="{{ $transaction->calculated }}">{{ $transaction->title }}</option>
                                @endif
                            @endforeach
                            </select>

                        </div>
        
                    </div>
                    <div class="row d-flex justify-content-end">
                        <div class="col-md-6 p-0">
                            <label for="end_employment_id" class="mb-0">Amount </label>      
                            <input type="number" id="amount" name="amount" class="form-control" step="0.01" placeholder="$ 0.00" >  
                        </div>     
                    </div>
                </div>
                
                <div id="calculationSection" class="col-md-4 col-6 flex-column pr-0">
                    <a href="#collapseCalc" role="button" data-toggle="collapse"><small> Show Calculation </small></a>
                    <div id="collapseCalc" class="collapse">
                        <div class="card card-body p-2">

                            <div class="row m-0" style="height: 25px;">
                                <small for="" class="col-6">Base Salary</small> 
                                <select name="employee_salary" id="employee_salary" class="custom-select custom-select-sm mx-auto" style="width: 75px;" disabled>

                                    @foreach ($employees as $employee)
                                        <option value="{{ $employee->id }}" >{{ $employee->getCurrentPay() }}</option>
                                    @endforeach
                
                                </select>
                            </div>
                            <div class="row m-0 d-flex align-items-center" style="height: 35px;">
                                <small for="" class="col-6">Working Days</small> 
                                <label for="" id="calAmount" name="calAmount" class="col-4 text-center mx-auto my-auto" style="width: 75px;">{{ $payroll->working_days }}</label>
                            </div>
                            <div class="row m-0" style="height: 35px;">
                                <div class="col-6 my-auto">
                                    <small for="calRate" class="">Rate</small>
                                </div>
                                <div class="col-6 justify-right">                           
                                    <input type="text" id="calRate" name="calRate" class="form-control form-control-sm mx-auto " step="0.01" 
                                        placeholder=""  style="width: 75px;"onblur="performCal()">  
                                </div>
                            </div>
                            <div class="row m-0" style="height: 35px;">
                                <small for="calHours" class="col-6 my-auto">Hours</small> 
                                <div class="col-6 justify-right">
                                    <input type="number" id="calHours" name="calHours" class="form-control form-control-sm mx-auto" step="0.01" 
                                        placeholder="" style="width: 75px;" onblur="performCalAmount()">  
                                </div>
                            </div>
                            <div class="row m-0" style="height: 25px;" >
                                <small for="" class="col-6">Overtime</small> 
                                <select name="employee_overtime" id="employee_overtime" class="custom-select custom-select-sm mx-auto" style="width: 75px;" disabled>
    
                                    @foreach ($employees as $employee)
                                        <option value="{{ $employee->id }}" >{{ $employee->getOvertime() }}</option>
                                    @endforeach
                
                                </select>
                            </div>    
                        </div>
                        <small class="text-muted pl-2">Calculations can be performed within "Rate"</small>

                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col text-bottom pb-1 d-flex justify-content-end pr-0">
                    <button type="submit" class="btn btn-sm btn-primary mr-2">Add Transaction</button>
                    <button type="button" class="btn btn-sm btn-danger" onclick="resetCalc(this)">Reset / Clear</button>
                </div>
            </div>
        </form>
@endif

            <div class="row pt-1 my-3">
                <div class="col-6 pl-0">
                    <h4 class="my-1">{{ $payroll->title }} Transactions</h4>
                </div>
                <div class="col-6 px-0">
                    <input class="form-control form-control-sm col-md-8 col-sm-7 float-right" id="myInput" type="text" placeholder="Search.." onfocus="this.setSelectionRange(0, this.value.length)">
                </div>
            </div>            

            <div class="row">
                <table id="payrollTable" class="table table-sm border mb-1" >
                    <thead class="tbl-dark">
                        <tr>
                            <th class="align-middle">#</th>
                            <th class="align-middle">Employee Name</th>
                            <th class="align-middle" hidden>Employee Name</th>
                            <th class="align-middle">Department</th>
                            <th class="align-middle">Type</th>
                            <th class="align-middle" hidden>$ amount</th>
                            <th class="align-middle text-right">$ Addition</th>
                            <th class="align-middle text-right">$ Deduction</th>
                            <th hidden>hours</th>
                            <th hidden>salary</th>
                            <th hidden>name</th>
                        @if( $payroll->approved_by == null )
                            <th class="align-middle text-center">Edit</th>
                            <th class="align-middle text-center">Delete</th>
                        @endif
                        </tr>
                    </thead>
                    <tbody id="myTable" style="font-size: 12px;">
                    @foreach ($payrollTransactions as $trans)                        
                    <tr style="">
                        <td class="align-middle">{{ $trans->id }}</td>
                        <td class="align-middle" style="">
                            @if ( !$trans->PayrollEmployee->approved && $payroll->approved_by == null  )
                            <a href="" onclick="editTransaction({{ $trans->id }}); return false;" style="font-size: 13px;"> 
                                {{ $trans->PayrollEmployee->Employee->name }}
                            </a>
                            @else
                                {{ $trans->PayrollEmployee->Employee->name }}
                            @endif
                            <small class="d-block">{{ $trans->PayrollEmployee->Employee->getCurrentDesignation() }}</small>
                        </td>
                        <td class="align-middle" style="" hidden>{{ $trans->name }}</td>
                        <td class="align-middle" style="">{{ $trans->PayrollEmployee->Employee->getCurrentDepartment()->title }}</td>
                        <td class="align-middle"> {{ $trans->title }} </td>
                        <td class="align-middle" hidden> {{ number_format($trans->amount, 2) }}</td>
                    @if( $trans->addition )
                        <td class="align-middle text-right"> {{ number_format($trans->amount, 2) }}</td>
                        <td class="align-middle text-right"> - </td>
                    @else
                        <td class="align-middle text-right"> - </td>                        
                        <td class="align-middle text-right"> {{ number_format($trans->amount, 2) }}</td>
                    @endif
                        <td hidden>{{ $trans->daily_hours }}</td>
                        <td hidden>{{ $trans->basic_salary }}</td>   
                        <td hidden>{{ $trans->PayrollEmployee->Employee->name }}</td>
                    @if( $payroll->approved_by == null )
                        <td class="align-middle text-center" style="font-size: 16px;">
                            @if ( !$trans->PayrollEmployee->approved )
                            <a href="" onclick="editTransaction({{ $trans->id }}); return false;" >
                                <i class="fa fa-pencil my-auto px-2 text-info" ></i>
                            </a>
                            @endif
                        </td>
                        <td class="align-middle text-center" style="font-size: 16px;">
                            @if ( !$trans->PayrollEmployee->approved )
                            <a href="" onclick="deleteTransaction({{ $trans->id }}); return false;" >
                                <i class="fa fa-trash my-auto px-2 text-danger" ></i>
                            </a>
                            @endif
                        </td>                        
                    @endif
                    </tr>
                    @endforeach

                    <tr class="" style="background-color: #F3F3F3;">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td class="font-weight-bold">Totals</td>
                        <td class="font-weight-bold text-right">{{ number_format($payrollTransactions->where('addition', 1)->sum('amount'), 2) }}</td>
                        <td class="font-weight-bold text-right">{{ number_format($payrollTransactions->where('addition', 0)->sum('amount'), 2) }}</td>
                        <td hidden></td>
                        <td hidden></td>
                        @if( $payroll->approved_by == null )
                        <td></td>
                        <td></td>
                        @endif
                    </tr> 
                    </tbody>
                </table>
            </div>
    
        </div>
    </div>

    <div class="modal" tabindex="-1"  id="editModal">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <form id="editTransaction" method="POST" action="">
            @csrf                        
            @method('PATCH')
    
            <div class="modal-header">
              <i class="fa fa-money"></i>
              <h5 class="modal-title">Update Payroll Entry</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                
                <div class="row">
                    <div class="form-group col">
                        <label for="name">Name</label>
                        <input type="text" class="form-control emp_name_edit" id="name" name="name" disabled>
                    </div>    
                </div>
                <div class="row">            
                    <div class="form-group col">
                        <label for="department">Department</label>
                        <input type="text" class="form-control emp_department_edit" id="department" name="department" disabled>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-6">
                        <label for="transaction_type">Transaction Type</label>
                        <input type="text" class="form-control emp_type_edit" id="transaction_type" name="transaction_type" disabled>                        
                    </div>                                    
                    <div class="form-group col-6">
                        <label for="amount">Amount</label>
                        <input type="number" step="0.01" class="form-control emp_amount_edit" id="amount" name="amount" >                        
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-6">
                        <label for="hours">Hours / Days</label>
                        <input type="number" step="0.01" class="form-control emp_hours_edit" id="hours" name="hours" onblur="updateAmount()">                        
                    </div>
                    <div class="form-group col-6">
                        <label for="rate">Rate</label>
                        <input type="number" step="0.01" class="form-control emp_rate_edit" id="rate" name="rate" >                        
                    </div>                                
                </div>
                <div class="row">
                    <div class="form-group col-6">
                        <label for="base_salary" class="col-8 pl-0">Base Salary</label><label for="" class="col-4 base_salary">0.00</label>
                        <label for="working_days" class="col-8 pl-0">Working Days</label><label for="" class="col-4 working_days">{{ $payroll->working_days }}</label>
                        <label for="daily_hours" class="col-8 pl-0">Daily Hours</label><label for="" class="col-4 daily_hours">0.00</label>
                    </div>      
                    <div class="col-6 text-center">
                        <a href="" onclick="updateHours(true); return false;">
                            <div class="d-inline-block col-5 text-center py-2 m-1 border border-info " style="background-color: #E9E9E9">
                                <p class="d-inline-block mb-0">$</p><div class="d-inline-block" id="per_hour">2.89</div>
                                <small class="d-block">Per Hour</small>
                            </div>
                        </a>
                        <a href="" onclick="updateHours(false); return false;">
                            <div class="d-inline-block col-5 text-center py-2 m-1 border border-info " style="background-color: #E9E9E9">
                                <p class="d-inline-block mb-0">$</p><div class="d-inline-block" id="per_day">23.18</div>
                                <small class="d-block">Per Day</small>
                            </div>
                        </a>
                    </div>                              
                </div>
    
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
          </div>
        </div>
    </div>


    <div class="modal" tabindex="-1"  id="deleteModal">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <form id="deleteTransaction" method="POST" action="/payrolltransaction/">
            @method('DELETE')
            @csrf                        

            <div class="modal-header">
              <i class="fa fa-file-text-o pt-1"></i>
              <h5 class="modal-title pl-2">Delete Employee Payroll Transaction</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="form-group col text-center">
                        <label style="font-size: 16px;">Are you sure you would like to delete the following payroll transaction?</label>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col">
                        <label for="name">Name</label>
                        <input type="text" class="form-control emp_name_delete" id="name" name="name" disabled>
                    </div>    
                </div>
                <div class="row">            
                    <div class="form-group col">
                        <label for="department">Department</label>
                        <input type="text" class="form-control emp_department_delete" id="department" name="department" disabled>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-6">
                        <label for="transaction_type">Transaction Type</label>
                        <input type="text" class="form-control emp_type_delete" id="transaction_type" name="transaction_type" disabled>                        
                    </div>                                    
                    <div class="form-group col-6">
                        <label for="amount">Amount</label>
                        <input type="text" class="form-control emp_amount_delete" id="amount" name="amount" disabled>                        
                    </div>
                </div>
                    
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="submit" class="btn btn-danger">Yes</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
            </div>
            </form>
          </div>
        </div>
      </div>    
    
      @include('components.modal.add_allowance')

<script>
$(document).ready(function(){
  $('#employee_id').editableSelect();


  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

  var add = document.getElementById("additions");
  var ded = document.getElementById("deductions");

  document.getElementById("label_add_ded").innerText = "Addition Type";
  ded.style.display = "block";
  add.style.display = "none";
  
});

function resetCalc(a){
    document.getElementById('calHours').value = '0';

    findSalary();
    performCalAmount();
}

function findSalary(){
    var x = document.getElementsByClassName('es-visible selected');
    emp_id = x[0].value;

    document.getElementById('employee_salary').value = emp_id;
    document.getElementById('employee_overtime').value = emp_id;

    var salary = document.getElementById('employee_salary');
    salary = salary.options[salary.selectedIndex].text;


    var overtime = document.getElementById('employee_overtime');
    //overtime = overtime.options[salary.selectedIndex].text;


    var workdays = document.getElementById('calAmount').innerText;
//alert(salary);
    document.getElementById('calHours').value = '0';

    //    if( document.getElementById("label_add_ded").innerText == "Addition Type" )
    //      var addition = overtime.options[overtime.selectedIndex].text;
    //      if ( addition.includes('overtime') || addition.includes('Overtime') )
    //          
    document.getElementById('calRate').value = Number(salary/workdays).toFixed(4);
    //alert(document.getElementById('additions').value);
}

function performCalAmount(){
    var rate = document.getElementById('calRate').value;
    var hours = document.getElementById('calHours').value;

    document.getElementById('amount').value = Number(rate*hours).toFixed(2);    

}

function performCal(){
    var x = document.getElementById('calRate').value;
    //alert(isNaN(eval(x)));
    if (!isNaN(eval(x))){
        document.getElementById('calRate').value = Number(eval(x)).toFixed(4);
    }
}

function updateHours(hours){
    if(hours){
        document.getElementsByClassName("emp_rate_edit")[0].value = document.getElementById("per_hour").innerHTML;
    }
    else{
        document.getElementsByClassName("emp_rate_edit")[0].value = document.getElementById("per_day").innerHTML;
    }
}

function checkSelection(){
    var x = document.getElementById('deductions');
    var y = x.options.item(x.selectedIndex);

    if (y.getAttribute('data-calculated') == 1){
        document.getElementById('calculationSection').style.visibility = 'visible';
    }
    else{
        document.getElementById('calculationSection').style.visibility = 'hidden';
    }
}

function checkOvertime(a)
{
    var addition = a.options[a.selectedIndex].text;
    if (addition.includes('overtime') || addition.includes('Overtime'))
    {
        //alert('includes');
        var overtime = document.getElementById('employee_overtime');
        overtime = overtime.options[overtime.selectedIndex].text;
//        alert(overtime.options[overtime.selectedIndex].text);
 //        overtime = salary.options[overtime.selectedIndex].text;
        document.getElementById('calRate').value = Number(overtime).toFixed(4);

    }

}

function typeChanged(a) {
    var x = (a.value || a.options[a.selectedIndex].value);  //crossbrowser solution =)
    //alert(x);
    var add = document.getElementById("additions");
    var ded = document.getElementById("deductions");

    if (x == 1)
    {
        document.getElementById("label_add_ded").innerText = "Addition Type";
        add.style.display = "block";
        ded.style.display = "none";
    }
    else
    {
        document.getElementById("label_add_ded").innerText = "Deduction Type";
        ded.style.display = "block";
        add.style.display = "none";
    }
}

function updateAmount(){
    var hours = document.getElementsByClassName("emp_hours_edit")[0].value;
    var rate = document.getElementsByClassName("emp_rate_edit")[0].value;

    document.getElementsByClassName("emp_amount_edit")[0].value = Number(hours*rate).toFixed(2);
}

function editTransaction(id){
    document.getElementById("editTransaction").action = "/payrolltransaction/" + id;

    var oTable = document.getElementById("payrollTable");
    for (i = 1; i < oTable.rows.length; i++){
        var oCells = oTable.rows.item(i).cells;

        if (oCells.item(0).innerHTML == id){

            var sal = oCells.item(9).innerHTML;
            var hour = oCells.item(8).innerHTML;
            var working = document.getElementsByClassName('working_days')[0].innerText;

            document.getElementsByClassName("emp_name_edit")[0].value = oCells.item(10).innerHTML;
            document.getElementsByClassName("emp_department_edit")[0].value = oCells.item(3).innerHTML; 

            document.getElementsByClassName("emp_amount_edit")[0].value = Number(oCells.item(5).innerHTML).toFixed(2);                        
            document.getElementsByClassName("emp_type_edit")[0].value = oCells.item(4).innerHTML;
            
            //salary
            //hours
            document.getElementsByClassName("base_salary")[0].innerText = "$ "+sal;
            document.getElementsByClassName("daily_hours")[0].innerText = hour;
            document.getElementById("per_hour").innerHTML = Number(sal/working/hour).toFixed(2);
            document.getElementById("per_day").innerHTML = Number(sal/working).toFixed(2);

        }
    }  

    $("#editModal").modal('show');
}

function deleteTransaction(id){
    document.getElementById("deleteTransaction").action = "/payrolltransaction/" + id;

    var oTable = document.getElementById("payrollTable");
    for (i = 1; i < oTable.rows.length; i++){
        var oCells = oTable.rows.item(i).cells;

        if (oCells.item(0).innerHTML == id){
            document.getElementsByClassName("emp_name_delete")[0].value = oCells.item(10).innerHTML;
            document.getElementsByClassName("emp_department_delete")[0].value = oCells.item(3).innerHTML; 

            document.getElementsByClassName("emp_amount_delete")[0].value = Number(oCells.item(5).innerHTML).toFixed(2);                        
            document.getElementsByClassName("emp_type_delete")[0].value = oCells.item(4).innerHTML;
        }
    }  

    $("#deleteModal").modal('show');
}
</script>
</x-layout>
    