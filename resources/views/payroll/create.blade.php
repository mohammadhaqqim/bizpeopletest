<x-layout>
    
    @include('components.payroll_header') 

@if(count($errors) > 0)
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
                {{ $error }}<br>
        @endforeach
    </div>
@endif
    <div class="d-flex justify-content-center h-100">

    <div class="d-flex align-items-center ">
        <form method="POST" action="/payroll">
            @csrf

        <div class="card shadow" style="width: 29rem;">
            <h5 class="card-header bg-info text-white border-bottom-0">
                Create New Payroll Period
                <a href="/payroll">
                <button type="button" class="close text-white" data-dismiss="modal" aria-label="close" >
                    <span aria-hidden="true">&times;</span>
                </button>
                </a>
            </h5>

        
            <div class="card-body">   
                <div class="row">
                    <div class="form-group col">
                        <label for="title">Payroll Title</label><label class="text-danger"> *</label>
                        <input type="text" class="form-control" id="title" name="title" 
                            placeholder="Payroll Title" maxlength="100" value="{{ \Carbon\Carbon::parse($startDate)->format('F Y') }}" required>
                    </div>       
                </div>

                <div class="row">
                    <div class="form-group col">
                        <label for="start_date">Start Date</label><label class="text-danger"> *</label>
                        <div class="input-group">
                            <input type="date" class="form-control" id="start_date" name="start_date" placeholder="dd/mm/yyyy" value="{{ \Carbon\Carbon::parse($startDate)->format('Y-m-d') }}" required>
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>    
                    </div>
                    <div class="form-group col">
                        <label for="end_date">End Date</label><label class="text-danger"> *</label>
                        <div class="input-group">
                            <input type="date" class="form-control" id="end_date" name="end_date" onchange="changeDuePay()" placeholder="dd/mm/yyyy" value="{{ \Carbon\Carbon::parse($endDate)->format('Y-m-d') }}" required>
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>    
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col">
                        <label for="due_date">Due Date</label>
                        <div class="input-group">
                            <input type="date" class="form-control" id="due_date" name="due_date" placeholder="dd/mm/yyyy" value="{{ \Carbon\Carbon::parse($endDate)->format('Y-m-d') }}">
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>    
                    </div>
                    <div class="form-group col">
                        <label for="pay_date">Pay Date</label>
                        <div class="input-group">
                            <input type="date" class="form-control" id="pay_date" name="pay_date" placeholder="dd/mm/yyyy" value="{{ \Carbon\Carbon::parse($endDate)->format('Y-m-d') }}">
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>    
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-6">
                        <label for="working_days">Working Days</label><label class="text-danger"> *</label>
                        <input type="number" class="form-control " id="working_days" name="working_days" 
                            placeholder="Working days" required value="{{ $workingDays }}">
                    </div>       
                </div>   

                <div class="form-group px-0">
                    <label for="remarks">Remarks</label>
                    <textarea class="form-control" name="remarks" id="remarks" rows="2" maxlength="2000"></textarea>
                </div>

                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" >Save Changes</button>

            </div>
          </div>
        </form>
    </div>
</div>

</x-layout>