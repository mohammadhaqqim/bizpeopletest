<x-layout>
    
    @include('components.payroll_header') 

@if(count($errors) > 0)
    <div class="alert alert-danger">
        @foreach($errors->all() as $error)
                {{ $error }}<br>
        @endforeach
    </div>
@endif
    <div class="d-flex justify-content-center h-100">

    <div class="d-flex align-items-center ">
        <form method="POST" action="/payroll">
            @csrf

        <div class="card" style="width: 29rem;">
            <h5 class="card-header">
                Edit Employee Payroll - Overtime
              <button type="button" class="close" data-dismiss="modal" aria-label="close">
                  <span aria-hidden="true">&times;</span>
              </button>
            </h5>

            <div class="card-body">   
                <div class="row">
                    <div class="form-group col">
                        <label for="title">Employee</label>
                        <h3>{{ $employee->name }}</h3>
                    </div>       
                </div>

                <div class="row">
                    <div class="form-group col">
                        <label for="basic_salary">Remuneration Rate</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-append" style="width: 80px; ">
                                    <select name="currency_id" id="currency_id" class="custom-select aaa-prepend" >
                                    @if ($currencies->count() > 1)
                                    {{-- ???? old() --}}
                                        <option {{ old('country_id', null) !== null ? 'selected' : '' }} class="bg-white">...</option>
                                    @endif
    
                                    @foreach ($currencies as $currency)
                                        <option value="{{ $currency->id }}" {{ $currencies->count() == 1 ? 'selected' : '' }}>{{ $currency->currency_code }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <input type="number" class="form-control basePay" id="basic_salary" name="basic_salary" placeholder="0.00" step="0.01" value="">
                        </div>    
                    </div>
    
                    <div class="form-group col">
                        <label for="pay_type_id">Payment Type</label>
                        <div class="input-group">
                            <select name="payment_type_id" id="payment_type_id" class="custom-select paymentType">
                            @foreach ($paymenttypes as $paymenttype)                        
                                <option value="{{ $paymenttype->id }}">{{ $paymenttype->title }}</option>
                            @endforeach
                            </select>
                        </div>               
                    </div>                     
                </div>
    
                <div class="row">
                    <div class="form-group col">
                        <label for="basic_salary">Remuneration Rate</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-append" style="width: 80px; ">
                                    <select name="currency_id" id="currency_id" class="custom-select aaa-prepend" >
                                    @if ($currencies->count() > 1)
                                    {{-- ???? old() --}}
                                        <option {{ old('country_id', null) !== null ? 'selected' : '' }} class="bg-white">...</option>
                                    @endif
    
                                    @foreach ($currencies as $currency)
                                        <option value="{{ $currency->id }}" {{ $currencies->count() == 1 ? 'selected' : '' }}>{{ $currency->currency_code }}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <input type="number" class="form-control" id="basic_salary" name="basic_salary" placeholder="0.00" value="">
                        </div>    
                    </div>
    
                </div>
    
                <div class="row">
                    <div class="form-group col">
                        <label for="basic_salary">Overtime</label>
                        <div class="input-group">
                            <input type="number" class="form-control" id="overtime" name="overtime" placeholder="0.00" value="">
                        </div>
                    </div>
                    <div class="form-group col">
                        <label for="pay_date">Days / Hours</label>
                        <div class="input-group">
                            <input type="number" class="form-control" id="quantity" name="quantity" placeholder="0.00" value="">
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>    
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-6">
                        <label for="working_days">Working Days</label><label class="text-danger"> *</label>
                        <input type="number" class="form-control " id="working_days" name="working_days" 
                            placeholder="Working days" required value="{{ $workingDays }}">
                    </div>       
                </div>   

                <div class="form-group px-0">
                    <label for="remarks">Remarks</label>
                    <textarea class="form-control" name="remarks" id="remarks" rows="2" maxlength="2000"></textarea>
                </div>

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" >Save Changes</button>

            </div>
          </div>
        </form>
    </div>
</div>

</x-layout>