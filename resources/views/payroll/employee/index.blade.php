<x-layout>
    
    @include('components.payroll_header') 
    
    <div class="mt-0">
    
        <div class="main px-3 ">

            <div class="row border-bottom my-3 pb-3 biz-title">
                <div class="col-6 pl-0">
                    <i class="fa fa-address-book-o my-auto px-2"></i> <h4 class="my-1 d-inline">Current Employees Payroll Summary</h4>
                </div>
                <div class="col-6 px-0">
                    <input class="form-control form-control-sm col-6 float-right" id="bizSearch" type="text" placeholder="Search..">
                </div>
            </div>
            
            <div class="row">
                <table id="payrollTable" class="table table-sm border mb-1" >
                    <thead class="tbl-dark">
                        <tr>
                            <th class="align-middle">#</th>
                            <th class="align-middle" hidden>Name</th>
                            <th class="align-middle">Employee Name</th>
                            <th class="align-middle">Department</th>
                            <th class="align-middle" hidden>Base</th>
                            <th class="align-middle">Base Wage
                                <small class="d-block">$B</small> 
                            </th>
                            <th class="align-middle">Method</th>
                            <th class="align-middle">Addition
                                <small class="d-block">$B</small> 
                            </th>
                            <th class="align-middle">Deduction
                                <small class="d-block">$B</small> 
                            </th>
                        </tr>
                    </thead>
                    <tbody id="searchTable" style="font-size: 12px;">
                         @foreach($employees as $employee)
                        <tr style="">
                             <td class="align-middle">{{ $employee->id }}</td>
                            <td hidden>{{ $employee->name }}</td>
                            <td >
                                <a href="/employee/{{ $employee->id }}/edit" target="_blank" style="font-size: 13px;">{{ $employee->name }}</a>
                                <small class="d-block">{{ is_null($employee->getJob()) ? '' : $employee->getJob()->title }}</small>
                            </td>
                            @if( $employee->getDepartment() == null)
                            <td></td>
                            @else
                            <td class="align-middle">{{ $employee->getDepartment()->title }}</td>
                            @endif
                            <td class="align-middle">{{ number_format($employee->getBasePay(), 2) }}</td>
                            <td class="align-middle">
                                @foreach ($paymentmodes as $mode)
                                @if ( $employee->payment_mode_id == $mode->id )
                                {{ $mode->title }}
                                @endif
                                @endforeach
                            </td>
                            <td class="align-middle">{{ number_format($employee->getAdditions(), 2) }}</td>
                            <td class="align-middle">{{ number_format($employee->getDeductions(), 2) }}</td> 
                        </tr>
                        @endforeach 
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <script type="text/javascript" src="{{ URL::asset('js/bizsearch.js') }}"></script>

</x-layout>