<x-layout>
    @php($url = "APP_URL")   
    @include('components.payroll_header') 

    

    <div class="mt-0">    
        <div class="main px-3 ">

            <div class="row border-bottom my-3 pb-3 biz-title">
                <i class="fa fa-calendar my-auto px-2"></i> 
                <h2 class="mb-0">Payroll Audit Report </h2>
            </div>

            <div class="row pt-1">
                <div class="col-6 pl-0">
                    <h4>Payroll Auditing</h4>
                </div>
                <div class="col-6 px-0">
                    <input class="form-control form-control-sm col-6 float-right" id="bizSearch" type="text" placeholder="Search..">
                </div>
            </div>            
            <div class="row">
                <table id="tblPayrolls" class="table table-sm border mb-1" >
                    <thead class="tbl-dark">
                        <tr >
                            <th scope="col" class="align-middle">ID</th>
                            <th scope="col" class="align-middle">Payroll </th>
                            <th scope="col" class="align-middle" hidden>End date </th>
                            <th scope="col" class="align-middle">Created By</th>
                            <th scope="col" class="align-middle">Total Payout</th>
                            <th scope="col" class="align-middle">Payroll Totals</th>
                            <th scope="col" class="align-middle">Activity Report</th>
                            <th scope="col" class="align-middle">Tax Report</th>
                            <th scope="col" class="align-middle">Bank Report</th>

                        </tr>
                    </thead>
                    <tbody id="searchTable" style="font-size: 13px;">
 
                        @foreach ($payrolls as $payroll)
                        <tr>
                            <td class="align-middle">{{ $payroll->id }}</td>
                            <td class="align-middle">
                                <label for="" class="d-block mb-0 font-weight-bold">{{ $payroll->title }}</label>
                                <small>End Date: {{ \Carbon\Carbon::parse($payroll->end_date)->format('d-m-Y') }}</small>
                            </td>
                            <td class="align-middle" hidden>{{ $payroll->end_date }}</td>
                            <td class="align-middle">
                                {{ $payroll->CreatedBy->name}}
                            </td>
                            <td class="align-middle ">{{ number_format($payroll->getNetPayout(), 2) }}</td>
                            <td class="align-middle">
                                <a href="/report/payroll_totals/{{ $payroll->id }}" class="text-decoration-none" data-toggle="tooltip" data-placement="bottom" title="Download Payroll Totals">
                                    <button class="btn btn-primary d-flex flex-row" > 
                                        <i class="fa fa-cloud-download my-auto d-md-none d-block p-1"></i> 
                                        <span class="my-auto d-none d-md-block">Download</span>
                                    </button>
                                </a>
                            </td>
                            <td class="align-middle">
                                <a href="/report/activity/{{ $payroll->id }}" class="text-decoration-none" data-toggle="tooltip" data-placement="bottom" title="Download Activity">
                                    <button class="btn btn-primary d-flex flex-row" > 
                                        <i class="fa fa-cloud-download my-auto d-md-none d-block p-1"></i> 
                                        <span class="my-auto d-none d-md-block">Download</span>
                                    </button>
                                </a>
                            </td>
                            <td class="align-middle">
                                <a href="/report/tax/{{ $payroll->id }}" class="text-decoration-none" data-toggle="tooltip" data-placement="bottom" title="Download TAP/SCP Tax">
                                    <button class="btn btn-primary d-flex flex-row" > 
                                        <i class="fa fa-cloud-download my-auto d-md-none d-block p-1"></i> 
                                        <span class="my-auto d-none d-md-block">Download</span>
                                    </button>
                                </a>
                            </td>
                            <td class="align-middle">
                                <a href="/report/bank/{{ $payroll->id }}" class="text-decoration-none" data-toggle="tooltip" data-placement="bottom" title="Download Bank">
                                    <button class="btn btn-primary d-flex flex-row" > 
                                        <i class="fa fa-cloud-download my-auto d-md-none d-block p-1"></i> 
                                        <span class="my-auto d-none d-md-block">Download</span>
                                    </button>
                                </a>
                            </td>
                        </tr>                            
                        @endforeach   
            
                    </tbody>
                </table>
            </div>
    
        </div>
    </div>

    <script type="text/javascript" src="{{ URL::asset('js/bizsearch.js') }}"></script>

</x-layout>
    