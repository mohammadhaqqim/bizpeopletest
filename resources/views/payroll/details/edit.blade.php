<x-layout>

    @include('components.payroll_header') 
    
    <div class="min-content mt-0 ">
    
        <div class="main px-3 ">

            <div class="row border-bottom my-3 pb-3 biz-title">
                <i class="fa fa-calendar my-auto px-2"></i> 
                <h2 class="mb-0">Payroll Details</h2>
            </div>
            
<!-- display the payroll period information -->
            @include('components.payroll_details')

            @if(count($errors) > 0)
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                        {{ $error }}<br>
                @endforeach
            </div>
        @endif

<!-- add employee to list -->
@if( $payroll->approved_by == null )
            <form method="POST" action="/payroll/{{ $payroll->id }}/employee">
{{--             <form method="POST" action="/add/payroll/{{ $payroll->id }}"> --}}
                @csrf     
                @method('PATCH')           

                <div class="row pt-3">
                    <div class="col-md-6 col-8 px-0">
                        <label for="employee_id" class="mb-0">Employee</label>
                        <select name="employee_id" id="employee_id" class="custom-select emp_id" onchange="checkSelection(this)" onfocus="this.setSelectionRange(0, this.value.length)">
                            <option value="0" selected>Search ...</option>

                        @foreach ($employees as $employee)
                        @if($employee->getPayrollEmployees->count() == 0)
                            <option value="{{ $employee->id }}" >{{ $employee->name }} {{ is_null($employee->employee_code) ? '' : '('.$employee->employee_code.')' }}  </option>
                        @elseif($employee->getPayrollEmployees->count() > 0)
                            @if(!$employee->getPayrollEmployees[0]->approved )
                            <option value="{{ $employee->id }}" >{{ $employee->name }} {{ is_null($employee->employee_code) ? '' : '('.$employee->employee_code.')' }}  </option>
                            @endif
                        @endif
                        @endforeach

                        </select>
                    </div>                
                    <div class="col-md-3 col-4 text-bottom mt-auto pb-1">
                        <button id="addSubmit" type="submit" class="btn btn-sm btn-primary include-emp" onclick="return confirm('Are you sure you want perform the update?')" disabled>Add</button>
                    </div>
                </div>
            </form>
            
            <small>If you have forgotten someone you can add a staff member or reload a staff members details</small>

@endif

    
            <div class="row pt-1 my-3">
                <div class="col-6 pl-0">
                    <h4 class="my-1">Payroll Summary - {{ $payroll->title }}</h4>
                </div>
                <div class="col-6 px-0">
                    <input class="form-control form-control-sm col-6 float-right" id="bizSearch" type="text" placeholder="Search..">
                </div>
            </div>            

            <div class="row">
                <table id="payrollTable" class="table table-sm border mb-1" >
                    <thead class="tbl-dark">
                        <tr>
                            <th class="align-middle">#</th>
                            <th class="align-middle" hidden>Name</th>
                            <th class="align-middle ">Employee No.
                                <small class="d-block">Department</small>
                            </th>
                            <th class="align-middle">Employee Name
                                <small class="d-block">Designation</small>
                            </th>
                            <th class="align-middle" hidden>Base</th>
                            <th class="align-middle text-right">Base Wage
                                <small class="d-block">$B</small> 
                            </th>
                            <th class="align-middle text-right" >Unpaid Leave
                                <small class="d-block">$B</small> 
                            </th>
                            <th class="align-middle text-right">Addition
                                <small class="d-block">$B</small> 
                            </th>
                            <th class="align-middle text-right">Deduction
                                <small class="d-block">$B</small> 
                            </th>
                            <th class="align-middle text-right">Gross
                                <small class="d-block">$B</small> 
                            </th>

                            <th class="align-middle text-right" {{ $company->use_tap ? '' : 'hidden' }}>TAP
                                <small class="d-block">$B</small> 
                            </th>
                            <th class="align-middle text-right" {{ $company->use_scp ? '' : 'hidden' }}>SCP
                                <small class="d-block">$B</small> 
                            </th>
                            <th class="align-middle text-right" {{ $company->use_epf ? '' : 'hidden' }}>EFP
                                <small class="d-block">$B</small> 
                            </th>
                            <th class="align-middle text-right">Net Pay
                                <small class="d-block">$B</small> 
                            </th>
                            <th class="align-middle text-right">Remove</th>
                            <th hidden>bank</th>
                            <th hidden>account</th>
                            <th hidden>method</th>
                            <th hidden>working days</th>
                            <th hidden>total days</th>
                        </tr>
                    </thead>
                    <tbody id="searchTable" style="font-size: 12px;">
                    @foreach($employeePayrolls as $emp_payroll)
                    <tr style="">
                        <td class="align-middle">{{ $emp_payroll->id }}</td>
                        <td hidden>{{ $emp_payroll->Employee->name }}</td>
                        <td class="align-middle">
                            {{ $emp_payroll->Employee->employee_code }}
                            <small class="d-block">{{ $emp_payroll->Employee->getDepartment()->title }}</small>
                        </td>
                        <td class="align-middle" style="">  
                            @if ( !$emp_payroll->approved )
                            <a href="" onclick="editPayrollTrans({{ $emp_payroll->id }}); return false;" style="font-size: 13px;">{{ $emp_payroll->Employee->name }}</a>
                            <small class="d-block">{{ $emp_payroll->Employee->getJob()->title }}</small>
                            @else 
                            <a style="font-size: 13px;">{{ $emp_payroll->Employee->name }}</a>
                            <small class="d-block">{{ $emp_payroll->Employee->getJob()->title }}</small>
                            @endif
                        </td>
                        <td class="align-right" hidden>{{ $emp_payroll->base_salary }}</td>
                        <td class="align-middle text-right" > 
                            {{ number_format($emp_payroll->base_salary, 2) }}
                        </td>
                        <td class="align-middle text-right">{{ $emp_payroll->unpaid_amount == 0 ? '' : number_format($emp_payroll->unpaid_amount, 2) }}</td>
                        <td class="align-middle text-right">{{ number_format($emp_payroll->addition_amount, 2) }}</td>
                        <td class="align-middle text-right">{{ number_format($emp_payroll->deduction_amount, 2) }}</td>
                        <td class="align-middle text-right">
                            {{ number_format($emp_payroll->base_salary-$emp_payroll->unpaid_amount + $emp_payroll->addition_amount - $emp_payroll->deduction_amount, 2) }}
                        </td>

                        <td class="align-middle text-right" {{ $company->use_tap ? '' : 'hidden' }}>
                            {{ number_format($emp_payroll->tap_ee, 2)  }}
                        </td>
                        <td class="align-middle text-right" {{ $company->use_scp ? '' : 'hidden' }}>{{ number_format($emp_payroll->scp_ee, 2) }}</td>
                        <td class="align-middle text-right" {{ $company->use_epf ? '' : 'hidden' }}>{{ number_format($emp_payroll->epf_ee, 2) }}</td>
                        <td class="align-middle text-right">
                            {{ number_format($emp_payroll->base_salary-$emp_payroll->unpaid_amount + $emp_payroll->addition_amount - $emp_payroll->deduction_amount 
                                - $emp_payroll->tap_ee - $emp_payroll->scp_ee - $emp_payroll->epf_ee, 2) }} 
                        </td>
                        <td class="align-middle text-center" style="font-size: 16px;">
                            @if ( !$emp_payroll->approved )
                            <a href="" onclick="deleteTransaction({{ $emp_payroll->id }}); return false;">
                                <i class="fa fa-trash my-auto px-2 text-danger" ></i>
                            </a>
                            @endif
                        </td>
                        <td hidden>{{ $emp_payroll->bank_id }}</td>
                        <td hidden>{{ $emp_payroll->bank_account }}</td>
                        <td hidden>{{ $emp_payroll->payment_mode_id }}</td>
                        <td hidden>{{ $emp_payroll->work_days }}</td>
                        <td hidden>{{ $emp_payroll->PaymentType == null ? '' : $emp_payroll->PaymentType->periods }}</td>
                        <td hidden>{{ $emp_payroll->units }}</td>
                        <td hidden>{{ $emp_payroll->old_salary }}</td>
                        <td hidden>{{ $emp_payroll->total_days }}</td>
                    </tr>
                    @endforeach

                    <tr class="" style="background-color: #F3F3F3;">
                        <td></td>
                        <td></td>
                        <td class="font-weight-bold text-right">Totals</td>
                        <td class="font-weight-bold text-right">{{ number_format($employeePayrolls->sum('base_salary'), 2) }}</td>
                        <td class="font-weight-bold text-right">{{ number_format($employeePayrolls->sum('unpaid_amount'), 2) }}</td>
                        <td class="font-weight-bold text-right">{{ number_format($employeePayrolls->sum('addition_amount'), 2) }}</td>
                        <td class="font-weight-bold text-right">{{ number_format($employeePayrolls->sum('deduction_amount'), 2) }}</td>
                        <td class="font-weight-bold text-right">
                            {{ number_format($employeePayrolls->sum('base_salary') - $employeePayrolls->sum('unpaid_amount') + $employeePayrolls->sum('addition_amount') 
                            - $employeePayrolls->sum('deduction_amount'), 2) }} 
                        </td>
                        <td class="font-weight-bold text-right" {{ $company->use_tap ? '' : 'hidden' }}>{{ number_format($employeePayrolls->sum('tap_ee'), 2) }}</td>
                        <td class="font-weight-bold text-right" {{ $company->use_scp ? '' : 'hidden' }}>{{ number_format($employeePayrolls->sum('scp_ee'), 2) }}</td>
                        <td class="font-weight-bold text-right" {{ $company->use_epf ? '' : 'hidden' }}>{{ number_format($employeePayrolls->sum('epf_ee'), 2) }}</td>
                        <td class="font-weight-bold text-right">
                            {{ number_format($employeePayrolls->sum('base_salary') + $employeePayrolls->sum('addition_amount') 
                            - $employeePayrolls->sum('unpaid_amount') 
                            - $employeePayrolls->sum('deduction_amount') - $employeePayrolls->sum('tap_ee') 
                            - $employeePayrolls->sum('scp_ee') - $employeePayrolls->sum('epf_ee'), 2) }} 
                        </td>
                        <td></td>
                    </tr> 

                    </tbody>
                </table>
            </div>

        </div>
    </div>
    
    <div class="modal" tabindex="-1"  id="editModal">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <form id="editPayrollTrans" method="POST" action="">
            @csrf                        
            @method('PATCH')
    
            <div class="modal-header">
              <i class="fa fa-money"></i>
              <h5 class="modal-title">Update Payroll Entry.</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col">
                        <label for="name">Name</label>
                        <input type="text" class="form-control emp_name" id="name" name="name" disabled>
                    </div>    
                </div>
                <div class="row">            
                    <div class="form-group col">
                        <label for="department">Department</label>
                        <input type="text" class="form-control emp_department" id="department" name="department" disabled>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-6">
                        <label for="payment_mode_id">Payment Type</label>
                        <div class="input-group emp_payment_mode">
                            <select id="payment_mode_id" name="payment_mode_id" class="custom-select payment_method" onchange="checkPaymentType()">
                            @foreach ($paymentmodes as $mode)
                                <option value="{{ $mode->id }}" {{ $mode->id == $emp_payroll->payment_mode_id ? 'selected' :'' }}>{{ $mode->title }}</option>
                            @endforeach
                            </select>
                        </div>    
                    </div>                                    
                    <div class="form-group col-6">
                        <label for="pay">Base Wage</label>
                        <input type="number" class="form-control emp_base_salary" id="pay" name="pay" readonly>                        
                    </div>

                </div>

                <div class="row" >
                    <div class="form-group col-6" id="bankinstitute">
                        <label for="bank_id">Bank</label>
                        <div class="input-group emp_payment_mode">
                            <select id="bank_id" name="bank_id" class="custom-select to_bank">
                            @foreach ($banks as $bank)
                                <option value="{{ $bank->id }}" >{{ $bank->title }}</option>
                            @endforeach
                            </select>
                        </div>    
                    </div>                                    
                    <div class="form-group col-6" id="bankaccount">
                        <label for="bank_account">Account Number</label>
                        <input type="text" class="form-control emp_base_salary to_account" id="bank_account" name="bank_account" >                        
                    </div>
                </div>
                <div class="row" >
                    <div class="form-group col-6" id="days_worked">
                        <label for="work_days">Work Days</label>
                        <input type="number" class="form-control emp_work_days to_account" id="work_days" name="work_days" step="0.5">                        
                    </div>
                    <div class="form-group col-6" id="total_worked">
                        <label for="total_days">Total Days</label>
                        <input type="number" class="form-control emp_total_days to_account" id="total_days" name="total_days" step="0.5" disabled>                        
                    </div>
                    <div class="form-group col-6" id="workday_rate">
                        <label for="old_salary">Rate</label>
                        <input type="number" class="form-control emp_old_salary to_account" id="old_salary" name="old_salary" step="0.25" disabled>                        
                    </div>
                    <div class="form-group col-6" id="workday_units">
                        <label for="units">Units</label>
                        <input type="number" class="form-control emp_units to_account" id="units" name="units" step="0.25" onchange="calcBase()">                        
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
            </form>
          </div>
        </div>
    </div>
    

    <div class="modal" tabindex="-1"  id="deleteModal">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <form id="deletePayrollTrans" method="POST" action="/payrollemployee/">
            @method('DELETE')
            @csrf                        

            <div class="modal-header">
              <i class="fa fa-file-text-o pt-1"></i>
              <h5 class="modal-title pl-2">Delete Employee Payroll Entry</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="form-group col text-center">
                        <h5>Are you sure you would like to delete the following employee payroll entry?</h5>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col">
                        <label for="name">Name</label>
                        <input type="text" class="form-control emp_name_delete" id="name" name="name" disabled>
                    </div>    
                </div>
                <div class="row">            
                    <div class="form-group col">
                        <label for="department">Department</label>
                        <input type="text" class="form-control emp_department_delete" id="department" name="department" disabled>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-6">
                        <label for="base_salary">Base Wage</label>
                        <input type="text" class="form-control emp_base_salary_delete" id="base_salary" name="base_salary" disabled>                        
                    </div>
                </div>
                    
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="submit" class="btn btn-danger">Yes</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
            </div>
            </form>
          </div>
        </div>
      </div>

      <script type="text/javascript" src="{{ URL::asset('js/bizsearch.js') }}"></script>
{{--       <script type="text/javascript" src="{{ URL::asset('js/bizpayroll.js') }}"></script> --}}
      
<script>
$(document).ready(function(){
  $('#employee_name').editableSelect();
});

function deleteTransaction(id){
    document.getElementById("deletePayrollTrans").action = "/payrollemployee/" + id;
    
    var oTable = document.getElementById("payrollTable");
    for (i = 1; i < oTable.rows.length; i++){
        var oCells = oTable.rows.item(i).cells;

        if (oCells.item(0).innerHTML == id){
            document.getElementsByClassName("emp_name_delete")[0].value = oCells.item(1).innerHTML;
            document.getElementsByClassName("emp_department_delete")[0].value = oCells.item(3).innerHTML; 
            document.getElementsByClassName("emp_base_salary_delete")[0].value = Number(oCells.item(4).innerHTML).toFixed(2); 
           // document.getElementsByClassName("emp_work_days")[0].value = oCells.item(19.innerHTML; 

            break;
        }
    } 

    $("#deleteModal").modal('show');
}

function calcBase(){
    $salary = document.getElementById("units").value * document.getElementById("old_salary").value;
    document.getElementById("pay").value = $salary;
}

function editPayrollTrans(id){    
    document.getElementById("editPayrollTrans").action = "/payrollemployee/" + id;
    $("#editModal").modal('show');

    var oTable = document.getElementById("payrollTable");
    for (i = 1; i < oTable.rows.length; i++){
        var oCells = oTable.rows.item(i).cells;

        if (oCells.item(0).innerHTML == id){
            //alert(oCells.item(18).innerHTML);
            document.getElementsByClassName("emp_name")[0].value = oCells.item(1).innerHTML;
            document.getElementsByClassName("emp_department")[0].value = oCells.item(4).innerHTML; 
            document.getElementsByClassName("emp_base_salary")[0].value = oCells.item(5).innerHTML;                        
            document.getElementsByClassName("payment_method")[0].value = oCells.item(17).innerHTML;
            document.getElementsByClassName("emp_work_days")[0].value = oCells.item(18).innerHTML; 
            document.getElementsByClassName("emp_units")[0].value = '0'; 
            document.getElementsByClassName("emp_old_salary")[0].value =  oCells.item(21).innerHTML;
//            alert(oCells.item(22).innerHTML);
            document.getElementsByClassName("emp_total_days")[0].value =  oCells.item(22).innerHTML;

//            document.getElementsByClassName("emp_units")[0].style.display = 'none';
            document.getElementById('workday_units').style.display = 'none';
            document.getElementById('workday_rate').style.display = 'none';
            document.getElementById('days_worked').style.display = 'block';
            document.getElementById('total_worked').style.display = 'block';
            
            if( oCells.item(19).innerHTML == '0'){
                document.getElementById('days_worked').style.display = 'none';
                document.getElementById('total_worked').style.display = 'none';
                document.getElementById('workday_rate').style.display = 'block';

                //alert('zero '+oCells.item(20).innerHTML);
                document.getElementById('workday_units').style.display = 'block';
                document.getElementsByClassName("emp_units")[0].value = oCells.item(20).innerHTML; 
            }

            //document.getElementsByClassName("to_bank")[0].value = oCells.item(14).innerHTML;
            //document.getElementsByClassName("to_account")[0].value = oCells.item(15).innerHTML;
            break;
        }
    } 
}

function checkSelection(a){    
    //find the item in the list
    var name = a.value; 

    if ('Search ...' == name){
        document.getElementsByClassName('include-emp')[0].disabled = true;
        return;
    }

    document.getElementsByClassName('include-emp')[0].disabled = false;

    //find item in table
    var oTable = document.getElementById("payrollTable");

    document.getElementsByClassName('include-emp')[0].innerHTML = 'Add';

    for (i = 1; i < oTable.rows.length; i++){
        var oCells = oTable.rows.item(i).cells;

        if (oCells.item(1).innerHTML == name){
            document.getElementsByClassName('include-emp')[0].innerHTML = 'Reset';
            return;
        }
    }

}

function checkPaymentType()
{
    var element = document.getElementById("payment_mode_id");
    var mode = element.value;

    var str = element.options[element.selectedIndex].text;

    if ( str.includes("Bank") || str.includes("bank") ){
        document.getElementById("bankinstitute").style.display = "block";
        document.getElementById("bankaccount").style.display = "block";
    }
    else{
        document.getElementById("bankinstitute").style.display = "none";
        document.getElementById("bankaccount").style.display = "none";
    } 
}

function checkAll(bx) {
  var cbs = document.getElementsByTagName('input');
  for(var i=0; i < cbs.length; i++) {
    if(cbs[i].type == 'checkbox') {
      cbs[i].checked = bx.checked;
    }
  }
}
 
</script>
</x-layout>
    