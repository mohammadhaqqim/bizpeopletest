<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PDF View</title>
    <style>
        html, body {
            height: 100vh;
            margin: 0;
        }
    </style>
</head>
<body >

    <object data="{{ Storage::get('9-10-2020.pdf'); }}" type='application/pdf' width="100%" height="100%">
    
        Example fallback content: This browser does not support PDFs. Please download the PDF to view it: Download PDF.
        
    </object>
 
    <object data="/images/payslips/9-10-2020.pdf" type='application/pdf' width="100%" height="100%">
    
        Example fallback content: This browser does not support PDFs. Please download the PDF to view it: Download PDF.
        
    </object>

</body>
</html>