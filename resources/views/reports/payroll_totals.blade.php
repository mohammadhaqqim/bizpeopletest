@php($reportName = 'Payroll Totals Report')
@php($companyName = '$company->company_name')
<style>
    .row{margin-left:-15px;margin-right:-15px}
    .col-xs-12{float:left}
    .col-xs-12{width:100%}
    .col-xs-12{position:relative;min-height:1px;padding-left:15px;padding-right:15px}
    .text-left{text-align:left}
    .pull-right{float:right!important}
</style>
<style>
    @page { 
        margin-top: 0.5cm;
        margin-bottom: 0.5cm;
    }

    .sold {
        border-style: solid;
        border-width: 1px;
    }

    .dott {
        border-style: dotted;
        border-width: 1px;
    }

    tr{
        height: 20px;
    }

    td{
        padding-left: 5px;
    }

    table {
        border-collapse: collapse;
        border-spacing: 0;
    }

    .rotate {
        position: absolute;
        transform: rotate(-90.0deg); 
    }

    .e2{
        top: 725px;
        left: -30px;
    }

    .e1{
        top: 210px;
        left: -30px;   
    }

    .d1{
        top: 319px;
        left: -39px;
    }

    .a1{
        top: 410px;
        left: -33px;
    }
    
    .d2{
        top: 837px;
        left: -39px;
    }
    
    
    .a2{
        top: 927px;
        left: -32px;
    }
    
    .rotated1 {
        position: absolute;
        top: 180px;
        left: 0px;
        transform: rotate(-90.0deg); 
    }    

</style>


<div class="row body" >
    <div class="col">
        <strong> {{ $companyName }} </strong>
    </div>
    <div class="col">
        <strong> {{ $reportName }} </strong>
    </div>
    <div class="col">
        <strong> Report Period </strong>
    </div>

    <div class="row" style="font-size: 14px;">
        <table class="col-xs-12" style="">
            <thead>
                <tr style="border: 1px solid grey;">
                    <td style="width: 20%;">No </td>
                    <td style="width: 2%;">Name</td>
                    <td class="text-left" >                        
                        Basic Salary
                        B$
                    </td>
                    <td style="width: 3%;">
                        Unpaid Leave
                        B$
                    </td>    
                    <td style="width: 3%;">
                        Overtime
                        B$
                    </td>    
                    <td style="width: 3%;">
                        Bonus
                        B$
                    </td>    
                    <td style="width: 3%;">
                        Allowance                        
                        B$
                    </td>    
                    <td style="width: 3%;">
                        Deduction                        
                        B$
                    </td>    
                    <td style="width: 3%;">
                        Gross Salary
                        B$
                    </td>    
                    <td style="width: 3%;">
                        TAP
                        ER / EE
                        B$
                    </td>    

                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
</div>