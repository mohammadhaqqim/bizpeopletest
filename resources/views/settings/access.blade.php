<x-layout>

    <style>
    
    </style>
    
    @include('components.settings_aside')
    
    <div class="main pr-3 ">
        <div class="row border-bottom my-3 pb-3 biz-title">
            <i class="fa fa-briefcase my-auto px-2"></i> 
            <h2 class="mb-0">User Access Levels</h2>
        </div>

        @if(count($errors) > 0)
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                        {{ $error }}<br>
                @endforeach
            </div>
        @endif

        <div class="row mt-2">
            
            <div class="col-6 pl-0">
                <h4>Current List of Users</h4>
            </div>
            <div class="col-6 text-right text-biz-blue">
                <a href="" data-toggle="modal" data-target="#addUserModal">
                    <i class="fa fa-plus-circle my-auto px-2" style="font-size: 18px;" data-toggle="modal" data-target="#statusModal"></i> 
                    <label class="text-right">Add New User</label>
                </a>
            </div>

            <div class="col-12 mb-3 px-0">
                <input class="form-control" id="myInput" type="text" placeholder="Search..">
            </div>
                                
            <table id="tblUsers" class="table border mb-4">
                <thead  class="tbl-dark">
                <tr>
                    <th hidden>#</th>
                    <th  style="width: 30%;">User Name</th>
                    <th hidden>Name</th>
                    <th hidden>email</th>
                    <th hidden>employee id</th>
                    <th  style="">Employee</th>
                    <th scope="" width="75px">Payroll</th>
                    <th scope="" width="75px">HR</th>
                    <th scope="" width="75px">HoD</th>
                    <th scope="" width="75px">Manager</th>
                    <th scope="" width="75px">Admin</th>
                    <th scope="" style="width: 75px;">Block</th>
                    <th scope="" style="width: 75px;">Edit</th>
                </tr>
                </thead>
                <tbody id="myTable">
                    @foreach ( $users as $user)                        
                    <tr>
                        <th hidden>{{ $user->id }}</th>
                        <th>
                            <h4 class="mb-0">
                                {{ $user->name }}
                            </h4>
                            <label class="mb-0 text-secondary" style="font-size: 12px;">
                                {{ $user->email }}
                            </label>
                        </th>
                        <th hidden>{{ $user->name }}</th>
                        <th hidden>{{ $user->email }}</th>
                        <th hidden>{{ isset($user->Employee) ? $user->Employee->id : '' }}</th>
                        <th class="align-middle">{{ isset($user->Employee) ? $user->Employee->name : '' }}</th>
                        <th class="align-middle">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="payroll{{ $user->id }}" {{ $user->payroll ? 'checked' : ''}}>
                                <label class="custom-control-label" for="payroll{{ $user->id }}"></label>
                            </div>                        
                        </th>
                        <th class="align-middle">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="hr{{ $user->id }}" {{ $user->hr ? 'checked' : ''}}>
                                <label class="custom-control-label" for="hr{{ $user->id }}"></label>
                            </div>                        
                        </th>
                        <th class="align-middle">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="hod{{ $user->id }}" {{ $user->hod ? 'checked' : ''}}>
                                <label class="custom-control-label" for="hod{{ $user->id }}"></label>
                            </div>                        
                        </th>
                        <th class="align-middle">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="manager{{ $user->id }}" {{ $user->manager ? 'checked' : ''}}>
                                <label class="custom-control-label" for="manager{{ $user->id }}"></label>
                            </div>                        
                        </th>
                        <th class="align-middle">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="admin{{ $user->id }}" {{ $user->admin ? 'checked' : ''}}>
                                <label class="custom-control-label" for="admin{{ $user->id }}"></label>
                            </div>                        
                        </th>
                        <th class="align-middle">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="customSwitch{{ $user->id }}" {{ $user->block ? 'checked' : ''}}>
                                <label class="custom-control-label" for="customSwitch{{ $user->id }}"></label>
                            </div>
                        </th>
                        <th class="align-middle">
                            <a href="#" onclick="userEdit({{ $user->id }}); return false;" data-toggle="tooltip" data-placement="bottom" title="Edit User">
                                <i class="fa fa-pencil my-auto px-2 text-primary" style="font-size: 20px;"></i>
                            </a>
                        </th>
                    </tr>
                    @endforeach

                </tbody>
            </table>
        </div>

        <script>
            $(document).ready(function(){
                $("#myInput").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            });


            function userEdit(id){
                document.getElementById("editUser").action = "/user/" + id;

                var oTable = document.getElementById("tblUsers");
                for (i = 1; i < oTable.rows.length; i++){
                    var oCells = oTable.rows.item(i).cells;

                    if (oCells.item(0).innerHTML == id){
                        document.getElementsByClassName("editName")[0].value = oCells.item(2).innerHTML;
                        document.getElementsByClassName("editEmail")[0].value = oCells.item(3).innerHTML; 
                        document.getElementsByClassName("editPayroll")[0].checked = document.getElementById("payroll"+id).checked; 
                        document.getElementsByClassName("editHR")[0].checked = document.getElementById("hr"+id).checked; 
                        document.getElementsByClassName("editHOD")[0].checked = document.getElementById("hod"+id).checked; 
                        document.getElementsByClassName("editManager")[0].checked = document.getElementById("manager"+id).checked; 
                        document.getElementsByClassName("editAdmin")[0].checked = document.getElementById("admin"+id).checked; 
                        document.getElementsByClassName("editBlock")[0].checked = document.getElementById("customSwitch"+id).checked;
                        document.getElementsByClassName("emp_id")[0].value =  oCells.item(4).innerHTML;;
                    }
                }

                $("#editUserModal").modal('show');
            }    

        </script>
    
        <div class="modal" id="editUserModal">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header">
                <h5 class="modal-title">Edit User Access</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>

            <form id="editUser" method="POST" action="/user/">
                @method('PATCH')
                @csrf    

                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-12">
                            <label for="name" class="mb-0">Name</label>
                            <input type="text" name="name" id="name" class="form-control editName" >
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input editBlock" id="block" name="block">
                                <label class="custom-control-label" for="block">Block Access</label>
                            </div>
                        </div>

                        <div class="form-group col-12">
                            <label for="email" class="mb-0">Email</label>
                            <input type="email" class="form-control editEmail" id="email" name="email" aria-describedby="email" placeholder="Enter email">
                        </div>

    {{--                     <div class="form-group col-12">
                            <label for="email" class="mb-0">Password</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                        </div> --}}

                        <div class="form-group col">
                            <label for="employee_id" class="mb-0">Linked Employee</label>
                            <select name="employee_id" id="employee_id" class="custom-select emp_id" >
                                <option value="0" selected>Choose ...</option>
                                @foreach ($employees as $employee)
                                    <option value="{{ $employee->id }}" >{{ $employee->name }}</option>                            
                                @endforeach
                            </select>
                        </div>
                        

                    </div>

                    <div class="row ml-3">
                        <label for="security" class="mb-0">User Access</label>
                    </div>

                    <div class="row ml-3">
                        <div class="custom-control custom-checkbox col-4">
                            <input type="checkbox" class="custom-control-input editPayroll" id="payroll" name="payroll">
                            <label class="custom-control-label" for="payroll">Payroll</label>
                        </div>
                        <div class="custom-control custom-checkbox col-4">
                            <input type="checkbox" class="custom-control-input editHR" id="hr" name="hr">
                            <label class="custom-control-label" for="hr">HR</label>
                        </div>
                        <div class="custom-control custom-checkbox col-4">
                            <input type="checkbox" class="custom-control-input editHOD" id="hod" name="hod">
                            <label class="custom-control-label" for="hod">Department Head</label>
                        </div>

                        <div class="custom-control custom-checkbox col-4">
                            <input type="checkbox" class="custom-control-input editManager" id="manager" name="manager">
                            <label class="custom-control-label" for="manager">Manager</label>
                        </div>
                        <div class="custom-control custom-checkbox col-4">
                            <input type="checkbox" class="custom-control-input editAdmin" id="admin" name="admin">
                            <label class="custom-control-label" for="admin">Admin</label>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
            </div>
        </div>

        <div class="modal" id="addUserModal">
            <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <div class="modal-header">
                <h5 class="modal-title">Add New User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>

                <form method="POST" action="/user/">
                @csrf    
        
                <div class="modal-body">
                
                    <div class="row">
                        <div class="form-group col-12">
                            <label for="name" class="mb-0">Name</label>
                            <input type="text" name="name" id="name" class="form-control editName" >
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input editBlock" id="block" >
                                <label class="custom-control-label" for="block">Block Access</label>
                            </div>
                        </div>

                        <div class="form-group col-12">
                            <label for="email" class="mb-0">Email</label>
                            <input type="email" class="form-control editEmail" id="email" name="email" aria-describedby="email" placeholder="Enter email">
                        </div>

                        <div class="form-group col">
                            <label for="employee_id" class="mb-0">Linked Employee</label>
                            <select name="employee_id" id="employee_id" class="custom-select" required>
                                <option value="0" selected>Choose ...</option>
                                @foreach ($employees as $employee)
                                    <option value="{{ $employee->id }}" >{{ $employee->name }}</option>                            
                                @endforeach

                            </select>
                        </div>                    

                    </div>

                    <div class="row ml-3">
                        <label for="security" class="mb-0">User Access</label>
                    </div>

                    <div class="row ml-3">
                        <div class="custom-control custom-checkbox col-4">
                            <input type="checkbox" class="custom-control-input editPayroll" id="payroll_0" name="payroll_0">
                            <label class="custom-control-label" for="payroll_0">Payroll</label>
                        </div>
                        <div class="custom-control custom-checkbox col-4">
                            <input type="checkbox" class="custom-control-input editHR" id="hr_0" name="hr_0">
                            <label class="custom-control-label" for="hr_0">HR</label>
                        </div>
                        <div class="custom-control custom-checkbox col-4">
                            <input type="checkbox" class="custom-control-input editHOD" id="hod_0" name="hod_0">
                            <label class="custom-control-label" for="hod_0">Department Head</label>
                        </div>
                    </div>

                    <div class="row ml-3">
                        <div class="custom-control custom-checkbox col-4">
                            <input type="checkbox" class="custom-control-input editManager" id="manager_0" name="manager_0">
                            <label class="custom-control-label" for="manager_0">Manager</label>
                        </div>
                        <div class="custom-control custom-checkbox col-4">
                            <input type="checkbox" class="custom-control-input editAdmin" id="admin_0" name="admin_0">
                            <label class="custom-control-label" for="admin_0">Admin</label>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
            </div>
        </div>


    </div>
    
    
    </x-layout>