<x-layout>

    <style>
        .tbl-biz{
            background-color: rgb(230, 230, 230);
            color: black;
            border-radius: 15px !important; 
        }
    </style>
    
    @include('components.settings_aside')
    
    <div class="main pr-3 ">
        <div class="row border-bottom my-3 pb-3 biz-title">
            <i class="fa fa-briefcase my-auto px-2"></i> 
            <h2 class="mb-0">Leave Policies</h2>
        </div>

        <a href="/settings/timeoff/policy">
            <button type="button" class="btn btn-outline-primary" ><i class="fa fa-plus-circle text-prinary" ></i> New Policy</button>
        </a>

        <button type="button" class="btn btn-outline-primary float-right" onclick="AddLeaveType(); return false;"><i class="fa fa-plus-circle text-primary" ></i> New Time Off Type</button>

        <div class="row my-4">
            <table id="tblLeaveType" class="table border">
                <thead class="tbl-biz text-weight-bolder" hidden>
                    <tr>
                        <th scope="col" hidden>#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Actions</th>
                        <th hidden>Time Off</th>
                        <th hidden>Time</th>
                        <th hidden>Active</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($leavetypes as $leave)
                    <tr class="tbl-biz text-weight-bolder">
                        <td hidden>{{ $leave->id }}</td>
                        <td>{{ $leave->title }}</td>
                        <td class="text-right ">
                            <i class="fa fa-pencil text-primary px-1" data-toggle="tooltip" data-placement="bottom" 
                                title="Edit" onclick="EditLeavePolicy({{ $leave->id }}); return false;"></i>
                            <div class="custom-control custom-switch d-inline" data-toggle="tooltip" data-placement="bottom" title="Enable / Disable">
                                <input type="checkbox" class="custom-control-input" id="customSwitch{{ $leave->id }}" {{ $leave->active ? 'checked' : '' }} disabled>
                                <label class="custom-control-label" for="customSwitch{{ $leave->id }}" ></label>
                            </div>
                            <a class="fa fa-trash text-danger px-1" data-toggle="tooltip" data-placement="bottom" 
                                onclick="return confirm('Please confirm to delete {{ $leave->title }} ?')" 
                                href="{{ route('leavetype-delete', $leave->id) }}" title="Delete"></a>
                        </td>
                        <td hidden>{{ $leave->paid_time_off }}</td>
                        <td hidden>{{ $leave->track_time }}</td>
                        <td hidden>{{ $leave->active }}</td>
                    </tr>
                    @foreach ($leave->LeavePolicies as $policy)
                    <tr>
                        <td hidden></td>
                        <td>{{ $policy->policy_name }}</td>
                        <td class="text-right pr-4">
                            <a href="{{ route('policy-edit', $policy->id) }}" class="fa fa-pencil text-primary px-1" data-toggle="tooltip" data-placement="bottom" 
                            title="Edit" ></a>
                            <a class="fa fa-trash text-danger px-1" data-toggle="tooltip" data-placement="bottom" 
                                onclick="return confirm('Please confirm to delete {{ $policy->policy_name }} ?')" 
                                href="#" title="Delete"></a>
                        </td>
                        <td hidden>{{ $policy->id }}</td>
                        <td hidden></td>
                        <td hidden></td>
                    </tr>
                    @endforeach
                    <tr>
                        <td hidden></td>
                        <td ></td>
                        <td></td>
                        <td hidden></td>
                        <td hidden></td>
                        <td hidden></td>
                    </tr> 
                    @endforeach
                </tbody>
            </table>
        </div>

    @include('components.modal.add_leave_type')
    @include('components.modal.add_leave_policy')
    @include('components.modal.edit_leave_type')

    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })

        function AddLeaveType()
        {
            $("#leaveTypeAddModal").modal('show');
        }
        
        function AddLeavePolicy()
        {
            $("#leavePolicyAddModal").modal('show');
        }

        function EditLeavePolicy(id)
        {
            //alert(id);

            document.getElementById("editLeave").action = "/leavetypes/" + id;

            var oTable = document.getElementById("tblLeaveType");

            for (i = 1; i < oTable.rows.length; i++){
                var oCells = oTable.rows.item(i).cells;

                if (oCells.item(0).innerHTML == id){
                    document.getElementsByClassName("leaveTitle")[0].value = oCells.item(1).innerHTML;

                    document.getElementsByClassName("leaveActive")[0].checked 
                        = oCells.item(5).innerHTML == '1' ? true : false;
                        
                    document.getElementsByClassName("leavePaidOff")[0].checked 
                        = oCells.item(3).innerHTML == '1' ? true : false;

                    var e = document.getElementsByClassName("leaveTrack");
                    selectOptionVal(e[0], oCells.item(4).innerHTML);                
                }
            }

            $("#leaveTypeEditModal").modal('show');
        }

        function selectOptionVal(obj, value)
        {
            for ( var i = 0; i < obj.options.length; i++ ) {
                if ( obj.options[i].value == value) {
                    obj.options[i].selected = true;
                }
            }                
        }

        function confirmDelete(id){
            document.getElementById("deleteTransaction").action = "/allowances/" + id;

            var oTable = document.getElementById("tblAllowances");
            for (i = 1; i < oTable.rows.length; i++){
                var oCells = oTable.rows.item(i).cells;

                if (oCells.item(0).innerHTML == id){
                    document.getElementById("deleteDate").value = oCells.item(2).innerHTML;
                    document.getElementById("deleteTitle").setAttribute("value", oCells.item(1).innerHTML);
                    document.getElementById("deleteAmount").setAttribute("value", Number(oCells.item(4).innerHTML).toFixed(2));
                    document.getElementById("deleteRemarks").value = oCells.item(7).innerHTML;
                }
            }

            $("#deleteModal").modal('show');
        }

    </script>
    
    </div>
    
    
    </x-layout>