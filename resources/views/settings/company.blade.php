<x-layout logo="some text">

@include('components.settings_aside')

<div class="main px-2 pl-md-0">
    <div class="row border-bottom my-3 pb-3 biz-title">
        <i class="fa fa-briefcase my-auto px-2"></i> 
        <h2 class="mb-0">Company Information</h2>
    </div>

    @if(count($errors) > 0)
        <div class="alert alert-danger">
            @foreach($errors->all() as $error)
                    {{ $error }}<br>
            @endforeach
        </div>
    @endif
    <div class="row border-bottom">
        <div class="col-sm-6 col-md-4">
            <label for="" class="d-block font-weight-bold">Company Name 
                <a href="" class="text-primary" style="font-size: 12px;" data-toggle="modal" data-target="#companyModal">Edit</a>
            </label> 
            <i class="fa fa-building my-auto px-2"></i> 
            <label for="" class="mb-0">
                {{ $company->company_name }}
            </label> 
            <div class="row mb-2">
                <small class="col-6">Reg. No. {{ $company->registration_no }}</small>
                <small class="col-6">Lic. No. {{ $company->license_no }}</small>
            </div>
        </div>
        <div class="col-sm-6 col-md-4">
            <label for="" class="d-block font-weight-bold">Country</label>
            <i class="fa fa-map-marker my-auto px-2"></i> <label for="" class="">{{ $company->Country == null ? '' : $company->Country->country }}</label>
        </div>        
        <div class="col-sm-12 col-md-4">
            <label for="" class="d-block font-weight-bold">Account Owner</label>
            <i class="fa fa-user-o my-auto px-2"></i> <label for="" class="">N/A</label>
        </div>        
    </div>

    <div class="row border-bottom mt-2 pb-2">
        <div class="col-sm-6 col-md-4">
            <label for="" class="d-block font-weight-bold">Company Payroll Period
                <a href="" class="text-primary" style="font-size: 12px;" data-toggle="modal" data-target="#companyModal">Edit</a>
            </label> 
            <i class="fa fa-calendar-o my-auto px-2"></i><label for="" class="mb-0">Payroll period: {{ $company->PayPeriod == null ? '' : $company->PayPeriod->title }}</label>
            <small id="" class="form-text text-muted mt-0 pl-4">
                First period: {{ \Carbon\Carbon::parse($company->first_period_date)->format('j-F') }}
            </small>
            <small id="" class="form-text text-muted mt-0 pl-4">
                Periods per year: {{ $company->PayPeriod == null ? '' : $company->PayPeriod->periods_per_year }} 
            </small>
        </div>
        <div class="col">
            <label for="" class="d-block font-weight-bold">Company Address   
                <a href="" class="text-primary" style="font-size: 12px;" data-toggle="modal" data-target="#companyModal">Edit</a>
            </label>
            <label for="" class="d-block mb-0">{{ $company->address1 }}</label>
            <label for="" class="d-block mb-0">{{ $company->address2 }}</label>
            <label for="" class="d-block mb-0">{{ $company->city }}, {{ $company->postcode }}</label>
        </div>       
    </div>

<form method="POST" action="/company/logo" enctype="multipart/form-data">
    @csrf
    @method('PATCH')

    <div class="row border-bottom mt-2 pb-2">
        <div class="col-8">
            <div class="">
                <label for="" class="d-block font-weight-bold">Company Logo</label>

                <img id="preview_img" src="/storage/{{ $company->getLogo() }} " 
                    class="d-block border border-primary mb-2 my-3" style="height: 60px;" />
    
                <input type="file" name="logo" id="logo" onchange="loadPreview(this);" accept=".jpg, .jpeg, .png, .gif">

                <small id="" class="form-text text-muted">
                    The size/ratio should be restricted to 280px x 30px (approx)
                </small>

                <button type="submit" class="btn btn-sm btn-info text-white"> <i class="fa fa-cloud-upload"></i> Upload Logo</button>
            </div>
        </div>
    </div>
</form>

    <div class="row mt-2">
        <div class="col">
            <label for="" class="d-block font-weight-bold">Staff Contribution 
{{--                 <a href="" class="text-primary" style="font-size: 12px;" data-toggle="modal" data-target="#companyTax">Edit</a> --}}
            </label>  
            <small id="passwordHelpBlock" class="form-text text-muted">
                The accounts selected will appear in employee cards allowing to enter in account numbers
            </small>
        </div>
    </div>
    <div class="row border-bottom pb-2">

        <div class="col-sm-4 col-lg-3">            
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1" 
                    {{ $company->use_tap ? 'checked' : '' }} disabled>
                <label class="form-check-label" for="defaultCheck1">
                  TAP ( {{ $company->tap_percent }} % )
                </label>
                <a href="" class="text-primary" style="font-size: 12px;" data-toggle="modal" data-target="#editContributiontap">Edit</a>
            </div>
        </div>
        <div class="col-sm-4 col-lg-3">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2" 
                    {{ $company->use_scp ? 'checked' : '' }} disabled>
                <label class="form-check-label" for="defaultCheck2">
                  SCP ( {{ $company->scp_percent }} % )
                </label>
                <a href="" class="text-primary" style="font-size: 12px;" data-toggle="modal" data-target="#editContributionscp">Edit</a>
            </div>
        </div>
        <div class="col-sm-4  col-lg-3">
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck3" 
                {{ $company->use_epf ? 'checked' : '' }} disabled>
                <label class="form-check-label" for="defaultCheck3">
                  EPF ( {{ $company->epf_percent }} % )
                </label>
                <a href="" class="text-primary" style="font-size: 12px;" data-toggle="modal" data-target="#editContributionepf">Edit</a>
            </div>
        </div>
    </div>

    <div class="row mt-2">
        <div class="col">
            <label for="" class="d-block font-weight-bold">Reporting Period
                <a href="" class="text-primary" style="font-size: 12px;" data-toggle="modal" data-target="#reportingPeriodsModal">Edit</a>
                <a href="" class="text-primary" style="font-size: 12px;" data-toggle="modal" data-target="#reportingPeriodModal">Add</a>
            </label>  
            <small id="passwordHelpBlock" class="form-text text-muted">
                
            </small>
        </div>
    </div>

    <div class="row" style="height: 25px;">

        <div class="col-6">
            <div class="form-group">
                <label for="" class="font-weight-bold">Title: </label> 
                <label for="" class="">
                    @if($company->ReportingPeriod)
                        {{ $company->ReportingPeriod->title }}
                    @endif
                </label> 
            </div>
        </div>
    </div>       

    <div class="row border-bottom pb-2">
        <div class="col-sm-4 col-lg-3">            
            <div class="form-group">
                <label for="" class="font-weight-bold">Start: </label> 
                <label for="" class="">
                    @if($company->ReportingPeriod)
                        {{ Carbon\Carbon::parse($company->ReportingPeriod->start_date)->format('d F Y') }}
                    @endif
                </label> 
            </div>

        </div>
        <div class="col-sm-4 col-lg-3">            
            <div class="form-group">
                <label for="" class="font-weight-bold">End: </label> 
                <label for="" class="">
                    @if($company->ReportingPeriod)
                        {{ Carbon\Carbon::parse($company->ReportingPeriod->end_date)->format('d F Y') }}
                    @endif 
                </label> 
            </div>

        </div>

    </div>

     <div class="modal" id="companyModal">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <form method="POST" action="/companies/{{ $company->id }}">
            @csrf
            @method('PATCH')
                
            <div class="modal-header">
                <i class="fa fa-briefcase my-auto px-2"></i> 
                <h5 class="modal-title">Company Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col">
                        <label for="country" class="font-weight-bold">Company Name</label>
                        <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Company Name" value="{{ $company->company_name }}">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-6">
                        <label for="registration_no" class="font-weight-bold">Registration No.</label>
                        <input type="text" class="form-control" id="registration_no" name="registration_no" value="{{ $company->registration_no }}">
                    </div>
                    <div class="form-group col-6">
                        <label for="license_no" class="font-weight-bold">License No.</label>
                        <input type="text" class="form-control" id="license_no" name="license_no" value="{{ $company->license_no }}">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-6">
                        <label for="first_period_date" class="font-weight-bold">Start Financial Year</label>
                        <input type="date" class="form-control" id="first_period_date" name="first_period_date" value="{{ $company->first_period_date }}">
                    </div>
                    <div class="form-group col-6">
                        <label for="periods_per_year" class="font-weight-bold">Periods Per Year</label>
                        <input type="number" class="form-control" id="periods_per_year" name="periods_per_year" value="{{ $company->periods_per_year }}">
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col">
                        <label for="name" class="font-weight-bold">Address</label>
                        <input type="text" class="form-control" id="address1" name="address1" placeholder="Address Line 1" value="{{ $company->address1 }}">
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col">
                        <input type="text" class="form-control" id="address2" name="address2" placeholder="Address Line 2" value="{{ $company->address2 }}">
                    </div>
                </div>      
                <div class="row">
                    <div class="form-group col-6">
                        <input type="text" class="form-control" id="city" name="city" placeholder="City" value="{{ $company->city }}">
                    </div>
                    <div class="form-group col-6">
                        <input type="text" class="form-control" id="postcode" name="postcode" placeholder="Postcode" value="{{ $company->postcode }}">
                    </div>

                </div>
                <div class="row">
                    <div class="form-group col-6">
                        <label for="country" class="font-weight-bold">Country Location</label>
                        <select name="address_country_id" id="address_country_id" class="custom-select"> 
                            <option value="0" class="text-primary" onclick="AddCountry(); return false;">Add a Country</option>

                        @foreach ($countries as $country)
                            @if ( $company->address_country_id == $country->id ) 
                            <option value="{{ $country->id }}" selected >{{ $country->country }} </option>
                            @else
                            <option value="{{ $country->id }}" >{{ $country->country }}</option>
                            @endif
                        @endforeach            
                        </select>                        
                    </div>
                    <div class="form-group col-6">
                        <label for="" class="font-weight-bold">Company Pay Period</label> 
                        <select id="payment_period_id" name="payment_period_id" class="custom-select custom-select">
                            @foreach ($paySchedules as $pay )
                                <option value="{{ $pay->id }}" {{ $company->payment_period_id == $pay->id ? 'selected' : '' }}>{{ $pay->title }}</option>
                            @endforeach        
                        </select>        
                    </div>
                </div>
            </div>
    
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save Status</button>
            </div>
            </form>
        </div>
      </div>
    </div>    


@include('components.modal.add_reporting_period')
@include('components.modal.edit_contribution')

<x-contribution type="tap" :company="$company"></x-contribution>
<x-contribution type="scp" :company="$company"></x-contribution>
<x-contribution type="epf" :company="$company"></x-contribution>

<div class="modal" tabindex="-1"  id="reportingPeriodsModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form method="POST" action="/company/reportingperiod">
        @csrf         
        @method('PATCH')               

        <div class="modal-header">
          <i class="fa fa-calendar my-auto mr-2"></i>
          <h5 class="modal-title">Set Reporting Period</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

            <label for="payment_mode_id" class="">Reporting Period</label>
            <div class="input-group emp_payment_mode mb-3">
                <select class="custom-select custom-select" id="reporting_period_id" name="reporting_period_id" >
                    @foreach ($periods as $period)
                        <option value="{{ $period->id }}" {{ $company->reporting_period_id == $period->id ? 'selected' :'' }}>{{ $period->title }} {{ $company->reporting_period_id == $period->id ? '*' :'' }}</option>
                    @endforeach
                </select>
            </div>

            <div class="row mb-3">
                <table id="payrollTable" class="table table-sm border mb-1" >
                    <thead class="tbl-dark">
                        <tr>
                            <th class="align-middle" width="25"></th>
                            <th class="align-middle" width="150">Title</th>
                            <th class="align-middle" width="150">Start</th>
                            <th class="align-middle" width="150">End</th>    
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="myTable" style="font-size: 12px;">
                    
                    @foreach ( $periods as $period )
                        <tr style="font-size: 13px">
                            <td class="align-middle">
                                @if ($period->id == $company->reporting_period_id)
                                    <i class="fa fa-check-circle text-success"></i>
                                @endif
                            </td>
                            <td>
                                {{ $period->title}}
                            </td>
                            <td>
                                {{ \Carbon\Carbon::parse($period->start_date)->format('d-M-y') }}
                            </td>
                            <td>
                                {{ \Carbon\Carbon::parse($period->end_date)->format('d-M-y') }}
                            </td>
                            <td></td>
                        </tr>
                    @endforeach
                
                    </tbody>

                </table>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
    </div>
</div>

 
    <script>

    function loadPreview(input, id) {
        id = id || '#preview_img';
        if (input.files && input.files[0]) {
            var reader = new FileReader();
    
            reader.onload = function (e) {
                $(id)
                        .attr('src', e.target.result)
                        .height(40);
            };
    
            reader.readAsDataURL(input.files[0]);
        }
    }

    </script>



</div>


</x-layout>