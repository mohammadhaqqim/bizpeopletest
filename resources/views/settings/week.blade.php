<x-layout>

    <style>
        .aday{
            width: 200px;
            height: 50px;
        }
        .ampm{
            width: 50px;
            height: 25px;
        }
    </style>
    
    @include('components.settings_aside')
    
    <div class="main pr-3 ">
        <div class="row border-bottom my-3 pb-3 biz-title">
            <i class="fa fa-calendar my-auto px-2"></i> 
            <h2 class="mb-0">Working Week</h2>
        </div>

        @if(count($errors) > 0)
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                        {{ $error }}<br>
                @endforeach
            </div>
        @endif

        <div class="row pt-1">
            <div class="col-6 pl-0">
                <h4>Current Working Weeks</h4>
            </div>
            <div class="col-6 text-right text-biz-blue">
                <i class="fa fa-plus-circle my-auto px-2" style="font-size: 18px;" data-toggle="modal" data-target="#exampleModal"></i> 
                <a href="" data-toggle="modal" data-target="#exampleModal"><label class="text-right">Add New Working Week</label></a>
            </div>
        </div>

        <div class="row">

            <table id="tblworkweek" class="table border mb-4">
                <thead  class="tbl-dark">
                    <tr>
                        <th hidden>#</th>
                        <th style="width: 8px;">Default</th>
                        <th style="width: 18%;">Week Name</th>
                        <th scope="col" style="width: 8%;">Mon</th>
                        <th scope="col" style="width: 8%;">Tue</th>
                        <th scope="col" style="width: 8%;">Wed</th>
                        <th scope="col" style="width: 8%;">Thu</th>
                        <th scope="col" style="width: 8%;">Fri</th>
                        <th scope="col" style="width: 8%;">Sat</th>
                        <th scope="col" style="width: 8%;">Sun</th>
                        <th scope="col" style="width: 5%;">Total</th>
                        <th scope="col" style="width: 5%;">Edit</th>
                        <th hidden>default</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($workingweeks as $week )
                    <tr>
                        <th hidden>{{ $week->id }}</th>
                        <th>
                        @if ($week->id == $company->working_week_id)
                            <i class="fa fa-check-circle text-success" aria-hidden="true"></i>
                        @endif
                        </th>
                        <th style="width: 20%;">{{ $week->title }}</th>
                        <th scope="col" style="width: 7%;">{{ $week->mon }}</th>
                        <th scope="col" style="width: 7%;">{{ $week->tue }}</th>
                        <th scope="col" style="width: 7%;">{{ $week->wed }}</th>
                        <th scope="col" style="width: 7%;">{{ $week->thu }}</th>
                        <th scope="col" style="width: 7%;">{{ $week->fri }}</th>
                        <th scope="col" style="width: 7%;">{{ $week->sat }}</th>
                        <th scope="col" style="width: 7%;">{{ $week->sun }}</th>
                        <th scope="col" style="width: 7%;">{{ $week->getTotal() }}</th>
                        <th scope="col" style="width: 10%;">
                            <a href="" ><i class="fa fa-pencil text-info" onclick="EditWeek({{ $week->id }}); return false;" aria-hidden="true"></i></a> 
                            <a onclick="return confirm('Are you sure to delete {{ $week->title }}?')" 
                                href="{{ route('workweek-delete', $week->id)}}" >
                                <i class="fa fa-trash text-danger ml-2" aria-hidden="true"></i>
                            </a> 
                        </th>       
                        <th hidden>{{ $week->id == $company->working_week_id ? 1 : 0 }}</th>                                 
                    </tr>                      
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="modal" tabindex="-1" id="exampleModal">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
                <form method="POST" action="/workweek">
                @csrf
        
                <div class="modal-header">
                  <h5 class="modal-title">Add New Working Week</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col">
                            <label for="title">Working Week Name</Title></label>
                            <input type="text" class="form-control" id="title" name="title" placeholder="Working Week Name" required maxlength="100">
                        </div>                        
                    </div>
                    <div class="row">
                        <div class="custom-control custom-checkbox ml-3">
                            <input class="custom-control-input" type="checkbox" value="" id="default">
                            <label class="custom-control-label" for="default">
                              Set As Default
                            </label>
                        </div>
                    </div>
                    <div class="row mb-1 d-flex justify-content-center">
                        <label for="" class="col-3">Monday</label> 
                        <input type="number" name="mon" id="mon" class="form-control col-2" step="0.1" min="0" value="0">
                    </div>
                    <div class="row mb-1 d-flex justify-content-center">
                        <label for="" class="col-3">Tuesday</label> 
                        <input type="number" name="tue" id="tue" class="form-control col-2" step="0.1" min="0" value="0">
                    </div>
                    <div class="row mb-1 d-flex justify-content-center">
                        <label for="" class="col-3">Wednesday</label> 
                        <input type="number" name="wed" id="wed" class="form-control col-2" step="0.1" min="0" value="0">
                    </div>
                    <div class="row mb-1 d-flex justify-content-center">
                        <label for="" class="col-3">Thursday</label> 
                        <input type="number" name="thu" id="thu" class="form-control col-2" step="0.1" min="0" value="0">
                    </div>
                    <div class="row mb-1 d-flex justify-content-center">
                        <label for="" class="col-3">Friday</label> 
                        <input type="number" name="fri" id="fri" class="form-control col-2" step="0.1" min="0" value="0">
                    </div>
                    <div class="row mb-1 d-flex justify-content-center">
                        <label for="" class="col-3">Saturday</label> 
                        <input type="number" name="sat" id="sat" class="form-control col-2" step="0.1" min="0" value="0">
                    </div>
                    <div class="row mb-1 d-flex justify-content-center">
                        <label for="" class="col-3">Sunday</label> 
                        <input type="number" name="sun" id="sun" class="form-control col-2" step="0.1" min="0" value="0">
                    </div>

                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
              </div>
            </div>
          </div>        

    @include('components.modal.edit_working_week')

        <script>
            function EditWeek(id)
            {
                document.getElementById("workweek").action = "/workweek/" + id;

                var oTable = document.getElementById("tblworkweek");

                for (i = 1; i < oTable.rows.length; i++){
                    var oCells = oTable.rows.item(i).cells;

                    if (oCells.item(0).innerHTML == id){
                        document.getElementsByClassName("weekTitle")[0].value = oCells.item(2).innerHTML;
                        document.getElementsByClassName("mon")[0].value = oCells.item(3).innerHTML;
                        document.getElementsByClassName("tue")[0].value = oCells.item(4).innerHTML;
                        document.getElementsByClassName("wed")[0].value = oCells.item(5).innerHTML;
                        document.getElementsByClassName("thu")[0].value = oCells.item(6).innerHTML;
                        document.getElementsByClassName("fri")[0].value = oCells.item(7).innerHTML;
                        document.getElementsByClassName("sat")[0].value = oCells.item(8).innerHTML;
                        document.getElementsByClassName("sun")[0].value = oCells.item(9).innerHTML;

                        //12
                        document.getElementsByClassName('defaultWeek')[0].checked 
                            = (oCells.item(12).innerHTML.trim() == '1' ? true : false);


                    }
                }


                $("#workingWeekEditModal").modal('show');
            }

        </script>
    
    </div>
    
    
    </x-layout>