<x-layout>

    
    @include('components.settings_aside')
    
    <div class="main pr-3 ">
        <div class="row border-bottom my-3 pb-3 biz-title">
            <i class="fa fa-briefcase my-auto px-2"></i> 
            <h2 class="mb-0">Allowances</h2>
        </div>

        @if(count($errors) > 0)
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                        {{ $error }}<br>
                @endforeach
            </div>
        @endif

        <div class="row mt-2">
            
            <div class="col-6 pl-0">
                <h4>Current Transaction Types</h4>
            </div>
            <div class="col-6 text-right text-biz-blue">
                <a href="#" data-toggle="modal" data-target="#allowanceAddModal">
                    <i class="fa fa-plus-circle my-auto px-2" style="font-size: 18px;" ></i> 
                    <label class="text-right">Add New Type</label>
                </a>
            </div>

            <div class="col-12 mb-3 px-0">
                <input class="form-control" id="myInput" type="text" placeholder="Search..">
            </div>
                                
            <table id="tblTransactions" class="table border mb-4">
                <thead  class="tbl-dark">
                <tr>
                    <th hidden>#</th>
                    <th style="width: 30%;">Title</th>
                    <th scope="" width="75px">Active</th>
                    <th scope="" width="75px">Type</th>
                    <th hidden>Type</th>
                    <th scope="" width="75px">Calculated</th>
                    <th scope="" width="75px">Fixed</th>
                    <th scope="" width="75px">Action</th>
                </tr>
                </thead>
                <tbody id="myTable">
                @foreach ( $allowances as $allowance )
                <tr>
                    <th hidden>{{ $allowance->id }}</th>
                    <th>{{ $allowance->title }}</th>

                    <th>
                        <div class="custom-control custom-switch">
                            <input type="checkbox" class="custom-control-input" id="active{{ $allowance->id }}" {{ $allowance->active ? 'checked' : ''}}>
                            <label class="custom-control-label" for="active{{ $allowance->id }}"></label>
                        </div>
                    </th>
                    <th >
                        {{ $allowance->addition ? 'Addition' : 'Deduction'}}
                    </th>
                    <th hidden>
                        {{ $allowance->addition }}
                    </th>
                    <th>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="calc{{ $allowance->id }}" {{ $allowance->calculated ? 'checked' : ''}}>
                            <label class="custom-control-label" for="calc{{ $allowance->id }}"></label>
                        </div>
                    </th>
                    <th>
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="fixed{{ $allowance->id }}" {{ $allowance->fixed ? 'checked' : ''}}>
                            <label class="custom-control-label" for="fixed{{ $allowance->id }}"></label>
                        </div>
                    </th>
                    <th>
                        <a href="#" onclick="transactionEdit({{ $allowance->id }}); return false;" data-toggle="tooltip" data-placement="bottom" title="Edit Allowance">
                            <i class="fa fa-pencil my-auto px-2 text-primary" style="font-size: 20px;"></i>
                        </a>
                    </th>
                </tr>
                @endforeach
                
                </tbody>
            </table>
        </div>

        @include('components.modal.add_allowance')
        @include('components.modal.edit_allowance')
    
        <script>
            $(document).ready(function(){
                $("#myInput").on("keyup", function() {
                    var value = $(this).val().toLowerCase();
                    $("#myTable tr").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                });
            });


            function transactionEdit(id){
                document.getElementById("editTrans").action = "/transaction/" + id;

                var oTable = document.getElementById("tblTransactions");
                for (i = 1; i < oTable.rows.length; i++){
                    var oCells = oTable.rows.item(i).cells;
                    if (oCells.item(0).innerHTML == id){
                        document.getElementsByClassName("editTitle")[0].value = oCells.item(1).innerHTML;                        
                        document.getElementsByClassName("editType")[0].value = oCells.item(3).innerHTML.trim();
                        document.getElementsByClassName("editActive")[0].checked = document.getElementById("active"+id).checked; 
                        document.getElementsByClassName("editCalc")[0].checked = document.getElementById("calc"+id).checked; 
                        document.getElementsByClassName("editFixed")[0].checked = document.getElementById("fixed"+id).checked; 
                    }
                }

                $("#allowanceEditModal").modal('show');

            }    

        </script>
    


    </div>
    
    
    </x-layout>