<x-layout>

    <style>
        .tbl-biz{
            background-color: rgb(230, 230, 230);
            color: black;
            border-radius: 15px !important; 
        }
    </style>
    
    @include('components.settings_aside')
    
    <div class="main pr-3 ">
        <div class="row border-bottom my-3 pb-3 biz-title">
            <i class="fa fa-briefcase my-auto px-2"></i> 
            <h2 class="mb-0">Customised Data</h2>
        </div>

        
        <aside class="float-left mr-0 bg-aside p-2 d-none d-md-block mr-3" style="height: 100%;">     
            Menu
            <div class="sidebar-sticky">
                @foreach ( $menu as $item )                    
                <ul class="nav flex-column">
                    <li class="nav-item p-1">
                        <a class="nav-link {{ $segment == $item->table ? 'bg-white' : '' }}" href="/settings/customise/{{ $item->table }}">{{ $item->display }}</a>
                    </li>
                </ul>
                @endforeach
            </div>
        </aside>

        <div>
            <div class="row">
                <div class="col"><h4>{{ $selected->display }}</h4></div>
 {{--                <div class="col text-right">Add {{ $selected->display }}</div> --}}
                <div class="col text-right text-biz-blue">
                    <i class="fa fa-plus-circle my-auto px-2" style="font-size: 18px;" data-toggle="modal" data-target="#customisedAddModal"></i> 
                    <a href="" data-toggle="modal" data-target="#customisedAddModal"><label class="text-right">Add {{ $selected->display }}</label></a>
                </div>
            </div>
            
            <div class="row">
                <table id="tblCustomised" class="table table-sm border mb-1" >
                    <thead class="tbl-dark">
                        <tr>                    
                            <th >ID</th>
                            <th hidden>Default</th>
                            <th hidden>Title</th>                    
                            <th >Title</th>                    
                            <th> Action </th>
                            <th hidden>action</th>
                        </tr>
                    </thead>
                    <tbody id="myTable" style="font-size: 15px;">
                        @if ($entries->count() == 0)
                        <tr class="align-middle" style="height: 50px; font-size:15px;">
                            <td class="align-middle text-center" colspan="4">No current {{ $selected->display }} found</td>
                        </tr>
                        @endif  

                        @foreach ( $entries as $entry)
                        
                            <tr class="" >
                                <td class="align-middle">{{ $entry->id }}</td>
                                <td hidden>
                                    @if($entry->id == $defaultid)                                    
                                        1
                                    @endif

                                </td>
                                <td hidden>{{ $entry->title }}</td>
                                <td>
                                
                                @if($entry->id == $defaultid)                                    
                                    <i class="fa fa-check-circle text-success" aria-hidden="true"></i>
                                @else
                                    <i class="fa fa-check-circle text-white" aria-hidden="true"></i>
                                @endif

                                    {{ $entry->title }}</td>
                                <td>
                                    
{{--                                     @if(is_null($selected->modalAdd)) --}}
                                    <a href="" onclick="statusEdit({{ $entry->id }}); return false;"><i class="fa fa-pencil text-info" aria-hidden="true" ></i></a> 
                                    <a onclick="return confirm('Are you sure to delete {{ $entry->title }}?')" 
                                        href="{{ '/delete/'.$segment.'/'.$entry->id }}" >
                                        <i class="fa fa-trash text-danger ml-2" aria-hidden="true"></i>
                                    </a>
{{--                                     @endif  --}}
        
                                </td>
                                <td hidden>
                                    {{ $selected->action }}/{{ $entry->id}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

        </div>
    </div>
    @include('components.modal.add_customised_data') 
    @include('components.modal.edit_customised_data') 

{{-- @foreach ( $menu as $item )
    @if( $item->modalAdd != '' )
    @include( $item->modalAdd )    
    @endif
@endforeach --}}

<script>
    function statusEdit(id){

        var oTable = document.getElementById("tblCustomised");

        for (i = 1; i < oTable.rows.length; i++){
            var oCells = oTable.rows.item(i).cells;

            if (oCells.item(0).innerHTML == id){

                document.getElementsByClassName("title")[0].value = oCells.item(2).innerHTML;

                document.getElementById("editStatus").action = oCells.item(5).innerHTML.trim();
                document.getElementsByClassName('default-id')[0].checked 
                    = (oCells.item(1).innerHTML.trim() == '1' ? true : false);

                $("#customisedEditModal").modal('show');

                return;
            }
        }
    }
</script>

</x-layout>