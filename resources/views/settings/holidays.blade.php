<x-layout>

    @include('components.settings_aside')
    
    <div class="main pr-3 ">
        <div class="row border-bottom my-3 pb-3 biz-title">
            <i class="fa fa-briefcase my-auto px-2"></i> 
            <h2 class="mb-0">Public Holidays</h2>
        </div>
    
        <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#holidayAddModal">
            <i class="fa fa-plus-circle text-prinary" ></i> New Holiday
        </button>
            
        @if(count($errors) > 0)
        <div class="row alert alert-danger mt-4">
            @foreach($errors->all() as $error)
                    {{ $error }}<br>
            @endforeach
        </div>
        @endif

        <div class="row my-4">
            <table id="tbl" class="table border">
                <thead  class="tbl-dark">
                    <tr>
                        <th scope="col" hidden>#</th>
                        <th scope="col">Holiday</th>
                        <th scope="col" hidden>Start</th>
                        <th scope="col" hidden>End</th>
                        <th scope="col">Date</th>
                        <th scope="col">Variable</th>
                        <th hidden>variable</th>
                        <th >Edit</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($holidays as $holiday)                        
                    <tr>
                        <td scope="col" hidden>{{ $holiday->id }}</td>
                        <td scope="col">{{ $holiday->title }}</td>
                        <td scope="col" hidden>{{ $holiday->start_date }}</td>
                        <td scope="col" hidden>{{ $holiday->end_date }}</td>
                        @if($holiday->end_date == $holiday->start_date)
                        <td scope="col">
                            {{ \Carbon\Carbon::parse($holiday->start_date)->format('d-m-Y') }}
                        </td>
                        @else
                        <td scope="col">
                            {{ \Carbon\Carbon::parse($holiday->start_date)->format('d-m-Y') }} - {{ \Carbon\Carbon::parse($holiday->end_date)->format('d-m-Y') }}
                        </td>
                        @endif
                        <td scope="col">
                            <div class="custom-control custom-checkbox ml-3">
                                <input type="checkbox" class="custom-control-input" id="variable{{ $holiday->id }}" {{ $holiday->variable_date ? 'checked' : '' }} disabled>
                                <label class="custom-control-label" ></label>
                            </div>
                        </td>
                        <td hidden>{{ $holiday->variable_date }}</td>
                        <td>
                            <a href="" class="" onclick="editHoliday({{ $holiday->id }}); return false;" >
                                <i class="fa fa-pencil my-auto px-2 text-primary"></i>
                            </a>
                            <a href="" class="" onclick="deleteHoliday({{ $holiday->id }}); return false;">
                                <i class="fa fa-trash my-auto px-2 text-danger"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>

        
    <div class="modal" tabindex="-1"  id="deleteModal">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <form id="deleteHoliday" method="POST" action="/holiday/">
            @method('DELETE')
            @csrf                        

            <div class="modal-header">
              <i class="fa fa-trash pt-1"></i>
              <h5 class="modal-title pl-2">Delete Holiday</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="form-group col text-center">
                        <label style="font-size: 16px;">Are you sure you would like to delete the following holiday?</label>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col">
                        <label for="name">Title</label>
                        <input type="text" class="form-control title_delete" id="title" name="title" disabled>
                    </div>    
                </div>
                <div class="row">
                    <div class="form-group col-6">
                        <label for="start_date">Starting Date</label><label class="text-danger"> *</label>

                        <div class="input-group">
                            <input type="date" class="form-control start_delete" id="start_date" name="start_date" placeholder="dd/mm/yyyy" 
                                disabled >
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>     
                    </div>    
                </div>

            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button type="submit" class="btn btn-danger">Yes</button>
                <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
            </div>
            </form>
          </div>
        </div>       
    </div>

    <div class="modal" id="holidayAddModal">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
            <form method="POST" action="/holidays">
            @csrf                   

            <div class="modal-header">
              <i class="fa fa-trash pt-1"></i>
              <h5 class="modal-title pl-2">Add New Holiday</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="form-group col">
                        <label for="title">Holiday Title</Title></label><label class="text-danger"> *</label>
                        <input type="text" class="form-control" id="title" name="title" placeholder="Public Holiday Title" max="100">
                    </div>
                </div>        
                <div class="row">
                    <div class="form-group col-6">
                        <label for="start_date">Date</label><label class="text-danger"> *</label>
                        <div class="input-group">
                            <input type="date" class="form-control" id="start_date" name="start_date" placeholder="dd/mm/yyyy" 
                                value="{{ date('Y-m-d', strtotime('0 day')) }}" required >
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>    
                    </div>    
                    <div class="form-group col-6">
                        <label for="end_date">To Date</label>
                        <div class="input-group">
                            <input type="date" class="form-control" id="end_date" name="end_date" placeholder="dd/mm/yyyy">
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>    
                    </div>    
                </div>
                <div class="row">
                    <div class="custom-control custom-checkbox ml-3">
                        <input type="checkbox" class="custom-control-input" id="variable_date" name="variable_date">
                        <label class="custom-control-label" for="variable_date">Holiday may vary this date</label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save Holiday</button>
            </div>
            </form>
          </div>
        </div>       
    </div>

@include('components.modal.edit_holiday')

<script>
    function showAdd()
    {
        $("#holidayAddModal").modal('show');
    }

    function deleteHoliday(id){
        document.getElementById("deleteHoliday").action = "/holiday/" + id;

        var oTable = document.getElementById("tbl");
        for (i = 1; i < oTable.rows.length; i++){
            var oCells = oTable.rows.item(i).cells;

            if (oCells.item(0).innerHTML == id){
                var dt = oCells.item(2).innerText.trim();
                document.getElementsByClassName("title_delete")[0].value = oCells.item(1).innerHTML;
                document.getElementsByClassName("start_delete")[0].value = dt;
            }
        }  

        $("#deleteModal").modal('show');
    }

    function editHoliday(id){
        document.getElementById("holiday").action = "/holiday/" + id;

        var oTable = document.getElementById("tbl");
        for (i = 1; i < oTable.rows.length; i++){
            var oCells = oTable.rows.item(i).cells;

            if (oCells.item(0).innerHTML == id){
                var dt = oCells.item(2).innerText.trim();
                document.getElementsByClassName("editTitle")[0].value = oCells.item(1).innerHTML;
                document.getElementsByClassName("editStart")[0].value = dt;
                dt = oCells.item(3).innerText.trim();
                document.getElementsByClassName("editEnd")[0].value = dt;

                if(oCells.item(6).innerHTML == "1"){
                    document.getElementsByClassName("editVariable")[0].checked = true;
                }
                else{
                    document.getElementsByClassName("editVariable")[0].checked = false;
                }

            }
        }  

        $("#holidayEditModal").modal('show');        
    }

</script>

</x-layout>