<x-layout>

{{--     @include('components.employee_header')  --}}

<style>
    .card-body {
        min-width: 200px;
        margin-right: 5px;
    }

    .myDIV:hover .hidden{
        display: block;
        top: 0px;
        left: 0px;        
        background-color: white;
    }

    .hidden{
        display: none;
    }

</style>    
    <div class="container mt-0">
        <div class="mt-3">
            @include('components.modal.back')
        </div>
    
{{--       @include('components.employee_aside') --}}
      
      <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
      
      <div class="main pr-3">
        <form method="POST" action="/inlieu/employee/{{ $employee->id }}">
        @csrf

        <div class="row border-bottom my-3 pb-3 biz-title">
            <i class="fa fa-clock-o my-auto px-2"></i> 
            <h4 class="mb-0">Time Inlieu - {{ $employee->name }}</h4>
        </div>

        @if(count($errors) > 0)
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                        {{ $error }}<br>
                @endforeach
            </div>
        @endif

    <div class="row">
        <div class="col-md-8">
            <div class="row">
                <div class="form-group form-date pl-0 mr-1">
                    <label for="start_date">From</label><label class="text-danger"> *</label>
                    <div class="input-group">
                        <input type="date" class="form-control" id="start_date" name="start_date" placeholder="dd/mm/yyyy" 
                            value="{{ is_null(old('start_date')) ? date('Y-m-d', strtotime('0 day')) : old('start_date') }}" onchange="getDaysOff({{ $employee->id }}); return false;" required>
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>                    
                </div>
                <div class="form-group form-date pl-0 mr-1">
                    <label for="end_date">To</label><label class="text-danger"> *</label>
                    <div class="input-group">
                        <input type="date" class="form-control" id="end_date" name="end_date" placeholder="dd/mm/yyyy" 
                            value="{{ date('Y-m-d', strtotime('0 day')) }}" onchange="getDaysOff({{ $employee->id }}); return false;" required>
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>                    
                </div>

                <div class="cal-date d-flex flex-column text-center mb-3">
                    <label class="bg-secondary text-white" for="" >
                      Days
                    </label>
                    <h5 id="display_days" class="font-weight-bold">
                      1
                    </h5>
                </div>

            </div>
            <div class="row">
                <div class="form-group pl-0 mb-0" style="width: 440px;">
                    <label for="leave_type_id">Time off policy type</label><label class="text-danger"> *</label>
                    <select name="leave_type_id" id="leave_type_id" class="custom-select" required
                      onchange="getDaysOff({{ $employee->id }}); return false;">   
                        <option value="0" >-- Choose --</option>
                    @foreach ($leaveTypes as $leave)
                        @if( !is_null($leave->getCurrentEmployeePolicy($employee->id)) )
                        <option value="{{ $leave->id }}" {{ $leave->id == $policy ? 'selected' : '' }}>{{ $leave->title }}</option>                    
                        @endif
                    @endforeach
                    </select>
                </div>                 
            </div>

{{--             <div class="row mb-6" style="width: 440px;">
                <div class="mr-2">
                    <small id="over" class="bg-danger text-white px-3 " hidden>Over Allocated</small>
                    <small id="under" class="bg-success text-white px-3 ">Within Limits</small>
                </div>
                <div class="col">
                    <small> Available : </small>
                    <small id="balanceAmt" class="ml-2">.. Hour(s)</small>
                    <small id="balanceDayHour" class="ml-2">.. Day(s)</small>
                </div>
                <div class="col p-0 text-right">
                    <small id="maxLimit">Max :</small>
                    <small class="bg-success px-3 text-white " id="goodLimit" data-toggle="tooltip" data-placement="bottom" title="Value remaining shown">Good</small>
                    <small class="bg-danger px-3 text-white " id="overLimit" data-toggle="tooltip" data-placement="bottom" title="Value remaining shown" hidden>Over</small>
                </div>
            </div> --}}
            
            <div class="row mt-2">
                <div class="row"  style="width: 440px;">
                    <label for="date_joined" id="daysoff" class="mb-0">Days Inlieu</label>
                    <small for="" id="total_hours" class="col text-right pt-1">Total Hours</small>
                </div>
            </div>
            <div class="row">
                <div class="daysoff border rounded" style="width: 440px;">
                    <div class="mb-3 mx-auto " >
                        <div id="days_off_list" class="" ></div>
                    </div>
                </div>
            </div>
            <div class="row" id="policyValues" hidden>
                bal <small id="availableBalance"></small>
                track <small id="track"></small>
                avg <small id="avg" class="mr-2" ></small>
                sched- <small id="scheduled" class="mr-2" ></small>
                max- <small id="max" class="mr-2" ></small>
                taken- <small id="taken" class="mr-2" ></small>
                carry- <small id="carry" class="mr-2" ></small>
            </div>

            <div class="row">
                <div class="form-group pl-0" style="width: 440px;">
                    <label for="comment">Comment</label>
                    <textarea class="form-control" name="comment" id="comment" rows="3"></textarea>                
                </div>
            </div>

            <div class="row" style="width: 440px;">
                <div class="col text-right">
                    <button type="submit" id="btnOver" class="btn btn-primary" onclick="return confirm('You have used more leave than available are you sure to save changes?')" hidden>Save Changes.</button>
                    <button type="submit" id="btnUnder" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
{{--         <div class="col-4">
            @include('components.modal.leave_summary')
        </div> --}}
    </div>

        </form>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            // Code to run as soon as the page is ready
            var id = {!! json_encode($employee->id) !!};
            getDaysOff(id);
        })

        function getDaysOff(id) {
            start = $("input[name=start_date]").val();
            end = $("input[name=end_date]").val();
            type = $('#leave_type_id').find(":selected").val();
            
            if(start > end){
                document.getElementById('end_date').value = start;
                end = start;
            }
            //alert('id: ' + id + ' start: ' + start + ' end: ' + end + ' type: ' + type);

            $.ajax({
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              type:'POST',
//              url:'/getDaysOff',
              url:'/getInlieu',
              data: {
                "_token": "{{ csrf_token() }}",
                "id": id,
                "start_date" : start,
                "end_date" : end,
                "type" : type,
                "leaveID" : -1
                },
              success:function(data) {
                if(data.balance){
                    bal = data.balance;
                }else{
                    bal=0;
                }

                $("#days_off_list").html(data.msg);
                $("#policyValues").html(data.policy);
                //alert(data.days);
                $("#display_days").html(data.days);

                if(data.track == 'd'){
                    $('#balanceDayHour').html('Day(s)');
                    bal = bal - data.hours/data.AvgHours;
                 }else{
                    $('#balanceDayHour').html('Hour(s)');
                    bal = bal - data.hours;
                 }

                $("#availableBalance").html(data.balance);
                 hideAll();

                 if( (bal) >= 0){
                    $("#under").attr('hidden', false);
                    $("#btnUnder").attr('hidden', false);
                }else{
                    $("#over").attr('hidden', false);
                    $("#btnOver").attr('hidden', false);
                 }
                 addDaysOff();
              }
           });
            
         }
    
         function hideAll(){
            $("#over").attr('hidden', true);
            $("#under").attr('hidden', true);
            $("#btnOver").attr('hidden', true);
            $("#btnUnder").attr('hidden', true);
         }

         function addDaysOff() {
            var x = 0;
            var day = document.getElementById('day'+x);
            var total = 0;

            while (day){
                total += Number(day.value);
                x++;
                day = document.getElementById('day'+x);
            }

            document.getElementById('total_hours').innerHTML = 'Total Hours: ' + total;

            bal = document.getElementById('availableBalance').innerHTML;

            if (document.getElementById('track').innerHTML == 'd'){
                avg = document.getElementById('avg').innerHTML;
                bal = bal - total/avg;
            }else{
                bal = bal - total;
            }

            document.getElementById('balanceAmt').innerHTML = bal;

            hideAll();
            checkMax();
            if(bal >= 0){
                $("#under").attr('hidden', false);
                $("#btnUnder").attr('hidden', false);
            }else{
                $("#over").attr('hidden', false);
                $("#btnOver").attr('hidden', false);
            }
         }

         function checkMax(){

            bal1 = Number(document.getElementById('availableBalance').innerText);
            bal = Number(document.getElementById('balanceAmt').innerText);

            sch = Number(document.getElementById('scheduled').innerText);
            max = Number(document.getElementById('max').innerText);
            tkn = Number(document.getElementById('taken').innerText);
            cyo = Number(document.getElementById('carry').innerText);

            limit = max + cyo + - tkn - sch + bal - bal1;
            document.getElementById('maxLimit').innerText = "";
            document.getElementById('overLimit').innerText = "Over - Max: "+ limit.toFixed(2);
            document.getElementById('goodLimit').innerText = "Within - Max: "+limit.toFixed(2);

            if(limit >= 0){
                $("#overLimit").attr('hidden', true);
                $("#goodLimit").attr('hidden', false);
            }else{
                $("#overLimit").attr('hidden', false);
                $("#goodLimit").attr('hidden', true);
            }

         }
    </script>

</x-layout>

