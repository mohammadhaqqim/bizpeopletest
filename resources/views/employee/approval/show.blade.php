<x-layout>
  <style>
    .cal-date{
      width: 65px;
    }

  </style>

<div class="container">
    <div class="mt-5">
      @include('components.modal.back')
    </div>

    <div class="card">
      
      <div class="card-header bg-secondary text-white mb-3 d-flex">
        <div class="my-auto col px-0">
          <h4 for="" class="d-inline">Time Off Request - {{ $leave->LeaveType->title }}</h4> 
        </div>
        
        <div class="col justify-content-end text-right pr-0">
          @if(is_null( $leave->approved_at ) )
          <form method="POST" action="/timeoff/approval/{{ $leave->id }}" class="d-inline">
            @csrf
            @method('PATCH')
              <button type="submit" class="btn btn-success">Approve</button>
          </form>
          @endif

          @if(is_null( $leave->rejected_at ) )
          <form method="POST" action="/timeoff/deny/{{ $leave->id }}" class="d-inline">
            @csrf
            @method('PATCH')

              <a onclick="return promptReason()" 
                href="#" >
                <button type="submit" class="btn btn-outline-light">Deny</button>
              </a>
              
              <input type="text" id="deny_comment" name="deny_comment"  value="hello" hidden>
            </form>
            @endif
          </div>
        </div>
        <div class="card-body">
          <div class="row ml-md-2 ml-sm-1">

            <div class="d-none d-sm-block my-auto" style="width: 86px;" >

             <div class="text-center align-items-center bg-secondary m-2 rounded-circle" style="width: 86px; hight: 86px;">
              <span class="col text-white ">
                <h3 class="mb-0">{{ $leave->Employee->getInitals() }}</h3>
              </span>
             </div>
            </div>

            <div class="col ml-md-3">
              <div class="row">
                <a href="/people/timeoff/{{ $leave->employee_id }}" target="_blank">
                  <h4 class="row font-weight-bold px-3" for="">{{ $leave->Employee->name}}</h4>
                </a>
              </div>
              <div class="row">
                <div class="col-sm-12 col-md-6 d-flex px-0 pb-2">
                  <div class="cal-date d-flex flex-column text-center">
                    <label class="bg-secondary text-white" for="" >
                      {{ \Carbon\Carbon::parse($leave->start_date)->format("M 'y") }}
                    </label>
                    <h5 class="font-weight-bold">
                      {{ \Carbon\Carbon::parse($leave->start_date)->format('d') }}
                    </h5>
                  </div>

                  <div class="my-auto">
                    <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" fill="currentColor" class="bi bi-arrow-right-short" viewBox="0 0 16 16">
                      <path fill-rule="evenodd" d="M4 8a.5.5 0 0 1 .5-.5h5.793L8.146 5.354a.5.5 0 1 1 .708-.708l3 3a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708-.708L10.293 8.5H4.5A.5.5 0 0 1 4 8z"/>
                    </svg>
                  </div>

                  <div class="cal-date d-flex flex-column text-center">
                    <label class="bg-secondary text-white" for="">
                      {{ \Carbon\Carbon::parse($leave->end_date)->format("M 'y") }}
                    </label>
                    <h5 class="font-weight-bold">
                      {{ \Carbon\Carbon::parse($leave->end_date)->format('d') }}
                    </h5>
                  </div>              
                </div>
                <div class="col-sm-12 col-md-6  d-flex flex-column my-auto border-left">
                  <label class="mb-0 font-weight-bolder" for="">{{ $leave->hours_off }} Hours</label>
                  <label class="mb-0" for="">{{ $leave->LeaveType->title }}</label>
                  @if ($leave->LeaveType->paid_time_off)
                  <label class="mb-0" for="">{{ $leave->getCalculatePolicyTotals()->balance }} Remaining Hours</label>    
                  @endif
                </div>
              </div>
            </div>

          </div>

          <div class="row mt-0 ml-md-3">
            <div class="d-none d-sm-block" style="width: 80px;" ></div>
            <div class="col ml-md-2">
              <small class="">
                @if(is_null($leave->approved_by_id) && is_null($leave->rejected_by_id))
                  In Review 
                @elseif( !is_null($leave->approved_by_id) )
                   Approved by {{ $leave->ApprovedBy->name }} on {{ date("d-m-Y", strtotime($leave->approved_at)) }} 
                @else
                   Denied by {{ $leave->DeniedBy->name }} on {{ date("d-m-Y", strtotime($leave->rejected_at)) }} 
                @endif
              </small>
            </div>
          </div>

          <div class="row mt-2 ml-md-3">
            <small class="text-muted">Employees Comment</small>
          </div>
          <div class="row mx-md-3 p-3 border mt-0 mb-md-3" >
            {{ $leave->comment }}
          </div>

        </div>
      </div>

  </div>
<script>
  function promptReason() {
    var comment = prompt("Please enter a comment why you denied the request?", "");
    if (comment != null){
      document.getElementById('deny_comment').value = comment;
    }else{
      document.getElementById('deny_comment').value = '';
      return false;
    }
  }
</script>
</x-layout>