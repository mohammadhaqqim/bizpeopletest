<x-layout>

    @include('components.settings_aside')
   
    <div class="main px-2 pl-md-0">
        <div class="container">

            <div class="card bg-light mt-3">

                <div class="card-header">
                    Import Export Employees
                </div>

                <div class="card-body">
                    <form action="/employee-import/" method="POST" enctype="multipart/form-data">
                        @csrf

                        <input type="file" name="file" class="form-control" required>
                        <br>
                        <button class="btn btn-success" type="submit">Import User Data</button>
                        <a class="btn btn-warning" href="employee-export">Export User Data</a>
                    </form>
                </div>

            </div>
        </div>
    </div>

</x-layout>