<x-layout>


@include('components.employee_header') 

<div class="main-content mt-0">

    @include('components.employee_aside')

    <div class="main pr-3 ">

        @if(count($errors) > 0)
            <div class="alert alert-danger pt-1">
                @foreach($errors->all() as $error)
                        {{ $error }}<br>
                @endforeach
            </div>
        @endif

        <div class="row border-bottom my-3 pb-3 biz-title">
            <i class="fa fa-address-book-o my-auto px-2"></i> 
            <h2 class="mb-0">Personal Contacts</h2>
        </div>

        <div class="row pt-1">
            <div class="col-6 pl-0">
                <h4>General Contact Information</h4>
{{--                 <h4>Family / Relation Information</h4> --}}
            </div>
            <div class="col-6 text-right text-biz-blue">
                <i class="fa fa-plus-circle my-auto px-2" style="font-size: 18px;" onclick="addRelation({{ $employee->id }}); return false;"></i> 
                <a href="" onclick="addRelation({{ $employee->id }}); return false;"><label class="text-right">Add a Family Member</label></a>
            </div>
        </div>

        <div class="row">
            <table class="table border mb-4" id="tblContacts">
                <thead class="tbl-dark">
                    <tr>
                        <th scope="col" hidden>#</th>
{{--                         <th scope="col" style="width: 10px;"></th> --}}
                        <th scope="col">Name</th>
                        <th scope="col">Relationship</th>
                        <th scope="col">IC Number</th>
                        <th scope="col">Phone Number</th>
                        <th scope="col">Mobile</th>
                        <th hidden>Relation</th>
                        <th hidden>DOB</th>
                        <th hidden>emergency</th>
                        <th hidden>dependant</th>
                        <th hidden>pp number</th>
                        <th hidden>pp exp</th>
                        <th hidden>dep exp</th>
                        <th hidden>sp exp</th>
                        <th>Edit</th>
                        <th hidden>nok</th>
                    </tr>
                </thead>
                <tbody>

                @foreach ($relations as $person)
                    <tr>
                        <td hidden>{{ $person->id }}</td>
                        <td>{{ $person->name }}</td>
                        <td>{{ $person->Relationship->title }}</td>
                        <td>{{ $person->ic_number }}</td>
                        <td>{{ $person->phone_number }}</td>
                        <td>{{ $person->hand_phone }}</td>
                        <td hidden>{{ $person->relationship_id }}</td>
                        <td hidden>{{ $person->date_of_birth }}</td>
                        <td hidden>{{ $person->emergency }}</td>
                        <td hidden>{{ $person->dependant }}</td>

                    @if($person->RelationExtras == null)
                        <td hidden></td>
                        <td hidden></td>
                        <td hidden></td>
                        <td hidden></td>
                    @else
                        <td hidden>{{ $person->RelationExtras->passport_number }}</td>
                        <td hidden>{{ $person->RelationExtras->passport_expiry }}</td>
                        <td hidden>{{ $person->RelationExtras->dependant_expiry }}</td>
                        <td hidden>{{ $person->RelationExtras->student_pass_expiry }}</td>
                    @endif

                        <td>
                            <a href="" onclick="relationEdit({{ $person->id }}); return false;"><i class="fa fa-pencil my-auto px-2 text-primary" ></i></a>
                            <a href="" onclick="deleteRelation({{ $person->id }}); return false;"><i class="fa fa-trash my-auto px-2 text-danger" ></i></a> 

                        </td>
                        <td hidden>{{ $person->next_of_kin }}</td>

                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>

        <div class="row pt-1">
            <div class="col-6 pl-0">
                <h4>Emergency Contant</h4>
            </div>
            <div class="col-6 text-right text-biz-blue">
                <i class="fa fa-plus-circle my-auto px-2" style="font-size: 18px;" onclick="addEmergency({{ $employee->id }}); return false;"></i> 
                <a href="" onclick="addEmergency({{ $employee->id }}); return false;"><label class="text-right">Add a Emergency Contact</label></a>
            </div>
        </div>

        <div class="row">
            <table class="table border mb-4">
                <thead class="tbl-dark">
                    <tr>
                        <th scope="col" hidden>#</th>
{{--                         <th scope="col" style="width: 10px;"></th> --}}
                        <th scope="col">Name</th>
                        <th scope="col">Relationship</th>
                        <td hidden>IC</td>
                        <th scope="col">Phone Number</th>
                        <th scope="col">Mobile</th>
                        <th hidden>Relation</th>
                        <th hidden>DOB</th>
                        <th hidden>emergency</th>
                        <th hidden>dependant</th>

{{--                     @if($person->relationship_id == null) --}}
                        <td hidden></td>
                        <td hidden></td>
                        <td hidden></td>
                        <td hidden></td>
{{--                     @else
                        <td hidden>{{ $person->relationship_id }}</td>
                        <td hidden>{{ $person->date_of_birth }}</td>
                        <td hidden>{{ $person->emergency }}</td>
                        <td hidden>{{ $person->dependant }}</td>
                    @endif  --}}
                        <th>Edit</th>
                        <td hidden></td>

                    </tr>
                </thead>
                <tbody>

                @foreach ($relations as $person)
                  @if($person->emergency == true)
                    <tr>
                        <td hidden>{{ $person->id }}</td>
                        <td>{{ $person->name }}</td>
                        <td>{{ $person->Relationship->title }}</td>
                        <td hidden>{{ $person->ic_number }}</td>
                        <td>{{ $person->phone_number }}</td>
                        <td>{{ $person->hand_phone }}</td>
                        <td hidden>{{ $person->relationship_id }}</td>
                        <td hidden>{{ $person->date_of_birth }}</td>
                        <td hidden>{{ $person->emergency }}</td>
                        <td hidden>{{ $person->dependant }}</td>

                    @if($person->RelationExtras == null)
                        <td hidden></td>
                        <td hidden></td>
                        <td hidden></td>
                        <td hidden></td>
                    @else
                        <td hidden>{{ $person->RelationExtras->passport_number }}</td>
                        <td hidden>{{ $person->RelationExtras->passport_expiry }}</td>
                        <td hidden>{{ $person->RelationExtras->dependant_expiry }}</td>
                        <td hidden>{{ $person->RelationExtras->student_pass_expiry }}</td>
                    @endif

                        <td>
                            <a href="" onclick="relationEdit({{ $person->id }}); return false;"><i class="fa fa-pencil my-auto px-2 text-primary" ></i></a>
                            <a href="" onclick="deleteRelation({{ $person->id }}); return false;"><i class="fa fa-trash my-auto px-2 text-danger" ></i></a> 
                        </td>
                        <td hidden>{{ $person->next_of_kin }}</td>

                    </tr>
                  @endif
                @endforeach

                </tbody>
            </table>
        </div>
        @include('components.modal.add_relation')
        @include('components.modal.edit_relation')
        @include('components.modal.delete_relation')

        <script>
            function addEmergency(id){
                document.getElementById("relationTitle").innerHTML = "New Emergency Contact";
                document.getElementById("icInformation").style.display = "none";

                document.getElementById("passportpass").style.display = "none";

                document.getElementById("emergency").checked = true;
                document.getElementById("user").style.display = "none";
                document.getElementById("med").style.display = "block";
    
                document.getElementById("passport").style.display = "none";
                document.getElementById("studentdependent").style.display = "none";

                showRelationModal();
            }

            function addRelation(id){
                document.getElementById("relationTitle").innerHTML = "New Relation";
                document.getElementById("icInformation").style.display = "block"; 

                document.getElementById("passportpass").style.display = "block";

                document.getElementById("emergency").checked = false;
                document.getElementById("med").style.display = "none";
                document.getElementById("user").style.display = "block";

                document.getElementById("passport").style.display = "none";
                document.getElementById("studentdependent").style.display = "none";

                showRelationModal();
            }

            function showRelationModal(){
                $("#addRelationModal").modal('show');
            }

            function deleteRelation(id){
                document.getElementById("deleteRelation").action = "/people/relations/" + id;

                var oTable = document.getElementById("tblContacts");

                for (i = 1; i < oTable.rows.length; i++){
                    var oCells = oTable.rows.item(i).cells;

                    if (oCells.item(0).innerHTML == id){
//alert('found');
                        document.getElementsByClassName("deleteName")[0].value = oCells.item(1).innerHTML;
                        document.getElementsByClassName("deleteRelation")[0].value = oCells.item(6).innerHTML;
                    }
                }

                $("#deleteRelationModal").modal('show');
            } 

            function relationEdit(id){
                document.getElementById("editRelation").action = "/people/relations/" + id;

                var oTable = document.getElementById("tblContacts");

                for (i = 1; i < oTable.rows.length; i++){
                    var oCells = oTable.rows.item(i).cells;

                    if (oCells.item(0).innerHTML == id){
                        document.getElementsByClassName("editName")[0].value = oCells.item(1).innerHTML;
                        document.getElementsByClassName("editRelation")[0].value = oCells.item(6).innerHTML;
                        document.getElementsByClassName("editIC")[0].value = oCells.item(3).innerHTML;
                        document.getElementsByClassName("editMobile")[0].value = oCells.item(5).innerHTML;
                        document.getElementsByClassName("editPhone")[0].value = oCells.item(4).innerHTML;
                        //alert(oCells.item(8).innerHTML);
                        if(oCells.item(8).innerHTML == "1"){
                            document.getElementsByClassName("editEmergency")[0].checked = true;
                        }else{
                            document.getElementsByClassName("editEmergency")[0].checked = false;
                        }
                        if(oCells.item(9).innerHTML == "1"){
                            document.getElementsByClassName("editDependant")[0].checked = true;
                        }else{
                            document.getElementsByClassName("editDependant")[0].checked = false;
                        }
                        document.getElementsByClassName("editDOB")[0].value = oCells.item(7).innerHTML;

/*                        document.getElementsByClassName("editStartDate")[0].value = oCells.item(10).innerHTML;
                        document.getElementsByClassName("editEndDate")[0].value = oCells.item(11).innerHTML;
                        document.getElementsByClassName("editWorking")[0].value = oCells.item(6).innerHTML.trim();
                        document.getElementsByClassName("editDueDate")[0].value = oCells.item(12).innerHTML;
                        document.getElementsByClassName("editPayDate")[0].value = oCells.item(13).innerHTML;
                        document.getElementsByClassName("editRemarks")[0].value = oCells.item(9).innerHTML; */
                    }
                }

                $("#editRelationModal").modal('show');
            }

            
        </script>
    </div>
</div>

</x-layout>