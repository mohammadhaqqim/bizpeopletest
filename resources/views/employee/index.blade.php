<x-layout>

<div class="min-content mt-0 ">

  <div class="main px-3 ">

{{--     @if ( session()->get('access')->hr || session()->get('access')->admin ) --}}
    @if ( optional(request()->access())->hr || optional(request()->access())->admin )                
    <div class="row pt-1 my-3">
        <div class="col-6 pl-0">
            <h4 class="my-1">Staff Directory</h4>
        </div>
    </div>            
    <div class="row pt-1 my-3">
        <div class="col-6 px-0">
            <a href="/employee/create" onclick=""><i class="fa fa-plus-circle my-auto px-2 text-primary" style="font-size: 18px;" onclick=""></i> </a>
            <a href="/employee/create" onclick=""><label class="text-right">Add a Employee</label></a>
        </div>
        <div class="col-6 px-0">
            <input class="form-control form-control-sm col-6 float-right" id="myInput" type="text" placeholder="Search..">
        </div>
    </div>
    @else
    <div class="row pt-1 my-3">
        <div class="col-6 pl-0">
            <h4 class="my-1">Staff Directory</h4>
        </div>
        <div class="col-6 px-0">
            <input class="form-control form-control-sm col-6 float-right" id="myInput" type="text" placeholder="Search..">
        </div>
    </div>

    @endif

    <div class="row">
        <table id="payrollTable" class="table table-sm border mb-1" >
            <thead class="tbl-dark">
                <tr>                    
                    {{-- one avatar --}}
                    <th class="pl-3" style="width: 100px;">Employees</th>                    
                    {{-- name,position,Department --}}
                    <th style="width: 300px;"></th>
                    {{-- email, bus phone, mobile --}}
                    <th style="width: 250px;"></th>
                    <th></th>
                    <td hidden>End Service</td>
                </tr>
            </thead>
            <tbody id="myTable" style="font-size: 12px;">
                @if ($employees->count() == 0)
                <tr class="align-middle" style="height: 50px; font-size:15px;">
                    <td class="align-middle text-center" colspan="4">No current employees found</td>
                </tr>
                @endif  

                @foreach ($employees as $employee)  
                <tr class="" style="height: 70px;" >
                <td class="align-middle"># {{ $employee->id }}</td>
                    <td class="align-middle">

                        <h5 class="d-block my-0">
{{--                             @if( auth()->user()->hr ) --}}
                            @if (optional(request()->access())->hr)
                            <a href="/people/personal/{{ $employee->id }}/edit">{{ $employee->name }}</a>
                            @else
                            {{ $employee->name }}
                            @endif
                        </h5>

                        @if (is_null($employee->designation_id))
                        <label for="my-0"></label>
                        @else
                        <label class="my-0">{{ $employee->getCurrentDesignation() }} </label>
                        @endif

                        @if (is_null($employee->department_id))
                        <label for="my-0"></label>
                        @else
                        <label class=" my-0">{{ $employee->getCurrentDepartment()->title }}</label>
                        @endif 
                    </td>
                    <td class="align-middle">
                        <label class="d-block my-0">{{ $employee->work_email }}</label>
                        <label class="d-block my-0">{{ $employee->work_phone }} Ext: {{ $employee->work_phone_ext }}</label>
                        <label class="d-block my-0">{{ $employee->hand_phone }}</label>
                    </td>
                    <td></td>
                    <td hidden>{{ $employee->end_date }}</td>
                </tr>
                @endforeach 
            </tbody>
        </table>
    </div>
  </div>
</div>

<script>
    $(document).ready(function(){
    
      $("#myInput").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });
</script>

</x-layout>
