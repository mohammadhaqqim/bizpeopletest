<x-layout>

<style>

    .aaa{
        background-color: #E9ECEF !important;
        border-radius: 0px 4px 4px 0px !important;
    }

</style>

<div class="container">
    <form method="POST" action="/employee">
        @csrf

        <div class="row border-bottom my-3 pb-3 biz-title">
            <i class="fa fa-user-circle my-auto px-2" data-toggle="tooltip" title = "Logout"></i> 
            <h2 class="mb-0">New Employee </h2>
        </div>
        
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                        {{ $error }}<br>
                @endforeach
            </div>
        @endif

        
        <div class="biz-section mb-3 border-bottom mb-3">
            </div>
        </div>
        
        <nav class="px-3 bg-light-biz fixed-bottom border-top" >
            <div class="container py-3 text-right">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="button" class="btn btn-danger">Cancel</button>
            </div>
        </nav>

    </form>

</div>

<script>

    // when the tab is clicked on then refresh page
    // in event on new designation / department added
    document.addEventListener('visibilitychange', function(ev) {
        if( `${document.visibilityState}` == 'visible'){
            console.log(`Tab state : ${document.visibilityState}`);
            location.reload();
        } 
    });

    function AddNewVisa(){
        $('#exampleModalCenter').modal(options)
    }

    function checkJobSelection(a){
        var x = a.selectedIndex; 

        if (a.value == -1){
            //open new tab to add new deisgnation
            a.value = '-2';
            var win = window.open( "{{ route('designations.create') }}", '_blank');
            win.focus();
        }        

    }

    function checkDepartmentSelection(a){
        var x = a.selectedIndex; 

        if (a.value == -1){
            a.value = '-2';
            var win = window.open( "{{ route('departments.create') }}", '_blank');
            win.focus();

        }
    }


    function checkPaymentMode(a){
        var x = a.selectedIndex;

        var txt = a.options[a.selectedIndex].text;

        var id = document.getElementById("id_bank");
        var account = document.getElementById("account_bank");

        if(txt == "Bank Transfer")
        {
            id.style.display = "block";
            account.style.display = "block";
        }else{
            id.style.display = "none";
            account.style.display = "none";
        }
    }

    function checkNationality()
    {
        var passport = document.getElementById('passport');
        var nationality = document.getElementById('nationality_id');
        var nation = document.getElementById('nation_id');
        
        if (nation.innerHTML == nationality.value){
            passport.style.visibility = 'hidden';
            toggle('ic-expiry', 'none');
        }else{
            passport.style.visibility = 'visible';
            toggle('ic-expiry', 'block');
        }
    }

    function toggle(className, displayState){
        var elements = document.getElementsByClassName(className)

        for (var i = 0; i < elements.length; i++){
            elements[i].style.display = displayState;
        }
    }   
</script>


</x-layout>
