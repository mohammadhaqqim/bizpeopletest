<x-layout>

@include('components.employee_header') 

<div class="main-content mt-2">                    

    @include('components.employee_aside')

    <div class="main pr-3">
        <form method="POST" action="/employee/{{ $employee->id }}">
            <!-- /{{ $employee->id }} -->
            @method('PATCH')
            @csrf

            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                            {{ $error }}<br>
                    @endforeach
                </div>
            @endif
            
            <div class="biz-section mb-3 border-bottom ">
                <div class="row mt-2">
                    <div class="col-12 col-lg-3 pl-0">
                        <h4>Profile Information </h4>
                        <label for="" id="nation_id" hidden>{{ Session::get('address_country_id') }}</label>
                    </div>
                    <div class="col-12 col-lg-9 px-0">
                        <div class="row">
                            <div class="form-group col-6 col-sm-3 pl-0">
                                <label for="employee_code" class="mb-0 text-secondary">Employee Code</label>
                                <input type="text" class="form-control" id="employee_code" name="employee_code" 
                                    placeholder="Your Unique ID" value="{{ $employee->employee_code }}">
                                <small class="form-text text-muted">** Needs to be unique</small>
                            </div>        
                        </div>
                        <div class="row">
                            <div class="form-group col-12 col-sm-9 pl-0">
                                <label for="name">Employee Name</label><label class="text-danger"> *</label>
                                <input type="text" class="form-control" id="name" name="name" placeholder="Employee name" 
                                    value="{{ $employee->name }}" required>
                            </div>
                            <div class="form-group col col-sm-3 pl-0">
                                <label for="preferred_name">Preferred Name</label>
                                <input type="text" class="form-control" id="preferred_name" name="preferred_name" 
                                    placeholder="Preferred name" maxlength="50" value="{{ $employee->preferred_name }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6 col-sm-4 pl-0">
                                <label for="gender_id">Gender</label>
                                <select name="gender_id" id="gender_id" class="custom-select">
            
                                @foreach ($genders as $gender)
                                    @if ( $employee->gender_id == $gender->id) 
                                    <option value="{{ $gender->id }}" selected>{{ $gender->title }}</option>
                                    @else
                                    <option value="{{ $gender->id }}" >{{ $gender->title }}</option>
                                    @endif
                                @endforeach
            
                                </select>
                            </div>
                            <div class="form-group col-6 col-sm-4 pl-0">
                                <label for="date_of_birth">Date of Birth</label><label class="text-danger"> *</label>
                                <div class="input-group">
                                    <input type="date" class="form-control" id="date_of_birth" name="date_of_birth" 
                                        value="{{ $employee->date_of_birth }}" required>
                                </div>
                            </div>
                            <div class="form-group col-12 col-sm-4 pl-0">
                                <label for="place_birth">Place of Birth</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="place_birth" name="place_birth" value="{{ $employee->place_birth }}" >
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group col-6 col-sm-4 pl-0">
{{--                                 <span class="badge badge-secondary">New</span> --}}
                                <label for="religion_id">Religion </label><label class="text-danger"> *</label>
                                <select name="religion_id" id="religion_id" class="custom-select" required>
                                    <option value="0" class="text-primary" onclick="AddReligion(); return false;">Add a Religion</option>

                                @foreach ($religions as $religion)
                                    @if ( $employee->religion_id == $religion->id || Session::get('religion_id') == $religion->id ) 
                                    <option value="{{ $religion->id }}" selected>{{ $religion->title }}</option>
                                    @else
                                    <option value="{{ $religion->id }}" >{{ $religion->title }}</option>
                                    @endif
                                @endforeach
            
                                </select>
                            </div>
                            <div class="form-group col-6 col-sm-4 pl-0">
                                <label for="marital_status_id">Marital Status</label>
                                <select name="marital_status_id" id="marital_status_id" class="custom-select">
            
                                @foreach ($maritalStatus as $status)
                                    <option value="{{ $status->id }}" {{$employee->marital_status_id == $status->id ? "selected" : ''}} >{{ $status->title }}</option>
                                @endforeach
            
                                </select>
                            </div>

                            <div class="form-group col-6 col-sm-4 pl-0">
                                <label for="race_id">Race</label>
                                <select name="race_id" id="race_id" class="custom-select">
                                    <option value="0" class="text-primary" onclick="AddRace(); return false;">Add a Race</option>
            
                                @foreach ($races as $race)
                                    @if ( $employee->race_id == $race->id || Session::get('race_id') == $race->id ) 
                                    <option value="{{ $race->id }}" selected>{{ $race->title }}</option>
                                    @else
                                    <option value="{{ $race->id }}" >{{ $race->title }}</option>
                                    @endif
                                @endforeach
            
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="biz-section mb-3 border-bottom ">
                <div class="row">
                    <div class="col-12 col-lg-3 pl-0">
                        <h4>Email Notifications<h4>
                    </div>
                    <div class="form-group col-6 col-md-4 col-lg-3 pl-0">
                        <label for=""></label>
                        <div class="custom-control custom-checkbox mr-sm-2 ">
                            <input type="checkbox" class="custom-control-input" name="email_payslip" id="email_payslip" {{ $employee->email_payslip ? 'checked' : '' }}>
                            <label class="custom-control-label" for="email_payslip">Email Payslip</label>
                        </div>                    
                    </div>  

                    <div class="form-group col-6 col-md-4 col-lg-3 pl-0">
                        <label for=""></label>
                        <div class="custom-control custom-checkbox mr-sm-2 ">
                            <input type="checkbox" class="custom-control-input" name="email_timeoff" id="email_timeoff" {{ $employee->email_timeoff ? 'checked' : '' }}>
                            <label class="custom-control-label" for="email_timeoff">Email Timeoff</label>
                        </div>                    
                    </div>  

                </div>
            </div>
            <div class="biz-section mb-3 border-bottom ">
                <div class="row">
                    <div class="col-12 col-lg-3 pl-0">
                        <h4>Identification Information </h4>
                    </div>
                    <div class="col-12 col-lg-9 px-0">
                        <div class="row">
                            <div class="form-group col-6 col-sm-4 pl-0">
                                <label for="nationality_id">Nationality</label><label class="text-danger"> *</label>
                                <select name="nationality_id" id="nationality_id" class="custom-select" onchange="checkNationality()" required> 
                                    <option value="0" class="text-primary" onclick="AddCountry(); return false;">Add a Country</option>

                                @foreach ($countries as $nationality)
                                    @if ( $employee->nationality_id == $nationality->id || Session::get('nationality_id') == $nationality->id ) 
                                    <option value="{{ $nationality->id }}" selected>{{ $nationality->nationality }}</option>
                                    @else
                                    <option value="{{ $nationality->id }}" >{{ $nationality->nationality }}</option>
                                    @endif
                                @endforeach            
                                </select>
                            </div>
                            {{-- <label for="" id="default_country">{{ Session::get('address_country_id') }}</label> --}}

                            <div class="form-group col-6 col-md-4 col-lg-3 pl-0">
                                <label for=""></label>
                                <div class="custom-control custom-checkbox mr-sm-2 ">
                                    <input type="checkbox" class="custom-control-input" name="driving_license" id="driving_license" {{ $employee->driving_license ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="driving_license">Driving License</label>
                                </div>                    
                            </div>  
        
                        </div>

                        <div class="row">
                            <div class="form-group col-6 col-sm-4 pl-0">
                                <label for="ic_number">IC Number</label><label class="text-danger"> *</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="ic_number" name="ic_number" value="{{ $employee->ic_number }}" >
                                </div>
                            </div>
                            <div class="form-group col-6 col-sm-4 pl-0 ic-expiry country-id">
                                <label for="ic_expiry">IC Expiry</label>
                                <div class="input-group">
                                    <input type="date" class="form-control" id="ic_expiry" name="ic_expiry" value="{{ $employee->ic_expiry }}">
                                </div>
                            </div>
                            <div class="form-group col-6 col-sm-4 pl-0">
                                <label class="text-danger"> *</label>
                                <label for="ic_color_id">IC Color</label>
                                <select name="ic_color_id" id="ic_color_id" class="custom-select" required>            
                                @foreach ($icColors as $color)
                                    @if ( $employee->ic_color_id == $color->id) 
                                    <option value="{{ $color->id }}" selected>{{ $color->title }}</option>
                                    @else
                                    <option value="{{ $color->id }}" >{{ $color->title }}</option>
                                    @endif
                                @endforeach            
                                </select>
                            </div>
                        </div>
                        <div class="row country-id" id="passport">
                            <div class="form-group col-6 col-sm-4 pl-0">
                                <label for="passport_number">Passport No</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="passport_number" name="passport_number" value="{{ $employee->passport_number }}" >
                                </div>
                            </div>
                            <div class="form-group col-6 col-sm-4 pl-0">
                                <label for="passport_expiry">Passport Expiry</label>
                                <div class="input-group">
                                    <input type="date" class="form-control" id="passport_expiry" name="passport_expiry" value="{{ $employee->passport_expiry }}" >
                                </div>
                            </div>
                            <div class="form-group col-6 col-sm-4 pl-0">
                                <label for="passport_place_issued">Place Issued</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="passport_place_issued" name="passport_place_issued" value="{{ $employee->passport_place_issued }}" >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="biz-section mb-3 border-bottom ">
                <div class="row">
                    <div class="col-12 col-lg-3 pl-0">
                        <h4>Employment Status </h4>
                    </div>
                    <div class="form-group col-sm-6 col-lg-3 col-md-4 pl-0">
                        <label for="employment_type_id">Status</label>
                        <select name="employment_type_id" id="employment_type_id" class="custom-select" disabled> 

                        @foreach ($employmentStatuses as $employmentStatus)
                            {{-- <option value="{{ $employmentStatus->id }}" >{{ $employmentStatus->title }}</option> --}}

                        @if (is_null($employee->getCurrentStatus()))
                            <option value="{{ $employmentStatus->id }}" >{{ $employmentStatus->title }}</option>
                        @else
                            <option value="{{ $employmentStatus->id }}" {{ $employee->getCurrentStatus()->employment_status_id == $employmentStatus->id ? 'selected' : '' }}>{{ $employmentStatus->title }}</option>
                        @endif

                        @endforeach

                        </select>
                    </div>           
                    <div class="form-group col-sm-6 col-lg-3 col-md-4 pl-0">
                        <label for="working_week_id">Working Week</label>
                        <select name="working_week_id" id="working_week_id" class="custom-select" > 

                        @foreach ($workingWeeks as $workingweek)
                            <option value="{{ $workingweek->id }}" {{ $employee->working_week_id == $workingweek->id ? 'selected' : '' }}>{{ $workingweek->title }}</option>
                        @endforeach
                        </select>

{{--                         @foreach ($workingWeeks as $workingweek)
                        <small id="emailHelp" class="form-text text-muted">
                            ?? Mon, Tue, Wed, Thu, Fri, Sat, Sun ??
                        </small>
                        @endif --}}
                    </div>           
                </div>            
            </div>

            <div class="biz-section mb-3 border-bottom ">
                <div class="row">
                    <div class="col-12 col-lg-3 pl-0">
                        <h4>Address</h4>
                    </div>
                    <div class="col-12 col-lg-9 px-0">
                        <div class="row">
                            <div class="form-group col-12 col-sm-8 pl-0 mb-2">
                                <input type="text" class="form-control" id="address1" name="address1" placeholder="Address 1" value="{{ $employee->address1 }}" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-12 col-sm-8 pl-0 mb-2">
                                <input type="text" class="form-control" id="address2" name="address2" value="{{ $employee->address2 }}" placeholder="Address 2">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-6 col-sm-4 pl-0">
                                <input type="text" class="form-control" id="city" name="city" placeholder="Kampong" value="{{ $employee->city }}" >
                            </div>
                            <div class="form-group col-6 col-sm-4 pl-0">
                                <input type="text" class="form-control" id="area" name="area" placeholder="District" value="{{ $employee->area }}">
                            </div>
                            <div class="form-group col-6 col-sm-4 pl-0">
                                <input type="text" class="form-control" id="postcode" name="postcode" placeholder="Postcode" value="{{ $employee->postcode }}" >
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-12 col-sm-4 pl-0">
                                <select name="address_country_id" id="address_country_id"  name="address_country_id" class="custom-select" >
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}" {{ $employee->address_country_id == $country->id ? 'selected' : '' }}>{{ $country->country }}</option>
                                @endforeach
                                </select>
                            </div>           
                        </div>
                    </div>
                </div>
            </div>

            <div class="biz-section mb-3 border-bottom ">
                <div class="row">
                    <div class="col-12 col-lg-3 pl-0">
                        <h4>Contact</h4>
                    </div>
                    <div class="col-12 col-lg-9 px-0">
                        <div class="row form-group">
                            <label for="">Phone Numbers - one required</label>
                        </div>
                        <div class="row">
                            <div class="form-group col-8 col-sm-6 pl-0">
                                <div class="input-group ">
                                    <div class="input-group-prepend" data-toggle="tooltip" data-placement="bottom" title="Work Phone">
                                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-building"></i></span>
                                    </div>
                                    <input type="number" id="work_phone" name="work_phone" class="form-control" placeholder="Work Number" 
                                        aria-label="Work Number" aria-describedby="basic-addon1" value="{{ $employee->work_phone }}"
                                        pattern="[0-9]{7}" maxlength="7" minlength="7">
                                </div>
                            </div>
{{--                             <div class="form-group col-4 col-sm-3 pl-0"  data-toggle="tooltip" data-placement="bottom" title="Work Extention">
                                <input type="number" class="form-control" id="work_phone_ext" name="work_phone_ext" placeholder="Ext" value="{{ $employee->work_phone_ext }}"
                                pattern="[0-9]{3}" maxlength="3" >
                            </div> --}}
                        </div>
                        <div class="row">
                            <div class="form-group col-12 col-sm-6 pl-0">
                                <div class="input-group ">
                                    <div class="input-group-prepend"  data-toggle="tooltip" data-placement="bottom" title="Mobile Phone">
                                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-mobile" style="font-size: 22px;"></i></span>
                                    </div>
                                    <input type="text" class="form-control" id="hand_phone" name="hand_phone" placeholder="Mobile Number" value="{{ $employee->hand_phone }}">
                                </div>
                            </div>
                            <div class="form-group col-12 col-sm-6 pl-0">
                                <div class="input-group ">
                                    <div class="input-group-prepend" data-toggle="tooltip" data-placement="bottom" title="Home Phone">
                                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-home" style="font-size: 19px;"></i></span>
                                    </div>
                                    <input type="text" class="form-control" id="home_phone" name="home_phone" placeholder="Home Number" value="{{ $employee->home_phone }}">
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <label for="">Email Addresses - one required</label>
                        </div>        
                        <div class="row">
                            <div class="form-group col-12 col-sm-6 pl-0 mb-0">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend" data-toggle="tooltip" data-placement="bottom" title="Business Email">
                                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-building"></i></span>
                                    </div>
                                    <input type="email" id="work_email" name="work_email" class="form-control" placeholder="Work Email" 
                                        aria-label="Work Email" aria-describedby="basic-addon1" value="{{ $employee->work_email }}">
                                </div>
                            </div>
            
                            <div class="form-group col-12 col-sm-6 pl-0 mb-2">
                                <div class="input-group mb-2">
                                    <div class="inpcol-12 input-group-prepend" data-toggle="tooltip" data-placement="bottom" title="Personal Email">
                                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-home" style="font-size: 19px;"></i></span>
                                    </div>
                                    <input type="email" class="form-control" id="home_email" name="home_email" placeholder="Home Email" value="{{ $employee->home_email }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>        
            </div>

            <div class="biz-section mb-3 border-bottom mb-5">
                <div class="row">
                    <div class="col-12 col-lg-3 pl-0">
                        <h4>Self-service Access</h4>
                    </div>
                    <div class="col-12 col-lg-9 pl-0">
                        <label for="">Self service allows the user to interact with the system though items such as appling for leave or seaching the company directory.</label>
                        <div class="custom-control custom-checkbox mb-4">
                            <div class="custom-control custom-checkbox mr-sm-2">
                                <input type="checkbox" class="custom-control-input" id="self_service" name="self_service" {{ $employee->self_service == 1 ? 'checked' : '' }}>
                                <label class="custom-control-label" for="self_service">Check this to provide basic employee access</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <nav class="px-3 bg-light-biz fixed-bottom border-top" >
                <div class="container py-2 text-right">
                    <button type="button" class="btn btn-danger">Cancel</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </nav>

        </form>
    </div>

    @include('components.modal.add_religion')
    @include('components.modal.add_race')
    @include('components.modal.add_country')

</div>
<script>
window.onload = function(){
    checkNationality();
}

    function AddNewVisa(){
        $('#exampleModalCenter').modal(options)
    }

    function AddReligion(){
        $("#religionAddModal").modal('show');
    }

    function AddRace(){
        $("#raceAddModal").modal('show');
    }

    function AddCountry(){
        $("#countryAddModal").modal('show');
    }

    function checkNationality()
    {
        var passport = document.getElementById('passport');
        var nationality = document.getElementById('nationality_id');
        var nation = document.getElementById('nation_id');
        
        if (nation.innerHTML == nationality.value){
            passport.style.visibility = 'hidden';
            toggle('ic-expiry', 'none');
        }else{
            passport.style.visibility = 'visible';
            toggle('ic-expiry', 'block');
        }
    }

    function toggle(className, displayState){
        var elements = document.getElementsByClassName(className)

        for (var i = 0; i < elements.length; i++){
            elements[i].style.display = displayState;
        }
    }   
</script>

</x-layout>