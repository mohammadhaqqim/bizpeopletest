<x-layout>

    <style>
        .image{
          display: block;
          width: 100%;
          height: auto;
        }
      
        .overlay{
          position: absolute;
          top: 0px;
          left: 0;
          height: 100%;
          width: 100%;
          opacity: 0;
          background-color: white;
        }

        .avatar-profile-image{            
            width: 240px;
            height: 240px;
            border: 2px solid lightgrey;
        }

        .avatar-profile-image-a{            
            width: 140px;
            height: 140px;
            border: 2px solid blue;
        }

        .avatar-profile-image:hover .overlay {
            opacity: 1;
        }  

        .hide{
            opacity: 0;
        }
    </style>

    <div class="card"style="width: 550px;" >
        <div class="card-header">
          
            <div class="col-6 my-auto">
                <h4 class="my-auto"> <span class="fa fa-camera mr-2"></span> Manage Avatar Photo</h4> 
            </div>

        </div>

        <form method="POST" action="/employee/avatar/{{ $employee->id }}">

        <div class="card-body">
            <div class="row mx-2">
                <div class="col-8">
                    <div class="row">
                        <div class="position-relative bg-white profile-image rounded text-center mx-auto overflow-hidden avatar-profile-image" style="z-index: 99;">
                            <div class="d-flex align-items-center">
                                <img src="{{ is_null($employee->avatar) ? '' : "/".$employee->avatar }}" alt="" id="profile_pic" class="image" >
                            </div>

                            <div class="d-flex overlay text-center " >
                                <a href="" class="d-flex icon w-100 align-items-center justify-content-center" onclick="loadAvatar()">
                                <i class="fa fa-camera text-dark" id="select_logo" style="font-size: 25px;"></i>
                                </a>
                            </div>                      
                        </div>
                    </div>
               
                </div>
                <div class="col-4">
                    <div class="row border-bottom mb-3">
                        <label for="" class="mb-1">Preview</label>
                    </div>
                    <div class="position-relative bg-white profile-image rounded text-center mx-auto overflow-hidden avatar-profile-image-a" style="z-index: 99;">

                        <div class="d-flex align-items-center">
                            <img src="{{ is_null($employee->avatar) ? '' : "/".$employee->avatar }}" alt="" id="profile_pic_preview" class="image" >
                        </div>
                      
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-8 text-center my-3">
                    <button class="btn btn-outline-secondary" type="button" onclick="loadAvatar()">Choose Image</button>
                </div>
                <div class="col-4">

                </div>
            </div>

            <div class="row">
                <input type="file" id="avatar" name="avatar" onchange="loadFile(event)" 
                    accept=".jpg, .jpeg, .png, .gif" hidden>
            </div>

            <div class="row mx-5">
                <button type="submit" class="btn btn-success mx-3">Save</button>
                <a href="" class="my-auto">Cancel</a>
            </div>
        </div>
        </form>

    </div>




<script>
    $("#select_logo").click(function(e){
       e.preventDefault();
       $("#avatar").trigger('click');
    });


    var loadFile = function(event) {
        var output = document.getElementById('profile_pic');
        //alert(event.target.files[0]);
        output.src = URL.createObjectURL(event.target.files[0]);
        var outputPreview = document.getElementById('profile_pic_preview');
        outputPreview.src = URL.createObjectURL(event.target.files[0]);
        
        output.onload = function(e) {     
            URL.revokeObjectURL(output.src); // free memory
        }
    };

    function loadAvatar(){
      var fileInput = document.getElementById('avatar');
      //alert(fileInput);
      if(fileInput) {
          fileInput.click();
      }
    }

</script>

</x-layout>