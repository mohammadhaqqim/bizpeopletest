<x-layout>

    <style>

    </style>

    <h1>Create Avatar</h1>

    <p>hello world</p>

    <div class="form-group row mb-2">
        <div class="col-md-12 text-center">
            <label for="" class="text-center text-muted">{{ __('Load An Image') }}</label>
        </div>
        <div class="position-relative bg-white profile-image rounded text-center mx-auto overflow-hidden" style="z-index: 99;">
            <div class="d-flex align-items-center">
                <img src="" alt="" id="profile_pic" class="image " style="width: 200px;">
            </div>

            <div class="overlay profile-image" >
                <label for="building_image" class="btn btn-default row h-100 w-100">
                    <span class="fa fa-camera text-dark d-flex h-100 align-items-center justify-content-center" id="select_logo" style="font-size: 25px;"></span>
                </label>
            </div>
        </div>
    </div>
    <div class="form-group row mb-2">
        <div class="col">
            <label for="building_image" class="col-md-12 col-form-label text-muted pb-1 px-md-0">Add an Picture/Document</label>
            <input type="file" id="avatar" name="avatar" class="form-control" onchange="loadFile(event)" accept=".jpg, .jpeg, .png, .gif" >
        </div>
    </div>

<div class="row">

    <div class="card" id="editAvatar" class="avatar my-auto">
        <div class="card-header">
          <h4 class="mb-0">Edit Avatar</h4>
        </div>
        <div class="card-body">
            <div class="position-relative bg-white profile-image rounded text-center mx-auto overflow-hidden" style="z-index: 99;">
                <div class="d-flex align-items-center">
                    <img src="" alt="" id="profile_pic" class="image " style="width: 200px; height: 200px;">
                </div>
            </div>


{{--             <div class="position-relative bg-white profile-image rounded text-center mx-auto overflow-hidden" style="z-index: 99;">
                <div class="d-flex align-items-center">
                    <img src="" alt="" id="profile_pic" class="image " style="width: 200px; height: 200px;">
                    <div class="overlay profile-image" >
                        <label for="building_image" class="btn btn-default row h-100 w-100">
                            <span class="fa fa-camera text-dark d-flex h-100 align-items-center justify-content-center" id="select_logo" style="font-size: 25px;"></span>
                        </label>
                    </div>
                </div>
    
                <div class="position-relative bg-white profile-image rounded text-center mx-auto overflow-hidden" style="z-index: 99;">
                    <img src="" alt="" id="profile_pic" class="image" style="width: 200px; height: 200px;">
                    <div class="overlay profile-image" >
                        <label for="building_image" class="btn btn-default row h-100 w-100">
                            <span class="fa fa-camera text-dark d-flex h-100 align-items-center justify-content-center" id="select_logo" style="font-size: 25px;"></span>
                        </label>
                    </div>
                </div>

                <div class="overlay profile-image" >
                    <label for="building_image" class="btn btn-default row h-100 w-100">
                        <span class="fa fa-camera text-dark d-flex h-100 align-items-center justify-content-center" id="select_logo" style="font-size: 25px;"></span>
                    </label>
                </div>
            </div>
    
 --}}
            <h5 class="card-title">Special title treatment</h5>
          <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
          <a href="#" class="btn btn-sm btn-primary">Change Avatar</a>
        </div>
    </div>
</div>


<script>
    
    var loadFile = function(event) {
        var output = document.getElementById('profile_pic');
        output.src = URL.createObjectURL(event.target.files[0]);

        output.onload = function(e) {     
            URL.revokeObjectURL(output.src); // free memory
        }
    };

</script>

</x-layout>