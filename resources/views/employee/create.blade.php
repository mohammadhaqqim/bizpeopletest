<x-layout>

<style>

    .aaa{
        background-color: #E9ECEF !important;
        border-radius: 0px 4px 4px 0px !important;
    }

</style>

<div class="container">
    <form method="POST" action="/employee">
        @csrf

        <div class="row border-bottom my-3 pb-3 biz-title">
            <i class="fa fa-user-circle my-auto px-2" data-toggle="tooltip" title = "Logout"></i> 
            <h2 class="mb-0">New Employee </h2>
        </div>
        
        @if(count($errors) > 0)
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                        {{ $error }}<br>
                @endforeach
            </div>
        @endif

        <div class="biz-section mb-3 border-bottom ">
            <div class="row pl-3">
                <h4>Basic Information </h4>
            </div>
            <div class="row pl-3">
                <div class="form-group col-12 col-sm-4 col-lg-3 pl-0">
                    <label for="employee_code" class="mb-0 text-secondary">Employee Code</label>
                    <input type="text" class="form-control" id="employee_code" name="employee_code" 
                        placeholder="Your Unique ID" maxlength="20" value="{{ old('employee_code') }}">
                    <small class="text-secondary">* Required to be unique</small>
                </div>                        
            </div>
            <div class="row pl-3">
                <div class="form-group col-12 col-sm-8 col-lg-6 pl-0">
                    <label for="name">Employee Full Name</label><label class="text-danger"> *</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Employee name" 
                        value="{{ old('name') }}" required>
                </div>
                <div class="form-group col-12 col-sm-4 col-lg-3 pl-0">
                    <label for="preferred_name">Preferred Name</label>
                    <input type="text" class="form-control" id="preferred_name" name="preferred_name" 
                        placeholder="Preferred name" maxlength="50" value="{{ old('preferred_name') }}">
                </div>
            </div>
            <div class="row pl-3">
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="date_of_birth">Date of Birth</label><label class="text-danger"> *</label>
                    <div class="input-group">
                        <input type="date" class="form-control" id="date_of_birth" name="date_of_birth" 
                            value="{{ old('date_of_birth') }}" required>
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="gender_id">Gender</label>
                    <select name="gender_id" id="gender_id" class="custom-select">
                        <option {{ old('gender_id', null) !== null ? 'selected' : '' }}>Choose..</option>

                    @foreach ($genders as $gender)
                        <option value="{{ $gender->id }}" {{ old('gender_id') == "$gender->id" ? 'selected' : '' }}>{{ $gender->title }}</option>
                    @endforeach

                    </select>
                </div>
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="nationality_id">Nationality</label><label class="text-danger"> *</label>
                    <select name="nationality_id" id="nationality_id" class="custom-select" onchange="checkNationality()" required>
                        <option {{ is_null(old('nationality_id')) ? 'selected' : '' }} selected>Choose..</option>

                    @foreach ($countries as $country)
                        <option value="{{ $country->id }}" {{ old('nationality_id') == "$country->id" ? 'selected' : '' }}>{{ $country->nationality }}</option>
                    @endforeach

                    </select>

                </div>            
                <label for="" id="nation_id" hidden>{{ Session::get('address_country_id') }}</label>
            </div>
            <div class="row pl-3">
                <div class="form-group col-12 col-sm-6 col-md-4 col-lg-3 pl-0">
                    <label for="religion_id">Religion</label><label class="text-danger"> *</label>
                    <select name="religion_id" id="religion_id" class="custom-select" required>
                        <option {{ is_null(old('religion_id')) ? 'selected' : '' }} selected>Choose..</option>

                    @foreach ($religions as $religion)
                        <option value="{{ $religion->id }}" {{ old('religion_id') == "$religion->id" ? 'selected' : '' }}>{{ $religion->title }}</option>
                    @endforeach

                    </select>
                </div>
                <div class="form-group col-12 col-sm-6 col-md-4 col-lg-3 pl-0">
                    <label for="race_id">Race</label>
                    <select name="race_id" id="race_id" class="custom-select">
                        <option {{ is_null(old('race_id')) ? 'selected' : '' }} selected>Choose..</option>
    
                    @foreach ($races as $race)
                        <option value="{{ $race->id }}" {{ old('race_id') == "$race->id" ? 'selected' : '' }}>{{ $race->title }}</option>
                    @endforeach
    
                    </select>
                </div>
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="marital_status_id">Marital Status</label><label class="text-danger"> *</label>
                    <select name="marital_status_id" id="marital_status_id" class="custom-select">
                        <option {{ is_null(old('marital_status_id')) ? 'selected' : '' }} selected>Choose..</option>

                    @foreach ($maritalStatus as $status)
                        <option value="{{ $status->id }}" {{ old('marital_status_id') == "$status->id" ? 'selected' : '' }}>{{ $status->title }}</option>
                    @endforeach

                    </select>
                </div>   
            </div>
        </div>            

        <div class="biz-section mb-3 border-bottom ">
            <div class="row pl-3">
                <h4>Identification Information </h4>
            </div>
            <div class="row pl-3">
                <div class="form-group col-sm-4 col-lg-3 pl-0">
                    <label for="ic_number">IC Number</label><label class="text-danger"> *</label>
                    <input type="text" class="form-control" id="ic_number" name="ic_number" placeholder="IC Number" 
                        value="{{ old('ic_number') }}" required>
                </div>
                <div class="form-group col-sm-4 col-lg-3 pl-0  ic-expiry">
                    <label for="preferred_name">IC Expiry</label>
                    <div class="input-group">
                        <input type="date" class="form-control" id="ic_expiry" name="ic_expiry" 
                            value="{{ old('ic_expiry') }}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="ic_color_id">IC Color</label><label class="text-danger"> *</label>
                    <select name="ic_color_id" id="ic_color_id" class="custom-select">
                        <option {{ is_null(old('ic_color_id')) ? 'selected' : '' }} selected>Choose..</option>

                    @foreach ($icColors as $color)
                        <option value="{{ $color->id }}" {{ old('ic_color_id') == "$color->id" ? 'selected' : '' }}>{{ $color->title }}</option>
                    @endforeach

                    </select>
                </div>                   
            </div>
            <div class="row pl-3" id="passport">
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="passport_number">Passport No</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="passport_number" name="passport_number" value="{{ old('passport_number') }}" >
                    </div>
                </div>
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="passport_expiry">Passport Expiry</label>
                    <div class="input-group">
                        <input type="date" class="form-control" id="passport_expiry" name="passport_expiry" value="{{ old('passport_expiry') }}" >
                    </div>
                </div>
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="passport_place_issued">Place Issued</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="passport_place_issued" name="passport_place_issued" value="{{ old('passport_place_issued') }}" >
                    </div>
                </div>
            </div>

        </div>

        <div class="biz-section mb-3 border-bottom ">
            <div class="row pl-3">
                <h4>Employment Information</h4>
            </div>       
            <div class="row pl-3">
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="date_joined">Date Joined</label><label class="text-danger"> *</label>
                    <div class="input-group">
                        <input type="date" class="form-control" id="date_joined" name="date_joined" placeholder="dd/mm/yyyy" value="{{ old('date_joined') }}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="date_joined">Employment Status</label><label class="text-danger"> *</label>
                    <select name="employment_status_id" id="employment_status_id" class="custom-select">
                        <option {{ old('employment_status_id', null) !== null ? 'selected' : '' }}>Choose..</option>
                    @foreach ($employmentStatuses as $employmentStatus)
                        <option value="{{ $employmentStatus->id }}" {{ old('employment_status_id') == "$employmentStatus->id" ? 'selected' : '' }}>{{ $employmentStatus->title }}</option>
                    @endforeach
                    </select>
                </div>         
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for=""></label>
                    <div class="custom-control custom-checkbox mr-sm-2 ">
                        <input type="checkbox" class="custom-control-input" name="driving_license" id="driving_license">
                        <label class="custom-control-label" for="driving_license">Driving License</label>
                    </div>                    
                </div>  
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="date_joined">Work Permit Expiry</label>
                    <div class="input-group">
                        <input type="date" class="form-control" id="work_permit_expiry" name="work_permit_expiry" placeholder="dd/mm/yyyy" value="{{ old('date_joined') }}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="biz-section mb-3 border-bottom ">
            <div class="row pl-3">
                <h4>Job Information</h4>
            </div>
            <div class="row pl-3">
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="designation_id">Designation (Job Title)</label><label class="text-danger"> *</label>
                    <select name="designation_id" id="designation_id" class="custom-select" onchange="checkJobSelection(this)" required>
                        <option value="-2" {{ old('designation_id', null) !== null ? 'selected' : '' }}>Choose..</option>                            
                        <option value="-1" class="text-white bg-info ">+ Add New</option>
                    @foreach ($designations as $designation)
                        <option value="{{ $designation->id }}" {{ old('designation_id') == "$designation->id" ? 'selected' : '' }}>{{ $designation->title }}</option>
                    @endforeach
                    </select>
                </div>

                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="department_id">Department</label><label class="text-danger"> *</label>
                    <select name="department_id" id="department_id" class="custom-select" onchange="checkDepartmentSelection(this)" required>
                        <option value="-2" selected>Choose..</option>
                        <option value="-1" class="text-primary">+ Add New</option>
                    @foreach ($departments as $department)
                        <option value="{{ $department->id }}" {{ old('department_id') == "$department->id" ? 'selected' : '' }}>{{ $department->title }}</option>                                
                    @endforeach

                    </select>
                </div>

                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="report_to_id">Reports To</label>
                    <select name="report_to_id" id="report_to_id" class="custom-select">
                        <option selected>Choose..</option>
                    @foreach ($managers as $manager)                        
                        <option value="{{ $manager->id }}">{{ $manager->name }}</option>
                    @endforeach 
                    </select>
                </div>             
            </div>

        </div>

        <div class="biz-section mb-3 border-bottom ">
            <div class="row pl-3">
                <h4>Compensation</h4>
            </div>
            <div class="row pl-3">
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="basic_salary">Basic Salary </label>
                    <div class="input-group ">
                        <input type="number" id="basic_salary" name="basic_salary" class="form-control text-right" 
                            min="0.01" step="0.01" value="0.00" value="{{ old('basic_salary') }}">
                        <div class="input-group-append" style="width: 80px; ">
                            <select name="currency_id" id="currency_id" class="custom-select aaa" >
                            @foreach ($countries as $country)
                                <option value="{{ $country->id }}" {{ Session::get('address_country_id') == $country->id ? 'selected' : '' }}>{{ $country->currency_code }}</option>
                            @endforeach
                            </select>
                        </div>
                    </div>    
                </div>
                <div class="form-group pl-0 text-center">
                    <label for="" class="mt-3 pt-3 ">per </label><label class="text-danger"> *</label>
                </div>
                <div class="form-group col-2 pl-0 pt-3 ml-2">
                    <select name="payment_type_id" id="payment_type_id" class="custom-select mt-1" >
                        <option value="-2" selected>Choose..</option>
                    @foreach ($paymentTypes as $paytype)
                        <option value="{{ $paytype->id }}" {{ old('payment_type_id') == $paytype->id ? 'selected' : '' }}>{{ $paytype->title }}</option>
                    @endforeach              

                    </select>            
                </div>
            </div>    
            
            <div class="row pl-3">
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="payment_mode_id">Payment Mode </label><label class="text-danger"> *</label>
                    <select name="payment_mode_id" id="payment_mode_id" class="custom-select" onchange="checkPaymentMode(this)">
                        <option value="-2" selected>Choose..</option>
                    @foreach ($paymentmodes as $paymentmode)
                        <option value="{{ $paymentmode->id }}" {{ old('payment_mode_id') == $paymentmode->id ? 'selected' : '' }}>{{ $paymentmode->title }}</option>
                    @endforeach  

                    </select> 
                </div>

                <div id="id_bank" class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="bank_id">Bank </label>
                    <select name="bank_id" id="bank_id" class="custom-select" >
                        <option value="0" selected>Choose..</option>
                    @foreach ($banks as $bank)
                        <option value="{{ $bank->id }}" {{ old('bank_id') == $bank->id ? 'selected' : '' }}>{{ $bank->title }}</option>
                    @endforeach  

                    </select> 
                </div>

                <div id="account_bank" class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <label for="bank_account">Bank Account </label><label class="text-danger">
                    <input type="text" class="form-control" id="bank_account" name="bank_account" placeholder="Account No." value="{{ old('bank_account') }}" >
                </div>

            </div>
        </div>

        <div class="biz-section mb-3 border-bottom ">
            <div class="row pl-3">
                <h4>Address</h4>
            </div>
            
            <div class="row pl-3">
                <div class="form-group col-5 col-md-6 col-lg-3 pl-0 mb-2">
                    <input type="text" class="form-control" id="address1" name="address1" placeholder="Address 1" value="{{ old('address1') }}" >
                </div>
            </div>
        
            <div class="row pl-3">
                <div class="form-group col-5 col-md-6 col-lg-3 pl-0 mb-2">
                    <input type="text" class="form-control" id="address2" name="address2" value="{{ old('address2') }}" placeholder="Address 2">
                </div>
            </div>

            <div class="row pl-3">
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <input type="text" class="form-control" id="city" name="city" placeholder="Kampong" value="{{ old('city') }}" >
                </div>
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <input type="text" class="form-control" id="area" name="area" placeholder="District" value="{{ old('area') }}">
                </div>
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <input type="text" class="form-control" id="postcode" name="postcode" placeholder="Postcode" value="{{ old('postcode') }}" >
                </div>
            </div>

            <div class="row pl-3">
                <div class="form-group col-12 col-md-4 col-lg-3 pl-0">
                    <select name="address_country_id" id="address_country_id"  name="address_country_id" class="custom-select" >
                    @foreach ($countries as $country)
                        <option value="{{ $country->id }}" {{ Session::get('address_country_id') == $country->id ? 'selected' : '' }}>{{ $country->country }}</option>
                    @endforeach

                    </select>
                </div>           
            </div>
        </div>

        <div class="biz-section mb-3 border-bottom ">
            <div class="row pl-3">
                <h4>Contact</h4>
            </div>
            <div class="row pl-3 form-group">
                <label for="">Phone - one required</label>
            </div>
            <div class="row pl-3">
                <div class="form-group col-6 col-md-3 col-lg-3 pl-0">
                    <div class="input-group ">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-building"></i></span>
                        </div>
                        <input type="text" id="work_phone" name="work_phone" class="form-control" placeholder="Work Number" 
                            aria-label="Work Number" aria-describedby="basic-addon1" value="{{ old('work_phone') }}">
                    </div>
                </div>
                <div class="form-group col-6 col-md-3 col-lg-3 pl-0">
                    <div class="input-group ">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-mobile"></i></span>
                        </div>
                        <input type="text" class="form-control" id="hand_phone" name="hand_phone" placeholder="Mobile Number" value="{{ old('hand_phone') }}">
                    </div>
                </div>
                <div class="form-group col-6 col-md-3 col-lg-3 pl-0">
                    <div class="input-group ">
                        <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-home"></i></span>
                        </div>
                        <input type="text" class="form-control" id="home_phone" name="home_phone" placeholder="Home Number" value="{{ old('home_phone') }}">
                    </div>
                </div>
            </div>
            <div class="row pl-3 form-group">
                <label for="">Email - one required</label>
            </div>        
            <div class="row pl-3">
                <div class="form-group col-6 pl-0 mb-0">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-building"></i></span>
                        </div>
                        <input type="text" id="work_email" name="work_email" class="form-control" placeholder="Work Email" 
                            aria-label="Work Email" aria-describedby="basic-addon1" value="{{ old('work_email') }}">
                    </div>
                </div>

                <div class="form-group col-6 pl-0 mb-2">
                    <div class="input-group mb-2">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-home"></i></span>
                        </div>
                        <input type="text" class="form-control" id="home_email" name="home_email" placeholder="Home Email" value="{{ old('home_email') }}">
                    </div>
                </div>
            </div>
        
        </div>
        
        <div class="biz-section mb-3 border-bottom mb-5">
            <div class="row pl-3">
                <h4>Self-service Access</h4>
            </div>
            <label for="">Self service allows the user to interact with the system though items such as appling for leave or seaching the company directory.</label>
            <div class="custom-control custom-checkbox mb-4">
                <div class="custom-control custom-checkbox mr-sm-2">
                    <input type="checkbox" class="custom-control-input" id="self_service">
                    <label class="custom-control-label" for="self_service">Check this to provide basic employee access</label>
                </div>

            </div>
        </div>
        <div class="biz-section mb-3 border-bottom mb-3">
            </div>
        </div>
        
        <nav class="px-3 bg-light-biz fixed-bottom border-top" >
            <div class="container py-3 text-right">
                <button type="submit" class="btn btn-success">Save</button>
                <button type="button" class="btn btn-danger">Cancel</button>
            </div>
        </nav>

    </form>

</div>

<script>

    // when the tab is clicked on then refresh page
    // in event on new designation / department added
    document.addEventListener('visibilitychange', function(ev) {
        if( `${document.visibilityState}` == 'visible'){
            console.log(`Tab state : ${document.visibilityState}`);
            location.reload();
        } 
    });

    function AddNewVisa(){
        $('#exampleModalCenter').modal(options)
    }

    function checkJobSelection(a){
        var x = a.selectedIndex; 

        if (a.value == -1){
            //open new tab to add new deisgnation
            a.value = '-2';
            var win = window.open( "{{ route('designations.create') }}", '_blank');
            win.focus();
        }        

    }

    function checkDepartmentSelection(a){
        var x = a.selectedIndex; 

        if (a.value == -1){
            a.value = '-2';
            var win = window.open( "{{ route('departments.create') }}", '_blank');
            win.focus();

        }
    }


    function checkPaymentMode(a){
        var x = a.selectedIndex;

        var txt = a.options[a.selectedIndex].text;

        var id = document.getElementById("id_bank");
        var account = document.getElementById("account_bank");

        if(txt == "Bank Transfer")
        {
            id.style.display = "block";
            account.style.display = "block";
        }else{
            id.style.display = "none";
            account.style.display = "none";
        }
    }

    function checkNationality()
    {
        var passport = document.getElementById('passport');
        var nationality = document.getElementById('nationality_id');
        var nation = document.getElementById('nation_id');
        
        if (nation.innerHTML == nationality.value){
            passport.style.visibility = 'hidden';
            toggle('ic-expiry', 'none');
        }else{
            passport.style.visibility = 'visible';
            toggle('ic-expiry', 'block');
        }
    }

    function toggle(className, displayState){
        var elements = document.getElementsByClassName(className)

        for (var i = 0; i < elements.length; i++){
            elements[i].style.display = displayState;
        }
    }   
</script>


</x-layout>
