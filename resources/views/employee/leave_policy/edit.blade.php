<x-layout>

    {{--     @include('components.employee_header')  --}}
    <style>
        .card-body {
            height: 150px;
            min-width: 200px;
            margin-right: 5px;
        }
    
        .myDIV:hover .hidden{
            display: block;
            top: 0px;
            left: 0px;        
            background-color: white;
        }
    
    
    
        .hidden{
            display: none;
        }
    </style>    
      <div class="main-content mt-0">
        
          {{-- @include('components.employee_aside')
     --}}
    
    {{--     <div class="mt-sm-3 my-md-3 my-lg-4 mt-4 mb-4 pb-0 page-title position-relative" >
            <input type="file" id="avatar" hidden>
     --}}        
    
        <div class="main pr-3">
        <form method="POST" id="editLeave" action="/employee/{{ $employee->id }}/leavepolicy/{{ $leavetype->id }}">
            @method('PATCH')
            @csrf
        
            <div class="row border-bottom my-3 pb-3 biz-title">
                <i class="fa fa-suitcase my-auto px-2"></i> 
                <h2 class="mb-0">Allocate Staff Leave Policy - {{ $leavetype->title }}</h2>
            </div>
    
            <div class="row ">
                <h4 class="pb-0 mb-0 d-block">{{ $employee->name }}</h4>
            </div>
                @if (!is_null($employee->currentjob))
            <div class="row">
                <label class="d-block">{{ $employee->getjob()->title }}</label>
                <small class="mt-1 ml-1 text-muted">Hired - {{ $employee->date_joined }}</small>
            </div>
                @endif
    
            <div class="col-sm-6 col-md-6 col-lg-4 mt-2">
                <label for="track_time" class="mb-1 font-weight-bold">Leave Accrual Policy</label>
                <select name="leave_policy_id" id="leave_policy_id" class="custom-select leaveTrack">
                    <option>-- Choose --</option>
                    @foreach ( $leavePolicies as $policy)
                    
                    @if(is_null($currentPolicy))
                    <option value="{{ $policy->id }}">{{ $policy->policy_name }}</option>                    
                    @else
                    <option value="{{ $policy->id }}" {{ $policy->id == $currentPolicy->leave_policy_id ? 'selected' : '' }}>{{ $policy->policy_name }}</option>                    
                    @endif
    
                    @endforeach
                </select>
    
                @if(!is_null($currentPolicy))            
                <small class="mt-1 ml-1 text-muted">Current Policy: {{ $currentPolicy->LeavePolicy->policy_name }}</small>
                @endif
            </div>
    
            <div class="col-sm-6 col-md-6 col-lg-4 mt-2">
                <label for="track_time" class="mb-1 font-weight-bold">Accrual Start Sate</label>
                <div class="input-group">
                    <input type="date" class="form-control" id="start_from_date" name="start_from_date" placeholder="dd/mm/yyyy" 
                        value="{{ is_null($currentPolicy) ? date('Y-m-d', strtotime('0 day')) : $currentPolicy->start_from_date }}" min="{{ $employee->date_joined }}" > 
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>     
                @if(!is_null($currentPolicy))                           
                <small class="mt-1 ml-1 text-muted">Current Policy: {{ $currentPolicy->start_from_date }}</small>
                @endif
            </div>
    
            <button type="submit">Save</button>
        </form>
        </div>
    
      </div>
    
    <script>
        function AllocatePolicy(id)
        {
    
        }
    
        </script>
    </x-layout>