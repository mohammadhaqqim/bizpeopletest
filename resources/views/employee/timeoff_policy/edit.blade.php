<x-layout>

    <style>
        .card-body {
            min-height: 150px;
            min-width: 200px;
            margin-right: 5px;
        }
    </style>    
        <div class="main-content mt-0">
        <form method="POST" action="/timeoffpolicy/{{ $policy->id }}">
            @csrf
            @method('PATCH')
    
            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $error)
                            {{ $error }}<br>
                    @endforeach
                </div>
            @endif
    
            <div class="main pr-3">
    
                <div class="row border-bottom my-3 pb-3 biz-title">
                    <i class="fa fa-suitcase my-auto px-2"></i> 
                    <h2 class="mb-0">Edit Time Off Policy</h2>
                    <label for="" id="first_period_date">{{ session()->get('first_period_date') }}</label> 
                </div>
    
                <div class="row pl-3">
                    <div class="form-group col-3 pl-0">
                        <label class="mb-0 text-secondary" for="policy_name">Policy Name <label class="text-danger"> *</label></label>
                        <input type="text" class="form-control" id="policy_name" name="policy_name" placeholder="eg Medical Leave" style="width: 200px;" 
                            required maxlength="100" value="{{ $policy->policy_name }}">
                    </div>            
                </div>
    
                <div class="row pl-3">
                    <div class="form-group col-3 pl-0">
                        <label class="mb-1 text-secondary d-block" for="leave_type_id">Leave Type <label class="text-danger"> *</label></label>
                        <select name="leave_type_id" id="leave_type_id" class="custom-select" onchange="LeaveTypeChanged()" style="width: 250px;" required>
                            <option>Choose..</option>
                        @foreach( $leavetypes as $leave)
                            <option value="{{ $leave->id }}" data-time="{{ $leave->track_time }}" {{ $policy->leave_type_id == $leave->id ? 'selected' : '' }} >{{ $leave->title }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
        
                <div class="col-12 mt-2 pt-2 pl-2 border-top">
                    <label class="font-weight-bold" for="title">Accural Options</label>
                </div>
    
                <div class="row pl-3">
                    <div class="form-group col pl-0">
                        <label class="mb-1 text-secondary" for="yearly_amount">Accrual Amount - Year <label class="text-danger"> *</label></label>
                        <input type="number" class="form-control" id="yearly_amount" name="yearly_amount" step="1" style="width: 120px;" value="{{ $policy->yearly_amount }}"">  
                    </div>        
                </div>
    
                <div class="row pl-3">
                    <div class="form-group col pl-0">
                        <label class="mb-1 text-secondary col-12 px-0" for="">Carryover Type <label class="text-danger"> *</label></label>
                        <select name="carryover_type" id="carryover_type" class="custom-select d-inline float-left" style="width: 120px;" onchange="CarryoverAmountChange(this)" required>
                            <option>Choose..</option>
                            <option value="n" {{ $policy->carryover_type == "n" ? 'selected' : '' }} >None</option>
                            <option value="m" {{ $policy->carryover_type == "m" ? 'selected' : '' }} >Max No.</option>
                            <option value="u" {{ $policy->carryover_type == "u" ? 'selected' : '' }} >Unlimited</option>
                        </select>
                        <input type="number" class="form-control pt-1 float-left ml-2" id="max_carryover" name="max_carryover" step="1" 
                            style="width: 100px;" value="{{ $policy->max_carryover }}">
                    </div>
                </div>
    
                <div class="row pl-3">
                    <div class="form-group col-12 pl-0">
                        <label class="mb-0 text-secondary d-block" for="accrual">First Accural <label class="text-danger"> *</label></label>
                        <select name="first_accrual" id="first_accrual" class="custom-select" onchange="AccrualChanged(this)" style="width: 200px;" required>
                            <option>Choose..</option>
                            <option value="p" {{ $policy->first_accrual == "p" ? 'selected' : '' }} >Prorate</option>
                            <option value="f" {{ $policy->first_accrual == "f" ? 'selected' : '' }}>Full Amount</option>
                        </select>
                        <div id="accrual_p">
                            <small class=" d-block" >Employee accrues for part of the period</small>
                        </div>
                        <div id="accrual_f">
                            <small class="" >Employee gets the full amount of timeoff for the accrual period </small>
                        </div>
                    </div>
                </div>
                
                <div class="row pl-3">
                    <div class="form-group col-12 pl-0">
                        <label class="mb-0 text-secondary d-block" for="carryover">Carryover Date <label class="text-danger"> *</label></label>
                        <select name="carryover" id="carryover" class="custom-select" onchange="CarryoverDateChanged(this)" style="width: 200px;" required>
                            <option>Choose..</option>
                            <option value="j" {{ $policy->carryover == "j" ? 'selected' : '' }}>1st of January</option>
                            <option value="f" {{ $policy->carryover == "f" ? 'selected' : '' }}>Start Financial Year</option>
                            <option value="e" {{ $policy->carryover == "e" ? 'selected' : '' }}>Employee hire date</option>
                            <option value="o" {{ $policy->carryover == "o" ? 'selected' : '' }}>Other</option>
                        </select>            
                    </div>
                    <div id="carry_over" class="col-12 carry px-0 mb-3">
                        <p class="d-inline">on the</p> 
                        <select name="carryover_day_id" id="carryover_day_id" class="custom-select d-inline" style="width: 100px;" >
                            @for ($i = 1; $i < 32; $i++)
                                <option value="{{ $i }}" {{ $policy->carryover_day_id == $i ? 'selected' : '' }}>{{ $i }}</option>                  
                            @endfor
                        </select>   
                        <p class="d-inline">day of </p>         
                        <select name="carryover_month_id" id="carryover_month_id" class="custom-select d-inline" style="width: 200px;" >
                            <option value="0" {{ $policy->carryover_month_id == 0 ? 'selected' : '' }}>January</option>
                            <option value="1" {{ $policy->carryover_month_id == 1 ? 'selected' : '' }}>February</option>
                            <option value="2" {{ $policy->carryover_month_id == 2 ? 'selected' : '' }}>March</option>
                            <option value="3" {{ $policy->carryover_month_id == 3 ? 'selected' : '' }}>April</option>
                            <option value="4" {{ $policy->carryover_month_id == 4 ? 'selected' : '' }}>May</option>
                            <option value="5" {{ $policy->carryover_month_id == 5 ? 'selected' : '' }}>June</option>
                            <option value="6" {{ $policy->carryover_month_id == 6 ? 'selected' : '' }}>July</option>
                            <option value="7" {{ $policy->carryover_month_id == 7 ? 'selected' : '' }}>August</option>
                            <option value="8" {{ $policy->carryover_month_id == 8 ? 'selected' : '' }}>September</option>
                            <option value="9" {{ $policy->carryover_month_id == 9 ? 'selected' : '' }}>October</option>
                            <option value="10" {{ $policy->carryover_month_id == 10 ? 'selected' : '' }}>November</option>
                            <option value="11" {{ $policy->carryover_month_id == 11 ? 'selected' : '' }}>December</option>
                        </select>   
                    </div>
                </div>
    
                <div class="row pl-3 mb-5 pb-5">
                    <div class="form-group col-3 pl-0">
                        <label class="mb-0 text-secondary d-block" for="accrual_occurs">Accurals Occur At <label class="text-danger"> *</label></label>
                        <select name="accrual_occurs" id="accrual_occurs" class="custom-select" style="width: 200px;" required>
                            <option>Choose..</option>
                            <option value="e" {{ $policy->accrual_occurs == "e" ? 'selected' : '' }}>At the end of the period</option>
                            <option value="b" {{ $policy->accrual_occurs == "b" ? 'selected' : '' }}>At the beginning of the period</option>
                        </select>
                    </div>
                </div>
            </div>
    {{--         <div class="col-8 mb-2 pb-5">
                dfhs
            </div> --}}
            <nav class="px-5 bg-light-biz fixed-bottom border-top" >
                <div class="container py-4 text-right">
                    <button type="button" class="btn btn-danger">Cancel</button>
                    <button type="submit" class="btn btn-success">Save</button>
                </div>
            </nav>
    
        </form> 
     
    
    <script>
        $(document).ready(function() {
            var x = document.getElementById('carry_over');
            x.style.display = 'none';
            var x1 = document.getElementById("accrual_p");
            x1.style.display = 'none';
            var x2 = document.getElementById('accrual_f');
            x2.style.display = 'none';
        });
    
    
        function CarryoverDateChanged(obj)
        {
            //carryover j - jan, f - financail year, e - employee hire date, o -other
            var x = document.getElementById('carryover');
            var co = document.getElementById('carry_over');
            co.style.display = 'none';
    
            var period = document.getElementById('first_period_date').innerHTML;

            if(obj.value == 'j')        
            {
                // set to january 1st
                document.getElementById('carryover_month_id').value = '0';
                document.getElementById('carryover_day_id').value = '1';
    
                return;
            }
    
            if(obj.value == 'f' || obj.value == 'e')        
            {
                var dte = new Date(period);
                document.getElementById('carryover_month_id').value = dte.getMonth().toString();
                document.getElementById('carryover_day_id').value = dte.getDay().toString();
    
                return;
            }
            
            if (obj.value == 'o'){
                co.style.display = 'block';
            } 
    
        }
    
        function LeaveTypeChanged()
        {
            var x = document.getElementById('leave_type_id');
            var i = x.selectedIndex;
    
            var opt = x.item(i);
            if(opt.getAttribute('data-time') == 'd')
            {
                document.getElementById('dayshours').innerHTML = 'days'
                document.getElementById('accrualrate').innerHTML = 'days'
            }
            else{
                document.getElementById('dayshours').innerHTML = 'hours'
                document.getElementById('accrualrate').innerText = 'hours'
            }
    //        alert(opt.getAttribute('data-time'));
        }
    
        function CarryoverAmountChange(obj)
        {
            var max = document.getElementById("max_carryover");
            max.style.display = "none";
    
            if (obj.value == 'm'){
                max.style.display = 'block';
            }
    
        }
    
        function d(obj)
        {
            var xx = document.getElementById('carry_over');
            xx.style.display = 'none';
    
            if (obj.value == 'o'){
                xx.style.display = 'block';
            }
        } 
    
        function AccrualChanged(obj)
        {
    //            document.getElementsByClassName("emp_name_edit")[0].value = oCells.item(2).innerHTML;
    
            var x1 = document.getElementById("accrual_p");
            x1.style.display = 'none';
            var x2 = document.getElementById('accrual_f');
            x2.style.display = 'none';
    
            if (obj.value == 'p'){
                x2.style.display = 'none';
                x1.style.display = 'block';
            }
            if (obj.value == 'f'){
                x1.style.display = 'none';
                x2.style.display = 'block';
            } 
        }
    
            function datediff(){
                var date1 =  new Date(document.getElementById("date_from").value); 
                var date2 =  new Date(document.getElementById("date_to").value); 
     
                var add_day = date1;
    
                // To calculate the time difference of two dates 
                var Difference_In_Time = date2.getTime() - date1.getTime();
    
                // To calculate the no. of days between two dates 
                var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24); 
                //Difference_In_Days += 1;
    
                start = '<div class="row p-2">';
                labelDay = '<label for="" class="pt-2 ml-2" style="width:110px;">';
                inputdate = '</label><input type="date" class="form-control mr-1 date-timeoff" id="date_from" name="date_from" placeholder="dd/mm/yyyy" value="';
                inputhours = '<input type="number" style="width:70px;" class="form-control d-inline" value="';
                label = '<label for="" class="pt-2 pl-2" style="width:70px">Hours</label></div>';
    
                var add_days = "";
                add_day = date1;
    
                var daysweek = ['Sunday', 'Monday', 'Tuesday','Wednesday','Thursday','Friday','Saturday'];
    
                for(i=0; i <= Difference_In_Days; i++)
                {
                    var thedate = add_day.getFullYear() + "-" 
                        + (add_day.getMonth()+1).toString().padStart(2, "0") + "-" 
                        + add_day.getDate().toString().padStart(2, "0");
    
                    add_days += start 
                        + labelDay + daysweek[add_day.getDay()]
                        + inputdate + thedate + '" disabled>' 
                        + inputhours + '8">' 
                        + label;
                    add_day.setDate(add_day.getDate()+1);
                }
    
                document.getElementById('days_off_list').innerHTML 
                    = add_days;
     
            }
    
        </script>
    </x-layout>