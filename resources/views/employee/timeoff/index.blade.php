<x-layout>
    @php($dayoff1 = Session::get('daysoff1'))
    @php($dayoff2 = Session::get('daysoff2'))

    @include('components.employee_header') 
<style>
    .card-body {
        height: 150px;
        min-width: 200px;
        margin-right: 5px;
    }

    .myDIV:hover .hidden{
        display: block;
        top: 0px;
        left: 0px;        
        background-color: white;
    }

    .policyDIV:hover .hidden{
        display: block;    
        background-color: white;
    }

    .hidden{
        display: none;
        
    }

    .biz-btn{
        border: 1px solid #D4D4D4;
    }

    #myTable tr{
        line-height: 40px;
    }
</style>    
    <div class="main-content mt-0">
    
      @include('components.employee_aside')

        <div class="main pr-3">

        <div class="row border-bottom mt-3 pb-3 biz-title">
            <i class="fa fa-suitcase my-auto px-2"></i> 
            <h2 class="mb-0">Schedule Time Off</h2>

            <div class="col text-right text-biz-blue mt-2">
                <a href="/timeoff/employee/{{ $employee->id }}/create" class="">
                    <i class="fa fa-plus-circle px-2" style="font-size: 18px;" ></i> 
                    <label class="text-right">Add Timeoff</label>
                </a>
            </div>
        </div>

    
        <div class="container-fluid py-2 overflow-auto">
            <div class=" d-flex flex-row flex-nowrap">
            @foreach ( $leaveTypes as $leave )
                @if( $leave->LeavePolicies->count() >= 1 )
                <div class="">
                    <div class="mt-3 card card-body justify-content-center position-relative policyDIV" style="max-width: 250px; background-color: #F2F2F2; font-color: ">
                        <div class="row mx-auto font-weight-bolder">
                            <p>
                                <h5 class="text-nowrap mt-1 mb-1">{{ $leave->title }}</h5>
                            </p>
                        </div>

                        <div class="row mx-auto font-weight-bolder">
                            <h3 class="mb-0">
                                @if($leave->paid_time_off)
                                {{ is_null($leave->getCurrentEmployeePolicy($employee->id)) ? 0 
                                    : number_format($leave->getCurrentEmployeePolicy($employee->id)->balance, 1) }}                                    
                                @else 
                                {{ is_null($leave->getCurrentEmployeePolicy($employee->id)) ? 0 
                                    : number_format($leave->getCurrentEmployeePolicy($employee->id)->taken, 1) }}                                    
                                @endif
                            </h3>
                        </div>

                        @if($leave->paid_time_off)
                        <div class="row mx-auto text-uppercase text-secondary">{{ $leave->track_time == 'h' ? 'Hours' : 'Days' }} Available</div>
                        @else 
                        <div class="row mx-auto text-uppercase text-secondary">{{ $leave->track_time == 'h' ? 'Hours' : 'Days' }} Taken</div>
                        @endif

                        <div class="row mx-auto text-secondary">
                            <small>{{ is_null($leave->getCurrentEmployeePolicy($employee->id)) ? 0 : 
                                number_format($leave->getCurrentEmployeePolicy($employee->id)->scheduled, 1) }}
                            {{ $leave->track_time == 'h' ? 'hours' : 'days' }} scheduled</small>
                        </div>

                        <div class="align-items-middle" style="width: 100%; height:100%; ">
                            <div class="position-absolute d-inline-block text-center row ml-0 pl-0" style="top: -17px; width: 100%; left: 0px;">

                            @if( !is_null($leave->getCurrentEmployeePolicy($employee->id) ) )
                               <a class="d-inline-block" href="/employeeleavepolicy/calculatetotals/{{ $leave->getCurrentEmployeePolicy($employee->id)->id }}" 
                                    data-toggle="tooltip" title="Recalculate Totals" data-placement="bottom" style="height: 34px;">
                                    <div class="hidden biz-btn py-1 px-2 mx-1 " style="border-radius: 25px; "> 
                                        <i class="fa fa-calculator" style="font-size: 16px;"></i>
                                    </div>
                                </a>
                            @endif
                            
                            @if( (optional(request()->access())->admin || optional(request()->access())->hr) && !is_null($leave->getCurrentEmployeePolicy($employee->id) ))
                                 <a href="#" class="d-inline-block" data-toggle="tooltip" title="Alter Balance" data-placement="bottom" >
                                    <div class="hidden biz-btn py-1 px-2 text-center mx-1" style="border-radius: 35px; "> 
                                        <i class="fa fa-unsorted " style="font-size: 16px; width: 16px;"></i>
                                    </div>
                                </a>
                            @endif

                            @if( !is_null($leave->getCurrentEmployeePolicy($employee->id) ) )
                                <a href="/timeoff/employee/{{ $employee->id }}/create?policy={{ $leave->id }}" class="d-inline-block" data-toggle="tooltip" title="New Timeoff" data-placement="bottom">
                                    <div class="hidden biz-btn py-1 px-2 mx-1" style="border-radius: 45px;"> 
                                        <i class="fa fa-calendar-plus-o" style="font-size: 16px;"></i>
                                    </div>
                                </a>
                            @endif
                            </div>

                        </div>

                    </div>      
                    <div class="myDIV text-center position-relative mr-1 my-1">
              
                        @if( is_null($leave->getCurrentEmployeePolicy($employee->id)) )
                            <label for="" class=" d-block  font-weight-bold">Not Applied</label>  
                        @else
                            <label for="" class=" d-block  font-weight-bold">{{ $leave->getCurrentEmployeePolicy($employee->id)->LeavePolicy->policy_name }}</label>  
                        @endif
                        
                        @if( optional(request()->access())->admin || optional(request()->access())->hr )
                        <a href="/employee/{{ $employee->id }}/leavepolicy/{{ $leave->id }}" >
                            <label class="hidden position-absolute " style="width: 100%;"> 
                                <i class="fa fa-cog" ></i> Allocate Policy
                            </label>
                        </a>
                        @endif

                     </div>  

                </div>
                @endif
            @endforeach
            </div>
        </div>


        <div class="row border-bottom mt-3 pb-3 biz-title">
            <i class="fa fa-clock-o my-auto px-2 d-block"></i> 
            <h4 class="mb-0 d-inline-block my-auto ">Scheduled Events</h4>
        </div>  
        @foreach ($events as $event)
            <div class="row border-bottom mt-2">
                <div class="my-auto ml-3">
                    <label for="" class="bg-primary text-white p-2">{{ is_null($event->employee_id) ? "PH" : "TO" }}</label>
                </div>
                <div class="col-6">
                    <label for="" class="font-weight-bold col-12 mb-0 p-0">
                        {{ \Carbon\Carbon::parse($event->start_date)->format('j M') }}
                        {{ $event->end_date == $event->start_date ? '' :
                            \Carbon\Carbon::parse($event->end_date)->format('- j M') }}
                    </label>
                    <label for="" class="col-12 text-secondary p-0" style="font-size: 13px;">
                        {{ is_null($event->employee_id) ? $event->title : 
                            $event->LeaveType->title . ' - ' . $event->hours_off . ' hours off'
                        }}
                    </label>
                </div>
                <div class="col my-auto text-right pr-5">

                @if( !is_null($event->employee_id) )
                    <a href="/timeoff/{{ $event->id }}/edit">
                        <i class="fa fa-pencil my-auto px-2 text-primary" style="font--size: 18px;"></i>
                    </a>

                    <form id="delete-timeoff" action="/people/timeoff/{{ $event->id }}" method="post" class="d-inline">
                        @method('DELETE')
                        @csrf
                        
                        <a onclick="confirmDeleteTimeoff()" 
                            href="#">
                            <i class="fa fa-trash my-auto px-2 text-danger" style="font--size: 18px;"></i>    
                        </a>

                    </form>
                @endif
                </div>
            </div>
        @endforeach

        <div class="row border-bottom mt-4 biz-title pb-3">
            <div class="col-sm-8 col-md-9 col-12 my-auto pl-0 d-flex justify-content-between">
                <i class="fa fa-calendar-o my-auto px-2"></i> 
                <h4 class="mb-0 d-inline-block my-auto ">History</h4>

                <select id="leaveFilter" class="custom-select custom-select-sm col-md-4 col-3 ml-3 my-auto" onchange="filterHistory(this)">
                    <option value="0" >-- Select All --</option>
                    @foreach ( $leaveTypes as $leave )
                        @if( $leave->LeavePolicies->count() >= 1 )
                        <option value="{{ $leave->id }}" >{{ $leave->title }}</option>
                        @endif
                    @endforeach
                </select>

                <select id="yearFilter" class="custom-select custom-select-sm col-md-3 col-3 ml-3 my-auto" onchange="filterHistory(this)">
                    {{-- @foreach ( $leaves as $leave )
                        <option value="{{ $leave->id }}" >{{ $leave->title }}</option>
                    @endforeach --}}
                    <option value="2020" >2020</option>
                    <option value="2021" >2021</option>

                </select>

            </div>
            
            <div class="col-sm-4 col-md-3 col-12 px-0 mt-xs-3">
                <input class="form-control form-control-sm col-md-12 col-sm-12 float-right" id="myInput" type="text" placeholder="Search..">
            </div>

        </div>  


        <div class="row mb-5">
            <table id="payrollTable" class="table table-sm border mb-1" >
                <thead class="tbl-dark">
                    <tr>
                        <th class="align-middle" width="10"></th>
                        <th class="align-middle" width="150">Dates</th>
                        <th class="align-middle" width="150">Type</th>
                        <th class="align-middle" width="150">Used (-)</th>
                        <th class="align-middle" width="150">Balance (+)</th>
                        <th class="align-middle d-none d-sm-table-cell" width="300">Status</th>
                        <th class="align-middle" hidden>Start</th>
                        <th class="align-middle" hidden>End</th>
                        <th class="align-middle" hidden>Start Year</th>
                        <th class="align-middle" hidden>End Year</th>

                        <th></th>
                    </tr>
                </thead>
                <tbody id="myTable" style="font-size: 12px;">
                @foreach ($history as $event)
                <tr style="font-size: 13px">
                    <td >
                    </td>
                    <td class="align-middle">
                        <label for="" class="font-weight-bold col-12 mb-0 p-0 mt-1" >

                            @if( is_null($event->approved_by_id) && is_null($event->rejected_by_id) )
                            <a href="/timeoff/{{ $event->id }}/edit">

                            @endif
                                {{ \Carbon\Carbon::parse($event->start_date)->format('j M') }}
                                {{ $event->end_date == $event->start_date ? '' :
                                    \Carbon\Carbon::parse($event->end_date)->format('- j M') }}
                            @if( is_null($event->approved_by_id) && is_null($event->rejected_by_id) )
                                <i class="fa fa-pencil text-primary ml-2" aria-hidden="true"></i>
                            </a>
                            @endif

                        </label>
                    </td>
                    <td class="align-middle">
                        {{ $event->LeaveType->title }}
                    </td>
                    <td class="align-middle">
                        @if($event->hours_off > 0)
                        -{{ number_format($event->hours_off, 2) . ' hours' }}
                        @endif
                    </td>
                    <td class="align-middle">
                        @if($event->hours_off < 0)
                        {{ number_format($event->hours_off, 2)*-1 . ' hours' }}
                        @endif
                    </td>
                    <td class="align-middle d-none d-sm-table-cell">
                        @if(is_null($event->approved_by_id) && is_null($event->rejected_by_id))
                            <h5 class="m-0"><span class="badge badge-info text-white" >In Review</span></h5>
                        @elseif ( $event->approved_at != null )
                            <h5 class="m-0"><span class="badge badge-success">Approved</span></h5>
                            <h6 class="m-0" style="font-size: 10px;">by {{ $event->ApprovedBy->name }}</h6>  
                        @elseif ( !is_null($event->rejected_by_id) )
                            <h5 class="m-0"><span class="badge badge-danger" data-toggle="tooltip" data-placement="bottom" title="by {{ $event->DeniedBy->name }}">Denied</span></h5>
                            <h6 class="m-0" style="font-size: 10px;">{{ $event->deny_comment }}</h6>  
                        @endif
                    </td>
                    <td hidden>
                        {{ date("d-m-Y", strtotime($event->start_date)) }}
                    </td>
                    <td hidden>
                        {{ date("d-m-Y", strtotime($event->end_date)) }}
                    </td>
                    <td hidden>
                        {{ date("Y", strtotime($event->start_date)) }}
                    </td>
                    <td hidden>
                        {{ date("Y", strtotime($event->end_date)) }}
                    </td>
                    <td></td>
                </tr>
                 @endforeach
                </tbody>
            </table>

       </div>            


        
        </div>

    </div>



<script>
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })

  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

  function confirmDeleteTimeoff(){
    if(confirm('Are you sure you would like to delete this leave?')){
        document.getElementById('delete-timeoff').submit();
    }else{
        return false;
    }
  }

  function filterLeave(){
    var value = $('#leaveFilter option:selected').text().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  }

  function filterHistory(element){    
    var value = element.options[element.selectedIndex].text.toLowerCase();

    if (value == '-- select all --'){
        value = '';
        $("#myTable tr").filter(function() {
          $(this).show();
        });

        return;

    }

    $("#myTable tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    }); 
  }

</script>

</x-layout>