<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

        <!-- Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    
        <style>
            .page-title, .page-title-menu{
                 padding-left: 255px; 
            }

            .side-profile{
                width: 255px;
            }

            .side-profile-image{
                top: 20px;
                left: 35px;
                width: 180px;
                height: 180px;
                border: 2px solid blue;
            }

            .side-profile-image i{
                font-size: 120px;
            }

            .bg-biz{
/*                background-color: #41AF4B; */
                background-color: #0074E8;
            }

            .color-biz{
                color: #41AF4B;
            }

            .bg-light-biz{
                background-color: #F1F1F3;
            }

            .nav .nav-item:hover{
                background-color: white;
            }

            .nav-tabs .nav-link:hover{
                color: green !important;
            }

            .siteHeader{
            }

            .side-display, .main-content{
                height: calc(100vh - 210px);
            }

            .detail-content{
                width: calc(100% - 255px);
            }

            .nav-tabs{
                /* border-bottom-color: #41AF4B; */
            }

            .staff-image, .staff-image i{
                width: 175px;
                height: 175px;                
            }

            .staff-image i{

            }

            .header-image{
                width: 255px;
            }

            .biz-footer{
                height: 50px;
            }
        </style>
    </head>
    <body> 
   
        <div class="min-vh-100 d-flex flex-column ">

            <div class="h-100 ">
                <nav class="px-3 siteHeader bg-light-biz shadow">
                    <a class="navbar-brand align-items-stretch side-profile mr-0" href="#">
                        <i class="fa fa-id-badge" style="font-size: 22px;"></i>
                        {{ config('app.companyname', 'Laravel') }}
                    </a>            
                    <div class="navbar-brand mr-auto py-0 align-items-stretch ">
                        <ul class="nav ">
                            <li class="nav-item active">
                                <a class="nav-link  color-biz" href="#">Home <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Profile</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Payroll</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">People</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Reports</a>
                            </li>
                        </ul>
                    </div>
                    <div class="navbar-brand float-right py-0">
                      <ul class="nav">
                        <li class="nav-item ">
                            <a class="nav-link px-2" href="#" data-toggle="tooltip" title = "Logout">
                            <i class="fa fa-power-off  text-center"></i>              
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link px-2" href="#">
                            <i class="fa fa-cog text-center" data-toggle="tooltip" title = "Settings"></i>
                            </a>
                        </li>
                        <li class="nav-item ">
                            <a class="nav-link px-2" href="#">
                            <i class="fa fa-sign-out text-center" data-toggle="tooltip" title = "Logout"></i>              
                            </a>
                        </li>
                      </ul>
                    </div>                
                </nav>
        
                <div class="p-2 bg-biz position-relative" style="height: 164px;">
                    <div class="bg-white side-profile-image position-absolute rounded d-flex align-items-center justify-content-center" style="">
                        <img src="\images\photo_male.png" alt="">
                    </div>
                    <div class=" header-menu h-100">
                        <div class="mt-5 page-title" >
                            <h2 class="pb-4 text-white">Regan Nagorcka</h2>
                        </div>
                        <div class="page-title-menu">
                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                  <a class="nav-link active" href="#">Profile</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link text-white" href="#">Job</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link text-white" href="#">Time Off</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link text-white" href="#">Emergency</a>
                                </li>
                              </ul>
                        </div>
                    </div>
                </div>

                <div class="p-2 main-content">                    
                    <div class=" side-profile float-left h-100 px-2">
                        <div class="flex-column h-100 bg-light-biz px-1 text-center text-secondary pt-5">
                            <i class="fa fa-envelope " data-toggle="tooltip" title = "Logout"></i>
                             regan@bizitsolutions.biz
                        </div>
                    </div>
                    <div class="container-fluid detail-content float-right h-100">
                        this is the details content
                        @yield('content')
                    </div>
                </div>
            </div>
            <div class="fixed-bottom bg-light-biz py-3 " hidden>
                <div class="page-title-menu ">
                    <button class="btn btn-success mx-3">Save Updates</button>
                </div> 
            </div>
        </div>

        <div id='msg'>This message will be replaced using Ajax. 
            Click the button to replace the message.</div>

        <button onClick='getMessage(9)'>Replace Text</button>
        
    </body>

    <script>
        function getMessage(id) {
        //alert('in');
           $.ajax({
              headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
              type:'POST',
              url:'/getmsg',
              data: {
                "_token": "{{ csrf_token() }}",
                "id": id,
                "start_date" : '2020-12-02',
                "end_date" : '2020-12-10'
                },
              success:function(data) {
                 $("#msg").html(data.msg);
                 alert(data.msg);
              }
           });
        }
     </script>
</html>
