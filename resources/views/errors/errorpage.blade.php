<!doctype html>
<html lang='en'>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Biz-People</title>

  <link rel="icon" href="/images/logos/BizLogo.png" type="image/png">

  <link rel="stylesheet" href="{{ asset('css/appbootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('css/appbootstrap.min.css') }}">

  <script src="{{ asset('js/appbootstrap.js') }}"></script>
  <script src="{{ asset('js/appbootstrap.min.js') }}"></script>

  <!-- Add icon library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;800&display=swap" rel="stylesheet">

<style>
  * {
      padding: 0;
      margin: 0;
      box-sizing: border-box;
  }

  body {
      height: 100vh;
      width: 100%;
      background: #fff;
  }

  .container {
      height: 100%;
      padding: 5px;
      display: flex;
      align-items: center;
      justify-content: center;
      flex-direction: column;
      position: relative;
      overflow: hidden;
  }

  .title {
      font-size: 5rem;
      font-family: 'Montserrat', sans-serif;
      /* animation: slidedown 0.3s linear; */
  }

  .text {
      font-size: 1.25rem;
      margin-top: 0.5rem;
  }

  .fill-up {
      top: 50px;
      left: 80px;
      width: 75px;
      height: 75px;
      border-radius: 50%;
      background: rgb(130, 173, 216) url("/images/icons/iconerror.png") no-repeat center;
  }

  .btn {
      margin-top: 20px;
      padding: 1rem 3rem;
      border: 5px solid rgb(130, 173, 216);
      background: rgb(130, 173, 216);
      border-radius: 1rem;
      color: #fff;
      text-decoration: none;
      font-size: 1.25rem;
      font-weight: 600;
      font-family: 'Montserrat';
      /* animation: slideup 0.3 linear; */
  }

/*   @keyframes slideup {
      from { transform: translateY(50px);}
      to {transform: translateY(0px);}
  }

  @keyframes slidedown {
      from { transform: translate(-50px, -30px);}
      to { transform:  translate(0px);}
  } */

</style>

</head>

<body>

<div class="container">

    <div class="row">

        <div class="col-sm-12">

          <div class="fill-up"></div>

        </div>

    </div>

    <div class="row">

        <div class="col-sm-12" style="text-align: center">

            <h2 class="title" style="font-weight: bolder">Oops ! ! !</h2>

        </div>

    </div>

    <div class="row">

        <div class="col-sm-12" style="text-align: center">

         <h1 style="font-weight: bolder">- Error -</h1>

        </div>

    </div>

    <div class="row" style="text-align: center">

        <div class="col-sm-12">

         <h1 style="font-weight: bolder">Page Not Found</h1>

        </div>

    </div>

    <div class="row">

        <div class="col-sm-12" style="text-align: center">

          <img src="/images/errors/errorcross.jpg" width="100%"  height="300px" alt="Lost image" class="lost-image">

        </div>

    </div>

    <div class="row">

        <div class="col-sm-12">

          <a href="http://127.0.0.1:8000/doctopic" class="btn" style="font-weight: bolder;">Take Me Back to Previous Page</a>

        </div>

    </div>

</div>

</body>

</html>
