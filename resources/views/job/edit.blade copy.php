<x-layout>

@include('components.employee_header') 

<div class="main-content mt-0">

    @include('components.employee_aside')

    <div class="main pr-3 ">

        @if(count($errors) > 0)
            <div class="alert alert-danger">
                @foreach($errors->all() as $error)
                        {{ $error }}<br>
                @endforeach
            </div>
        @endif

        <div class="row border-bottom my-3 pb-3 biz-title">
            <i class="fa fa-briefcase my-auto px-2"></i> 
            <h2 class="mb-0">Job Information</h2>
        </div>

        <form method="POST" action="/employee/{{ $employee->id }}/jobinfo">
        @csrf     
        @method('PATCH')                   

        <div class="row pt-1">
            <div class="col-6 pl-0">
                <h4>Important Dates</h4>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-12 col-sm-6 col-xl-4 pl-0">
                <label for="date_joined">Date Joined</label><label class="text-danger"> *</label>
                <div class="input-group">
                    <input type="date" class="form-control" id="date_joined" name="date_joined" placeholder="dd/mm/yyyy" 
                        value="{{ $employee->date_joined }}" {{ optional(request()->access())->hr ? '' : 'disabled' }}>
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>                    
            </div>
            @if( $employee->end_date != null )
            <div class="form-group col-12 col-sm-6 col-xl-4 pl-0">
                <label for="end_date">End Date</label>
                <div class="input-group">
                    <input type="date" class="form-control" id="end_date" name="end_date" placeholder="dd/mm/yyyy" 
                        value="{{ $employee->end_date }}" {{ auth()->user()->hr ? '' : 'disabled' }} >
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>               
            </div>
            @endif
            <div class="form-group col-12 col-sm-6 col-xl-4 pl-0">
                <label for="working_week_id">Working Week</label>
                <div class="input-group">
                    <select name="working_week_id" id="working_week_id" class="custom-select" {{ optional(request()->access())->hr ? '' : 'disabled' }}>
                    @foreach ($workingWeeks as $workingWeek)                        
                        <option value="{{ $workingWeek->id }}" {{ $workingWeek->id == $employee->working_week_id ? 'selected' : '' }}>{{ $workingWeek->title }}</option>
                    @endforeach
                    </select>
                </div>               
            </div>
        </div>
        <div class="row">
            <div class="form-group col-12 col-sm-6 col-xl-4 pl-0">
                <label for="work_permit_expiry">Work Permit Expiry</label>
                <div class="input-group">
                    <input type="date" class="form-control" id="work_permit_expiry" name="work_permit_expiry" placeholder="dd/mm/yyyy" 
                        value="{{ $employee->work_permit_expiry }}" {{ optional(request()->access())->hr ? '' : 'disabled' }}>
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>                    
            </div>
            <div class="form-group col-12 col-sm-6 col-xl-4 pl-0">
                <label for="labour_license_expiry">Labor License Expiry</label>
                <div class="input-group">
                    <input type="date" class="form-control" id="labour_license_expiry" name="labour_license_expiry" placeholder="dd/mm/yyyy" 
                        value="{{ $employee->labour_license_expiry }}" {{ optional(request()->access())->hr ? '' : 'disabled' }}>
                    <div class="input-group-append">
                        <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>                    
            </div>
        </div>

        <div class="row pt-1">
            <div class="col-6 pl-0">
                <h4>Account Information</h4>
            </div>
        </div>

        <div class="row">
            <div class="form-group col-12 col-sm-6 col-xl-4 pl-0">
                <label for="email_payslip_to">Email Payslip To</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <input type="checkbox" name="email_payslip" id="email_payslip" {{ $employee->email_payslip ? 'checked' : '' }}>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="email_payslip_to" name="email_payslip_to" placeholder="Payslip Email Address"  
                        value="{{ $employee->email_payslip && $employee->email_payslip_to == '' ? $employee->work_email : $employee->email_payslip_to }}"
                        {{ optional(request()->access())->hr ? '' : 'disabled' }}>
                </div>    
            </div>
            <div class="form-group col-12 col-sm-6 col-xl-4 pl-0">
                <label for="payment_mode_id">Payment Mode</label><label class="text-danger"> *</label>
                <select name="payment_mode_id" id="payment_mode_id" class="custom-select" onchange="checkMode()">

                    @foreach ($paymentmodes as $paymentmode)
                        @if ( $employee->payment_mode_id == $paymentmode->id) 
                        <option value="{{ $paymentmode->id }}" selected>{{ $paymentmode->title }}</option>
                        @else
                        <option value="{{ $paymentmode->id }}" >{{ $paymentmode->title }}</option>
                        @endif
                    @endforeach 
    
                </select>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-12 col-sm-6 col-xl-4 pl-0" id="bankinstitute">
                <label for="bank_id">Bank Institute</label>
                <select name="bank_id" id="bank_id" class="custom-select">
                    <option value="0" class="text-primary" onclick="AddBank(); return false;">Add a Bank </option>

                    @foreach ($banks as $bank)
                        @if ( $employee->bank_id == $bank->id) 
                        <option value="{{ $bank->id }}" selected>{{ $bank->title }}</option>
                        @else
                        <option value="{{ $bank->id }}" >{{ $bank->title }}</option>
                        @endif
                    @endforeach 
    
                </select>
            </div>
            <div class="form-group col-12 col-sm-6 col-xl-4 pl-0" id="bankaccount">
                <label for="bank_account">Account Number</label>
                <div class="input-group">
                    <input type="text" class="form-control" id="bank_account" name="bank_account" placeholder="Account Number" value="{{ $employee->bank_account }}">
                </div>
            </div>
        </div>

        <div class="row mb-1">
            @if ($employee->nationality_id == $company->address_country_id)
            <div class="form-group col-12 col-sm-6 col-xl-4 pl-0">
                <label for="tap_member">TAP</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <input type="checkbox" name="tap_member" id="tap_member" {{ $employee->tap_member ? 'checked' : '' }}>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="tap_account" name="tap_account" placeholder="Account No."  value="{{ $employee->tap_account }}">
                </div>    
            </div>
            <div class="form-group col-12 col-sm-6 col-xl-4 pl-0">
                <label for="scp_member">SCP</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <input type="checkbox" name="scp_member" id="scp_member" {{ $employee->scp_member ? 'checked' : '' }}>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="scp_account" name="scp_account" placeholder="Account No." value="{{ $employee->scp_account }}">
                </div>    
            </div>
            @else
            <div class="form-group col-12 col-sm-6 col-xl-4 pl-0">
                <label for="epf_member">EPF</label>
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <input type="checkbox" name="epf_member" id="epf_member" {{ $employee->epf_member ? 'checked' : '' }}>
                        </div>
                    </div>
                    <input type="text" class="form-control" id="epf_account" name="epf_account" placeholder="Account No."  value="{{ $employee->epf_account }}">
                </div>    
            </div>
            @endif
        </div>
        
        <div class="container mb-4 text-right">
            <button class="btn btn-success">Save Changes</button>
        </div>

        </form>

        <div class="row pt-1">
            <div class="col-6 pl-0">
                <h4>Employee Status</h4>
            </div>

        @if ( optional(request()->access())->hr )
            <div class="col-6 text-right text-biz-blue">
                <i class="fa fa-plus-circle my-auto px-2" style="font-size: 18px;" data-toggle="modal" data-target="#statusModal"></i> 
                <a href="" data-toggle="modal" data-target="#statusModal"><label class="text-right">Change Employment Status</label></a>
            </div>
        @endif

        </div>

        <div class="row mb-4">
            <table id="tblStatus" class="table border mb-1">
                <thead class="tbl-dark">
                    <tr>
                        <th scope="col" hidden>#</th>
                        <th scope="col" style="width: 10px;"></th>
                        <th scope="col">Effective Date</th>
                        <th scope="col">Status</th>
                        <th scope="col" class="d-none d-sm-table-cell">Comments</th>
                        <th {{ optional(request()->access())->hr || optional(request()->access())->payroll ? '' : 'hidden'}}>Edit</th>
                    </tr>
                </thead>
                <tbody>
                    @php $current_item = false @endphp

                    @foreach ($employeeStatus as $status)
                    @if ( $current_item == false && $status->effective_date < now() )
                        <tr class="bg-white">
                    @else
                        <tr>
                    @endif

                        <td hidden>{{ $status->id }}</td>

                        @if ( $current_item == false && $status->effective_date < now() )
                            <td><i class="fa fa-check-circle text-success" aria-hidden="true"></i></td>
                            @php $current_item = true @endphp
                        @else
                            <td></td>
                        @endif

                        <td>{{ $status->effective_date }}</td>
                        <td>{{ $status->StatusTitle == null ? '' : $status->StatusTitle->title }}</td>
                        <td class="d-none d-sm-table-cell">{{ $status->status_comment }}</td>

                        @if( ($current_item || strtotime($status->effective_date) > now() ) && (optional(request()->access())->payroll || optional(request()->access())->hr) )
                            <td>
                                <a href="#" onclick="statusEdit({{ $status->id }}); return false;"><i class="fa fa-pencil my-auto px-2 text-primary" style="font--size: 18px;" data-toggle="" data-target=""></i></a>
                            </td>
                        @else
                            <td hidden></td>
                        @endif

                    </tr>
                    @endforeach

                    @if ($employeeStatus->count() == 0)
                        <td colspan="5" class="text-center">No data at this time</td>
                    @endif
                </tbody>
            </table>
        </div>

        <div class="row pt-1">
            <div class="col-6 pl-0">
                <h4>Job Information</h4>
            </div>

        @if ( optional(request()->access())->hr )
            <div class="col-6 text-right text-biz-blue">
                <i class="fa fa-plus-circle my-auto px-2" style="font-size: 18px;" data-toggle="modal" data-target="#jobModal"></i> 
                <a href="" data-toggle="modal" data-target="#jobModal"><label class="text-right">Change Job Information</label></a>
            </div>
        @endif

        </div>

        <div class="row mb-4">
            <table id="tblJob" class="table border mb-1">
                <thead class="tbl-dark">
                    <tr>
                        <th scope="col" hidden>#</th>
                        <th scope="col"></th>
                        <th scope="col">Effective Date</th>
{{--                         <th scope="col">Location</th> --}}
                        <th scope="col">Department
                        @if( $current_item && optional(request()->access())->hr )
                            <a href="" data-toggle="modal" data-target="#departmentAddModal"><span class="badge badge-primary">Add</span></a>
                        @endif
                        </th>
                        <th scope="col">Designation
                        @if( $current_item && optional(request()->access())->hr )
                            <a href="" data-toggle="modal" data-target="#designationAddModal"><span class="badge badge-primary">Add</span></a>
                        @endif
                        </th>
                        <th scope="col" hidden>Reports To</th>
                        <th scope="col" class="d-none d-sm-table-cell">Reports To</th>
                        <th {{ optional(request()->access())->hr || optional(request()->access())->payroll ? '' : 'hidden'}}>Edit</th>
                    </tr>
                </thead>
                <tbody>
                    @php $current_item = false @endphp

                    @foreach ($employeeJobs as $employeeJob)

                    @if ( $current_item == false && $employeeJob->effective_date < now() )
                        <tr class="bg-white">
                    @else
                        <tr>
                    @endif

                        <td hidden>{{ $employeeJob->id }}</td>

                        @if ( $current_item == false && $employeeJob->effective_date < now() )
                            <td><i class="fa fa-check-circle text-success" aria-hidden="true"></i></td>
                            @php $current_item = true @endphp
                        @else
                            <td></td>
                        @endif

                        <td>{{ $employeeJob->effective_date }}</td>
                        {{-- Need to implement --}}
{{--                         <td> -- </td> --}}
                        @if( $employeeJob->Department != null)
                            <td>{{ $employeeJob->Department->title }}</td>
                        @else
                            <td></td>
                        @endif

                        @if( $employeeJob->Designation != null )
                            <td>{{ $employeeJob->Designation->title }}</td>
                        @else
                            <td></td>
                        @endif
                        {{-- Need to implement --}}
                        <td hidden>{{ $employeeJob->report_to_id }}</td>

                        <td class="d-none d-sm-table-cell">{{ is_null($employeeJob->ReportsTo) ? '' : $employeeJob->ReportsTo->name }}</td>
                        @if( ($current_item || $employeeStatus->effective_date > now() ) && (optional(request()->access())->payroll || optional(request()->access())->hr) )
                            <td>
                                <a href="" onclick="jobEdit({{ $employeeJob->id }}); return false;"><i class="fa fa-pencil my-auto px-2 text-primary" style="font--size: 18px;" data-toggle="" data-target=""></i></a>
                            </td>
                        @else
                            <td hidden></td>
                        @endif

                    </tr>
                    @endforeach

                    @if ($employeeJobs->count() == 0)
                        <td colspan="6" class="text-center">No data at this time</td>
                    @endif
                </tbody>
            </table>
        </div>
{{--         <div class="custom-control custom-checkbox mb-4 ml-1">
            <input type="checkbox" class="custom-control-input" id="currentJob">
            <label class="custom-control-label" for="currentJob">Show only current job</label>
        </div> --}}

        <div class="row pt-1">
            <div class="col-6 pl-0">
                <h4>Remuneration Information</h4>
            </div>
        @if (optional(request()->access())->hr)
            <div class="col-6 text-right text-biz-blue">
                <i class="fa fa-plus-circle my-auto px-2" style="font-size: 18px;" data-toggle="modal" data-target="#paymentModal"></i> 
                <a href="" data-toggle="modal" data-target="#paymentModal"><label class="text-right">Change Payment Information</label></a>
            </div>
        @endif
        </div>

        <div class="row">
            <table id="tblPayment" class="table border mb-4">
                <thead class="tbl-dark">
                    <tr>
                        <th scope="col" hidden>#</th>
                        <th scope="col"></th>
                        <th scope="col">Effective Date</th>
                        <th scope="col">Remuneration Rate</th>
                        <th scope="col">Overtime</th>
{{--                         <th scope="col">Reason for Change</th> --}}
                        <th hidden>type</th>
                        <th scope="col" class="d-none d-sm-table-cell">Comment</th>
                        <th {{ optional(request()->access())->hr || optional(request()->access())->payroll ? '' : 'hidden'}}>Edit</th>
                        <th scope="col" hidden>Overtime</th>
                    </tr>
                </thead>
                <tbody>
                    @php $current_item = false @endphp

                @foreach ($employeePayments as $employeePayment)

                    @if ( $current_item == false && $employeePayment->effective_date < now() )
                        <tr class="bg-white">
                    @else
                        <tr>
                    @endif
    
                        <td hidden>{{ $employeePayment->id }}</td>
                        @if ( $current_item == false && $employeePayment->effective_date < now() )
                            <td><i class="fa fa-check-circle text-success" aria-hidden="true"></i></td>
                            @php $current_item = true @endphp
                        @else
                            <td></td>
                        @endif

                        <td>{{ $employeePayment->effective_date }}</td>
                        <td> {{ $employeePayment->Currency->currency_symbol }} {{ number_format($employeePayment->basic_salary, 2) }} {{ $employeePayment->Currency->currency_code }}</td>
                        <td> {{ $employeePayment->Currency->currency_symbol }} {{ number_format($employeePayment->overtime, 2) }} {{ $employeePayment->Currency->currency_code }}</td>
{{--                         <td>Reason</td> --}}
                        <td hidden>{{ $employeePayment->payment_type_id }}</td>
                        <td class="d-none d-sm-table-cell">{{ $employeePayment->comment }}</td>

                        {{-- {{ dd($employeeStatus) }} --}}
                        @if( ($current_item || $employeeStatus->effective_date > now() ) && (optional(request()->access())->payroll || optional(request()->access())->hr) )
                            <td>
                                <a href="" onclick="paymentEdit({{ $employeePayment->id }}); return false;"><i class="fa fa-pencil my-auto px-2 text-primary" data-toggle="" data-target=""></i></a>
                            </td>
                        @else
                            <td hidden></td>
                        @endif
                        <td hidden>{{ number_format($employeePayment->overtime, 2) }}</td>
                    </tr>
                @endforeach

                    @if ($employeePayments->count() == 0)
                        <td colspan="6" class="text-center">No data at this time</td>
                    @endif

                </tbody>
            </table>
        </div>



        @if (optional(request()->access())->hr)   
        <div class="row pt-1">
            <div class="col-12 pl-0 align-middle">
                <label style="font-size: 20px;">End of Service</label>
                <a href="" data-toggle="modal" data-target="#reasonAddModal"><span class="badge badge-primary">Add</span></a>

{{--                 <button class="btn btn-outline-primary" data-toggle="modal" data-target="#endModal">edit</button> --}}
                <a href="" class="pb-2" data-toggle="modal" data-target="#endModal"><span class="badge badge-primary">Enter</a>
                    <div class="row">
                        <table id="tblPayment" class="table border mb-4">
                            <thead class="tbl-dark">
                                <tr>
                                    <th scope="col" hidden>#</th>
                                    <th scope="col" width="20%">Effective Date</th>
                                    <th scope="col" width="20%">Reason</th>
                                    <th scope="col" >Comment</th>
                                    <th scope="col" width="20%">Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($employee->EndService as $endService )                                    
                                <tr class="bg-white">
                                    <td hidden>{{ $endService->id }}</td>
                                    <td>{{ \Carbon\Carbon::parse($endService->end_service_date)->format('Y-m-d') }}</td>
                                    <td>{{ $endService->endEmployment->title }}</td>
                                    <td>{{ $endService->end_employment_comment }}</td>
                                    <td>
                                        <a href="" onclick=""><i class="fa fa-pencil my-auto px-2 text-primary" style="font--size: 18px;" data-toggle="" data-target=""></i></a>
                                        <a href="" onclick=""><i class="fa fa-trash my-auto px-2 text-danger" style="font--size: 18px;" data-toggle="" data-target=""></i></a>
                                    </td>        
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
            </div>
            @if( !is_null($employee->end_reason_id) )
            <div class="col-12 pl-0 align-middle">
                <form method="POST" action="/employee/{{ $employee->id }}/endservice">
                    
                {{ $employee->EndService->title }} - {{ $employee->end_contract_date }} 
                    <a href="/employee/delete_end_service/{{ $employee->id }}"><i class="fa fa-trash my-auto px-2 text-danger" ></i></a> 
              </form>
            </div>
            @endif
        </div>
        @endif

        @include('components.modal.add_employee_status')
        @include('components.modal.edit_employee_status')
        @include('components.modal.add_employee_job')
        @include('components.modal.edit_employee_job')
        @include('components.modal.add_employee_payment')
        @include('components.modal.edit_employee_payment')
        @include('components.modal.add_employment_status')
        @include('components.modal.add_department')
        @include('components.modal.add_designation')
        @include('components.modal.add_new_reason')


        <div class="modal" id="endModal">
            <div class="modal-dialog modal-dialog-centered">
              <div class="modal-content">
              <form method="POST" action="/employee/{{ $employee->id }}/endservice">
              @csrf
            
                <div class="modal-header">
                  <h5 class="modal-title">End of Service</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>

                <div class="modal-body">
                  
                    <div class="row">
                        <div class="form-group col-6">
                            <label for="end_service_date">Final Date of Employment</label>
                            <div class="input-group">
                                <input type="date" class="form-control" id="end_service_date" name="end_service_date" placeholder="dd/mm/yyyy" value="{{ $employee->end_date }}">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>                    
                        </div>
                        <div class="form-group col-6">
                            <label for="end_reason_id">End Employment Reason </label>        
                            <select name="end_reason_id" id="end_reason_id" class="custom-select">
                                <option value="0" selected>Choose ... </option>

                                @foreach ($endEmployments as $endemployment)
                                    @if ( $employee->end_reason_id == $endemployment->id) 
                                    <option value="{{ $endemployment->id }}" selected>{{ $endemployment->title }}</option>
                                    @else
                                    <option value="{{ $endemployment->id }}" >{{ $endemployment->title }}</option>
                                    @endif
                                @endforeach 
                
                            </select>
                        </div>            
                    </div>
                    <div class="row">
                        <div class="form-group col">
                            <label for="end_employment_comment">End Employment Comment </label>
                            <textarea class="form-control" name="end_employment_comment" id="end_employment_comment" rows="2"></textarea>
                        </div>                                    
                    </div>
            
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
              </form>
              </div>
            </div>
        </div>

        <script>
            $( document ).ready(function() {
                $('#employment_status_id').editableSelect();
                checkMode();
            }); 

            function AddEmploymentStatus(){
                $("#statusModal").modal('toggle');
                $("#employmentStatusAddModal").modal('show');
            }

            $('#basic').editableSelect();

            function checkMode(){
                var element = document.getElementById("payment_mode_id");
                var mode = element.value;

                if (element.options[element.selectedIndex].text == "Bank Transfer" || element.options[element.selectedIndex].text == "Bank"){
                    document.getElementById("bankinstitute").style.display = "block";
                    document.getElementById("bankaccount").style.display = "block";
                }
                else{
                    document.getElementById("bankinstitute").style.display = "none";
                    document.getElementById("bankaccount").style.display = "none";
                }
            }

            function statusEdit(id){

                document.getElementById("editStatus").action = "/statuses/" + id;

                var oTable = document.getElementById("tblStatus");
                for (i = 1; i < oTable.rows.length; i++){
                    var oCells = oTable.rows.item(i).cells;

                    if (oCells.item(0).innerHTML == id){
                        var e = document.getElementsByClassName("statusID");

                        for ( var i = 0; i < e[0].options.length; i++ ) {
                            if ( e[0].options[i].text == oCells.item(3).innerHTML) {
                                e[0].options[i].selected = true;
                            }
                        }

                        document.getElementsByClassName("statusDate")[0].value = oCells.item(2).innerHTML;
                        document.getElementsByClassName("statusComment")[0].value = oCells.item(4).innerHTML;
                    }
                } 

                $("#statusModalEdit").modal('show');
            }

            function selectOption(obj, value)
            {
                for ( var i = 0; i < obj.options.length; i++ ) {
                    if ( obj.options[i].text == value) {
                        obj.options[i].selected = true;
                    }
                }                
            }

            function jobEdit(id){
                document.getElementById("editJob").action = "/jobs/" + id;
                
                var oTable = document.getElementById("tblJob");
                for (i = 1; i < oTable.rows.length; i++){
                    var oCells = oTable.rows.item(i).cells;
                    
                    if (oCells.item(0).innerHTML == id){
                         var e = document.getElementsByClassName("departmentEdit");
                        selectOption(e[0], oCells.item(3).innerHTML);

                        e = document.getElementsByClassName("designationEdit");
                        selectOption(e[0], oCells.item(4).innerHTML);
                        //(oCells.item(2).innerHTML);

                        e = document.getElementsByClassName("reportsEdit");
                        selectOption(e[0], oCells.item(6).innerHTML);
 
                        document.getElementsByClassName("jobDate")[0].value = oCells.item(2).innerHTML;
                    }
                }  

                $("#jobEditModal").modal('show');
            }

            function paymentEdit(id){
                document.getElementById("editPayment").action = "/payments/" + id;
                
                var oTable = document.getElementById("tblPayment");
                for (i = 1; i < oTable.rows.length; i++){
                    var oCells = oTable.rows.item(i).cells;

                    if (oCells.item(0).innerHTML == id){
                        document.getElementsByClassName("paymentDate")[0].value = oCells.item(2).innerHTML;

                        var x = oCells.item(3).innerHTML;
                        x = x.replace('$ ', '');
                        x = x.replace(' BND', '');

                        e = document.getElementsByClassName("paymentType");
                        obj = e[0];

                        //e[0].value = "3";
                        for ( var i = 0; i < obj.options.length; i++ ) {
                            if ( obj.options[i].value == oCells.item(5).innerHTML) {
                                obj.options[i].selected = true;
                            }
                        }  

                        document.getElementsByClassName("paymentComment")[0].value = oCells.item(6).innerHTML;
                        document.getElementsByClassName("basePay")[0].value = parseFloat(x);
                        document.getElementsByClassName("overtime")[0].value = oCells.item(8).innerHTML;

                    }
                }   

                $("#paymentEditModal").modal('show');
            }

        </script>
    
{{--     </form> --}}
    </div>
</div>

</x-layout>

