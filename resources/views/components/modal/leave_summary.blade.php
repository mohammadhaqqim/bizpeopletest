<div class="card">
    <div class="card-header">
      As At {{ date("d M Y", strtotime(Carbon\Carbon::now())) }}
    </div>

    <div class="card-body">

@foreach ($leaveTypes as $leave)
{{-- getCurrentEmployeePolicy
@if( !is_null($leave->getEmployeeLeavePolicy($employee->id)) ) --}}
    @if( !is_null($leave->getCurrentEmployeePolicy($employee->id)) )
        <div class="d-flex flex-column mb-2">
            <label class="mb-0">{{ $leave->policy_name }}</label>
{{--             <small class="text-muted">{{ $leave->getCurrentEmployeeLeavePolicy($employee->id)->balance }}
                {{ $leave->LeaveType->track_time == 'h' ? 'hours' : 'days' }} available</small> --}}
        </div>
    @endif
@endforeach


    </div>
</div>
