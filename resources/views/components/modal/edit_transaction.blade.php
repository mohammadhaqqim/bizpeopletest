<div class="modal" tabindex="-1"  id="editModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form id="editTransaction" method="POST" action="/allowances/">
        @method('PATCH')
        @csrf                        

        <div class="modal-header">
          <i class="fa fa-file-text-o pt-1"></i>
          <h5 class="modal-title pl-2">Edit a Trasnaction</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <input id="employee_id" name="employee_id" value="{{ $employee->id }}" hidden>

            <div class="row">
                <div class="form-group col-6">
                <label for="date_joined">Effective Date</label>
                <div class="input-group">
                    <input type="date" class="form-control editDate" id="effective_date" name="effective_date" placeholder="dd/mm/yyyy" 
                         disabled>
                </div>    
                </div>
            </div>
            <div class="row">


                <div class="form-group col-6">
                    <label for="title">Allowance Type</label>
                    <input type="text" class="form-control editTitle" id="edittitle" name="edittitle" value="---" disabled>
                </div>   
                
                <div class="form-group col-6">
                    <label for="deleteAmount">Amount $</label>
                    <input type="number" class="form-control editAmount" id="amount" name="amount"  step="0.01" placeholder="0.00">
                </div>  
            </div>
            <div class="row">
                <div class="form-group col">
                    <label for="deleteRemarks">Remark</label>
                    <textarea type="text" class="form-control editRemarks" rows="3" id="remarks" name="remarks" placeholder="Enter any comments" maxlength="200"></textarea>
                </div>                        
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-success">Update</button>
        </div>
        </form>
      </div>
    </div>
</div>
