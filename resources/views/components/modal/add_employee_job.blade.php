<div class="modal" id="jobModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form method="POST" action="/employee/{{ $employee->id }}/job">
        @csrf
            
        <div class="modal-header">
            <i class="fa fa-briefcase my-auto px-2"></i> 
            <h5 class="modal-title">Update Job Information</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col-6">
                    <label for="effective_date">Effective Date</label>
                    <div class="input-group">
                        <input type="date" class="form-control" id="effective_date" name="effective_date" placeholder="dd/mm/yyyy" value="{{ date('Y-m-d', strtotime('0 day')) }}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>    
                </div>
{{--                 <div class="form-group col">
                    <label for="work_week_id">Working Week</label>
                    <div class="input-group">
                        <select name="work_week_id" id="work_week_id" class="custom-select">
                        @foreach ($workingWeeks as $workingWeek)                        
                            <option value="{{ $workingWeek->id }}">{{ $workingWeek->title }}</option>
                        @endforeach
                        </select>
                    </div>               
                </div>
 --}}
            </div>
            <div class="row">                        
                <div class="form-group col">
                    <label for="department_id">Department</label>
                    <div class="input-group">
                        <select name="department_id" id="department_id" class="custom-select">
                        @foreach ($departments as $department)                        
                            <option value="{{ $department->id }}">{{ $department->title }}</option>
                        @endforeach
                        </select>
                    </div>               
                </div>

            </div>

{{--                     <p>Location</p>  
--}}
{{--                     <div class="form-group col-3 pl-0">
                <label for="employment_type_id">Location</label>
                <div class="input-group">
                    <select name="location_id" id="location_id" class="custom-select">
                    @foreach ($employmentStatuses as $employmentStatus)                        
                        <option value="{{ $employmentStatus->id }}">{{ $employmentStatus->title }}</option>
                    @endforeach
                    </select>
                </div>               
            </div>  
--}}

            <div class="row">
                <div class="form-group col">
                    <label for="designation_id">Job Title</label>
                    <div class="input-group">
                        <select name="designation_id" id="designation_id" class="custom-select">
                            @foreach ($designations as $designation)                        
                            <option value="{{ $designation->id }}">{{ $designation->title }}</option>
                            @endforeach
                        </select>
                    </div>               
                </div>
            </div>

            <div class="row">
                {{-- HOD, Supervisor, Line Manager etc. --}}
                <div class="form-group col">
                    <label for="report_to_id">Reports To</label>
                    <div class="input-group">
                        <select name="report_to_id" id="report_to_id" class="custom-select">
                            <option >-- Choose --</option>
                        @foreach ($managers as $manager)                        
                            <option value="{{ $manager->id }}">{{ $manager->name }}</option>
                        @endforeach 
{{--                             <option value="1">Ambrose</option>
                            <option value="2">Jenifer</option>
                            <option value="3">Regan</option>
 --}}
                        </select>
                    </div>               
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save Status</button>
        </div>
        </form>
    </div>
  </div>
</div>
