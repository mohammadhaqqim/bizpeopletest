<div class="modal" id="paytypeAddModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form method="POST" action="/paytypes">
        @csrf
            
        <div class="modal-header">
            <i class="fa fa-briefcase my-auto px-2"></i> 
            <h5 class="modal-title">Add New Payment Type</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col">
                    <label for="title">Payment Type Title</Title></label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Race Title">
                </div>
            </div>        
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save Status</button>
        </div>
        </form>
    </div>
  </div>
</div>
