<div class="modal" id="leaveTypeAddModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form method="POST" action="/leavetypes">
        @csrf
            
        <div class="modal-header">
            <i class="fa fa-briefcase my-auto px-2"></i> 
            <h5 class="modal-title">Add Leave Type</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="col-12">
                <label for="title">Leave Type Name</label>
                <input type="text" class="form-control" id="title" name="title" placeholder="eg Medical Leave" required>
            </div>

            <div class="col-6">   
                <div class="custom-control custom-checkbox my-3 pl-2">
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input" id="paid_time_off" name="paid_time_off">
                        <label class="custom-control-label" for="paid_time_off">This is paid time off</label>
                    </div>
                </div>
            </div>    
            <div class="col-6">
                <label for="track_time">Track Time By</label>
                <select name="track_time" id="track_time" class="custom-select">
                    <option {{ old('gender_id', null) !== null ? 'selected' : '' }}>Choose..</option>
                    <option value="h" >Hours</option>
                    <option value="d" >Days</option>
                </select>
            </div>
        </div>        

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save Status</button>
        </div>
        </form>
    </div>
  </div>
</div>
