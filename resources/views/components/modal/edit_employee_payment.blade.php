<div class="modal" tabindex="-1"  id="paymentEditModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form id="editPayment" method="POST" action="/employee/{{ $employee->id }}/payment">
        @csrf                        
        @method('PATCH')

        <div class="modal-header">
          <i class="fa fa-money"></i>
          <h5 class="modal-title">Remuneration Update</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            
            <div class="row">
                <div class="form-group col-6">
                    <label for="date_joined">Effective Date</label>
                    <div class="input-group">
                        <input type="date" class="form-control paymentDate" id="effective_date" name="effective_date" placeholder="dd/mm/yyyy" value="{{ date('Y-m-d', strtotime('0 day')) }}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>    
                </div>
            </div>

            <div class="row">
                <div class="form-group col">
                    <label for="pay_type_id">Payment Type</label>
                    <div class="input-group">
                        <select name="payment_type_id" id="payment_type_id" class="custom-select paymentType">
                        @foreach ($paymentTypes as $paymentType)                        
                            <option value="{{ $paymentType->id }}">{{ $paymentType->title }}</option>
                        @endforeach
                        </select>
                    </div>               
                </div>            
                <div class="form-group col">
                    <label for="basic_salary">Remuneration Rate</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-append" style="width: 80px; ">
                                <select name="currency_id" id="currency_id" class="custom-select aaa-prepend currencyType" >
{{--                                 @if ($currencies->count() > 1)
                                    <option {{ old('country_id', null) !== null ? 'selected' : '' }} class="bg-white">...</option>
                                @endif --}}

                                @foreach ($currencies as $currency)
                                    <option value="{{ $currency->id }}" {{ $currencies->count() == 1 ? 'selected' : '' }}>{{ $currency->currency_code }}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                        <input type="number" class="form-control basePay" id="basic_salary" name="basic_salary" placeholder="0.00" step="0.01" value="">
                    </div>    
                </div>
            </div>

            <div class="row">
                <div class="form-group col-6">
                    <label for="basic_salary">Overtime</label>
                    <div class="input-group">
                        <input type="number" class="form-control overtime" id="overtime" name="overtime" placeholder="0.00" step="0.01" value="">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="comment">Comment - Reason for change</label>
                <textarea class="form-control paymentComment" name="comment" id="comment" rows="3"></textarea>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
    </div>
</div>
