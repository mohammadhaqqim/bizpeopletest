<div class="modal" id="countryAddModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form method="POST" action="/countries">
        @csrf
            
        <div class="modal-header">
            <i class="fa fa-briefcase my-auto px-2"></i> 
            <h5 class="modal-title">Add New Country</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col">
                    <label for="country">Country Name</label>
                    <input type="text" class="form-control" id="country" name="country" placeholder="Country Name">
                </div>
            </div>
            <div class="row">
                <div class="form-group col">
                    <label for="name">Nationality - Person</label>
                    <input type="text" class="form-control" id="nationality" name="nationality" placeholder="Nationality">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-4">
                    <label for="currency_code">Currency Code</label>
                    <input type="text" class="form-control" id="currency_code" name="currency_code" placeholder="Currency Code" maxlength="3">
                </div>
                <div class="form-group col-4">
                    <label for="country_symbol">Corrency Symbol</label>
                    <input type="text" class="form-control" id="currency_symbol" name="currency_symbol" placeholder="Currency Symbol" maxlength="3">
                </div>
            </div>      
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save Status</button>
        </div>
        </form>
    </div>
  </div>
</div>
