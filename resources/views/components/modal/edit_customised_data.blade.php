<div class="modal" id="customisedEditModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form id="editStatus" method="POST" action="{{ $selected->action }}">
        @csrf
        @method('PATCH')
            
        <div class="modal-header">
            <i class="fa fa-briefcase my-auto px-2"></i> 
            <h5 class="modal-title">Edit {{ $selected->display }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col">
                    <label for="country">Title</label>
                    <input type="text" class="form-control title" id="title" name="title" placeholder="{{ $selected->display }} Title">
                </div>
            </div>
            <div class="row">
                <div class="form-group col-4">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input default-id" id="default" name="default">
                        <label class="custom-control-label" for="default"></label>
                    </div>
                </div>
            </div>      
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Update</button>
        </div>
        </form>
    </div>
  </div>
</div>
