<div class="modal" id="leavePolicyAddModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form method="POST" action="/leavepolicies">
        @csrf
            
        <div class="modal-header">
            <i class="fa fa-briefcase my-auto px-2"></i> 
            <h5 class="modal-title">Add Leave Policy</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
{{--             <div class="row">
 --}}                
            <div class="col-6 mt-2">
                <label class="mb-1 text-secondary" for="leave_type_id">Leave Type</label>
                <select name="leave_type_id" id="leave_type_id" class="custom-select">
                    <option>Choose..</option>
                @foreach( $leavetypes as $leave)
                    <option value="{{ $leave->id }}" >{{ $leave->title }}</option>
                @endforeach
                </select>
            </div>
            <div class="col-8 mt-2 mb-3">
                <label class="mb-0 text-secondary" for="policy_name">Policy Name</label>
                <input type="text" class="form-control" id="policy_name" name="policy_name" placeholder="eg Medical Leave">
            </div>            
            <div class="col-12 mt-2 mb-3 pt-2 pl-2 border-top ">
                <label class="mb-0 font-weight-bold" for="title">Accural Schedule</label>
            </div>
            <div class="col-12 mt-2 pt-2 pl-2 border-top">
                <label class="mb-0 font-weight-bold" for="title">Accural Options</label>
            </div>
{{--             </div>    --}} 
            <div class="col-6 mt-2">
                <label class="mb-0 text-secondary" for="accrual">First Accural</label>
                <select name="accrual" id="accrual" class="custom-select">
                    <option>Choose..</option>
                    <option value="p" >Prorate</option>
                    <option value="f" >Full Amount</option>
                </select>
            </div>
            <div class="col-8 mt-2">
                <label class="mb-0 text-secondary" for="carryover_id">Carryover Date</label>
                <select name="carryover_id" id="carryover_id" class="custom-select">
                    <option>Choose..</option>
                    <option value="j" >1st of January</option>
                    <option value="e" >Employee hire date</option>
                    <option value="o" >Other</option>
                </select>
            </div>
            <div class="col-8 mt-2 mb-2">
                <label class="mb-0 text-secondary" for="accrual_occurs">Accurals Occur At</label>
                <select name="accrual_occurs" id="accrual_occurs" class="custom-select">
                    <option>Choose..</option>
                    <option value="e" >At the end of the period</option>
                    <option value="b" >At the beginning of the period</option>
                </select>
            </div>
        </div>        

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save Status</button>
        </div>
        </form>
    </div>
  </div>
</div>
