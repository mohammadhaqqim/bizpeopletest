<div class="modal" tabindex="-1"  id="reportingPeriodModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form method="POST" action="/company/period">
        @csrf                        

        <div class="modal-header">
          <i class="fa fa-calendar"></i>
          <h5 class="modal-title">Add Reporting Period</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col px-0">
                    <label for="title">Period Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Reporting Period Title">
                </div>
            </div>        
            <div class="row">
                <div class="form-group col-6 pl-0">
                    <label for="start_date">Starting Date</label>
                    <div class="input-group">
                        <input type="date" class="form-control" id="start_date" name="start_date" placeholder="dd/mm/yyyy" value="{{ date('Y-m-d', strtotime('0 day')) }}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar-check-o"></i></span>
                        </div>
                    </div>    
                </div>

                <div class="form-group col-6 pr-0">
                    <label for="end_date">Ending Date</label>
                    <div class="input-group">
                        <input type="date" class="form-control" id="end_date" name="end_date" placeholder="dd/mm/yyyy" value="{{ date('Y-m-d', strtotime('0 day')) }}">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar-times-o"></i></span>
                        </div>
                    </div>    
                </div>
            </div>

            <div class="form-group">
                <label for="comment">Comment</label>
                <textarea class="form-control" name="comment" id="comment" rows="3" max="191"></textarea>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
    </div>
</div>
