<div class="modal" id="statusModalEdit">
    <div class="modal-dialog modal-dialog-centered">
        <form id="editStatus" method="POST" action="/employee/id/status">
            @method('PATCH')        
            @csrf

            <div class="modal-content ">
                <div class="modal-header">
                    <i class="fa fa-user my-auto px-2"></i> 
                    <h5 class="modal-title">Edit Employee Status</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col">
                            <label for="effective_date">Effective Date</label>
                            <div class="input-group">
                                <input type="date" class="form-control statusDate" id="effective_date" name="effective_date" placeholder="dd/mm/yyyy" value="{{ date('Y-m-d', strtotime('0 day')) }}">
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                                </div>
                            </div>    
                        </div>
                        <div class="form-group col">
                            <label for="employment_status_id">Employment Status</label>
                            <div class="input-group">
                                <select name="employment_status_id" id="employment_status_id" class="custom-select statusID">
{{--                                     <option value="0" class="text-primary" onclick="AddEmploymentStatus(); return false;">Add a Status</option> --}}
                                    <option value="0" selected>Select ...</option>
                                @foreach ($employmentStatuses as $employmentStatus)                        
                                    <option value="{{ $employmentStatus->id }}">{{ $employmentStatus->title }}</option>
                                @endforeach
                                </select>
                            </div>               
                        </div>       
                    </div>

                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Comment</label>
                        <textarea class="form-control statusComment" name="status_comment" id="status_comment" rows="3"></textarea>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" >Save Changes</button>
                </div>
            </div>
        </form>
    </div>
</div>
