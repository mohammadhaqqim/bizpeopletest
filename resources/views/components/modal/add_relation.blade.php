<div class="modal" tabindex="-1"  id="addRelationModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form method="POST" action="/employee/{{ $employee->id }}/relation">
        @csrf                        

        <div class="modal-header">
          <i class="fa fa-users pr-2" id="user"></i>
          <i class="fa fa-medkit text-danger pr-2" id="med"></i>
          <h5 class="modal-title" id="relationTitle">New Relation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col">
                    <label for="name">Person's Name</label><label class="text-danger"> *</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Person's full name" required>
                </div>    
            </div>

            <div class="row">
                <div class="form-group col-sm-12 col-md-6">
                    <label for="relationship_id">Relationship</label><label class="text-danger"> *</label>
                    <div class="input-group">
                        <select name="relationship_id" id="relationship_id" class="custom-select" required>
                        @foreach ($relationships as $relationship)                        
                            <option value="{{ $relationship->id }}">{{ $relationship->title }}</option>
                        @endforeach
                        </select>
                    </div>               
                </div>       
                <div class="form-check col-sm-12 col-md-6 pl-3 pt-3">
                    <div class="col">
                        <input class="form-check-input" type="checkbox" value="emergency" id="emergency" name="emergency">
                        <label class="form-check-label" for="emergency">
                            Emergency Contact
                        </label>
                    </div>

                    <div class="col">
                        <input class="form-check-input" type="checkbox" value="dependant" id="dependant" name="dependant">
                        <label class="form-check-label" for="dependant">
                          Dependent
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-12 col-md-6">
                    <label for="date_of_birth">Date of Birth</label>
                    <div class="input-group">
                        <input type="date" class="form-control" id="date_of_birth" name="date_of_birth" placeholder="dd/mm/yyyy" >
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>                
                </div>
                <div class="form-group col-sm-12 col-md-6" id="icInformation">
                    <label for="ic_number">IC Number</label>
                    <input type="text" class="form-control" id="ic_number" name="ic_number" placeholder="Identification Card #">
                </div>       
            </div>
            <div class="row">
                <div class="form-group col-sm-12 col-md-6">
                    <label for="hand_phone">Mobile Phone</label>

                    <div class="input-group ">
                        <div class="input-group-prepend" data-toggle="tooltip" data-placement="bottom" title="Mobile Phone #">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-mobile"></i></span>
                        </div>
                        <input type="number" id="hand_phone" name="hand_phone" class="form-control" placeholder="Mobile Number" 
                            aria-label="Mobile Number" aria-describedby="basic-addon1" value=""
                            pattern="[0-9]{7}" maxlength="7" minlength="7">
                    </div>
                </div>

                <div class="form-group col-sm-12 col-md-6">
                    <label for="phone_number">Home Phone</label>
                    <div class="input-group ">
                        <div class="input-group-prepend" data-toggle="tooltip" data-placement="bottom" title="Home Phone">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-home"></i></span>
                        </div>
                        <input type="number" id="phone_number" name="phone_number" class="form-control" placeholder="Home Phone #" 
                            aria-label="Home Phone #" aria-describedby="basic-addon1" value="{{ $employee->work_phone }}"
                            pattern="[0-9]{7}" maxlength="7" minlength="7">
                    </div>
                </div>
            </div>

            <div class="row my-3" id="passportpass">
                <div class="row ml-0 mr-0">
                    <div class="col-6">
                        <button class="btn btn-primary btn-block" type="button" onclick="togglePassport(); return false;">Passport Details</button>
                    </div>
                    <div class="col-6">
                        <button class="btn btn-primary btn-block" type="button" onclick="togglePasses(); return false;">Depend/Student Pass</button>
                    </div>
                </div>
            </div>            

            <div class="row " id="passport">
                <div class="row">
                    <div class="form-group col ml-3" id="icInformation">
                        <label for="passport_number">Passport Number</label>
                        <input type="text" class="form-control" id="passport_number" name="passport_number" placeholder="Passport Number">
                    </div>       

                    <div class="form-group col mr-3">
                        <label for="passport_expiry">Passport Expiry Date</label>
                        <div class="input-group">
                            <input type="date" class="form-control" id="passport_expiry" name="passport_expiry" placeholder="dd/mm/yyyy" >
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>    
                    </div>
                </div>

            </div>

            <div class="row " id="studentdependent">
                <div class="row">
                    <div class="form-group col ml-3">
                        <label for="dependent_expiry">Dependent Expiry</label>
                        <div class="input-group">
                            <input type="date" class="form-control" id="dependent_expiry" name="dependent_expiry" placeholder="dd/mm/yyyy" >
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>    
                    </div>

                    <div class="form-group col mr-3">
                        <label for="student_pass_expiry">Student Pass Expiry</label>
                        <div class="input-group">
                            <input type="date" class="form-control" id="student_pass_expiry" name="student_pass_expiry" placeholder="dd/mm/yyyy" >
                            <div class="input-group-append">
                                <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                            </div>
                        </div>    
                    </div>
                </div>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
      <script>
            $( document ).ready(function() {
                hideBoth();
            }); 

          function togglePasses()
          {
            if (document.getElementById("studentdependent").style.display == "block")
            {
                document.getElementById("studentdependent").style.display = "none";
            }
            else
            {
                hideBoth();
                document.getElementById("studentdependent").style.display = "block";
            }
          }

          function togglePassport()
          {
            if (document.getElementById("passport").style.display == "block")
            {
                document.getElementById("passport").style.display = "none";
            }
            else
            {
                hideBoth();
                document.getElementById("passport").style.display = "block";
            }
          }

          function hideBoth(){
            document.getElementById("passport").style.display = "none";
            document.getElementById("studentdependent").style.display = "none";
          }
      </script>
    </div>
</div>
