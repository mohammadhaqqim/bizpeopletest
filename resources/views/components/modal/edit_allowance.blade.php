<div class="modal" id="allowanceEditModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
    <form id="editTrans" method="POST" action="/transaction/">
        @csrf
        @method('PATCH')
            
        <div class="modal-header">
            <i class="fa fa-briefcase my-auto px-2"></i> 
            <h5 class="modal-title">Edit New Allowance</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col">
                    <label for="title">Allowance Title</Title></label><label class="text-danger"> *</label>
                    <input type="text" class="form-control editTitle" id="title" name="title" placeholder="Allowance Title">
                </div>
            </div>                    

            <div class="row">
                <div class="form-group col-6">
                    <label for="employment_status_id">Addition / Deduction</label>
                    <input type="text" class="form-control editType" disabled>
                </div>        
                <div class="form-group col-6 pt-4">
                    <div class="custom-control custom-switch">
                        <input type="checkbox" class="custom-control-input editActive" id="activeEdit" name="activeEdit">
                        <label class="custom-control-label" for="activeEdit">Active</label>
                    </div>
                </div>
            </div>

            <div class="form-group col-12 mb-1">
                <label for="">Options</label>
            </div>                

            <div class="row px-5">

                <div class="custom-control custom-checkbox col">
                    <input type="checkbox" class="custom-control-input editCalc" id="calculatedEdit" name="calculatedEdit">
                    <label class="custom-control-label" for="calculatedEdit">Calculated</label>
                </div>
                <div class="custom-control custom-checkbox col">
                    <input type="checkbox" class="custom-control-input editFixed" id="fixedEdit" name="fixedEdit" >
                    <label class="custom-control-label" for="fixedEdit">Fixed</label>
                </div>

            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save Status</button>
        </div>
    </form>
    </div>
  </div>
</div>
