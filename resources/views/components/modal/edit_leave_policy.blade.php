<div class="modal" id="leavePolicyEditModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form method="POST" id="editLeave" action="/leavepolicy/0">
        @csrf
            
        <div class="modal-header">
            <i class="fa fa-briefcase my-auto px-2"></i> 
            <h5 class="modal-title" id="lpAddTitle">Leave Accrual Policy</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">

            <div class="col-6">
                <label for="track_time">Leave Accrual Policy</label>
                <select name="leave_policy_id" id="leave_policy_id" class="custom-select leaveTrack">
                    <option>-- Choose --</option>
                    <option value="h" >Hours</option>
                    <option value="d" >Days</option>
                </select>
            </div>

            <div class="col-12">
                <label for="title">Leave Type Name</label>
                <input type="text" class="form-control leaveTitle" id="title" name="title" placeholder="eg Medical Leave">
            </div>

            <div class="col-6 mt-3">   
                <div class="custom-control custom-switch d-inline" >
                    <input type="checkbox" class="custom-control-input leaveActive" id="active" name="active">
                    <label class="custom-control-label" for="active" >Active</label>
                </div>
            </div>

            <div class="col-6">   
                <div class="custom-control custom-checkbox my-3 pl-2">
                    <div class="custom-control custom-checkbox mr-sm-2">
                        <input type="checkbox" class="custom-control-input leavePaidOff" id="paid_time_off" name="paid_time_off">
                        <label class="custom-control-label " for="paid_time_off">This is paid time off</label>
                    </div>
                </div>
            </div>    

        </div>        

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save Status</button>
        </div>
        </form>
    </div>
  </div>
</div>
