<div class="modal" tabindex="-1"  id="additionModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form method="POST" action="/allowances/">
        @csrf                        

        <div class="modal-header">
          <i class="fa fa-file-text-o pt-1"></i>
          <h5 class="modal-title pl-2">Add an Addition</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <input id="employee_id" name="employee_id" value="{{ $employee->id }}" hidden>

            <div class="row">
                <div class="form-group col">
                    <label for="allowance_id">Transaction Type</label>
                    <select name="allowance_id" id="allowance_id" class="custom-select" onchange="checkAdditionSelection(this)" required>
                        <option value="-1" selected>Choose ...</option>
                        <option value="0" >+ Add</option>

                    @foreach ($transactions as $transaction)
                        @if($transaction->addition)
                            <option value="{{ $transaction->id }}" >{{ $transaction->title }}</option>
                        @endif

                    @endforeach

                    </select>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-6">
                    <label for="date_joined">Effective Date</label>
                    <div class="input-group">
                        <input type="date" class="form-control" id="effective_date" name="effective_date" placeholder="dd/mm/yyyy" 
                            value="{{ date('Y-m-d', strtotime('0 day')) }}" required >
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>    
                </div>

                <div class="form-group col-6">
                    <label for="amount">Amount $</label>
                    <input type="number" class="form-control" id="amount" name="amount" placeholder="0.00" value="" step="0.01" required>
                </div>        
            </div>
            <div class="row">
                <div class="form-group col">
                    <label for="remarks">Remark</label>
                    <textarea type="text" class="form-control" rows="3" id="remarks" name="remarks" placeholder="Enter any comments" maxlength="200"></textarea>
                </div>                        
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
    </div>
</div>