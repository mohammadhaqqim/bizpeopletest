<div class="modal" tabindex="-1" id="workingWeekEditModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form method="POST" id="workweek" action="/workweek">
        @csrf
        @method('PATCH')

        <div class="modal-header">
          <h5 class="modal-title">Edit Working Week</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col">
                    <label for="title">Working Week Name</Title></label>
                    <input type="text" class="form-control weekTitle" id="title" name="title" placeholder="Working Week Name" required maxlength="100">
                </div>                        
            </div>
            <div class="row">
                <div class="custom-control custom-checkbox ml-3">
                    <input class="custom-control-input defaultWeek" type="checkbox" id="defaultWeek" name="defaultWeek">
                    <label class="custom-control-label" for="defaultWeek">
                      Set As Default
                    </label>
                </div>
            </div>
            <div class="row mb-1 d-flex justify-content-center">
                <label for="" class="col-3">Monday</label> 
                <input type="number" name="mon" id="mon" class="form-control col-2 mon" step="0.1" min="0" value="0">
            </div>
            <div class="row mb-1 d-flex justify-content-center">
                <label for="" class="col-3">Tuesday</label> 
                <input type="number" name="tue" id="tue" class="form-control col-2 tue" step="0.1" min="0" value="0">
            </div>
            <div class="row mb-1 d-flex justify-content-center">
                <label for="" class="col-3">Wednesday</label> 
                <input type="number" name="wed" id="wed" class="form-control col-2 wed" step="0.1" min="0" value="0">
            </div>
            <div class="row mb-1 d-flex justify-content-center">
                <label for="" class="col-3">Thursday</label> 
                <input type="number" name="thu" id="thu" class="form-control col-2 thu" step="0.1" min="0" value="0">
            </div>
            <div class="row mb-1 d-flex justify-content-center">
                <label for="" class="col-3">Friday</label> 
                <input type="number" name="fri" id="fri" class="form-control col-2 fri" step="0.1" min="0" value="0">
            </div>
            <div class="row mb-1 d-flex justify-content-center">
                <label for="" class="col-3">Saturday</label> 
                <input type="number" name="sat" id="sat" class="form-control col-2 sat" step="0.1" min="0" value="0">
            </div>
            <div class="row mb-1 d-flex justify-content-center">
                <label for="" class="col-3">Sunday</label> 
                <input type="number" name="sun" id="sun" class="form-control col-2 sun" step="0.1" min="0" value="0">
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>
        </form>
      </div>
    </div>
  </div>        

