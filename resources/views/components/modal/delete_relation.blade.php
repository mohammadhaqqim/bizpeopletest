<div class="modal" tabindex="-1"  id="deleteRelationModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form method="POST" id="deleteRelation" action="/employee/{{ $employee->id }}/relation">
        @csrf          
        @method('DELETE')              

        <div class="modal-header">
          <i class="fa fa-users pr-2" id="user"></i>
          <i class="fa fa-medkit text-danger pr-2" id="med"></i>
          <h5 class="modal-title" id="relationTitle">Delete Relation</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col text-center">
                    <h5>Are you sure you would like to delete the following employee relation?</h5>
                </div>
            </div>

            <div class="row">
                <div class="form-group col">
                    <label for="name">Name</label>
                    <input type="text" class="form-control deleteName" id="name" name="name" disabled>
                </div>    
            </div>

            <div class="row">
                <div class="form-group col-sm-12 col-md-6">
                    <label for="relationship_id">Relationship</label><label class="text-danger"> *</label>
                    <input type="text" class="form-control deleteRelation" id="relation" name="relation" disabled>
                </div>       
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Delete Relation</button>
        </div>
        </form>
      </div>
    <script>

    </script>
    </div>
</div>
