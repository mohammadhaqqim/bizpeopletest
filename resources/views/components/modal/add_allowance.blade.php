<div class="modal" id="allowanceAddModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
    <form method="POST" action="/transaction">
        @csrf       
        <div class="modal-header">
            <i class="fa fa-briefcase my-auto px-2"></i> 
            <h5 class="modal-title">Add New Allowance</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col">
                    <label for="title">Allowance Title</Title></label><label class="text-danger"> *</label>
                    <input type="text" class="form-control" id="titleAdd" name="titleAdd" placeholder="Allowance Title">
                </div>
            </div>                    

            <div class="row">
                <div class="form-group col-6">
                    <label for="employment_status_id">Addition / Deduction</label>
                    <div class="input-group">
                        <select class="custom-select mr-sm-2" id="addded" name="addded">
                        <option selected>Choose...</option>
                        <option value="1">Addition</option>
                        <option value="0">Deduction</option>
                        </select>
                    </div>
                </div>        
            </div>

            <div class="row">
                <div class="form-group col-12 mb-1">
                    <label for="">Options</label>
                </div>                
                <div class="form-group col-6">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="calculatedAdd" name="calculatedAdd">
                        <label class="custom-control-label" for="calculatedAdd">Calculated</label>
                    </div>
                </div>
                <div class="form-group col-6">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="fixedAdd" name="fixedAdd">
                        <label class="custom-control-label" for="fixedAdd">Fixed</label>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save Status</button>
            </div>
        </div>
    </form>
    </div>
  </div>
</div>
