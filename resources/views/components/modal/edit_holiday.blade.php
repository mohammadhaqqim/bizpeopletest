<div class="modal" id="holidayEditModal">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form id="holiday" method="POST" action="/holiday">
        @csrf              
        @method('PATCH')     

        <div class="modal-header">
          <i class="fa fa-trash pt-1"></i>
          <h5 class="modal-title pl-2">Edit Holiday</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
            <div class="row">
                <div class="form-group col">
                    <label for="title">Holiday Title</Title></label><label class="text-danger"> *</label>
                    <input type="text" class="form-control editTitle" id="title" name="title" placeholder="Public Holiday Title" max="100">
                </div>
            </div>        
            <div class="row">
                <div class="form-group col-6">
                    <label for="start_date">Date From</label><label class="text-danger"> *</label>
                    <div class="input-group">
                        <input type="date" class="form-control editStart" id="start_date" name="start_date" placeholder="dd/mm/yyyy" 
                            value="{{ date('Y-m-d', strtotime('0 day')) }}" required >
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>    
                </div>    
                <div class="form-group col-6">
                    <label for="end_date">To Date</label>
                    <div class="input-group">
                        <input type="date" class="form-control editEnd" id="end_date" name="end_date" placeholder="dd/mm/yyyy">
                        <div class="input-group-append">
                            <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                        </div>
                    </div>    
                </div>    
            </div>
            <div class="row">
                <div class="custom-control custom-checkbox ml-3">
                    <input type="checkbox" class="custom-control-input editVariable" id="variabledate" name="variabledate">
                    <label class="custom-control-label" for="variabledate">Holiday may vary this date</label>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save Holiday</button>
        </div>
        </form>
      </div>
    </div>       
</div>
