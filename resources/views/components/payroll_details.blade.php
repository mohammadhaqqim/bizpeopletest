<div class="row pb-3 border-bottom" style="font-size: 12px;">
    <div class="justify-content-middle m-2">
        <a href="{{ $payroll->approved_by == null ? '/payroll' : '/payroll/historical' }}">
            <svg width="2.1em" height="2.1em" viewBox="0 0 16 16" class="bi bi-arrow-left" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"/>
            </svg>
        </a>
    </div>
    <div style="width: 225px;">
        <div class="row">
            <label for="" class="m-0" style="width: 60px;"><b>Title:</b></label>
            <label for="" class="m-0" style="width: 165px;">{{ $payroll->title }}</label>   
        </div>
        <div class="row">
            <label for="" class="m-0" style="width: 60px;"><b>Payroll #:</b></label>
            <label for="" class="m-0" style="width: 165px;">{{ $payroll->id }}</label>       
        </div>
    </div>
    <div style="width: 155px;">
        <div class="row">
            <label for="" class="m-0" style="width: 65px;"><b>Pay Date:</b></label>
            <label for="" class="m-0" style="width: 90px;">
                {{ \Carbon\Carbon::parse($payroll->pay_date)->format('d-m-Y') }}
            </label>   
        </div>
        <div class="row">
            <label for="" class="m-0" style="width: 65px;"><b>Due Date:</b></label>
            <label for="" class="m-0" style="width: 90px;">
                {{ \Carbon\Carbon::parse($payroll->due_date)->format('d-m-Y') }}
            </label>       
        </div>
    </div>
    <div style="width: 155px;">
        <div class="row">
            <label for="" class="m-0" style="width: 65px;"><b>Start Date:</b></label>
            <label for="" class="m-0" style="width: 90px;">
                {{ \Carbon\Carbon::parse($payroll->start_date)->format('d-m-Y') }}
            </label>   
        </div>
        <div class="row">
            <label for="" class="m-0" style="width: 65px;"><b>End Date:</b></label>
            <label for="" class="m-0" style="width: 90px;">
                {{ \Carbon\Carbon::parse($payroll->end_date)->format('d-m-Y') }}
            </label>       
        </div>
    </div>
    <div class="col my-auto d-flex justify-content-end px-0">
        @if($payroll->approved_by == null)
        <button type="button" class="btn btn-sm {{ Request::is('payroll/*/employees') ? 'btn-success' : 'btn-outline-success' }}" 
            onclick="window.location='{{ url('payroll/'.$payroll->id.'/employees') }}'">Employees</button>
        <button type="button" class="btn btn-sm {{ Request::is('payroll/*/transactions') ? 'btn-success' : 'btn-outline-success' }} mx-2" 
            onclick="window.location='{{ url('payroll/'.$payroll->id.'/transactions') }}'">Transactions</button>
        <button type="button" class="btn btn-sm {{ Request::is('payroll/*/approval') ? 'btn-success' : 'btn-outline-success' }}"
            onclick="window.location='{{ url('payroll/'.$payroll->id.'/approval') }}'">Approval</button>
        @else
        <button type="button" class="btn btn-sm {{ Request::is('payroll/historical/*/employees') ? 'btn-success' : 'btn-outline-success' }}" 
            onclick="window.location='{{ url('payroll/historical/'.$payroll->id.'/employees') }}'">Employees</button>
        <button type="button" class="btn btn-sm {{ Request::is('payroll/historical/*/transactions') ? 'btn-success' : 'btn-outline-success' }} mx-2" 
            onclick="window.location='{{ url('payroll/historical/'.$payroll->id.'/transactions') }}'">Transactions</button>
        <button type="button" class="btn btn-sm {{ Request::is('payroll/historical/*/approval') ? 'btn-success' : 'btn-outline-success' }}"
            onclick="window.location='{{ url('payroll/historical/'.$payroll->id.'/approval') }}'">Approval</button>
        @endif
    </div>                
</div>