    <div class="p-2 bg-biz position-relative employee-header d-flex align-items-center justify-content-center justify-content-md-start " >
        {{-- photo --}}
        <div class="bg-white side-profile-image position-absolute rounded d-flex align-items-center justify-content-center overflow-hidden" 
          style="z-index: 99;">
            <img src="/images/male-user.png" alt="" id="profile_pic" class="image" >
            <div class="overlay text-center my-auto " >
              <a href="" class="icon" onclick="loadRead()">
                <i class="fa fa-camera text-dark pt-5 mt-2" id="select_logo" style="font-size: 25px;"></i>

              </a>
            </div>
        </div>

<style>
  .image{
    display: block;
    width: 100%;
    height: auto;
  }

  .overlay{
    position: absolute;
    top: 0px;
    left: 0;
    height: 100%;
    width: 100%;
    opacity: 0;
    background-color: white;
  }

  .side-profile-image:hover .overlay {
    opacity: 1;
  }  

  .hide{
    opacity:0;
  }


</style>

<script>
    window.onscroll = function() {myFunction()};

    $("#avatar").css('opacity','0');
    
    $("#select_logo").click(function(e){
       e.preventDefault();
       $("#avatar").trigger('click');
    });

    function loadRead(){
      var fileInput = document.getElementById('avatar');
      if(fileInput) {
          fileInput.click();
      }
    }

    var loadFile = function(event) {
      var output = document.getElementById('profile_pic');
      output.src = URL.createObjectURL(event.target.files[0]);

      output.onload = function(e) {     
        URL.revokeObjectURL(output.src) // free memory
      }
    };

    function render(src){
      var image = new Image();
      var MAX_HEIGHT = 136;

      image.onload = function(){
        var canvas = document.getElementById("myCanvas");
        if(image.height > MAX_HEIGHT) {
          image.width *= MAX_HEIGHT / image.height;
          image.height = MAX_HEIGHT;
        }
        var ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        canvas.width = image.width;
        canvas.height = image.height;
        ctx.drawImage(image, 0, 0, image.width, image.height);
      };
      image.src = src;
    }    

</script>


{{-- Name and job title mt-sm-3 my-md-3 my-lg-4 mt-4 mb-4  --}}
        <div class="pb-0 page-title position-relative" >


          <h2 class="pb-0 mb-0 text-white d-none d-sm-block">{{ $employee->name }}</h2>
          <h4 class="pb-0 mb-0 text-white d-block d-sm-none">{{ $employee->name }}</h4>
          @if (!is_null($employee->currentjob))
          <h5 class="text-white d-none d-sm-block">{{ $employee->getjob() == null ? '' : $employee->getjob()->title }}</h5>
          <label class="text-white d-block d-sm-none mb-0">{{ $employee->getjob() == null ? '' : $employee->getjob()->title }}</label>
          @endif
          {{-- <label class="text-white ">Ironer</label> --}}

          <input type="file" id="avatar" name="avatar" onchange="loadFile(event)" 
            accept=".jpg, .jpeg, .png, .gif" hidden>

        </div>

    </div>
    <div id="page-title-menu2" class="page-title-menu2 bg-biz sticky-top d-flex justify-content-center justify-content-md-start justify-content-lg-start" 
        style="z-index: 90;">
        <div class="d-inline text-center text-white sidebar-name" >
          <label id="sidebar-name" for="" style="width: 255px; font-size: 18px;" class="hide"></label>
        </div>       
        <div class="d-inline-flex overflow-auto">
          <ul class="nav nav-tabs flex-nowrap" >
              <li class="nav-item">
                <a class="nav-link {{ Request::is('employee/*') || Request::is('people/personal/*') ? 'active' : 'text-white' }}" href="/people/personal/{{ $employee->id }}/edit">Personal</a>
              </li>
              <li class="nav-item">
                <a class="nav-link {{ Request::is('jobs/*') || Request::is('people/jobs/*') ? 'active' : 'text-white' }}" href="/people/jobs/{{ $employee->id }}/edit">Job</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link {{ Request::is('allowances/*') || Request::is('people/allowances/*') ? 'active' : 'text-white' }}" href="/people/allowances/{{ $employee->id }}/edit">Allowances</a>
              </li>  
              <li class="nav-item">
                <a class="nav-link text-nowrap {{ Request::is('timeoff/*') || Request::is('people/timeoff/*') ? 'active' : 'text-white' }}" href="/people/timeoff/{{ $employee->id }}/">Time Off</a>
              </li>
  {{--                     <li class="nav-item">
                <a class="nav-link  {{ Request::is('emergency/*') ? 'active' : 'text-white' }}" href="#">Emergency</a>
              </li> --}}
              <li class="nav-item">
                <a class="nav-link  {{ Request::is('relations/*') || Request::is('people/relations/*') ? 'active' : 'text-white' }}" href="/people/relations/{{ $employee->id }}/edit">Family</a>
              </li>
          </ul>
        </div>
    </div>
