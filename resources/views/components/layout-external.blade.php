<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

         <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> 

{{--         <script src="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.js"></script>
        <link href="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.css" rel="stylesheet">   --}}
              
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>

        <link rel="stylesheet" href="{{ asset('css/bizit.css') }}">

    </head>
    <body class="vh-100 d-flex flex-column m-0"> 

        <nav class="px-3 bg-light-biz shadow fixed-top ">
            <li class="nav-item dropdown d-md-none d-inline-block mr-1">
                <a class="nav-link border px-2" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="margin-top: 2px;">
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-list" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M2.5 11.5A.5.5 0 0 1 3 11h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 3h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
                    </svg>
                </a>    
{{--                 <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Menu</a> --}}
                <div class="dropdown-menu">
                    <a class="dropdown-item {{ Request::is('home/*') || Request::is('home') ? 'color-biz' : '' }}" href="/home">
                        <i class="fa fa-home text-center mr-1"></i> Home 
                        <span class="sr-only">(current)</span>
                    </a>
                @if ( Auth::user()->employee_id != null )                
                    <a class="dropdown-item {{ Request::is('employee/*') || Request::is('employee') ? 'color-biz' : '' }}" href="/employee/{{ Auth::user()->employee_id }}/edit">
                        <i class="fa fa-user text-center mr-1"></i> Personal
                    </a>
                @endif
                @if ( Auth::user()->payroll )
                    <a class="dropdown-item {{ Request::is('payroll/*') || Request::is('payroll') ? 'color-biz' : '' }}" href="/payroll/">
                        <i class="fa fa-money text-center mr-1"></i> Payroll
                    </a>
                @endif

                    <a class="dropdown-item {{ Request::is('people/*') || Request::is('people') ? 'color-biz' : '' }}" href="/people/">
                        <i class="fa fa-users text-center mr-1"></i> People
                    </a>

                @if ( Auth::user()->payroll || Auth::user()->hr || Auth::user()->admin )
                    <a class="dropdown-item {{ Request::is('reports/*') || Request::is('reports') ? 'color-biz' : '' }}" href="/reports">
                        <i class="fa fa-bar-chart text-center mr-1"></i> Reports
                    </a>
                @endif

                </div>
            </li>

                <a class="navbar-brand align-items-stretch side-profile mr-0 " href="#">

                @auth
                    <img src="/storage/{{ Session::get('logo') }}" alt="" style="height: 30px">
                @else
                    <img src="/images/BizPeopleBlack.png" alt="" style="height: 30px">
                @endauth

                </a>      

            <div class="navbar-brand mr-auto py-0 align-items-stretch d-none d-md-inline-block">
                <ul class="nav ">
                    <li class="nav-item active">
                        <a class="nav-link {{ Request::is('home/*') || Request::is('home') ? 'color-biz' : '' }}" href="/home">Home <span class="sr-only">(current)</span></a>
                    </li>

                    @if ( Auth::user()->employee_id != null )                
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('employee/*') || Request::is('employee') ? 'color-biz' : '' }}" href="/employee/{{ Auth::user()->employee_id }}/edit">Personal</a>
                    </li>
                    @endif
                    
                    @if ( Auth::user()->payroll )
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('payroll/*') || Request::is('payroll') ? 'color-biz' : '' }}" href="/payroll/">Payroll</a>
                    </li>
                    @endif

                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('people/*') || Request::is('people') ? 'color-biz' : '' }}" href="/people/">People</a>
                    </li>

                    @if ( Auth::user()->payroll || Auth::user()->hr || Auth::user()->admin )
                    <li class="nav-item">
                        <a class="nav-link {{ Request::is('reports/*') || Request::is('reports') ? 'color-biz' : '' }}" href="/reports">Reports</a>
                    </li>
                    @endif
                    
                </ul>
            </div>
            <div class="navbar-brand float-right py-0">
                <ul class="nav">

                @if ( Auth::user()->admin )                
                <li class="nav-item">
                    <a class="nav-link px-2 {{ Request::is('settings/*') || Request::is('settings') || Request::is('*/approval/*') ? 'color-biz' : '' }}" href="/settings/company">
                    <i class="fa fa-cog text-center" data-toggle="tooltip" title = "Settings"></i>
                    </a>
                </li>
                @endif

                <li>
                    @auth
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->getInitals() }} <span class="caret"></span>
                    </a>
                    @endauth
                    
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out text-center" data-toggle="tooltip" title = "Logout"></i> {{ __('Logout') }} 
                        </a>
                        <a class="dropdown-item"" href="password/change" >
                            <i class="fa fa-asterisk textcenter" title="ChangePassword"></i> {{ __('Change Passord') }} 
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    
                    </div>
                </li>

                </ul>
            </div>                
        </nav>
        
        <main class="container-fluid overflow-auto px-0 vh-100">
            @include('components.flash-message')
            {{ $slot }}
        </main>

    </body>
</html>
