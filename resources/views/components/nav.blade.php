<nav class="px-3 bg-light-biz shadow fixed-top ">
    <li class="nav-item dropdown d-md-none d-inline-block mr-1">
        <a class="nav-link border px-2" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="margin-top: 2px;">
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-list" viewBox="0 0 16 16">
                <path fill-rule="evenodd" d="M2.5 11.5A.5.5 0 0 1 3 11h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4A.5.5 0 0 1 3 3h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"/>
            </svg>
        </a>    

        <div class="dropdown-menu">

            <a class="dropdown-item {{ Request::is('home/*') || Request::is('home') ? 'color-biz' : '' }}" href="/home">
                <i class="fa fa-home text-center mr-1"></i> Home. 
                <span class="sr-only">(current)</span>
            </a>


    @tenant 
        @if ( Auth::user()->employee_id != null )                
            <a class="dropdown-item {{ Request::is('employee/*') || Request::is('employee') ? 'color-biz' : '' }}" href="/employee/{{ Auth::user()->employee_id }}/edit">
                <i class="fa fa-user text-center mr-1"></i> Personal -{{ Auth::user()->employee_id }}-
            </a>
        @endif

        @if ( optional(request()->access())->payroll )
            <a class="dropdown-item {{ Request::is('payroll/*') || Request::is('payroll') ? 'color-biz' : '' }}" href="/payroll/">
                <i class="fa fa-money text-center mr-1"></i> Payroll
            </a>
        @endif

            <a class="dropdown-item {{ Request::is('people/*') || Request::is('people') ? 'color-biz' : '' }}" href="/people/">
                <i class="fa fa-users text-center mr-1"></i> People
            </a>

        @if ( optional(request()->access())->payroll || optional(request()->access())->hr || optional(request()->access())->payroll )
            <a class="dropdown-item {{ Request::is('reports/*') || Request::is('reports') ? 'color-biz' : '' }}" href="/reports">
                <i class="fa fa-bar-chart text-center mr-1"></i> Reports
            </a>
        @endif
    @endtenant

        </div>
    </li>

    @tenant
    <div class="navbar-brand align-items-stretch side-profile mr-0 " href="#">
        <div class="text-left">

{{--             @if($company->logo == null)
                <img src="/images/b.png" alt="" class="d-inline" style="height: 30px">
            @else
                <img src="/storage/{{ $company->logo }}" alt="" style="height: 30px">
            @endif
                
            <a href="" class="navbar-brand">
                {{ optional(request()->tenant())->name ?: config('app.name') }}
            </a> --}}

        </div>
    </div>      
    @endtenant

    <div class="navbar-brand mr-auto py-0 align-items-stretch d-none d-md-inline-block">
        <ul class="nav mt-1">

            <li class="nav-item active">
                <a class="nav-link {{ Request::is('home/*') || Request::is('home') ? 'color-biz' : '' }}" href="/home">
                    Home
                    <span class="sr-only">(current)</span>
                </a>
            </li>


        @tenant   

            @if ( optional(request()->access())->employee_id != null )                
            <li class="nav-item ">
                <a class="nav-link {{ Request::is('employee/*') || Request::is('employee') ? 'color-biz' : '' }}" href="/employee/{{ optional(request()->access())->employee_id }}/edit">Personal</a>
            </li>
            @endif
            
            @if ( optional(request()->access())->payroll )
            <li class="nav-item">
                <a class="nav-link {{ Request::is('payroll/*') || Request::is('payroll') ? 'color-biz' : '' }}" href="/payroll/">Payroll</a>
            </li>
            @endif
            
            @if ( optional(request()->access())->business_id > 0 )                
            <li class="nav-item">
                <a class="nav-link {{ Request::is('people/*') || Request::is('people') ? 'color-biz' : '' }}" href="/people/">People</a>
            </li>
            @endif
    
            @if ( optional(request()->access())->payroll || optional(request()->access())->hr || optional(request()->access())->payroll )
            <li class="nav-item">
                <a class="nav-link {{ Request::is('reports/*') || Request::is('reports') ? 'color-biz' : '' }}" href="/reports">Reports</a>
            </li>
            @endif
        @endtenant
            
        </ul>
    </div>

    <div class="navbar-brand float-right py-0">
        <ul class="nav">

        @if ( optional(request()->access())->admin )                
        <li class="my-1">
            <a class="nav-link px-2 {{ Request::is('settings/*') || Request::is('settings') || Request::is('*/approval/*') ? 'color-biz' : '' }}" href="/settings/company">
            <i class="fa fa-cog text-center" data-toggle="tooltip" title = "Settings"></i>
            </a>
        </li>
        @endif

        <li class="my-1">
            
            <div id="navbadDropdown" href="#" class="nav-link dropdown-toggle text-info" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>                               
                <i class="fa fa-building-o" data-toggle="tooltop" title="Switch Companies"></i>
            </div>

            
             <div class="dropdown-menu" aria-labelledby="navbadDropdown">
                @if($companies->count())
                    @foreach ($companies as $company)
                        <a href="{{ route('tenant.switch', $company) }}" class="dropdown-item">
                            {{ $company->name }}
                        </a>
                    @endforeach
                @endif
                @if(auth()->user()->db_admin)
                    <div class="dropdown-divider"></div>
                <a href="{{ route('businesses.create') }}" class="dropdown-item">New Company</a>
                @endif
            </div>
        </li>

        <li  class="my-1">
            @auth
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                {{-- {{ Auth::user()->getInitals() }} --}}
                <i class="fa fa-user-circle" data-toggle="tooltop" title="{{ Auth::user()->getInitals() }}"></i><span class="caret"></span>
            </a>
            @endauth
            
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                    <i class="fa fa-sign-out text-center" data-toggle="tooltip" title = "Logout"></i> {{ __('Logout') }} 
                </a>
                <a class="dropdown-item"" href="password/change" >
                    <i class="fa fa-asterisk textcenter" title="ChangePassword"></i> {{ __('Change Passord') }} 
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            
            </div>
        </li>

        </ul>
    </div>                

</nav>
