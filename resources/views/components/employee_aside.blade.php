
<aside class="empsidebar bg-aside mx-3 ">
    <div class="d-inline text-center text-white pl-2" >
        <label id="sidebar-name" for="" style="font-size: 18px;" class="text-primary mr-3">{{ $employee->name }}</label>
    </div>

    <div class="row mx-2 text-secondary" style="font-size: 12px;">
    
    @if( !is_null($employee->work_email) && !is_null($employee->work_phone_ext 
        && !is_null($employee->work_phone) && !is_null($employee->hand_phone)) ) 
        <label for="" class="col-12 px-0 text-primary mt-3 pt-1">Contact Details</label>
    @endif    
    
    @if( !is_null($employee->work_email) ) 
        <div class="col-12 px-0" >
            <i class="fa fa-envelope my-auto px-2"></i> {{ $employee->work_email }}
        </div>
    @endif

    @if( !is_null($employee->work_phone) ) 
        <div class="col-7 px-0">
            <i class="fa fa-building my-auto px-2"></i> {{ $employee->work_phone}}
        </div>
    @endif

    @if( !is_null($employee->work_phone_ext) ) 
        <div class="col-5 pl-0">
            Ext. {{ $employee->work_phone_ext }}
        </div>
    @endif
    
    @if( !is_null($employee->hand_phone) ) 
        <div class="col-md-12 px-0">
            <i class="fa fa-mobile my-auto px-2 " style="font-size: 20px;"></i>
            {{ $employee->hand_phone }}
        </div>
    @endif

        <label for="" class="col-12 px-0 border-top text-primary mt-4 pt-1">Hired Date</label>
        <div class="col-12 px-0 pl-2">
            {{ Carbon\Carbon::parse($employee->date_joined)->format('d F Y') }}
        </div>
        <div class="col-12 px-0 pl-2" style="font-size: 10px;">
            ( {{ $employee->getServiceYearsAttribute() }} years - {{ $employee->getServiceMonthsAttribute() }} months )
        </div>

        <label for="" class="col-12 px-0 border-top text-primary mt-4 pt-1">Company Details</label>    
        <div class="col-12 px-0">
            <i class="fa fa-hashtag my-auto px-2"></i> {{ $employee->id }}
        </div>
        @if (!is_null($employee->currentStatus))
        <div class="col-12 px-0">
            <i class="fa fa-vcard my-auto px-2"></i> {{ $employee->currentStatus->title }}
        </div>
        @endif
        @if (!is_null($employee->getCurrentDepartment()))
        <div class="col-12 px-0">
            <i class="fa fa-building my-auto px-2"></i> {{ $employee->getCurrentDepartment()->title }}
        </div>
        @endif
        @if (!is_null($employee->getJob()))
        <div class="col-12 px-0">
            <i class="fa fa-wrench my-auto px-2"></i> {{ $employee->getJob()->title }}
        </div>
        @endif

    </div>
</aside>
