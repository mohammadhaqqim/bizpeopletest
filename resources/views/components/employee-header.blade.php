<x-layout>
    <div class="p-2 bg-biz position-relative" style="height: 162px;">
        <div class="bg-white side-profile-image position-absolute rounded d-flex align-items-center justify-content-center" style="">
            <i class="fa fa-user-o" ></i> 
        </div>
        <div class=" header-menu h-100">
            <div class="mt-5 page-title" >
                <h2 class="pb-4 text-white">Regan Nagorcka..</h2>
            </div>
            <div class="page-title-menu ">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                      <a class="nav-link active" href="/personnel/{{ $employee->id }}/edit">Personal</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="/jobs/{{ $employee->id }}/edit">Job</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="#">Time Off</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link text-white" href="#">Emergency</a>
                    </li>
                  </ul>
            </div>
        </div>
    </div>
</x-layout>    