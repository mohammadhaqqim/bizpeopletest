<div class="modal" id="editContribution{{ $type }}">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form method="POST" action="/contribution/{{ $type }}">
        @csrf
        @method('PATCH')

        <div class="modal-header">
          <h5 class="modal-title">Modify Staff Contributions - {{ strtoupper($type) }}</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

            <div class="row">
                <div class="form-group col">
                    <label for="country">Company Name</label>
                    <input type="text" class="form-control" id="company_name" name="company_name" placeholder="Company Name" value="{{ $company->company_name }}" disabled>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-sm-4 col-6 mb-2">
                    <label for="email_payslip">Use {{ strtoupper($type) }} (%)</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <input type="checkbox" name="use_{{ $type }}" id="use_{{ $type }}" {{ $company['use_'.$type] ? 'checked' : '' }}>
                            </div>
                        </div>
                        <input type="number" class="form-control" id="{{ $type }}_percent" name="{{ $type }}_percent" min="0" step="0.1"
                            placeholder="SCP Percentage" value="{{ $company[$type.'_percent'] }}">
                    </div>    
                </div>

                <div class="form-group col-sm-4 col-6 mb-2">
                    <label for="email_payslip">{{ strtoupper($type) }} min $</label>
                    <div class="input-group">
                        <input type="number" class="form-control" id="{{ $type }}_min" name="{{ $type }}_min" min="0" step="0.1"
                            placeholder="scp Minimum" value="{{ $company->scp_min }}">
                    </div>    
                </div>
                <div class="form-group col-sm-4 col-6 mb-2">
                    <label for="email_payslip">{{ strtoupper($type) }} max $</label>
                    <div class="input-group">
                        <input type="number" class="form-control" id="{{ $type }}_max" name="{{ $type }}_max" min="0" step="0.1"
                            placeholder="scp Maximum" value="{{ $company[$type.'_max'] }}">
                    </div>    
                </div>         

            </div>

            <div class="row mb-3">
                <div class="form-group col-sm-4 col-6 mb-2">
                    <label for="email_payslip">{{ strtoupper($type) }} Decimals</label>
                    <div class="input-group">
                        <input type="number" class="form-control" id="{{ $type }}_decimals" name="{{ $type }}_decimals" min="0" max="2"
                            placeholder="" value="{{ isset($company[$type.'_decimals']) ? $company[$type.'_decimals'] : 2 }}">
                    </div>    
                </div>  
            </div> 
 
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Save changes</button>
        </div>

        </form>
      </div>
    </div>
  </div> 