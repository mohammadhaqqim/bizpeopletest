<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

        <script src="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.js"></script>
        <link href="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.css" rel="stylesheet">        

        <style>
            html, body {
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .page-title, .page-title-menu{
                 padding-left: 255px; 
            }

            .side-profile{
                width: 245px;
            }

            .side-profile-image{
                top: 13px;
                left: 55px;
                width: 140px;
                height: 140px;
                border: 2px solid blue;
            }

            .side-profile-image i{
                font-size: 120px;
            }

            .bg-biz{
                background-color: #0074E8;
            }

            .color-biz{
                color: #41AF4B;
            }

            .text-biz-blue{
                color: #0074E8;
            }
            .text-biz-green{
                color: #41AF4B;
            }

            .bg-light-biz{
                background-color: #F1F1F3;
            }

            .nav .nav-item:hover{
                background-color: white;
            }

            .nav-tabs .nav-link:hover{
                color: green !important;
            }

            .second-menu .nav-link{
                color: white;    
            }

            .second-menu .nav-link:hover{
                color: #0074E8;
                border-radius: 25px;
            }


            .second-menu .nav-link.active{
                color: #0074E8;
                background-color: white;
                
            }

            .staff-image, .staff-image i{
                width: 175px;
                height: 175px;                
            }

            .header-image{
                width: 255px;
            }

            main{
                padding-top: 45px;
            }

            .biz-height{
                min-height: 100%;
            }

            
    .main-content, aside{
        height: calc(100vh - 217px);
    }
    aside{
        width: 225px;
    }
    .bg-aside{
        background-color: #E9ECEF;
        color: white;
        color: darkslategray;
    }
    .main{
        margin-left: 255px;
    }
    .tbl-dark{
        background-color: rgb(230, 230, 230);
        color: white;
        color: darkslategray;
        border-radius: 15px !important; 
    }
        </style>
    </head>
    <body> 
        <div class="d-flex flex-column vh-100">

            <nav class="px-3 bg-light-biz shadow fixed-top ">
                <a class="navbar-brand align-items-stretch side-profile mr-0" href="#">
{{--                     @if ( !is_null($company->logo) )
                    <img src="{{ $company->logo }}" alt="" style="height: 30px">                    
                    @else
 --}}                    <img src="/images/BizPeopleBlack.png" alt="" style="height: 30px">
{{--                     @endif
 --}}
{{--                     <i class="fa fa-id-badge" style="font-size: 22px;"></i> --}}
                    {{-- {{ config('app.companyname', 'BizPeople') }} --}}

                </a>            
                <div class="navbar-brand mr-auto py-0 align-items-stretch">
                    <ul class="nav ">
                        <li class="nav-item active">
                            <a class="nav-link color-biz" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="personnel">Personal</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Payroll</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">People</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Reports</a>
                        </li>
                    </ul>
                </div>
                <div class="navbar-brand float-right py-0">
                    <ul class="nav">
                    <li class="nav-item ">
                        <a class="nav-link px-2" href="#" data-toggle="tooltip" title = "Logout">
                        <i class="fa fa-power-off  text-center"></i>              
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-2" href="/settings/">
                        <i class="fa fa-cog text-center" data-toggle="tooltip" title = "Settings"></i>
                        </a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link px-2" href="#">
                        <i class="fa fa-sign-out text-center" data-toggle="tooltip" title = "Logout"></i>              
                        </a>
                    </li>
                    </ul>
                </div>                
            </nav>
            
            <main class="container-fluid overflow-auto px-0 vh-100">
                {{-- @yield('content') --}}
                {{ $slot }}
            </main>

        </div>

    </body>
</html>
