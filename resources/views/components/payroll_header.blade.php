<div id="page-title-menu2" class="page-title-menu2 bg-biz sticky-top" style="padding-top: 5px; z-index: 90;">
    <div class="d-inline-flex">
      <ul class="nav nav-tabs " >
{{--           <li class="nav-item">
          <a class="nav-link {{ Request::is('payroll/dashboard*') ? 'active' : 'text-white' }}" href="/payroll/dashboard">Home</a>
          </li> --}}
          <li class="nav-item">
            @if( Request::is('payroll/historical*') )
            <a class="nav-link text-white" href="/payroll/">Payroll Center</a>
            @else
            <a class="nav-link {{ ( Request::is('payroll') or Request::is('payroll*transactions') or Request::is('payroll/*/employees') 
              or Request::is('payroll*approval') ) ? 'active' : 'text-white' }}" href="/payroll/">Payroll Center</a>
            @endif
          </li>
          <li class="nav-item">
              <a class="nav-link {{ Request::is('payroll/employees*') ? 'active' : 'text-white' }}" href="/payroll/employees">Employees</a>
          </li>  
          <li class="nav-item">
            <a class="nav-link  {{ Request::is('payroll/historical*') ? 'active' : 'text-white' }}" href="/payroll/historical">Historical</a>
          </li>
{{--           <li class="nav-item">
            <a class="nav-link  {{ Request::is('payroll/reports*') ? 'active' : 'text-white' }}" href="/payroll/reports/audit">Reports</a>
          </li> --}}
 
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle {{ Request::is('payroll/reports*') ? 'active' : 'text-white' }}" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Reports
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
              <a class="dropdown-item" href="/payroll/reports/audit">Audit Report</a>
            </div>
          </li>

      </ul>
    </div>
</div>