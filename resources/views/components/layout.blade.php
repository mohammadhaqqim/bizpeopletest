<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'BizPeople') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script> 

{{--         <script src="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.js"></script> --}}
{{--         <link href="//rawgithub.com/indrimuska/jquery-editable-select/master/dist/jquery-editable-select.min.css" rel="stylesheet">   --}}
              
        <script src="{{ asset('js/jquery-editable-select.min.js') }}"></script>
        <link rel="stylesheet" href="{{ asset('css/jquery-editable-select.min.css') }}">

        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
        <link rel="stylesheet" href="{{ asset('css/bizit.css') }}">

    </head>
    <body class="vh-100 d-flex flex-column m-0"> 
        @tenant
            @include('components._nav')
        @else
            @include('components.nav')
        @endtenant

        <main class="container-fluid overflow-auto px-0 vh-100">
            @include('components.flash-message')
            {{ $slot }}
        </main>

    </body>
</html>
