

<nav class="navbar sticky-top navbar-light bg-muted ">
    <div class="navbar-brand mr-auto py-0 align-items-stretch pl-3 pr-5" style="font-size: 14px;">
        <ul class="nav d-flex flex-row flex-nowrap">
            <li class="nav-item p-1">
                <a class="nav-link py-0 {{ Request::is('settings/company/*') ? 'active' : '' }}" href="/settings/company">Company</a>
            </li>
            <li class="nav-item p-1">
                <a class="nav-link py-0 {{ Request::is('settings/access/*') ? 'active' : '' }}" href="/settings/access/">Access Levels</a>
            </li>
            <li class="nav-item p-1">
                <a class="nav-link py-0 {{ Request::is('settings/week/*') ? 'active' : '' }}" href="/settings/week/">Working Week</a>
            </li>
            <li class="nav-item p-1">
                <a class="nav-link py-0 {{ Request::is('settings/holidays/*') ? 'active' : '' }}" href="/settings/holidays/edit">Holidays</a>
            </li>
            <li class="nav-item p-1">
                <a class="nav-link py-0 {{ Request::is('settings/timeoff/*') ? 'active' : '' }}" href="/settings/timeoff">Leave</a>
            </li>

        </ul>
    </div>

</nav>

{{-- <aside class="mr-0 mx-3 bg-aside mx-2 p-2" style="height: 100%;">        
    <div class="mt-3">
        <ul class="nav">
            <li class="nav-item p-1">
                <a class="nav-link {{ Request::is('settings/company/*') ? 'active' : '' }}" href="/settings/company">Comapany</a>
            </li>
            <li class="nav-item p-1">
                <a class="nav-link {{ Request::is('settings/access/*') ? 'active' : '' }}" href="/settings/access/">Access Levels</a>
            </li>
            <li class="nav-item p-1">
                <a class="nav-link {{ Request::is('settings/week/*') ? 'active' : '' }}" href="/settings/week/">Working Week</a>
            </li>
            <li class="nav-item p-1">
                <a class="nav-link {{ Request::is('settings/holidays/*') ? 'active' : '' }}" href="/settings/holidays/edit">Holidays</a>
            </li>
            <li class="nav-item p-1">
                <a class="nav-link {{ Request::is('settings/timeoff/*') ? 'active' : '' }}" href="/settings/timeoff">Leave</a>
            </li>

        </ul>
    </div>
</aside>
 --}}