
<aside class="empsidebar bg-aside mx-3">
    <div class="d-inline text-center text-white pl-2" >
        <label id="sidebar-name" for="" style="font-size: 18px;" class="text-primary">{{ $employee->name }}</label>
    </div>      

    <div class="row mx-2 text-secondary" style="font-size: 12px;">
        <label for="" class="col-12 px-0 text-primary mt-3 pt-1">Contact Details</label>
    
        <div class="col-12 px-0" >
            <i class="fa fa-envelope my-auto px-2"></i> {{ $employee->work_email }}
        </div>

        <div class="col-7 px-0">
            <i class="fa fa-building my-auto px-2"></i> {{ $employee->work_phone}}
        </div>
        <div class="col-5 pl-0">
            Ext. {{ $employee->work_phone_ext }}
        </div>
        <div class="col-md-12 px-0">
            <i class="fa fa-mobile my-auto px-2 " style="font-size: 20px;"></i>
            {{ $employee->hand_phone }}
        </div>

        <label for="" class="col-12 px-0 border-top text-primary mt-4 pt-1">Hired Date</label>
        <div class="col-12 px-0 pl-2">
            {{ Carbon\Carbon::parse($employee->date_joined)->format('d F Y') }}
        </div>
        <div class="col-12 px-0 pl-2" style="font-size: 10px;">
            ( {{ $employee->getServiceYearsAttribute() }} years - {{ $employee->getServiceMonthsAttribute() }} months )
        </div>

        <label for="" class="col-12 px-0 border-top text-primary mt-4 pt-1">Company Details</label>    
        <div class="col-12 px-0">
            <i class="fa fa-hashtag my-auto px-2"></i> {{ $employee->id }}
        </div>
        <div class="col-12 px-0">
            <i class="fa fa-vcard my-auto px-2"></i> {{ $employee->currentStatus->title }}
        </div>
        <div class="col-12 px-0">
            <i class="fa fa-building my-auto px-2"></i> {{ $employee->getCurrentDepartment()->title }}
        </div>
        <div class="col-12 px-0">
            <i class="fa fa-wrench my-auto px-2"></i> {{ $employee->currentjob()->title }}
        </div>

    </div>
</aside>
