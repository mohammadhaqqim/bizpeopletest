<div class="row border-bottom px-0 py-2 mx-2 justify-content-center justify-content-md-start justify-content-lg-start justify-content-xl-start">
    <i class="fa fa-cog my-auto px-2"></i> 
    <h2 class="mb-0">Settings</h2>
</div> 

<div class="row overflow-auto d-block d-sm-block d-md-none">
    @include('components.settings_horizontal')
</div>

<aside class="float-left mr-0 mx-3 bg-aside mx-2 p-2 d-none d-md-block" style="height: 100%;">        
    <div class="sidebar-sticky mt-3">
        <ul class="nav flex-column">
            <li class="nav-item p-1">
                <a class="nav-link {{ Request::is('settings/company/*') ? 'active' : '' }}" href="/settings/company">Company</a>
            </li>
            <li class="nav-item p-1">
                <a class="nav-link {{ Request::is('settings/access/*') ? 'active' : '' }}" href="/settings/access/">Access Levels</a>
            </li>
            <li class="nav-item p-1">
                <a class="nav-link {{ Request::is('settings/week/*') ? 'active' : '' }}" href="/settings/week/">Working Week</a>
            </li>
            <li class="nav-item p-1">
                <a class="nav-link {{ Request::is('settings/holidays/*') ? 'active' : '' }}" href="/settings/holidays">Holidays</a>
            </li>
            <li class="nav-item p-1">
                <a class="nav-link {{ Request::is('settings/timeoff/*') ? 'active' : '' }}" href="/settings/timeoff">Leave</a>
            </li>
            <li class="nav-item p-1">
                <a class="nav-link {{ Request::is('settings/customise/*') ? 'active' : '' }}" href="/settings/customise">Customise Data</a>
            </li>
            <li class="nav-item p-1">
                <a class="nav-link {{ Request::is('employee-import/*') ? 'active' : '' }}" href="/employee-import">Employee Import</a>
            </li>
            <li class="nav-item p-1">
                <a class="nav-link {{ Request::is('settings/allowance/*') ? 'active' : '' }}" href="/settings/allowance">Allowances</a>
            </li>
        </ul>
    </div>
</aside>
