@extends('layouts.external')

@section('content')

<div class="biz-login rounded flex-column p-4 m-3 bg-white">

    <div class="header px-auto" >
        <div class="text-center mt-3" >
            <img src="/images/b.png" alt="" style="height: 50px">
            <h1>BizPeople</h1>
        </div> 
        <div class="text-center pt-2">
            <h4>Forgot Your Password?</h4>
        </div>
        <div class="text-center mt-3" style="font-size: 14px;">
            <p>Enter your email address and we will send you the instructions to reset your password.</p>
        </div>
        <form method="POST" action="{{ route('password.email') }}">
            @csrf

            <div class="form-group mb-1">

                <small for="exampleInputEmail1" class="text-muted mb-2">Email</small>            
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus style="height: 50px;">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>

            <div class="my-3" >
                <button type="submit" class="col btn btn-primary" style="height: 50px;">
                    {{ __('Continue') }}
                </button>
            </div>
        </form>

        <div class="text-center col mt-4">
            <a href="/login"><small>Back to login</small></a>
        </div>

    </div>
</div>

@include('components.external_footer')  

@endsection