@extends('layouts.external')

@section('content')

<!-- bg-color #004CEF -->

<div class="biz-login rounded flex-column p-4 m-3 bg-white">
    <form method="POST" action="{{ route('login') }}">
    @csrf

    <div class="header px-auto" >
        <div class="text-center mt-3" >
            <img src="/images/b.png" alt="" style="height: 50px">
            <h1> BizPeople</h1>
        </div> 
        <div class="text-center pt-2">
            <h4>Log in</h4>
        </div>
    </div>
    <div>
        <div class="form-group mb-1" >
            <small for="exampleInputEmail1" class="text-muted mb-2">Email</small>
            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror bg-white py-3" name="email" value="{{ old('email') }}" required autocomplete="email" style="height: 50px;" autofocus>
            @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
        </div> 
        <div class="form-group mb-1" style="height: 98px;">
            <small for="exampleInputPassword" class="text-muted mb-2">Password</small>
            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror py-3" name="password" required autocomplete="current-password" style="height: 50px;">
            @error('password')
            <div class="alert alert-danger mb-1 py-0">
                <small>{{ $message }}</small>
            </div>
            @enderror    
        </div> 
    </div>
    <div class="mt-0">
        @if (Route::has('password.request'))
            <a class="btn btn-link p-0" href="{{ route('password.request') }}">
                <small>{{ __('Forgot password?') }}</small>
            </a>
        @endif
    </div>

    <div class="" >
        <button type="submit" class="col btn btn-primary" style="height: 50px;">
            {{ __('Login') }}
        </button>
    </div>
    </form>
</div>
@include('components.external_footer')  


@endsection


