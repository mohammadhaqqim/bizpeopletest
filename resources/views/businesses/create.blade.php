<x-layout>

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow">
                <h5 class="card-header bg-info text-white border-bottom-0">
                    Create a New Company
                </h5>

                <div class="card-body">
                    <form method="POST" action="{{ route('businesses.store') }}">
                        @csrf

                        <div class="form-group">
                            <label for="name" class="control-label">Company name</label><label class="text-danger"> *</label>
                            <input type="text" name="name" id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="name" class="control-label">Domain</label><label class="text-danger"> *</label>
                            <input type="text" name="domain" id="domain" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required>

                            @if ($errors->has('domain'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('domain') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group">
                            <label for="name" class="control-label">Financial Year</label><label class="text-danger"> *</label>
                            <input type="text" name="fin_year" id="fin_year" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required>

                            @if ($errors->has('domain'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('domain') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="row">
                            <div class="form-group col">
                                <label for="start_date">Start Date</label><label class="text-danger"> *</label>
                                <div class="input-group">
                                    <input type="date" class="form-control" id="start_date" name="start_date" placeholder="dd/mm/yyyy" value="{{ \Carbon\Carbon::parse($startDate)->format('Y-m-d') }}" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>    
                            </div>
                            <div class="form-group col">
                                <label for="end_date">End Date</label><label class="text-danger"> *</label>
                                <div class="input-group">
                                    <input type="date" class="form-control" id="end_date" name="end_date" onchange="changeDuePay()" placeholder="dd/mm/yyyy" value="{{ \Carbon\Carbon::parse($endDate)->format('Y-m-d') }}" required>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon4"><i class="fa fa-calendar"></i></span>
                                    </div>
                                </div>    
                            </div>
                        </div>
        

                        <button type="submit" class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</x-layout>
