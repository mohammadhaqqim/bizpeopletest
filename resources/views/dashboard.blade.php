<x-layout>

    @section('content')
    <div class="container">
        <div class="row pt-5">
            Home .. company id = {{ Session::get('companyid') }}       
        </div>
        
    </div>
    @endsection

{{--     <label for="">
        Home .. company id = {{ Session::get('logo') }}               
    </label>
    <label for="">
        Home .. tap = {{ Session::get('use_tap') }}               
    </label>
    <label for="">
        Home .. scp = {{ Session::get('use_scp') }}               
    </label>
    <label for="">
        Home .. epf = {{ Session::get('use_epf') }}               
    </label>

    <div class="chart-container biz-chart" >
        <div class="pie-chart-container">
          <canvas id="myChart" ></canvas>
        </div>
    </div>
 --}}{{--     <div height="400" style="height: 400px;">
    </div>
    <canvas id="myPieChart" width="400" height="400" class="d-block"></canvas>
 --}}{{--     <canvas id="myChart" width="400" height="400" class="d-block"></canvas> --}}

<style>
    .biz-chart{
        width: 400px;
    }
</style>

    <script>
        var ctx = document.getElementById('myChart').getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: ['Admin', 'Developers', 'Management'],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
/*                     yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }] */
                }
            }
        });

        var ctxA = document.getElementById('myPieChart').getContext('2d');
        var myPieChart = new Chart(ctxA, {
            type: 'pie',
            data: {
                labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }

        });
        </script>

<div class="row container mt-3">
    <div class="col-4">
        Menu - Tenancy
    </div>
    <div class="col-8">
        <table id="tblAllowances" class="table border">
            <thead class="tbl-dark">
                <tr>
                    <th scope="col" hidden>#</th>
                    <th scope="col" colspan="2">Notifications as at {{ \Carbon\Carbon::now()->format('d-m-Y') }} </th>
                    <th scope="col" class="text-right" style="width: 175px;"><span class="badge badge-info text-white">{{ $leaves->count() }}</span> Announcements</th>
                </tr>        
            </thead>
            <tbody>
{{--                 <tr>
                    <td hidden>1</td>
                    <td style="width: 75px;">
                        <span><i class="fa fa-calendar"></i></span>
                    </td>
                    <td class="">
                        <div class="font-weight-bold">Regan Nagorcka requested 3 Days of Vacation</div> 
                        <div class="text-muted">29-Mar to 31-March : 14 days ago</div>
                    </td>
                    <td class="text-right"> 
                        <button class="btn btn-outline-primary btn-sm">View</button>
                        <a href="/timeoff/approval/12">View</a>
                    </td>
                </tr>
 --}}

                @foreach($leaves as $leave)
                <tr>
                    <td hidden>1</td>
                    <td class="text-center align-middle" style="width: 50px; font-size: 30px;">
                        <span><i class="fa fa-calendar"></i></span>
                    </td>
                    <td class="pl-0">
                        <div class="font-weight-bold">{{ $leave->Employee->name }} requested 
                            {{ $leave->LeaveType->track_time == 'd' ? $leave->days_off.' Days of ' : $leave->hours_off.' Hours of ' }} 
                            {{ $leave->LeaveType->title }}</div> 
                        <div class="text-muted">{{ \Carbon\Carbon::parse($leave->start_date)->format('d-M') }} to 
                            {{ \Carbon\Carbon::parse($leave->end_date)->format('d-M') }} : 
                            Created {{ date_diff($leave->created_at, new \DateTime())->format("%d days ago") }}
                            </div>
                    </td>
                    <td class="text-right align-middle pr-4"> 
                        <a href="/timeoff/approval/{{ $leave->id }}">
                            <button class="btn btn-outline-primary btn-sm">View</button>
                        </a>
                    </td>
        
                </tr>
                @endforeach
            </tbody>
        </table>
        


    </div>
</div>
<style>

</style>
</x-layout>
