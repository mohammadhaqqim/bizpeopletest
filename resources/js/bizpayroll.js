$(document).ready(function(){
    $('#employee_id').editableSelect();
  });
  
  function deletePayrollTrans(id){
  
      document.getElementById("deletePayrollTrans").action = "/payrollemployee/" + id;
  
      var oTable = document.getElementById("payrollTable");
      for (i = 1; i < oTable.rows.length; i++){
          var oCells = oTable.rows.item(i).cells;
  
          if (oCells.item(0).innerHTML == id){
              document.getElementsByClassName("emp_name_delete")[0].value = oCells.item(1).innerHTML;
              document.getElementsByClassName("emp_department_delete")[0].value = oCells.item(3).innerHTML; 
              document.getElementsByClassName("emp_base_salary_delete")[0].value = Number(oCells.item(4).innerHTML).toFixed(2);                        
              document.getElementsByClassName("payment_method_delete")[0].value = document.getElementsByClassName("emp"+id)[0].value;
          }
      } 
  
      $("#deleteModal").modal('show');
  }
  
  function editPayrollTrans(id){    
      document.getElementById("editPayrollTrans").action = "/payrollemployee/" + id;
  
       var oTable = document.getElementById("payrollTable");
      for (i = 1; i < oTable.rows.length; i++){
          var oCells = oTable.rows.item(i).cells;
  
          if (oCells.item(0).innerHTML == id){
              document.getElementsByClassName("emp_name")[0].value = oCells.item(1).innerHTML;
              document.getElementsByClassName("emp_department")[0].value = oCells.item(3).innerHTML; 
              document.getElementsByClassName("emp_base_salary")[0].value = Number(oCells.item(4).innerHTML).toFixed(2);                        
              document.getElementsByClassName("payment_method")[0].value = document.getElementsByClassName("emp"+id)[0].value;
  
              document.getElementsByClassName("to_bank")[0].value = oCells.item(14).innerHTML;
              document.getElementsByClassName("to_account")[0].value = oCells.item(15).innerHTML;
          }
      } 
  
      checkPaymentType();
  
      $("#editModal").modal('show');
  }
  
  function checkSelection(a){    
      //find the item in the list
      var name = a.value; 
      document.getElementsByClassName('include-emp')[0].disabled = false;
  
      //find item in table
      var oTable = document.getElementById("payrollTable");
  
      for (i = 1; i < oTable.rows.length; i++){
          var oCells = oTable.rows.item(i).cells;
  
          if (oCells.item(1).innerHTML == name){
              document.getElementsByClassName('include-emp')[0].disabled = true;
              return;
          }
      }
  
  }
  
  function checkPaymentType()
  {
      var element = document.getElementById("payment_mode_id");
      var mode = element.value;
  
      if (element.options[element.selectedIndex].text == "Bank Transfer"){
          document.getElementById("bankinstitute").style.display = "block";
          document.getElementById("bankaccount").style.display = "block";
      }
      else{
          document.getElementById("bankinstitute").style.display = "none";
          document.getElementById("bankaccount").style.display = "none";
      } 
  }
  