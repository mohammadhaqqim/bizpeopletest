<?php

return [
    // Side Bar
    'documentationtopic' => 'Documentation Topic ',
    'docgettingstarted' => 'Getting Started',
    'docpayroll' => 'Payroll',
    'docpeople' => 'People',
    'docreport' => 'Report',

    'docusermanual' => 'User Manual',

    'docuser' => 'Users',
    'docsetting' => 'Settings',

    'bizpeople' => 'Biz People Biz',
    'docterm' => 'Terms of Use',
    'docpolicy' => 'Privacy Policy',

    'doclogin' => 'Login',
    'docforgetpassword' => 'Forget Password',
    'docdepartment' => 'Department',
    'docemployee' => 'Employee',

    'docerror' => 'Errors',

    'errorpage' => 'Error Page',

    'errorpage404' => '404 Error Page',



    // Top Bar
    'documentation' => 'Documentation',
    'doctopiclist' => 'List of Topic',
    'bizlogo' => url('/images/logos/BizGuide.png'),
    'changelanguage' => 'Change Language',
    'english' => 'English (United States)',
    'malay' => 'Malay (Brunei Darussalam)',

    'backtologin' => 'Back to Login',

    'bizpeoplelogin' => 'Biz-People.Biz | Login',

    'bizpeople' => 'Biz-People.Biz',



    // Getting Start Page
    'documentationgettingstarted' => 'Getting Started',
    'topicgetstart' => 'Topic : Getting Started',
    'readusermanual' => 'User Manual for How to use the System will be teach below; read the instruction carefully.',

    // Learn Bizpeople Page
    'howgetstart' => 'How to Get Started with Biz-People?',
    'installbiz' => 'Download and install BizPeople the System into your Personal Computer (PC). To use BizPeople System, you will need to have at least Window 8 (OS) or above.',
    'readnote' => 'Note :',

    'readbiztype' =>
    'The same BizPeople System can work in different types of business company that need a proper administration management system in monitoring company branches in one server,
     and these system has created as the biggest system for company, because the system inside the BizPeople has provide for Human Resource, Finance and Individual Employees.',

    'readvarious' => 'There are various settings at the Back office that allow adjusting the BizPeople System to your needs.',

    'readafterinstall' => 'After you have Install the BizPeople System into your computer,
     when you run the BizPeople Application. Welcoming screen will be appear with the BizPeople Logo and Login Features.',

     'readmustlive' => 'This system must have a live server, other employees under the company may be able to open or login through the live website of the system;',

     'forexamplewebsite' => 'For Example Website Link :',

     'readuserneed' => 'User need to fill in their Email and Password to log into the system, the registration are manually register by the Admin. Once registered,
      Admin will notify you through your email. Then, login with the given email. You will be able to enter the BizPeople System.',



     // Learn Dashboard Navigation Page
    'finishlogin' => 'Finish login setup? you will be able to see the Biz-People dashboard',

    'successlogged' => 'When you successfully logged into the BizPeople System, you will see the main Dashboard, there will be a event notification for every users stated on the homepage.',

    'maindashboard' => 'Main Dashboard',

    'topdashboard' => 'On the top of the Dashboard, there is a main logo of the system with the name of your Account that you login before.',

    'exampleuser' => 'This is the Example of User that show on the Dashboard, acting as an Admin. Each user will have a different dashboard navigation access depends on your position.',

    'navigationaccess' => 'Dashboard Navigation Access',

    'onlyadmin' => 'Only admin able to see the setting icon on the right top. if you click the icon, it will navigate you to the main setting of the system.',

    'dashboardicon' => 'Dashboard Icons Button',

    'besideicon' => 'Beside will be the icon for the department or other branches, here you be able to see the employees profiles. At the end of the icon will be your own profile, if you click the icon. it will show you the button for change password and option is for logging out.',



    // Learn Change Password Page
    'howtochangepassword' => 'How to change your password in Biz-People System?',

    'homepagedashboard' => 'Homepage dashboard of BizPeople System, if you want to change your account password, just click on the profile icon. There will be appear an option for changing password.',

    'useraccounticon' => 'User Account Icon',

    'showtwooption' => 'It will show you the two option which is logging out and changing password. Just click the Change Password Option, you will automatically navigate to the Changing Password page.',

    'passworddashboard' => 'Change Password Dashboard',

    'passwordform' => 'Changing Password Form will be stated at the main page, every user will do the changing process easily.',

    'updatepassword' => 'User only need to fill in the Current Password, then fill in the New Password field and Click on update password to successfully changed.',


    // Learn Content
    'continuebasic' => 'Continue Basic System Learning :',



    // Learn Payroll

    'topicpayroll' => 'Topic : Payroll',

    'whatfeature' => 'What feature of payroll can be done in BizPeople System?',

    'mainpayrolldashboard' => 'Feature under the Payroll Section, there will be a different function are applied according to the stated title of navigation',

    'mainpayrollnav' => 'Payroll with Sub Navigation Menu ',

    'readpayrollsubmenu' => 'You will see the submenu which is Payroll Center, Employees, Historical and Reports. It was setting up with function that relate a purpose in handling the payroll system for the user',

    'mainpayrollcenter' => 'The Uses in Payroll Center Features',

    'readcurrentpayroll' => 'There will be a list of table for current payroll, payment date and time created will be record',

    'mainpayrollcreatenew' => 'Create New Period Form',

    'readcurrentpayrollcreate' => 'You can also create a payment period by pressing the button at the end of the table. It will show you a form for you to fill out',

    'mainpayrolledit' => 'Edit Period Form',

    'readcurrentpayrolledit' => 'You will be able to edit the Payment made, just click the title on the list, it will show you the edit form',



    // Learn Payroll Transaction

    'howcheck' => 'How to check the payment setup in payroll transaction?',

    'mainpayrollcheck' => 'Payroll Transaction only can be find on the list table under the payroll center section, it only appear once you have create the payment period. You will be able to see the transaction button on the list',

    'mainpayrollchecklist' => 'Current Payroll : List of Table',

    'readcurrentlist' => 'Inside the list table, each payment period will have it own information. Each created list will have the action column of employees, transaction and approval button. It will lead to check, view and update to the page',

    'mainpayrolltransaction' => 'Payroll Transaction Dashboard',

    'readpayrolltransaction' => 'Payroll Transaction only be able view if click the transaction action button on the current payroll list. Here you able to see the information of the period. It can be add and edit the type of transaction and also have a calculation for salary. Each employees in the department will be stated on list table of the current period',

    'mainpayrollapproval' => 'Payroll Approval Dashboard',

    'readpayrollapproval' => 'Payroll Approval also only be able to check, by click the approval action button on previous list. It will show the available current period list with the total of payslip. It can be download, just by clicking the Payslip Button above the table. If payslip of every department are ready to be approve payment, just click the Approve Button below to finalizing the payment',



    // Learn Payroll Employee

    'wherecan' => 'Where can you see the summary of payroll with the list name of employees?',

    'mainpayrollemployee' => 'Payroll Employees will show the list of table for the current employee. On the list will state every available worker department and total payment that they are received',

    'mainpayrollemployeelist' => 'Payroll Employeee Dashboard',

    'reademployeelist' => 'Each name of employee on the list, you will be able to view their profile detail, just by clicking their name',

    'mainpayrollemployeeprofile' => 'Payroll Employee Profile : Under People Section',

    'readpayrollemployeeprofile' => 'As you choose any name on the list of employee, it will lead you to view their profile and you will be able to edit and update employee information',



     // Learn Payroll Historical

    'wheretocheck' => 'Where to check recently payroll that was already approved and proceed payment?',

    'mainpayrollhistory' => 'Payroll History only be available if there is payment period is done and approve. It will be listed on current payroll as a recently process',

    'mainpayrollhistorylist' => 'Payroll Historical Dashboard',

    'readhistorylist' => 'Recent payroll payslip or process that was made before, you will be able to view the information by choosing any action button the was listed on the table and you wont be able to undo the process if already submitted the payment',

    'mainpayrollhisotrycreate' => 'Payroll History : Create New Payroll',

    'readpayrollhistorycreate' => 'On the right top side of the previous list table, will provide you a button for create new payroll and once you click the button, it will show you the form for creating the new payroll period',



    // Learn Payroll Report

    'wheretodownload' => 'Where to download the payroll previous report as an excel file?',

    'mainpayrollreport' => 'Payroll Report only be able to see after all the payment process are done and an audit will be easily to lookup the report',

    'mainpayrollreportlist' => 'Payroll Report Dashboard',

    'readpayrollreportlist' => 'This feature are prepare for Payroll Auditing, it will show the list of payroll period that are done and you wil see the column name of payroll totals, activity report, tax report and bank report. You can see the report detail more clearly and it will be downloaded as you click the download button and it will save  as an Excel File.',

    'mainpayrollreportpaytotal' => 'Downloaded Payroll Total Report in Excel',

    'readpayrollreportpaytotal' => 'Payroll total will be automatic generate the data into excel with correct calculation',

    'mainpayrollreportactivity' => 'Downloaded Payroll Activity Report in Excel',

    'readpayrollreportactivity' => 'Activity will be automatic generate the data into excel with correct calculation',

    'mainpayrollreporttax' => 'Downloaded Payroll Tax Report in Excel',

    'readpayrollreporttax' => 'Tax Report will be automatic generate the data into excel with correct calculation',

    'mainpayrollreportbank' => 'Downloaded Payroll Bank Report in Excel',

    'readpayrollreportbank' => 'Bank Report will be automatic generate the data into excel with correct calculation',

    'readpayrollreportexcel' => 'This feature will help auditing to get the report easily without incorrect calculation',



    // Learn People

    'topicpeople' => 'Topic : People',

    'howtoadd' => 'How to add new employees in BizPeople System?',

    'mainpeople' => 'People is resemble as Company Staff, it only be able to see the name of the employees if it already hired or registered into the system by the Admin',

    'mainpeoplelist' => 'People Dashboard',

    'readpeoplelist' => 'Features for add new employee are provide, you just click the Add Employee Icon above the table. Table under the Staff Directory will state the recent created employee names. You be able to find any employee name by typing the name on the search bar above and it will automatically find the names that you want. You can also view the individual detail by clicking their names',

    'mainpeoplelistadd' => 'Staff Directory : Add New Employees Form',

    'readpeoplelistadd' => 'Here will show you the employee information form detail that need to be fill in',

    'readpeoplelistaddsave' => 'As you finish filling all the employees information, you can give an access for each employee to edit their information by their own in the future if there is a changing on personal detail. Bottom of the form will show you the button for saving and cancel the process on creating new employees. if you save it, the new employees name will be stated on the Staff Directory table',

    'mainpeoplelistaddsaveprofile' => 'People Staff Directory Name : View Employee Profile',

    'readpeoplelistaddsaveprofile' => 'Each employees name that was successfully created will show on the list. if you want to view the employee profile, just click the name',

    'mainpeoplelistviewnameprofile' => 'People Profile Dashboard : Employee Profile',

    'readpeoplelistviewnameprofile' => 'As you click the employee names, it will lead you to view full information of each employees',



    // Learn People Profile Upload

    'whatfunction' => 'What function feature that was provided under the People Section on Employees Profile in the BizPeople System?',

    'mainpeopleprofile' => 'People Profile is the section where the individual information of employees will be show in detail. You will be able to edit and update the profile if there is mistake of detail. This side will have separate form with navigation which contain Personal, Job, Allowances, Time Off, Family.',

    'mainpeopleprofileimage' => 'People Profile Dashboard',

    'readpeopleprofileimage' => 'Here you will see the main profile of employee which state the passport image and name. There will be a different section button to view other detail form of the employee.',

    'mainpeopleprofileupload' => 'Employee Profile Image Upload',

    'readpeopleprofileupload' => 'You will be able to upload the actual image for the employee by clicking the Profile Image Button. It will directly allow you to choose an image from your computer',

    'readpeopleprofileuploaded' => 'If you done choose image, just click on open and it will automatically change into your choosen image',



    // Learn People Profile Personal

    'wherepersonal' => 'Where to change employees personal detail in BizPeople System?',

    'mainpeopleprofilepersonal' => 'Under People Section, as you choose any employee name that was listed on the staff directory table. Each name you have choose will lead you to view their individual information and you able to change their personal detail',

    'mainpeopleprofilepersonalform' => 'People Profile Dashboard : Personal Detail',

    'readpeopleprofilerpersonalform' => 'Here you will be able to edit or change the information of the current employee. There is an Email Notification function for you to activate or deactived for the employee. it is for receiving an for payslip and time off notice and as usual very important is the identification',

    'readpeopleprofilerpersonalformsave' => 'It also showing you the employment status of the current staff with working week, personal address and contact information',

    'readpeopleprofilerpersonalformsaved' => 'if you are done in edit and updating their information or not confirm yet, you be able choose the button of saving and cancel process',



    // Learn People Profile Job

    'wherejob' => 'Where to change employees job detail in BizPeople System?',

    'mainpeopleprofilejob' => 'Under People Section, as you choose any employee name that was listed on the staff directory table. Each name you have choose will lead you to view their individual information and you able to change their job detail',

    'mainpeopleprofilejobform' => 'People Profile Dashboard : Job Detail',

    'readpeopleprofilejobform' => 'Here you will see separate section that stated with a title such as important date of the employment and also have account information for future payroll notice. There is a table of employment status with dates and working status. It has two button different fucntion which is Change Employment Status is to update status into new date and Icon Edit is to modify the current status date',

    'readpeopleprofilejobformedit' => 'You will see the title in every section such as Job Information is stating the date of working in the department and designation, it also can be update and modify the date, department, designation and where to report. For Remuneration Information section, you will be able to set the payment rate and it also can be updated and modify. For End of Service, you will able to create a reason for the employee service and it can be add new reason or update new date, also be modify the table',



    // Learn People Profile Allowance

    'whereallowance' => 'Where to change employees allowance detail in BizPeople System?',

    'mainpeopleprofileallowance' => 'Under People Section, as you choose any employee name that was listed on the staff directory table. Each name you have choose will lead you to view their individual information and you able to change their allowance detail',

    'mainpeopleprofileallowanceform' => 'People Profile Dashboard : Allowance Detail',

    'readpeopleprofileallowanceform' => 'Here you see the Allowance table, it will show you the allowance that the employee should have or need to be cut off the allowance depending on employee work time or working for a reason',


    'mainpeopleprofileallowanceformaddnew' => 'Add New Allowance',

    'readpeopleprofileallowanceformaddnew' => 'You will be able to give a allowance title either it was on addition or deduction and have an option for status, it can be calculated or fixed. if you done just click the button save and it will be listed on the table of allowance',


    'mainpeopleprofileallowanceformaddnewaddition' => 'Add Addition',

    'readpeopleprofileallowanceformaddnewaddition' => 'You be able add addition on allowance depending on what type of working allowance the employee has doing',


    'mainpeopleprofileallowanceformaddnewdeduction' => 'Add Deduction',

    'readpeopleprofileallowanceformaddnewdeduction' => 'You be able add deduction on allowance depending on what type of working problem that employee has faced',




    // Learn People Profile Time

    'wheretime' => 'Where to change employees time off detail in BizPeople System?',

    'mainpeopleprofiletime' => 'Under People Section, as you choose any employee name that was listed on the staff directory table. Each name you have choose will lead you to view their individual information and you able to change their time off detail',

    'mainpeopleprofiletimeform' => 'People Profile Dashboard : Time Off Detail',

    'readpeopleprofiletimeform' => 'Here you will see the Schedule for Time Off for the employees and schedulued event is table for updated event for having off day as a notice for the employees. An History for the event or time off will be recorded on the History Table and you be able to find it the search bar. You will be able to apply a time off for the individual staff that are available.',

    'mainpeopleprofiletimeformadd' => 'Add Time Off',

    'readpeopleprofiletimeformadd' => 'You will able to add new scheduled for having a off day by filling the form and it is depending on the empty slot date that are available, here will be clash with other that was requesting too',




    // Learn People Profile Family

    'wherefamily' => 'Where to change employees family detail in BizPeople System?',

    'mainpeopleprofilefamily' => 'Under People Section, as you choose any employee name that was listed on the staff directory table. Each name you have choose will lead you to view their individual information and you able to change their family detail',

    'mainpeopleprofilefamilyform' => 'People Profile : Family Detail',

    'readpeopleprofilefamilyform' => 'Here is the personal contact for the employee, name and contact number are required and need to be listed on the table. it is for emergency contact or if the employee cannot be reach, you will be able to contact the other as on behalf for asking any employee situation if having a problem',

    'mainpeopleprofilefamilyformaddmember' => 'Add Family Member',

    'readpeopleprofilefamilyformaddmember' => 'You can add new relation in family member contact, you just need to fill in the form and save it',

    'mainpeopleprofilefamilyformaddemergency' => 'Add Emergency Contact',

    'readpeopleprofilefamilyformaddemergency' => 'You can add new emergency contact that is very important to the employee and easy to contact',


    // Learn Report

    'topicreport' => 'Topic : Report',

    'wheretofind' => 'Where to find the payroll report for auditing?',





    // Learn User

    'howtolog' => 'How to login into the BizPeople System?',

    'howtoreset' => 'How to reset your password in Biz-People System?',



     // Unused Content
    'howtosetting' => 'How to setting up your organisation in Biz-People System?',
    'howtocreate' => 'How to Create account for the employee in Biz-People System?',
    'gettingdocumentationtopic' => 'Go to Page : Choose Documentation Topic',



    // Doc Topic Page
    'documentationusersystemguideline' => 'Documentation : User System Guideline',
    'choosetopic' => 'Choose Your Documentation Topic',
    'readtopic' => 'System Guideline will be breifly explain in Steps on every documentation Section. Please read and follow the instruction given.',



];

?>
