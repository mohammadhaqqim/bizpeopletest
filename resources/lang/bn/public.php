<?php

return [
    // Side Bar
    'documentationtopic' => 'Topik Dokumentasi',

    'docgettingstarted' => 'Bermula',
    'docpayroll' => 'Gaji',
    'docpeople' => 'Orang',
    'docreport' => 'Laporan',

    'docusermanual' => 'Manual Pengguna',

    'docuser' => 'Pengguna',
    'docsetting' => 'Tetapan',

    'bizpeople' => 'Biz People Biz',
    'docterm' => 'Syarat penggunaan',
    'docpolicy' => 'Dasar Privasi',

    'doclogin' => 'Log Masuk',
    'docforgetpassword' => 'Lupa Kata Laluan',
    'docdepartment' => 'Jabatan',
    'docemployee' => 'Pekerja',

    'docerror' => 'Masalah Teknikal',

    'errorpage' => 'Halaman Menghadapi Kesalahan Teknikal',

    'errorpage404' => 'Halaman Mengadapi Kesalahan Teknikal 404',



    // Top Bar
    'documentation' => 'Dokumentasi',
    'doctopiclist' => 'Senarai Topik',
    'bizlogo' => url('/images/logos/BizGuideMalay.png'),
    'changelanguage' => 'Tukar Bahasa',
    'english' => 'Bahasa Inggeris (Amerika Syarikat)',
    'malay' => 'Bahasa Melayu (Brunei Darussalam)',

    'backtologin' => 'Kembali Ke Log Masuk',

    'bizpeoplelogin' => 'Biz-People.Biz | Log Masuk',

    'bizpeople' => 'Biz-People.Biz',



    // Getting Start Page
    'documentationgettingstarted' => 'Bermula',
    'topicgetstart' => 'Topik : Bermula',
    'readusermanual' => 'Manual Pengguna untuk cara menggunakan sistem akan diajar di bawah; baca arahan dengan teliti',



     // Learn Bizpeople Page
    'howgetstart' => 'Bagaimana untuk bermula dengan Biz-People?',
    'installbiz' => 'Muat turun dan pasang BizPeople the System ke dalam Komputer Peribadi (PC) anda. Untuk menggunakan Sistem BizPeople, anda perlu mempunyai sekurang-kurangnya Window 8 (OS) atau lebih tinggi.',
    'readnote' => 'Catatan :',

    'readbiztype' =>
    'Sistem BizPeople yang sama boleh berfungsi dalam pelbagai jenis syarikat perniagaan yang memerlukan sistem pengurusan pentadbiran yang betul dalam memantau cawangan syarikat dalam satu pelayan,
     dan sistem ini telah dicipta sebagai sistem terbesar untuk syarikat, kerana sistem di dalam BizPeople telah menyediakan Sumber Manusia, Kewangan dan Pekerja Individu.',

     'readvarious' => 'Terdapat pelbagai tetapan di Pejabat Belakang yang membenarkan pelarasan Sistem BizPeople mengikut keperluan anda.',

     'readafterinstall' => 'Selepas anda memasang Sistem BizPeople ke dalam komputer anda, apabila anda menjalankan Aplikasi BizPeople.
      Skrin alu-aluan akan dipaparkan dengan logo BizPeople dan ciri Log Masuk.',

      'readmustlive' => 'Sistem ini mesti mempunyai pelayan langsung, pekerja lain di bawah syarikat mungkin boleh membuka atau log masuk melalui laman web langsung sistem;',

      'forexamplewebsite' => 'Sebagai Contoh Pautan Laman Web :',

      'readuserneed' => 'Pengguna perlu mengisi E-mel dan Kata Laluan mereka untuk log masuk ke sistem, pendaftaran didaftarkan secara manual oleh Admin. Setelah mendaftar, Admin akan memberitahu anda melalui email anda. Kemudian, log masuk dengan e-mel yang diberikan. Anda akan dapat memasuki Sistem BizPeople.',



    // Learn Dashboard Navigation Page
    'finishlogin' => 'Selesai persediaan log masuk? anda akan dapat melihat papan pemuka Biz-People',

    'successlogged' => 'Apabila anda berjaya log masuk ke Sistem BizPeople, anda akan melihat Papan Pemuka utama, akan ada pemberitahuan acara untuk setiap pengguna yang dinyatakan di halaman utama.',

    'maindashboard' => 'Papan Pemuka Utama',

    'topdashboard' => 'Di bahagian atas Papan Pemuka, terdapat logo utama sistem dengan nama Akaun anda yang anda log masuk sebelum ini.',

    'exampleuser' => 'Ini ialah Contoh Pengguna yang dipaparkan pada Papan Pemuka, bertindak sebagai Pentadbir. Setiap pengguna akan mempunyai akses navigasi papan pemuka yang berbeza bergantung pada kedudukan anda.',

    'navigationaccess' => 'Akses Navigasi Papan Pemuka',

    'onlyadmin' => 'Hanya pentadbir yang dapat melihat ikon tetapan di bahagian atas sebelah kanan. jika anda mengklik ikon, ia akan menavigasi anda ke tetapan utama sistem.',

    'dashboardicon' => 'Butang Ikon Papan Pemuka',

    'besideicon' => 'Di sebelah ikon untuk jabatan atau cawangan lain, di sini anda boleh melihat profil pekerja. Pada penghujung ikon akan menjadi profil anda sendiri, jika anda mengklik ikon. ia akan menunjukkan kepada anda butang untuk menukar kata laluan dan pilihan adalah untuk log keluar.',



    // Learn Change Password Page
    'howtochangepassword' => 'Bagaimana untuk menukar kata laluan anda dalam Sistem Biz-People?',

    'homepagedashboard' => 'Papan pemuka laman utama Sistem BizPeople, jika anda ingin menukar kata laluan akaun anda, cuma klik pada ikon profil. Akan muncul pilihan untuk menukar kata laluan.',

    'useraccounticon' => 'Ikon Akaun Pengguna',

    'showtwooption' => '
    Ia akan menunjukkan kepada anda dua pilihan iaitu log keluar dan menukar kata laluan. Hanya klik pada Pilihan Tukar Kata Laluan, anda akan secara automatik menavigasi ke halaman Menukar Kata Laluan.',

    'passworddashboard' => 'Tukar Papan Pemuka Kata Laluan',

    'passwordform' => 'Borang Penukaran Kata Laluan akan dinyatakan di halaman utama, setiap pengguna akan melakukan proses penukaran dengan mudah.',

    'updatepassword' => '
    Pengguna hanya perlu mengisi Kata Laluan Semasa, kemudian isi ruangan Kata Laluan Baru dan Klik pada kemas kini kata laluan untuk berjaya ditukar.',


    // Learn Content
    'continuebasic' => 'Teruskan Pembelajaran Sistem Asas :',



    // Learn Payroll

    'topicpayroll' => 'Topik : Gaji',

    'whatfeature' => 'Apakah ciri senarai gaji yang boleh dilakukan dalam Sistem BizPeople?',

    'mainpayrolldashboard' => 'Ciri di bawah Bahagian Gaji, akan ada fungsi berbeza digunakan mengikut tajuk navigasi yang dinyatakan',

    'mainpayrollnav' => 'Gaji dengan Menu Navigasi Sub',

    'readpayrollsubmenu' => '
    Anda akan melihat submenu iaitu Pusat Penyata Gaji, Pekerja, Sejarah dan Laporan. Ia telah disediakan dengan fungsi yang mengaitkan tujuan dalam mengendalikan sistem penggajian untuk pengguna',

    'mainpayrollcenter' => 'Ciri-ciri Penggunaan dalam Pusat Gaji',

    'readcurrentpayroll' => '
    Akan ada senarai jadual untuk senarai gaji semasa, tarikh dan masa pembayaran dibuat akan direkodkan ',

    'mainpayrollcreatenew' => 'Cipta Borang Tempoh Baharu',

    'readcurrentpayrollcreate' => '
     Anda juga boleh membuat tempoh pembayaran dengan menekan butang di hujung jadual. Ia akan menunjukkan kepada anda borang untuk anda isi',

    'mainpayrolledit' => 'Edit Borang Tempoh',

    'readcurrentpayrolledit' => 'Anda akan dapat mengedit pembayaran yang dibuat, hanya klik tajuk pada senarai, ia akan menunjukkan kepada anda borang edit',


    // Learn Payroll Transaction

   'howcheck' => 'Bagaimana untuk menyemak persediaan pembayaran dalam transaksi gaji?',

   'mainpayrollcheck' => 'Transaksi Gaji hanya boleh didapati pada jadual senarai di bawah bahagian pusat gaji, ia hanya muncul sebaik sahaja anda membuat tempoh pembayaran. Anda akan dapat melihat butang transaksi pada senarai',

   'mainpayrollchecklist' => 'Gaji Semasa : Senarai Jadual',

   'readcurrentlist' => 'Di dalam jadual senarai, setiap tempoh pembayaran akan mempunyai maklumatnya sendiri. Setiap senarai yang dibuat akan mempunyai lajur tindakan pekerja, transaksi dan butang kelulusan. Ia akan membawa kepada menyemak, melihat dan mengemas kini halaman',

   'mainpayrolltransaction' => 'Papan Pemuka Transaksi Gaji',

   'readpayrolltransaction' => 'Transaksi Gaji hanya boleh melihat jika klik butang tindakan transaksi pada senarai gaji semasa. Di sini anda boleh melihat maklumat tempoh tersebut. Ia boleh menambah dan mengedit jenis transaksi dan juga mempunyai pengiraan untuk gaji. Setiap pekerja di jabatan akan dinyatakan dalam jadual senarai tempoh semasa',

   'mainpayrollapproval' => 'Papan Pemuka Kelulusan Gaji',

   'readpayrollapproval' => 'Kelulusan Gaji juga hanya boleh menyemak, dengan mengklik butang tindakan kelulusan pada senarai sebelumnya. Ia akan menunjukkan senarai tempoh semasa yang tersedia dengan jumlah slip gaji. Ia boleh dimuat turun, hanya dengan mengklik Butang Payslip di atas jadual. Jika slip gaji setiap jabatan sedia untuk meluluskan pembayaran, cuma klik Butang Lulus di bawah untuk memuktamadkan pembayaran',



    // Learn Payroll Employee

   'wherecan' => 'Di mana anda boleh melihat ringkasan senarai gaji dengan senarai nama pekerja?',

   'mainpayrollemployee' => 'Pekerja Gaji akan menunjukkan senarai jadual untuk pekerja semasa. Dalam senarai akan menyatakan setiap jabatan pekerja yang ada dan jumlah bayaran yang diterima',


   'mainpayrollemployeelist' => 'Papan Pemuka Pekerja Gaji',

   'reademployeelist' => 'Setiap nama pekerja dalam senarai, anda akan dapat melihat butiran profil mereka, hanya dengan mengklik nama mereka',

   'mainpayrollemployeeprofile' => 'Profil Pekerja Gaji : Di Bawah Bahagian Orang',

   'readpayrollemployeeprofile' => 'Apabila anda memilih mana-mana nama dalam senarai pekerja, ia akan membawa anda melihat profil mereka dan anda akan dapat mengedit dan mengemas kini maklumat pekerja',



     // Learn Payroll Historical

     'wheretocheck' => 'Di mana untuk menyemak senarai gaji baru-baru ini yang telah diluluskan dan meneruskan pembayaran?',

     'mainpayrollhistory' => 'Sejarah Gaji hanya tersedia jika ada tempoh pembayaran dilakukan dan diluluskan. Ia akan disenaraikan pada senarai gaji semasa sebagai proses baru-baru ini',

     'mainpayrollhistorylist' => 'Papan Pemuka Sejarah Gaji',

     'readhistorylist' => 'Penyata gaji terkini atau proses yang telah dibuat sebelum ini, anda akan dapat melihat maklumat dengan memilih mana-mana butang tindakan yang disenaraikan di atas meja dan anda tidak akan dapat membuat asal proses jika sudah menyerahkan pembayaran',

     'mainpayrollhisotrycreate' => 'Sejarah Gaji : Buat Gaji Baharu',

     'readpayrollhistorycreate' => 'Di bahagian atas sebelah kanan jadual senarai sebelumnya, akan memberikan anda butang untuk membuat senarai gaji baharu dan sebaik sahaja anda mengklik butang itu, ia akan menunjukkan kepada anda borang untuk membuat tempoh gaji baharu',



    // Learn Payroll Report

    'wheretodownload' => 'Di manakah untuk memuat turun laporan gaji sebelumnya sebagai fail excel?',

    'mainpayrollreport' => 'Laporan Gaji hanya dapat dilihat selepas semua proses pembayaran selesai dan audit akan mudah untuk mencari laporan',

    'mainpayrollreportlist' => 'Papan Pemuka Laporan Gaji',

    'readpayrollreportlist' => 'Ciri ini disediakan untuk Pengauditan Gaji, ia akan menunjukkan senarai tempoh gaji yang telah dilakukan dan anda akan melihat nama lajur jumlah gaji, laporan aktiviti, laporan cukai dan laporan bank. Anda boleh melihat butiran laporan dengan lebih jelas dan ia akan dimuat turun semasa anda mengklik butang muat turun dan ia akan disimpan sebagai Fail Excel.',

    'mainpayrollreportpaytotal' => 'Laporan Jumlah Gaji dimuat turun dalam Excel',

    'readpayrollreportpaytotal' => 'Laporan Jumlah akan menjana data secara automatik ke dalam excel dengan pengiraan yang betul',

    'mainpayrollreportactivity' => 'Laporan Aktiviti Gaji dimuat turun dalam Excel',

    'readpayrollreportactivity' => 'Laporan Aktiviti akan menjana data secara automatik ke dalam excel dengan pengiraan yang betul',

    'mainpayrollreporttax' => 'Laporan Cukai Gaji dimuat turun dalam Excel',

    'readpayrollreporttax' => 'Laporan Cukai akan menjana data secara automatik ke dalam excel dengan pengiraan yang betul',

    'mainpayrollreportbank' => 'Laporan Bank Gaji dimuat turun dalam Excel',

    'readpayrollreportbank' => 'Laporan Bank akan menjana data secara automatik ke dalam excel dengan pengiraan yang betul',

    'readpayrollreportexcel' => 'Ciri ini akan membantu mengaudit untuk mendapatkan laporan dengan mudah tanpa pengiraan yang salah',



    // Learn People

    'topicpeople' => 'Topik : Orang Ramai',

    'howtoadd' => 'Cara menambah pekerja baharu dalam Sistem BizPeople',

    'mainpeople' => 'Orang adalah menyerupai Kakitangan Syarikat, ia hanya dapat melihat nama pekerja jika ia telah diambil atau didaftarkan ke dalam sistem oleh Admin',

    'mainpeoplelist' => 'Papan Pemuka Orang',

    'readpeoplelist' => 'Ciri-ciri untuk menambah pekerja baru disediakan, anda hanya klik Ikon Tambah Pekerja di atas jadual. Jadual di bawah Direktori Kakitangan akan menyatakan nama pekerja yang dibuat baru-baru ini. Anda boleh mencari mana-mana nama pekerja dengan menaip nama pada bar carian di atas dan ia akan mencari nama yang anda inginkan secara automatik. Anda juga boleh melihat butiran individu dengan mengklik nama mereka',

    'mainpeoplelistadd' => 'Direktori Kakitangan : Borang Tambah Pekerja Baharu',

    'readpeoplelistadd' => 'Di sini akan menunjukkan kepada anda butiran borang maklumat pekerja yang perlu diisi',

    'readpeoplelistaddsave' => 'Apabila anda selesai mengisi semua maklumat pekerja, anda boleh memberi akses kepada setiap pekerja untuk mengedit maklumat mereka sendiri pada masa hadapan jika terdapat perubahan pada butiran peribadi. Bahagian bawah borang akan menunjukkan kepada anda butang untuk menyimpan dan membatalkan proses mencipta pekerja baharu. jika anda menyimpannya, nama pekerja baharu akan dinyatakan pada jadual Direktori Kakitangan',

    'mainpeoplelistaddsaveprofile' => 'Nama Direktori Kakitangan Orang : Lihat Profil Pekerja',

    'readpeoplelistaddsaveprofile' => 'Setiap nama pekerja yang berjaya dibuat akan dipaparkan pada senarai. jika anda ingin melihat profil pekerja, klik sahaja nama',

    'mainpeoplelistviewnameprofile' => 'Papan Pemuka Profil Orang : Profil Pekerja',

    'readpeoplelistviewnameprofile' => 'Apabila anda mengklik nama pekerja, ia akan membawa anda melihat maklumat penuh setiap pekerja',



    // Learn People Profile

    'whatfunction' => 'Apakah ciri fungsi yang disediakan di bawah Bahagian Orang pada Profil Pekerja dalam Sistem BizPeople?',

     'mainpeopleprofile' => 'Profil Orang ialah bahagian di mana maklumat individu pekerja akan ditunjukkan secara terperinci. Anda akan dapat mengedit dan mengemas kini profil jika terdapat kesilapan perincian. Bahagian ini akan mempunyai borang berasingan dengan navigasi yang mengandungi Peribadi, Pekerjaan, Elaun, Masa Cuti, Keluarga.',

     'mainpeopleprofileimage' => 'Papan Pemuka Profil Orang',

     'readpeopleprofileimage' => 'Di sini anda akan melihat profil utama pekerja yang menyatakan imej dan nama pasport. Akan ada butang bahagian yang berbeza untuk melihat borang butiran lain pekerja.',

     'mainpeopleprofileupload' => 'Muat Naik Imej Profil Pekerja',

     'readpeopleprofileupload' => 'Anda akan dapat memuat naik imej sebenar untuk pekerja dengan mengklik Butang Imej Profil. Ia secara langsung akan membolehkan anda memilih imej dari komputer anda',

     'readpeopleprofileuploaded' => 'Jika anda selesai memilih imej, cuma klik pada buka dan ia akan bertukar secara automatik menjadi imej pilihan anda',

     'readpeopleprofilerpersonalformsaved' => 'if you are done in edit and updating their information or not confirm yet, you be able choose the button of saving and cancel process',




    // Learn People Profile Personal

    'wherepersonal' => 'Tempat untuk menukar butiran peribadi pekerja dalam Sistem BizPeople?',

    'mainpeopleprofilepersonal' => 'Di bawah Bahagian Orang, semasa anda memilih mana-mana nama pekerja yang disenaraikan pada jadual direktori kakitangan. Setiap nama yang anda pilih akan membawa anda melihat maklumat individu mereka dan anda boleh menukar butiran peribadi mereka',

    'mainpeopleprofilepersonalform' => 'Papan Pemuka Profil Orang : Butiran Peribadi',

    'readpeopleprofilerpersonalform' => 'i sini anda akan dapat mengedit atau menukar maklumat pekerja semasa. Terdapat fungsi Pemberitahuan E-mel untuk anda aktifkan atau dinyahaktifkan untuk pekerja. ia adalah untuk menerima slip gaji dan notis masa cuti dan seperti biasa sangat penting ialah pengenalan',

    'readpeopleprofilerpersonalformsave' => 'Ia juga menunjukkan kepada anda status pekerjaan kakitangan semasa dengan minggu bekerja, alamat peribadi dan maklumat hubungan',




    // Learn People Profile Job

    'wherejob' => 'Tempat untuk menukar butiran kerja pekerja dalam Sistem BizPeople?',

    'mainpeopleprofilejob' => 'Di bawah Bahagian Orang, semasa anda memilih mana-mana nama pekerja yang disenaraikan pada jadual direktori kakitangan. Setiap nama yang anda pilih akan membawa anda melihat maklumat individu mereka dan anda boleh menukar butiran kerja mereka',

    'mainpeopleprofilejobform' => 'Papan Pemuka Profil Orang : Butiran Kerja',

    'readpeopleprofilejobform' => 'Di sini anda akan melihat bahagian berasingan yang dinyatakan dengan tajuk seperti tarikh penting pekerjaan dan juga mempunyai maklumat akaun untuk notis gaji masa hadapan. Terdapat jadual status pekerjaan dengan tarikh dan status bekerja. Ia mempunyai dua fungsi butang yang berbeza iaitu Tukar Status Pekerjaan adalah untuk mengemas kini status ke tarikh baharu dan Edit Ikon adalah untuk mengubah suai tarikh status semasa',

    'readpeopleprofilejobformedit' => 'Anda akan melihat tajuk di setiap bahagian seperti Maklumat Jawatan adalah menyatakan tarikh bekerja di jabatan dan jawatan, ia juga boleh mengemaskini dan mengubah suai tarikh, jabatan, jawatan dan tempat melaporkan. Untuk bahagian Maklumat Ganjaran, anda akan dapat menetapkan kadar pembayaran dan ia juga boleh dikemas kini dan diubah suai. Untuk Tamat Perkhidmatan, anda akan dapat mencipta sebab untuk perkhidmatan pekerja dan ia boleh menambah sebab baharu atau mengemas kini tarikh baharu, juga mengubah suai jadual',





    // Learn People Profile Allowance

    'whereallowance' => 'Di mana untuk menukar butiran elaun pekerja dalam Sistem BizPeople?',

    'mainpeopleprofileallowance' => 'Di bawah Bahagian Orang, semasa anda memilih mana-mana nama pekerja yang disenaraikan pada jadual direktori kakitangan. Setiap nama yang anda pilih akan membawa anda melihat maklumat individu mereka dan anda boleh menukar butiran elaun mereka',

    'mainpeopleprofileallowanceform' => 'Papan Pemuka Profil Orang : Butiran Elaun',

    'readpeopleprofileallowanceform' => 'Di sini anda melihat jadual Elaun, ia akan menunjukkan kepada anda elaun yang pekerja sepatutnya ada atau perlu dipotong elaun bergantung pada masa kerja pekerja atau bekerja atas sebab tertentu',


    'mainpeopleprofileallowanceformaddnew' => 'Tambah Elaun Baru',

    'readpeopleprofileallowanceformaddnew' => 'Anda akan dapat memberi tajuk elaun sama ada atas penambahan atau potongan dan mempunyai pilihan untuk status, ia boleh dikira atau ditetapkan. jika anda selesai hanya klik butang simpan dan ia akan disenaraikan pada jadual elaun',


    'mainpeopleprofileallowanceformaddnewaddition' => 'Tambah Penambahan',

    'readpeopleprofileallowanceformaddnewaddition' => 'Anda boleh menambah tambahan elaun bergantung pada jenis elaun kerja yang pekerja telah lakukan',


    'mainpeopleprofileallowanceformaddnewdeduction' => 'Tambah Potongan',

    'readpeopleprofileallowanceformaddnewdeduction' => 'Anda boleh menambah potongan elaun bergantung pada jenis masalah kerja yang dihadapi oleh pekerja',



   // Learn People Profile Time

   'wheretime' => 'Tempat untuk menukar butiran masa rehat pekerja dalam Sistem BizPeople?',

   'mainpeopleprofiletime' => 'Di bawah Bahagian Orang, semasa anda memilih mana-mana nama pekerja yang disenaraikan pada jadual direktori kakitangan. Setiap nama yang anda pilih akan membawa anda melihat maklumat individu mereka dan anda boleh menukar butiran masa cuti mereka',

   'mainpeopleprofiletimeform' => 'Papan Pemuka Profil Orang : Butiran Masa Cuti',

   'mainpeopleprofiletimeform' => 'Papan Pemuka Profil Orang : Butiran Masa Cuti',

    'readpeopleprofiletimeform' => 'Di sini anda akan melihat Jadual Waktu Cuti untuk pekerja dan acara yang dijadualkan adalah jadual untuk acara yang dikemas kini untuk hari cuti sebagai notis untuk pekerja. Sejarah untuk acara atau masa rehat akan direkodkan pada Jadual Sejarah dan anda boleh menemuinya di bar carian. Anda boleh memohon cuti untuk setiap kakitangan yang ada',

    'mainpeopleprofiletimeformadd' => 'Tambah Masa Cuti',

    'readpeopleprofiletimeformadd' => 'Anda akan dapat menambah baru yang dijadualkan untuk mempunyai hari cuti dengan mengisi borang dan ia bergantung pada tarikh slot kosong yang tersedia, di sini akan bertembung dengan yang lain yang meminta juga',



    // Learn People Profile Family

    'wherefamily' => 'Tempat untuk menukar butiran keluarga pekerja dalam Sistem BizPeople?',

    'mainpeopleprofilefamily' => 'Di bawah Bahagian Orang, semasa anda memilih mana-mana nama pekerja yang disenaraikan pada jadual direktori kakitangan. Setiap nama yang anda pilih akan membawa anda melihat maklumat individu mereka dan anda boleh menukar butiran keluarga mereka',

    'mainpeopleprofilefamilyform' => 'Profil Orang : Butiran Keluarga',

    'readpeopleprofilefamilyform' => 'Berikut adalah kenalan peribadi untuk pekerja, nama dan nombor telefon diperlukan dan perlu disenaraikan di atas meja. ia adalah untuk hubungan kecemasan atau jika pekerja tidak dapat dihubungi, anda akan dapat menghubungi yang lain sebagai bagi pihak untuk bertanya sebarang situasi pekerja jika menghadapi masalah',

    'mainpeopleprofilefamilyformaddmember' => 'Tambah Ahli Keluarga',

    'readpeopleprofilefamilyformaddmember' => 'Anda boleh menambah hubungan baharu dalam kenalan ahli keluarga, anda hanya perlu mengisi borang dan menyimpannya',

    'mainpeopleprofilefamilyformaddemergency' => 'Tambah Kenalan Kecemasan',

    'readpeopleprofilefamilyformaddemergency' => 'Anda boleh menambah kenalan kecemasan baharu yang sangat penting kepada pekerja dan mudah dihubungi',


    // Learn Report

    'topicreport' => 'Topic : Laporan',

    'wheretofind' => 'Di mana untuk mencari laporan gaji untuk pengauditan?',


    // Learn User

    'howtolog' => 'Bagaimana untuk log masuk ke Sistem BizPeople',

    'howtoreset' => 'Cara menetapkan semula kata laluan anda dalam Sistem Biz-People?',







    // Unused Content
    'howtosetting' => 'Cara menyediakan organisasi anda dalam Sistem Biz-People?',
    'howtocreate' => 'Cara membuat akaun untuk pekerja dalam Sistem Biz-People?',
    'gettingdocumentationtopic' => 'Pergi ke Halaman : Pilihan Topik Dokumentasi',


    // DocTopic Page
    'documentationusersystemguideline' => ' Dokumentasi : Garis Panduan Pengguna Sistem',
    'choosetopic' => 'Pilih Topik Dokumentasi Anda',
    'readtopic' => 'Garis panduan sistem akan diterangkan secara ringkas dalam Langkah pada setiap bahagian dokumentasi. sila baca dan ikut arahan yang diberikan.',

];

?>
