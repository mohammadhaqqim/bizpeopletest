<?php

namespace Tests\Feature;

use App\Bank;
use App\Race;
use App\Religion;
use App\Department;
use Tests\TestCase;
use App\Designation;
use App\PaymentMode;
use App\PaymentType;
use App\Relationship;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CustomDataTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_add_a_department()
    {
        Department::create([
            'title' => 'Properties and Facilities'
        ]);

        $this->assertDatabaseHas('departments', ['title' => 'Properties and Facilities']);
    }

    public function test_add_a_designation()
    {
        Designation::create([
            'title' => 'CEO - Director'
        ]);

        $this->assertDatabaseHas('designations', ['title' => 'CEO - Director']);
    }

    public function test_add_a_new_bank()
    {
        Bank::create([
            'bank_code' => 'ANZ',
            'title' => 'Australian and New Zealand Bank'
        ]);

        $this->assertDatabaseHas('banks', ['bank_code' => 'ANZ']);
    }

    public function test_add_a_payment_mode()
    {
        PaymentMode::create([
            'title' => 'Crypto - BTC'
        ]);

        $this->assertDatabaseHas('payment_modes', ['title' => 'Crypto - BTC']);
    }


    public function test_add_a_race()
    {
        Race::create([
            'title' => 'Anglo Saxon'
        ]);

        $this->assertDatabaseHas('races', ['title' => 'Anglo Saxon']);
    }

    public function test_add_a_religion()
    {
        Religion::create([
            'title' => 'Lutheran'
        ]);

        $this->assertDatabaseHas('religions', ['title' => 'Lutheran']);
    }

    public function test_add_a_relationship()
    {
        Relationship::create([
            'title' => 'Second Cousin'
        ]);

        $this->assertDatabaseHas('relationships', ['title' => 'Second Cousin']);
    }

    public function test_add_a_payment_type()
    {
        PaymentType::create([
            'title' => 'Bi-Monthly'
        ]);

        $this->assertDatabaseHas('payment_types', ['title' => 'Bi-Monthly']);
    }
}
