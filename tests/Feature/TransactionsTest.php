<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Transaction;
use App\Tenant\Traits\ForTenants;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TransactionsTest extends TestCase
{
    //use RefreshDatabase;
    //use ForTenants;

    public function test_add_new_addition()
    {
        Transaction::create([
            'title' => 'Telecoms',
            'active' => true,
            'addition' => true,
            'calculated' => false,
            'fixed' => true
        ]);

        $this->assertDatabaseHas('transactions', ['title' => 'Telecoms']);
    }

    public function test_add_new_deduction()
    {
        Transaction::create([
            'title' => 'Electric',
            'active' => true,
            'addition' => false,
            'calculated' => false,
            'fixed' => true
        ]);

        $this->assertDatabaseHas('transactions', ['title' => 'Electric']);
    }
}
