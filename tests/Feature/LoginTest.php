<?php

namespace Tests\Feature;

use App\User;
use App\Business;
use App\BusinessUser;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use PhpOffice\PhpSpreadsheet\Calculation\Database;

class LoginTest extends TestCase
{


    /**
     * A basic feature test example.
     *
     * @return void
     */
    /*     public function testExample()
        {
            $response = $this->get('/');

            $response->assertStatus(200);
        } */

    public function test_login_displays_the_login_form()
    {
        $response = $this->get('/login');

        $user = User::find(1);
        $bus_user = BusinessUser::find(1);
        $bus = Business::where('uuid', 'cd4e8e36-e85d-11eb-9a03-0242ac130003')->first();

        session()->put(
            'tenant',
            'cd4e8e36-e85d-11eb-9a03-0242ac130003'
        );
        
        dd($bus);
        

        $response->assertStatus(200);
        $response->assertViewIs('auth.login');
    }

    public function test_login_function()
    {
    }

    protected function GetUser()
    {
    }

    /*     public function test_login_function()
        {
            $this->assertDatabaseHas('users', [
                'email' => 'admin@bizpeople.biz'
            ]);

                     session()->put(
                        'tenant',
                        'value of the tenant'
                    );

            //dd('tenant');

            //        session()->put('tenant', '');
            //$response = $this->get('/login');

            //$response->assertSuccessful();
            //$response->assertViewIs('auth.login');
        } */
}
