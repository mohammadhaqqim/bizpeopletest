<?php

namespace Tests;

use App\User;
use App\Business;
use App\TenantConnection;
use App\Events\Tenant\TenantIdentified;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
}
