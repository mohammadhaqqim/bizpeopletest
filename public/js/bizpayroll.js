/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/bizpayroll.js":
/*!************************************!*\
  !*** ./resources/js/bizpayroll.js ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  $('#employee_id').editableSelect();
});

function deletePayrollTrans(id) {
  document.getElementById("deletePayrollTrans").action = "/payrollemployee/" + id;
  var oTable = document.getElementById("payrollTable");

  for (i = 1; i < oTable.rows.length; i++) {
    var oCells = oTable.rows.item(i).cells;

    if (oCells.item(0).innerHTML == id) {
      document.getElementsByClassName("emp_name_delete")[0].value = oCells.item(1).innerHTML;
      document.getElementsByClassName("emp_department_delete")[0].value = oCells.item(3).innerHTML;
      document.getElementsByClassName("emp_base_salary_delete")[0].value = Number(oCells.item(4).innerHTML).toFixed(2);
      document.getElementsByClassName("payment_method_delete")[0].value = document.getElementsByClassName("emp" + id)[0].value;
    }
  }

  $("#deleteModal").modal('show');
}

function editPayrollTrans(id) {
  document.getElementById("editPayrollTrans").action = "/payrollemployee/" + id;
  var oTable = document.getElementById("payrollTable");

  for (i = 1; i < oTable.rows.length; i++) {
    var oCells = oTable.rows.item(i).cells;

    if (oCells.item(0).innerHTML == id) {
      document.getElementsByClassName("emp_name")[0].value = oCells.item(1).innerHTML;
      document.getElementsByClassName("emp_department")[0].value = oCells.item(3).innerHTML;
      document.getElementsByClassName("emp_base_salary")[0].value = Number(oCells.item(4).innerHTML).toFixed(2);
      document.getElementsByClassName("payment_method")[0].value = document.getElementsByClassName("emp" + id)[0].value;
      document.getElementsByClassName("to_bank")[0].value = oCells.item(14).innerHTML;
      document.getElementsByClassName("to_account")[0].value = oCells.item(15).innerHTML;
    }
  }

  checkPaymentType();
  $("#editModal").modal('show');
}

function checkSelection(a) {
  //find the item in the list
  var name = a.value;
  document.getElementsByClassName('include-emp')[0].disabled = false; //find item in table

  var oTable = document.getElementById("payrollTable");

  for (i = 1; i < oTable.rows.length; i++) {
    var oCells = oTable.rows.item(i).cells;

    if (oCells.item(1).innerHTML == name) {
      document.getElementsByClassName('include-emp')[0].disabled = true;
      return;
    }
  }
}

function checkPaymentType() {
  var element = document.getElementById("payment_mode_id");
  var mode = element.value;

  if (element.options[element.selectedIndex].text == "Bank Transfer") {
    document.getElementById("bankinstitute").style.display = "block";
    document.getElementById("bankaccount").style.display = "block";
  } else {
    document.getElementById("bankinstitute").style.display = "none";
    document.getElementById("bankaccount").style.display = "none";
  }
}

/***/ }),

/***/ 2:
/*!******************************************!*\
  !*** multi ./resources/js/bizpayroll.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\repo\biz-people\resources\js\bizpayroll.js */"./resources/js/bizpayroll.js");


/***/ })

/******/ });