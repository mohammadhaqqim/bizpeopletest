<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayrollEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_employees', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('payroll_master_id');
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('employee_job_id');
            $table->unsignedBigInteger('employee_status_id');
            $table->unsignedBigInteger('company_id');
            
            $table->double('base_salary', 8, 2);
            $table->double('old_salary', 8, 2);
            $table->unsignedBigInteger('payment_mode_id');
            $table->unsignedBigInteger('bank_id')->nullable();
            $table->string('bank_account', 50)->nullable();

            $table->double('tap', 8, 2);
            $table->double('tap_ee', 8, 2);
            $table->double('scp', 8, 2);
            $table->double('scp_ee', 8, 2);
            $table->double('epf', 8, 2);
            $table->double('epf_ee', 8, 2);
            $table->double('work_days', 8, 2);

            $table->double('addition_amount', 8, 2);
            $table->double('deduction_amount', 8, 2);

            $table->double('unpaid_amount', 8, 2);
            $table->double('overtime_amount', 8, 2);
            $table->double('doubletime_amount', 8, 2);

            $table->unsignedBigInteger('prepared_by');

            $table->string('remark', 300);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->foreign('payroll_master_id')->references('id')->on('payroll_masters');
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('employee_job_id')->references('id')->on('employee_jobs');
            $table->foreign('employee_status_id')->references('id')->on('employee_statuses');
            $table->foreign('payment_mode_id')->references('id')->on('payment_modes');
            $table->foreign('bank_id')->references('id')->on('banks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_employees');
    }
}
