<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeavePoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_policies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('leave_type_id');
            $table->unsignedBigInteger('carryover_id');

            // j - 1st Jan, e - Employee hire date, o - Other
            $table->char('carryover', 1);

            $table->unsignedTinyInteger('carryover_day_id')->default(0);
            $table->unsignedTinyInteger('carryover_month_id')->default(0);
            /*
                        $table->unsignedBigInteger('carryover_day_id');
                        $table->unsignedBigInteger('carryover_month_id');
             */
            $table->string('policy_name', 100);
            // full amount or Probate (get some for part of month/period)
            $table->boolean('full_amount');

            // end of month (e) , begining of month (b),
            $table->char('first_accrual', 1);
            $table->char('accrual_occurs', 1);
//            $table->enum('level', ['easy', 'hard']);
            $table->boolean('active');

            //
            $table->double('yearly_amount', 6, 2);
            $table->char('carryover_type', 1);
            $table->double('max_carryover', 6, 2)->nullable();

            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->foreign('leave_type_id')->references('id')->on('leave_types');
            //$table->foreign('_id')->references('id')->on('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_policies');
    }
}
