<?php

use Carbon\Carbon;
use App\Relationship;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $data = [
            ['title'=>'Father', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Mother', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Husband', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Wife', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Son', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Daughter', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Brother', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Sister', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Partner', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Friend', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
        ];
        Relationship::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
