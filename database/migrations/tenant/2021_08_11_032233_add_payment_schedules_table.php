<?php

use Carbon\Carbon;
use App\PaymentSchedule;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $data = [
            ['title'=>'Monthly', 'periods_per_year'=>12, 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Bi-Monthly', 'periods_per_year'=>24, 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
        ];
        PaymentSchedule::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
