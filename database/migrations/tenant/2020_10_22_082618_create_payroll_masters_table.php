<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayrollMastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_masters', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('created_by');
            $table->unsignedBigInteger('company_id');
            $table->string('title', 100);
            $table->boolean('run_payroll')->default(false);
            $table->date('start_date');
            $table->date('end_date');
            $table->date('due_date');
            $table->date('pay_date');
            $table->string('remarks', 2000)->nullable();
            $table->unsignedSmallInteger('working_days')->nullable();
            $table->unsignedBigInteger('approved_by')->nullable();
            $table->dateTime('approved_datetime')->nullable();
            $table->unsignedBigInteger('locked_by')->nullable();
            $table->dateTime('locked_datetime')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            //$table->foreign('company_id')->references('id')->on('companies');
            //$table->foreign('created_by')->references('id')->on('users');
            //$table->foreign('approved_by')->references('id')->on('users');
            //$table->foreign('locked_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_masters');
    }
}
