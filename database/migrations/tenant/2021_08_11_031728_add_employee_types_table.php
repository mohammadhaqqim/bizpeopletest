<?php

use Carbon\Carbon;
use App\EmployeeType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmployeeTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $data = [
            ['title'=>'Permanent', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Part Time', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Probation', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Intern', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
        ];
        EmployeeType::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
