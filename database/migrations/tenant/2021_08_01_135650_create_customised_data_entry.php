<?php

use App\CustomisedData;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomisedDataEntry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [
                'id' => '',
                'table' => 'departments',
                'display' => 'Department',
                'displays' => 'Departments',
                'modalAdd' => 'components.modal.add_department',
                'edit_route' => '/department/edit',
                'identifier' => 'department_id',
                'action' => '/departments',
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
                'deleted_at' => null
            ],
            [
                'id' => '',
                'table' => 'designations',
                'display' => 'Designation',
                'displays' => 'Designations',
                'modalAdd' => 'components.modal.add_designation',
                'edit_route' => '/designation/edit',
                'identifier' => 'designation_id',
                'action' => '/designations',
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
                'deleted_at' => null
            ],
            [
                'id' => '',
                'table' => 'banks',
                'display' => 'Bank',
                'displays' => 'Banks',
                'modalAdd' => 'components.modal.add_bank',
                'edit_route' => '/bank/edit',
                'identifier' => 'bank_id',
                'action' => '/banks',
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
                'deleted_at' => null
            ],
/*             [
                'id' => '',
                'table' => 'employee_types',
                'display' => 'Employee Type',
                'displays' => 'Employee Types',
                'modalAdd' => '',
                'edit_route' => '',
                'identifier' => 'employment_status_id',
                'action' => '/employee-types',
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
                'deleted_at' => null
            ], */
            [
                'id' => '',
                'table' => 'payment_modes',
                'display' => 'Payment Mode',
                'displays' => 'Payment Modes',
                'modalAdd' => '',
                'edit_route' => '',
                'identifier' => 'payment_mode_id',
                'action' => '/payment-modes',
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
                'deleted_at' => null
            ],
                        
            [
                'id' => '',
                'table' => 'races',
                'display' => 'Race',
                'displays' => 'Races',
                'modalAdd' => '',
                'edit_route' => '',
                'identifier' => 'race_id',
                'action' => '/races',
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
                'deleted_at' => null
            ],
            [
                'id' => '',
                'table' => 'religions',
                'display' => 'Religion',
                'displays' => 'Religions',
                'modalAdd' => '',
                'edit_route' => '',
                'identifier' => 'religion_id',
                'action' => '/religions',
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
                'deleted_at' => null
            ],
            [
                'id' => '',
                'table' => 'relationships',
                'display' => 'Employee Relationships',
                'displays' => 'Relationship',
                'modalAdd' => '',
                'edit_route' => '',
                'identifier' => 'relationship_id',
                'action' => '/relationships',
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
                'deleted_at' => null
            ],
            [
                'id' => '',
                'table' => 'payment_types',
                'display' => 'Payment Types',
                'displays' => 'Payment Types',
                'modalAdd' => '',
                'edit_route' => '',
                'identifier' => 'payment_type_id',
                'action' => '/payment_types',
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
                'deleted_at' => null
            ],

            [
                'id' => '',
                'table' => 'end_employment_reasons',
                'display' => 'End Employment Reasons',
                'displays' => 'End Employment Reasons',
                'modalAdd' => '',
                'edit_route' => '',
                'identifier' => 'end_reason_id',
                'action' => '/end_employment_reasons',
                'created_at'=>\Carbon\Carbon::now(),
                'updated_at'=>\Carbon\Carbon::now(),
                'deleted_at' => null
            ],
        ];
        CustomisedData::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
