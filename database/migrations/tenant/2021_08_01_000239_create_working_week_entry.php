<?php

use App\WorkingWeek;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkingWeekEntry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [
                //'company_id' => 1,
            'title' => 'Standard Week',
            'mon'=>8,
            'tue'=>8,
            'wed'=>8,
            'thu'=>8,
            'fri'=>8,
            'sat'=>0,
            'sun'=>0,
            'avg' => 8,
            'default_week'=>1,
            'created_at'=>\Carbon\Carbon::now(),
            'updated_at'=>\Carbon\Carbon::now()],
            
        ];
        WorkingWeek::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('working_week_entry');
    }
}
