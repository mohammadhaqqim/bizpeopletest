<?php

use App\IcColor;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIcColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $data = [
            ['title'=>'Red', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Yellow', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Green', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
        ];
        IcColor::insert($data);/*  */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
