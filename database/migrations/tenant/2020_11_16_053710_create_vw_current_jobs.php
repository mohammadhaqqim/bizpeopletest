<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVwCurrentJobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
                CREATE VIEW vw_current_jobs 
                AS SELECT 
                        employee_jobs.id AS jobs_id,
                        employee_jobs.employee_id AS employee_id,
                        employee_jobs.effective_date AS effective_date 
                        from employee_jobs 
                        where (employee_jobs.effective_date = (
                            select max(ej.effective_date) 
                            from employee_jobs ej 
                            where ((ej.effective_date <= now()) and (ej.employee_id = employee_jobs.employee_id) ) ))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_current_jobs");
    }
}
