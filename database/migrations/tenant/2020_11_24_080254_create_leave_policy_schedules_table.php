<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeavePolicySchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_policy_schedules', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('leave_policy_id')->nullable();

            $table->tinyInteger('level')->nullable();
            // Starts after number of (days/months/years)
            $table->smallInteger('starts');
            //days (d), weeks (w), months (m), years (y)
            $table->string('start_type', 1);
            $table->double('amount_accrued', 6, 2);
            //days (d), weeks (w), months (m)
            $table->char('amount_accrued_type', 1);
            // none (n), up to (t), unlimited (u)
            $table->double('max_accrual', 6, 2);
            $table->char('carryover_type', 1);
            $table->double('yearly_carryover', 6, 2)->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->foreign('leave_policy_id')->references('id')->on('leave_policies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_policy_schedules');
    }
}
