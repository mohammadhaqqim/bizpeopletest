<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVwCurrentPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
                CREATE VIEW vw_current_payments 
                AS SELECT 
                        employee_payments.id AS payment_id,
                        employee_payments.employee_id AS employee_id,
                        employee_payments.effective_date AS effective_date 
                        from employee_payments 
                        where (employee_payments.effective_date = (
                            select max(ep.effective_date) 
                            from employee_payments ep
                            where ((ep.effective_date <= now()) and (ep.employee_id = employee_payments.employee_id) ) ))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_current_payments");
    }
}
