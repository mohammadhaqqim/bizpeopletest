<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_leaves', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_id')->nullable();
            $table->unsignedBigInteger('employee_id')->nullable();
//            $table->unsignedBigInteger('created_by_id')->nullable();
            //???
            $table->unsignedBigInteger('leave_type_id')->nullable();
            $table->unsignedBigInteger('approved_by_id')->nullable();
            $table->unsignedBigInteger('rejected_by_id')->nullable();

            $table->string('title', 100)->nullable();
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->double('hours_off', 5, 2)->nullable();
            $table->double('days_off', 5, 2)->nullable();
            $table->boolean('variable_date')->nullable();

            $table->boolean('from_half')->nullable();
            $table->boolean('to_half')->nullable();
            $table->string('comment', 200)->nullable();
            $table->string('deny_comment', 200)->nullable();

//            $table->date('leave_from');

//            $table->date('leave_to');

            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamp('deny_at')->nullable();
            $table->timestamp('rejected_at')->nullable();

            //$table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('leave_type_id')->references('id')->on('leave_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_leaves');
    }
}
