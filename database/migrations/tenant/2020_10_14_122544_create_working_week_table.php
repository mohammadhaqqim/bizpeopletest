<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkingWeekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('working_weeks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('company_id')->nullable();
            $table->string('title', 50);
            $table->double('mon', 3, 1);
            $table->double('tue', 3, 1);
            $table->double('wed', 3, 1);
            $table->double('thu', 3, 1);
            $table->double('fri', 3, 1);
            $table->double('sat', 3, 1);
            $table->double('sun', 3, 1);
            $table->double('avg', 3, 1);
            $table->boolean('default_week')->default(0);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            //$table->foreign('company_id')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('working_weeks');
    }
}
