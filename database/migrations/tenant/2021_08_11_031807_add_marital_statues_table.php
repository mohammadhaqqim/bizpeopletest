<?php

use Carbon\Carbon;
use App\MaritalStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaritalStatuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $data = [
            ['title'=>'Single', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Married', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Divorced', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
        ];
        MaritalStatus::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
