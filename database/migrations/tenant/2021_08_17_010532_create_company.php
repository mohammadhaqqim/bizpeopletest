<?php

use App\Company;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompany extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [
                'owner_id'=>1,
                'company_name'=>session()->get('company_name'),
                'use_tap'=>true, 'tap_percent'=>5.00, 'tap_min'=>17.50, 'tap_max'=>98.00,
                'use_scp'=>true, 'scp_percent'=>3.50, 'scp_min'=>17.50, 'scp_max'=>98.00,
                'use_epf'=>true, 'epf_percent'=>5.00, 'epf_min'=>0, 'epf_max'=>0,
                'company_name'=>'BizPeople Solutions',
                'logo'=>'images/generic-logo.png',
                'address_country_id'=>1,
                //'payment_period_id'=>1,
                'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()]
        ];

        Company::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('company');
    }
}
