<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddContributionFieldsCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            //
            $table->boolean('tap_round')->default(0);
            $table->boolean('scp_round')->default(0);
            $table->boolean('efp_round')->default(0);

            $table->tinyInteger('tap_decimals')->default(2);
            $table->tinyInteger('scp_decimals')->default(2);
            $table->tinyInteger('epf_decimals')->default(2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('companies', function (Blueprint $table) {
            //
            $table->dropColumn('tap_round');
            $table->dropColumn('scp_round');
            $table->dropColumn('efp_round');

            $table->dropColumn('tap_decimals');
            $table->dropColumn('scp_decimals');
            $table->dropColumn('epf_decimals');
        });
    }
}
