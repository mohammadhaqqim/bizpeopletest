<?php

use Carbon\Carbon;
use App\PaymentMode;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentModesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            ['title'=>'Cash', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Bank Transfer', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Cheque', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
        ];
        PaymentMode::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
