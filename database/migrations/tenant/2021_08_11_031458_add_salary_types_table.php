<?php

use Carbon\Carbon;
use App\SalaryType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSalaryTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            ['title'=>'Monthly', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Daily', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Hourly', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
        ];
        SalaryType::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
