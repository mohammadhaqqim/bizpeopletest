<?php

use App\Religion;
use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReligionsInTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            ['title'=>'Islam', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Christian', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Other', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
        ];
        Religion::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
