<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRelationExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relation_extras', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('relation_id');
            $table->string('passport_number', 20)->nullable();
            $table->date('passport_expiry')->nullable();
            $table->date('dependant_expiry')->nullable();
            $table->date('student_pass_expiry')->nullable();
            $table->timestamps();

            $table->foreign('relation_id')->references('id')->on('relationships');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relation_extras');
    }
}
