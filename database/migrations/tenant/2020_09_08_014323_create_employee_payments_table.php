<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_payments', function (Blueprint $table) {
            $table->id();
            $table->date('effective_date');
            $table->unsignedBigInteger('employee_id');
            $table->unsignedBigInteger('currency_id');
            $table->double('basic_salary', 8, 2)->default("0");
            $table->double('overtime', 8, 2)->nullable();
            $table->double('double_time', 8, 2)->nullable();

            $table->unsignedBigInteger('payment_mode_id')->nullable();
//            $table->unsignedInteger('working_week_id');

            $table->unsignedBigInteger('payment_type_id');
            //$table->unsignedInteger('payment_schedule_id');
            $table->text('comment')->nullable();
            $table->unsignedBigInteger('entered_by');
            
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->timestamp('deleted_at')->nullable();

            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('currency_id')->references('id')->on('countries');
            $table->foreign('payment_mode_id')->references('id')->on('payment_modes');

            //$table->foreign('entered_by')->references('id')->on('users');
            //$table->foreign('payment_schedule_id')->references('id')->on('payment_schedules');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_payments');
    }
}
