<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayrollTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payroll_transactions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('payroll_employees_id');
            $table->unsignedBigInteger('transaction_id');
            $table->string('title', 100);
            $table->double('amount', 8, 2);
            $table->double('hours', 5, 2);
            $table->double('rate', 7, 4);
            $table->boolean('addition');
            $table->boolean('calculated');
            $table->boolean('fixed');

            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->foreign('payroll_employees_id')->references('id')->on('payroll_employees');
            $table->foreign('transaction_id')->references('id')->on('transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payroll_transactions');
    }
}
