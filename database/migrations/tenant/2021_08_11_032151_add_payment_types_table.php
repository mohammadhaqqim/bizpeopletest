<?php

use Carbon\Carbon;
use App\PaymentType;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPaymentTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $data = [
            ['title'=>'Hourly', 'periods'=>0, 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Weekly', 'periods'=>52, 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Fortnightly', 'periods'=>26, 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Monthly', 'periods'=>12, 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Yearly', 'periods'=>1, 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
        ];
        PaymentType::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
