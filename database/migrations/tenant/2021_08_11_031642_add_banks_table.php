<?php

use App\Bank;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            ['title'=>'BIBD', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Baiduri', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Standard Charter', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
        ];
        Bank::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
