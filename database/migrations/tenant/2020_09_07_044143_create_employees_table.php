<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->id();
            //basic info
            $table->unsignedBigInteger('company_id')->nullable();
            $table->unsignedBigInteger('gender_id')->nullable();
            $table->unsignedBigInteger('marital_status_id');
            $table->unsignedBigInteger('identification_card_id')->nullable();

            //address
            //$table->unsignedBigInteger('country_id');

            //job - passport
            //$table->unsignedBigInteger('passport_id');
            $table->string('passport_number', 30)->nullable();
            $table->date('passport_expiry')->nullable();
            $table->string('passport_place_issued', 100)->nullable();


            // N/A - on job / passport tab
            $table->unsignedBigInteger('religion_id')->nullable();
            $table->unsignedBigInteger('race_id')->nullable();
            $table->unsignedBigInteger('nationality_id');
            $table->unsignedBigInteger('designation_id')->nullable(); ///
//            $table->unsignedBigInteger('department_id');  ///
            $table->unsignedBigInteger('employment_type_id')->nullable();
            $table->unsignedBigInteger('employment_status_id');
            $table->boolean('active')->default(true);
//            $table->unsignedBigInteger('salary_type_id');
//            $table->unsignedBigInteger('employee_payment_id');
            $table->unsignedBigInteger('location_id')->nullable();
            $table->unsignedBigInteger('bank_id');
            $table->unsignedBigInteger('reports_to_id')->nullable();
            $table->unsignedBigInteger('end_reason_id')->nullable();

            $table->string('avatar')->nullable();

            // in job tab
//            $table->unsignedBigInteger('employee_type_id');

            //basic info
            // changed emp_id -> employee_number
            $table->string('employee_code', 20)->nullable(); //Employee #
            $table->string('name', 255);
            $table->string('preferred_name', 50)->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('place_birth', 100)->nullable();

            $table->boolean('driving_license')->default(false);

            $table->unsignedBigInteger('payment_mode_id')->nullable();

            $table->string('ic_number', 10)->nullable();
            $table->unsignedBigInteger('ic_color_id')->nullable();
            $table->date('ic_expiry')->nullable();

            //address
            $table->string('address1', 100)->nullable();
            $table->string('address2', 100)->nullable();
            $table->string('city', 50)->nullable();
            $table->string('area', 50)->nullable();
            $table->string('postcode', 20)->nullable();
            // ResidenceCountry
            $table->string('address_country_id')->nullable();

            //Contact
            $table->string('telephone', 50)->nullable();
            $table->string('work_phone', 50)->nullable();
            $table->string('work_phone_ext', 5)->nullable();
            $table->string('home_phone', 50)->nullable();
            $table->string('hand_phone', 50)->nullable();

            $table->string('work_email', 100)->nullable();
            $table->string('home_email', 100)->nullable();
            $table->string('email_payslip_to', 100)->nullable();

            //social links
            $table->string('linkedin', 100)->nullable();
            $table->string('twitter', 100)->nullable();
            $table->string('facebook', 100)->nullable();

            // ??
            $table->string('unit', 50)->nullable();
            // employee_type - emp_type
//            $table->string('emp_employeeType', 50)->nullable();
            $table->date('work_permit_expiry')->nullable();
            $table->date('labour_license_expiry')->nullable();
            $table->unsignedBigInteger('working_week_id')->nullable();
            
            // Changed title
//            $table->boolean('stopped_service')->default(0);
            // Changed title
            $table->date('stopped_date', 50)->nullable();
            // Changed title
            $table->text('stopped_work_reason', 50)->nullable();
            $table->text('remarks')->nullable();
            $table->double('basic_salary', 8, 2)->nullable();
            $table->unsignedBigInteger('payment_currency_id');
            $table->boolean('tap_member')->default(0);
            $table->string('tap_account', 50)->nullable();
            $table->boolean('scp_member')->default(0);
            $table->string('scp_account', 50)->nullable();
            $table->boolean('epf_member')->default(0);
            $table->string('epf_account', 50)->nullable();
            $table->string('bank_account', 50)->nullable();
            $table->boolean('email_timeoff')->nullable();
            $table->boolean('email_payslip')->nullable();
            $table->boolean('email_notification_to', 100)->nullable();

            $table->unsignedSmallInteger('medical_entitlement')->nullable();
            $table->unsignedSmallInteger('leave_entitlement')->default(0);
            $table->double('prorata', 8, 2)->nullable();
            // changed title - emp_leave_bf
            $table->double('leave_brought_forward', 8, 2)->nullable();
            $table->double('leave_balance', 8, 2)->nullable();
            $table->double('medical_balance', 8, 2)->nullable();
            $table->double('inlieu_balance', 8, 2)->nullable();
            // ph? public holiday - phinlieu_lb
            $table->double('holiday_inlieu_balance', 8, 2)->nullable();
            $table->unsignedSmallInteger('leave_unpaid')->default(0);
            $table->date('date_joined')->nullable();
            $table->date('end_contract_date')->nullable();
            $table->string('end_employment_comment', 191)->nullable();
            $table->unsignedSmallInteger('leave_weekend')->default(0);

            $table->boolean('self_service')->default(false);

            $table->boolean('hod')->default(false);
            $table->boolean('manager')->default(false);


            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->unsignedBigInteger('created_by');

            // foreign keys
            $table->foreign('religion_id')->references('id')->on('religions');
            $table->foreign('race_id')->references('id')->on('races');
            $table->foreign('nationality_id')->references('id')->on('countries');
            // $table->foreign('designation_id')->references('id')->on('designations');
            $table->foreign('employment_type_id')->references('id')->on('employment_types');
            $table->foreign('employment_status_id')->references('id')->on('employment_statuses');
            $table->foreign('payment_mode_id')->references('id')->on('payment_modes');
            $table->foreign('bank_id')->references('id')->on('banks');
            //??
            $table->foreign('end_reason_id')->references('id')->on('end_employment_reasons');
            //$table->foreign('address_country_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
