<?php

use App\Country;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $data = [
            ['country'=>'Brunei Darussalam', 'nationality' => 'Bruneian', 'use_currency'=>true, 'currency_code'=>'BND',
                'currency_symbol'=>'$', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['country'=>'Philippines', 'nationality' => 'Philippino', 'use_currency'=>false, 'currency_code'=>'PHP',
                'currency_symbol'=>'P', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['country'=>'Thailand', 'nationality' => 'Thai', 'use_currency'=>false, 'currency_code'=>'THB',
                'currency_symbol'=>'B', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['country'=>'Indonesia', 'nationality' => 'Indonesian', 'use_currency'=>false, 'currency_code'=>'IDR',
                'currency_symbol'=>'R', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['country'=>'Australia', 'nationality' => 'Australian', 'use_currency'=>false, 'currency_code'=>'AUD',
                'currency_symbol'=>'$', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['country'=>'Malaysia', 'nationality' => 'Malaysian', 'use_currency'=>false, 'currency_code'=>'MYR',
                'currency_symbol'=>'$', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],

        ];
        Country::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
