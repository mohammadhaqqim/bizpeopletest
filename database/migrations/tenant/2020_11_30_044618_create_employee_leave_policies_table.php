<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeLeavePoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_leave_policies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('leave_policy_id');
            $table->unsignedBigInteger('employee_id');
            // when policy is initiated
            $table->date('start_from_date');
            //current financial period start date
            $table->date('current_start_date');

            //brought forward from last year
            $table->double('carryover', 6, 2);
            // able to be taken at present
            $table->double('available', 6, 2);
            // that has approved and before today
            $table->double('taken', 6, 2);
            // what is scheduled in the system (approved & not approved) and not taken
            $table->double('scheduled', 6, 2);

            $table->boolean('active');

            // currently available
            // available + carryover - taken - scheduled (approved)
            $table->double('balance', 6, 2);

            $table->timestamps();

            $table->foreign('leave_policy_id')->references('id')->on('leave_policies');
            $table->foreign('employee_id')->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_leave_policies');
    }
}
