<?php

use App\Race;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRacesInTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            ['title'=>'Malay', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Chinese', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Indian', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Iban', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Other', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
        ];
        Race::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }
}
