<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeeDaysOffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_days_offs', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('employee_leave_id');
            $table->date('off_day');
            // hours usually worked
            $table->double('work_hours', 4, 2);
            // hours that have been taken off
            $table->double('off_hours', 4, 2);
            $table->boolean('holiday')->nullable();
            $table->timestamps();

            $table->foreign('employee_leave_id')->references('id')->on('employee_leaves');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_days_offs');
    }
}
