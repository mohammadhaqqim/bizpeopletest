<?php

use App\Gender;
use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGenderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $data = [
            ['title'=>'Male', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Female', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
        ];
        Gender::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
