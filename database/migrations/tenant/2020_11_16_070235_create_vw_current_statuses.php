<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVwCurrentStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
                CREATE VIEW vw_current_statuses 
                AS SELECT 
                        employee_statuses.id AS status_id,
                        employee_statuses.employee_id AS employee_id,
                        employee_statuses.effective_date AS effective_date 
                        from employee_statuses 
                        where (employee_statuses.effective_date = (
                            select max(es.effective_date) 
                            from employee_statuses es 
                            where ((es.effective_date <= now()) and (es.employee_id = employee_statuses.employee_id) ) ))");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS vw_current_statuses");
    }
}
