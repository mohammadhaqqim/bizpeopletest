<?php

use Carbon\Carbon;
use App\EmploymentStatus;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmploymentStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        $data = [
            ['title'=>'Intern', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Part-Time', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Permanent Full-Time', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['title'=>'Probation', 'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
        ];
        EmploymentStatus::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
