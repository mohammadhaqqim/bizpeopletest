<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomisedDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customised_data', function (Blueprint $table) {
            $table->id();
            $table->string('table', 50);
            $table->string('display', 50);
            $table->string('displays', 50);
            $table->string('modalAdd', 50);
            $table->string('edit_route', 50);
            $table->string('identifier', 50);
            $table->string('action', 50);
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customised_data');
    }
}
