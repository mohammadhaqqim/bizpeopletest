<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCreatedByIdEmployeeDaysOffTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_days_offs', function (Blueprint $table) {
            //G
            $table->double('day_off', 4, 2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_days_offs', function (Blueprint $table) {
            //
            $table->dropColumn('day_off', 4, 2);
        });
    }
}
