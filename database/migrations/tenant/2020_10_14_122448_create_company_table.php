<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('owner_id')->nullable();

            $table->boolean('use_tap')->default(true);
            $table->float('tap_percent', 8, 2);
            $table->float('tap_min', 8, 2);
            $table->float('tap_max', 8, 2);
            $table->boolean('use_scp')->default(true);
            $table->float('scp_percent', 8, 2);
            $table->float('scp_min', 8, 2);
            $table->float('scp_max', 8, 2);
            $table->boolean('use_epf')->default(false);
            $table->float('epf_percent', 8, 2);
            $table->float('epf_min', 8, 2);
            $table->float('epf_max', 8, 2);

            $table->string('company_name', 100);
            // file location
            $table->text('logo')->nullable();

            //address
            $table->string('address1', 100)->nullable();
            $table->string('address2', 100)->nullable();
            $table->string('city', 50)->nullable();
            $table->string('area', 50)->nullable();
            $table->string('postcode', 20)->nullable();
            // ResidenceCountry
            $table->unsignedBigInteger('address_country_id')->nullable();
            $table->unsignedBigInteger('payment_period_id')->nullable();

            $table->unsignedBigInteger('reporting_period_id')->nullable();
            $table->date('first_period_date')->nullable();
//            $table->date('end_financial_date')->nullable();
            $table->tinyInteger('periods_per_year')->nullable();

            $table->string('registration_no', 20);
            $table->string('license_no', 20);


            //$table->
            $table->timestamps();

            //$table->foreign('owner_id')->references('id')->on('users');
            $table->foreign('address_country_id')->references('id')->on('countries');
//            $table->foreign('payment_period_id')->references('id')->on('payment_periods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
