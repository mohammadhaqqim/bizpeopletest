<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUseraccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//            $table->foreign('company_id')->references('id')->on('companies');
        $data = [

            // Admin User Testing

            ['name'=>'admin', 'email'=>'samplecodeprogram@gmail.com',
                'password' => Hash::make('code12345'),
                //'payroll'=>false, 'hr'=>false, 'admin'=>true,
                //'company_id'=>1,
                'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],

            // Actual User

            ['name'=>'admin', 'email'=>'admin@bizpeople.biz',
                'password' => Hash::make('p@55w0rd1'),
                //'payroll'=>false, 'hr'=>false, 'admin'=>true,
                //'company_id'=>1,
                'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['name'=>'accountant', 'email'=>'accounts@bizpeople.biz',
                'password' => Hash::make('p@55w0rd2'),
                //'payroll'=>true, 'hr'=>false, 'admin'=>false,
                //'company_id'=>1,
                'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
            ['name'=>'hr', 'email'=>'hr@bizpeople.biz',
                'password' => Hash::make('p@55w0rd3'),
                //'payroll'=>false, 'hr'=>true, 'admin'=>false,
                //'company_id'=>1,
                'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],


                // User Testing

                ['name'=>'hr', 'email'=>'usersamplecodeprogram@gmail.com',
                'password' => Hash::make('code12345'),
                //'payroll'=>false, 'hr'=>true, 'admin'=>false,
                //'company_id'=>1,
                'created_at'=>Carbon::now(), 'updated_at'=>Carbon::now()],
        ];

        User::insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Schema::dropIfExists('useraccounts');
    }
}
