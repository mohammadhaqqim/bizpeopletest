<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIdentificationCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('identification_cards', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('ic_color_id');
            $table->string('number', 10);
            $table->date('expiration');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            //$table->foreign('ic_color_id')->references('id')->on('ic_colors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identification_cards');
    }
}
