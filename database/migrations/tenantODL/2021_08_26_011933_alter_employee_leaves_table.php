<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterEmployeeLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_leaves', function (Blueprint $table) {
            //
            //$table->timestamp('deny_date')->nullable()->change();
            //$table->renameColumn('deny_date', 'deny_at');
            //$table->timestamp('deleted_at')->nullable()->change();
            $table->timestamp('rejected_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_leaves', function (Blueprint $table) {
            //
            //$table->renameColumn('deny_at', 'deny_date');
            $table->dropColumn('rejected_at');
        });
    }
}
