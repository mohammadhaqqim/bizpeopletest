<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAccessBusUsrTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_user', function (Blueprint $table) {
            $table->unsignedBigInteger('employee_id')->nullable();
            $table->boolean('payroll')->default(0);
            $table->boolean('hr')->default(0);
            $table->boolean('hod')->default(0);
            $table->boolean('manager')->default(0);
            $table->boolean('admin')->default(0);
            $table->boolean('update-admin')->default(0);
            $table->boolean('block')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business_user', function (Blueprint $table) {
            $table->dropColumn('employee_id');
            $table->dropColumn('payroll');
            $table->dropColumn('hr');
            $table->dropColumn('hod');
            $table->dropColumn('manager');
            $table->dropColumn('admin');
            $table->dropColumn('block');
        });
    }
}
