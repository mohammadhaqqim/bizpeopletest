<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePassportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('passports', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('nationality_id');

            $table->string('passport_number', 50);
            $table->date('passport_expiry', 50);
            $table->string('passport_place_issued', 100)->nullable();
//            $table->date('passport_date_issued')->nullable();
            $table->date('passport_expiration_date')->nullable();
            $table->date('work_expiry_date')->nullable();
            // not sure if this is - wp_expiry
            $table->date('labor_expiry_date')->nullable();
            $table->string('birth_place', 100);

            
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();

            $table->foreign('nationality_id')->references('id')->on('countries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('passports');
    }
}
