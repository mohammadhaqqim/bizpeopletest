<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForignKeysCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('companies', function (Blueprint $table) {
            //$table->foreign('nationality_id')->references('id')->on('countries');
            //$table->foreign('payment_mode_id')->references('id')->on('payment_modes');
            //$table->foreign('bank_id')->references('id')->on('banks');
            //$table->foreign('employment_status_id')->references('id')->on('employment_statuses');
            $table->foreign('payment_type_id')->references('id')->on('payment_types');
            $table->foreign('designation_id')->references('id')->on('designations');
            $table->foreign('department_id')->references('id')->on('departments');
            $table->foreign('working_week_id')->references('id')->on('working_weeks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
