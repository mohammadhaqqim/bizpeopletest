<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserLoginAuditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_login_audits', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
//            $table->string('ip_address')->nullable();
            $table->unsignedBigInteger('employee_id')->nullable();
            $table->string('comment', 100);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_login_audits');
    }
}
