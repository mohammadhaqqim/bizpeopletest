<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
//            $table->string('username')->unique();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            //$table->boolean('db_admin');
            //this is found in employee not here
            $table->text('avatar')->nullable();
            /*             $table->unsignedBigInteger('company_id')->nullable();
                        $table->unsignedBigInteger('employee_id')->nullable();
                        $table->boolean('payroll')->default(0);
                        $table->boolean('hr')->default(0);
                        $table->boolean('hod')->default(0);
                        $table->boolean('manager')->default(0);
                        $table->boolean('admin')->default(0);
             */
            $table->boolean('block')->default(0);
            $table->rememberToken();
            $table->timestamps();

//            $table->foreign('company_id')->references('id')->on('companies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
