const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .css('resources/css/bizit.css', 'public/css');

//jQuery
mix.js('resources/js/bizsearch.js', 'public/js');
mix.js('resources/js/bizpayroll.js', 'public/js');
//mix.css('resources/css/jquery-editable-select.min.css', 'public/css');
mix.js('resources/js/jquery-editable-select.min.js', 'public/js');
